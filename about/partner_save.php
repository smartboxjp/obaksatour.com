<?php   
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連

    date_default_timezone_set('Asia/Seoul');
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/mailer/PHPMailerAutoload.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMailer.php";
    $common_mailer = new CommonMailer(); //メール関連
?>
<!doctype html>
<html prefix="og: http://ogp.me/ns#">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">

    <title>제휴문의</title>
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->

    <!--[if lt IE 8]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="../scripts/html5shiv.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>
<?
    $datetime = date("Y-m-d H:i:s");
    
    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
    
    if($partner_name=="" || $partner_email=="" || $partner_tel=="" || $partner_comment=="")
    {
        $common_connect-> Fn_javascript_back("필수항목을 입력해주세요.");
    }


    //Thank youメール
    if ($partner_email != "")
    {
        $subject = " [오키나와 오박사]제휴메일이 전송되었습니다. ";


        $body = "";
        $body = file_get_contents($_SERVER['DOCUMENT_ROOT']."/about/mail/partner_save.user.php");
        $body = str_replace("[partner_name]", $partner_name, $body);
        $body = str_replace("[partner_email]", $partner_email, $body);
        $body = str_replace("[partner_tel]", $partner_tel, $body);
        $body = str_replace("[partner_comment]", $partner_comment, $body);
        $body = str_replace("[datetime]", $datetime, $body);

    }
    $common_mailer->Fn_send_utf ($partner_email,global_test_mail.$subject,$body,$global_mail_from,$global_send_mail);

    $common_connect -> Fn_email_log($partner_email, global_test_mail.$common_connect->h($subject), $common_connect->h($body)); //メールログ

    $common_mailer->Fn_send_utf ($global_send_mail,global_test_mail.$subject,$body,$global_mail_from,$global_send_mail);

    $common_connect-> Fn_redirect(global_no_ssl."/about/partner_thankyou.php");
?>

</body>

</html>
