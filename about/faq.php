<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title>자주하는 질문</title>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/about/css/about.css" rel="stylesheet" >

</head>
<body>

<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

  <div class="l-container">
    <div class="pankuzu-wapper">
      <ul class="pankuzu">
        <li><a href="/">오키나와 오박사 홈</a></li>
        <li>자주하는 질문</li>
      </ul>
    </div>
    <!-- /.pankuzu-wapper -->
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <div class="localnavs">
      <p class="localnavs-title">고객센터</p>
      <ul class="localnavs-list">
        <li><a href="/about/notice.php">공지사항</a></li>
        <li class="localnavs-list-current"><a href="/about/faq.php">자주하는 질문</a></li>
        <li><a href="/about/onebyone.php">1:1문의</a></li>
        <li><a href="/about/partner.php">제휴 문의</a></li>
      </ul>
      <p class="localnavs-text color-lightblue bg-gy">! 원하시는 정보를 찾지 못하셨거나 답변이 충분치 않으셨다면 1:1 문의를 통해 더 자세히 문의해 주세요.</p>
    </div>
    <!-- /.localnavs -->
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <div class="freeword">
      <p class="freeword-label">자주하는 질문 검색</p>
      <input class="form-control freeword-input" type="text" placeholder="! 검색을 이용하시면 보다 빠르게 원하시는 답변을 얻으실 수 있습니다. ">
      <button type="submit" class="freeword-btn">질문 찾기</button>
    </div>
    <!-- /freeword -->
  </div>
  <!-- /.l-container -->

  <div class="l-container clearfix">
    <h1 class="headline-glay fsize-lg left-bdr"><span class="color-blue">자주하는 질문</span></h1>
    <ul class="colnavs">
      <li class="colnavs-current"><a href="#">전체 질문 보기</a></li>
      <li><a href="#">원데이 / 반나절 투어</a></li>
      <li><a href="#">패키지 투어</a></li>
      <li><a href="#">오키나와 가이드</a></li>
      <li><a href="#">해양 스포츠</a></li>
      <li><a href="#">렌트카</a></li>
      <li><a href="#">호텔 및 펜션</a></li>
      <li><a href="#">취소 및 환불 </a></li>
      <li><a href="#">기타 질문</a></li>
    </ul>
  </div>
  <!-- /.l-container -->

  <div class="l-container mb-30">
    <div class="table-responsive">
      <table class="table table-bordered table-faq">
        <thead>
          <tr>
            <th class="cel-a">번호</th>
            <th class="cel-b">분류</th>
            <th class="cel-c">제목</th>
          </tr>
        </thead>
        <tbody>
          <tr class="bdr-none-td slide-parent">
            <td class="bdr-none">1</td>
            <td class="text-left">원데이/반나절 투어</td>
            <td class="text-left">반나절 투어 신청 시 여권의 영문과 항공권의 영문이 다릅니다. 어떻게 하나요?</td>
          </tr>
          <tr class="bdr-none-td slide-child">
            <td colspan="5" class="text-left pos-rel">
              <dl class="qa">
                <dt>접수할 때 작성한 도착일로부터 24시간인가요? 아니면 무조건 사용일 기준 24시간인가요?</dt>
                <dd>안녕하세요. 오키나와 오박사를 이용해 주셔서 감사드립니다. 1:1문의 담당자 천명관입니다.<br>
문의하신 오키나와 실속 렌터카 상품 문의에 대해 답변 안내 드립니다.<br>
업체측으로 전달하여 해당 내용 확인 후 sms 안내 도와드리도록 하겠으며 확인까지 영업일 1~2일 소요될 수 있으며 조금만 시간 양해 부탁드리겠습니다.<br>
오늘하루 고객님께 행복한 일들만 가득하기를 기원합니다. 감사합니다. </dd>
              </dl>
              <ul class="funcs">
                <li><span class="category-tag">수정</span></li>
                <li><span class="category-tag">삭제</span></li>
              </ul>
            </td>
          </tr>
          <tr class="bdr-none-td slide-parent">
            <td>1</td>
            <td class="text-left">원데이/반나절 투어</td>
            <td class="text-left">반나절 투어 신청 시 여권의 영문과 항공권의 영문이 다릅니다. 어떻게 하나요?</td>
          </tr>
          <tr class="bdr-none-td slide-child">
            <td colspan="5" class="text-left pos-rel">
              <dl class="qa">
                <dt>접수할 때 작성한 도착일로부터 24시간인가요? 아니면 무조건 사용일 기준 24시간인가요?</dt>
                <dd>안녕하세요. 오키나와 오박사를 이용해 주셔서 감사드립니다. 1:1문의 담당자 천명관입니다.<br>
문의하신 오키나와 실속 렌터카 상품 문의에 대해 답변 안내 드립니다.<br>
업체측으로 전달하여 해당 내용 확인 후 sms 안내 도와드리도록 하겠으며 확인까지 영업일 1~2일 소요될 수 있으며 조금만 시간 양해 부탁드리겠습니다.<br>
오늘하루 고객님께 행복한 일들만 가득하기를 기원합니다. 감사합니다. </dd>
              </dl>
              <ul class="funcs">
                <li><span class="category-tag">수정</span></li>
                <li><span class="category-tag">삭제</span></li>
              </ul>
            </td>
          </tr>
          <tr class="bdr-none-td slide-parent">
            <td>1</td>
            <td class="text-left">원데이/반나절 투어</td>
            <td class="text-left">반나절 투어 신청 시 여권의 영문과 항공권의 영문이 다릅니다. 어떻게 하나요?</td>
          </tr>
          <tr class="bdr-none-td slide-child">
            <td colspan="5" class="text-left pos-rel">
              <dl class="qa">
                <dt>접수할 때 작성한 도착일로부터 24시간인가요? 아니면 무조건 사용일 기준 24시간인가요?</dt>
                <dd>안녕하세요. 오키나와 오박사를 이용해 주셔서 감사드립니다. 1:1문의 담당자 천명관입니다.<br>
문의하신 오키나와 실속 렌터카 상품 문의에 대해 답변 안내 드립니다.<br>
업체측으로 전달하여 해당 내용 확인 후 sms 안내 도와드리도록 하겠으며 확인까지 영업일 1~2일 소요될 수 있으며 조금만 시간 양해 부탁드리겠습니다.<br>
오늘하루 고객님께 행복한 일들만 가득하기를 기원합니다. 감사합니다. </dd>
              </dl>
              <ul class="funcs">
                <li><span class="category-tag">수정</span></li>
                <li><span class="category-tag">삭제</span></li>
              </ul>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <ul class="paging">
      <li><a href="#"><span></span></a></li>
      <li class="paging-current"><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li><a href="#">6</a></li>
      <li><a href="#">7</a></li>
      <li><a href="#">8</a></li>
      <li><a href="#">9</a></li>
      <li><a href="#">10</a></li>
      <li><a href="#"><span></span></a></li>
    </ul>
  </div>
  <!-- /.container-lg -->

<script>
$(function(){
  $('tr.slide-parent').click(function(){
    $(this).toggleClass('table-notice-bg');
    $(this).next().fadeToggle();
  });
})
</script>

</div>
<!-- /.contents -->

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>