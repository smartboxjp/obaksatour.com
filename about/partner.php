<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連
?>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?
  $meta_title = "제휴문의";
  $meta_description = "";
  require_once $_SERVER['DOCUMENT_ROOT']."/include/meta.php";
?>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/common/css/form.css" rel="stylesheet">
<link href="/about/css/partner.css" rel="stylesheet">

  <script type="text/javascript">

  $(function() {

    $('#form_confirm').click(function() {
      err_default = "";
      err_check_count = 0;
      bgcolor_default = "#FFFFFF";
      bgcolor_err = "#FFCCCC";
      background = "background-color";

      err_check_count += check_input("partner_name");
      err_check_count += check_input_email("partner_email");
      err_check_count += check_input("partner_tel");
      err_check_count += check_input("partner_comment");

      if(err_check_count!=0)
      {
        alert("필수 항목을 정확히 입력해주세요.");
        return false;
      }
      else
      {
        //$('#form_confirm').submit();
        $('#form_confirm', "body").submit();
        return true;
      }


    });

    function check_input($str)
    {
      $("#err_"+$str).html(err_default);
      $("#"+$str).css(background,bgcolor_default);

      if($('#'+$str).val()=="")
      {
        err ="정확히 입력해주세요.";
        $("#err_"+$str).html(err);
        $("#"+$str).css(background,bgcolor_err);

        return 1;
      }
      return 0;
    }

    function check_input_email($str)
    {
      $("#err_"+$str).html(err_default);
      $("#"+$str).css(background,bgcolor_default);

      if($('#'+$str).val()=="")
      {
        err ="정확히 입력해주세요.";
        $("#err_"+$str).html(err);
        $("#"+$str).css(background,bgcolor_err);

        return 1;
      }
      else if(checkIsEmail($('#'+$str).val()) == false)
      {
        err ="정확한 이메일을 입력해주세요.";
        $("#err_"+$str).html(err);
        $("#"+$str).css(background,bgcolor_err);

        return 1;
      }

      return 0;
    }


    //メールチェック
    function checkIsEmail(value) {
      if (value.match(/.+@.+\..+/) == null) {
        return false;
      }
      return true;
    }

  });

//-->
</script>
</head>
<body>

<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

  <div class="l-container">
    <div class="pankuzu-wapper">
      <ul class="pankuzu">
        <li><a href="/">오키나와오박사 홈</a></li>
        <li>제휴문의</li>
      </ul>
    </div>
    <!-- /.pankuzu-wapper -->
  </div>
  <!-- /.l-container -->


  <div class="l-container clearfix">
    <h1 class="headline-glay fsize-lg left-bdr mb-30"><span class="color-blue">제휴문의</span></h1>
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <div class="table-responsive">
      <form class="signup-main" action="partner_save.php" name="form_regist" id="form_regist" method="post">
        <table class="table table-bordered">
          <tbody>
            <tr>
              <td colspan="2" class="main-board">

                <h1 class="headline-glay fsize-lg mb-20">오키나와오박사는<br /> 현지 기업과 여행객 <br />여행객과 자연을 이어 <br /> <span class="color-lightblue">보다 나은 오키나와 여행</span>을 만들기 위한 <br /> 다양한 사업 및 서비스 제안을 기다립니다.</h1>
              </td>
            </tr>
            <tr>
              <td class="ta-left wid-a hidden-xs">담당자 이름</td>
              <td class="ta-left b">
                <? $var = "partner_name";?>
                <input type="text" class="form-control input-textbox" id="<? echo $var;?>" name="<? echo $var;?>" placeholder="담당자 이름을 입력하세요" maxlength="30">
                <span id="err_<?=$var;?>" class="alert-red"></span>
              </td>
            </tr>
            <tr>
              <td class="ta-left wid-a hidden-xs">연락가능한 이메일</td>
              <td class="ta-left b">
                <? $var = "partner_email";?>
                <input type="text" class="form-control input-textbox" id="<? echo $var;?>" name="<? echo $var;?>" placeholder="연락가능한 이메일을 입력하세요" maxlength="100">
                <span id="err_<?=$var;?>" class="alert-red"></span>
              </td>
            </tr>
            <tr>
              <td class="ta-left wid-a hidden-xs">전화번호</td>
              <td class="ta-left b">
                <? $var = "partner_tel";?>
                <input type="text" class="form-control input-textbox" id="<? echo $var;?>" name="<? echo $var;?>" placeholder="연락가능한 전화번호를 입력하세요" maxlength="20">
                <span id="err_<?=$var;?>" class="alert-red"></span>
              </td>
            </tr>
            <tr>
              <td class="ta-left wid-a hidden-xs">제휴내용</td>
              <td class="ta-left b">
                <? $var = "partner_comment";?>
                <textarea class="form-control animated" id="<? echo $var;?>" name="<? echo $var;?>" placeholder="제휴내용할 내용을 입력하세요"></textarea>
                <span id="err_<?=$var;?>" class="alert-red"></span>
              </td>
            </tr>
            <tr>
              <td class="b text-center" colspan="2">
                <div class="col-xs-12">
                  <? $var = "form_confirm";?>
                  <button type="submit" class="btn btn-primary btn-lg2" id="<? echo $var;?>" name="<? echo $var;?>">제휴문의하기</button>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

</div>
<!-- /.contents -->

<script type="text/javascript">
  $(function(){
    $('.normal').autosize();
    $('.animated').autosize({append: "\n"});
  });
</script>
<script src="/common/js/textarea.js"></script>

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>
