<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連

  $arr_cate_notice = array();
  $sql = " SELECT cate_notice_id, cate_notice_title FROM cate_notice order by view_level" ;
  $db_result = $common_dao->db_query_bind($sql);
  if($db_result)
  {
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
      $arr_cate_notice[$db_result[$db_loop]["cate_notice_id"]] = $db_result[$db_loop]["cate_notice_title"];
    }
  }
?>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?
  $meta_title = "공지사항";
  $meta_description = "";
  require_once $_SERVER['DOCUMENT_ROOT']."/include/meta.php";
?>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/about/css/about.css" rel="stylesheet" >

</head>
<body>

<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

  <div class="l-container">
    <div class="pankuzu-wapper">
      <ul class="pankuzu">
        <li><a href="/">오키나와 오박사 홈</a></li>
        <li>공지사항</li>
      </ul>
    </div>
    <!-- /.pankuzu-wapper -->
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <div class="localnavs">
      <p class="localnavs-title">고객센터</p>
      <ul class="localnavs-list">
        <li class="localnavs-list-current"><a href="/about/notice.php">공지사항</a></li>
        <li><a href="/about/faq.php">자주하는 질문</a></li>
        <li><a href="/about/onebyone.php">1:1문의</a></li>
        <li><a href="/about/partner.php">제휴 문의</a></li>
      </ul>
      <p class="localnavs-text color-lightblue bg-gy">! 원하시는 정보를 찾지 못하셨거나 답변이 충분치 않으셨다면 1:1 문의를 통해 더 자세히 문의해 주세요.</p>
    </div>
    <!-- /.localnavs -->
  </div>
  <!-- /.l-container -->

<?php
    
    $view_count=10;   // List count
    $offset=0;

    if(!$page)
    {
        $page=1;
    }
    Else
    {
        $offset=$view_count*($page-1);
    }

    $where = "";
    if($s_keyword!="")
    {
        $where .= " and (notice_title collate utf8_unicode_ci like '%".$s_keyword."%' or notice_comment collate utf8_unicode_ci like '%".$s_keyword."%' or notice_id = '".$s_keyword."' ) ";
    }

    //合計
    $all_count = 0;
    $sql = "SELECT count(notice_id) as all_count FROM notice where 1 ".$where ;
    $arr_goods_all = $common_dao->db_query_bind($sql);
    $all_count = $arr_goods_all[0]["all_count"];


    $arr_db_field = array("notice_id", "cate_notice_id", "view_date", "notice_title", "notice_comment");
    $arr_db_field = array_merge($arr_db_field, array("flag_top", "regi_date", "up_date"));

    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM notice where 1 ".$where;
    $sql .= " order by flag_top, view_date desc";
    $sql .= " limit $offset,$view_count";
?>
  <div class="l-container">
    <h1 class="headline-glay fsize-lg left-bdr mb-30"><span class="color-blue">새소식 및</span> 공지사항</h1>
    <div class="table-responsive">
      <table class="table table-bordered table-notice">
        <thead>
          <tr>
            <th class="cel-a">분류</th>
            <th>공지내용</th>
          </tr>
        </thead>
        <tbody>
          <?php 

          $db_result = $common_dao->db_query_bind($sql);
          if($db_result)
          {
            for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
            {
              foreach($arr_db_field as $val)
              {
                $$val = $db_result[$db_loop][$val];
              }
          ?>
          <tr class="bdr-none-td table-notice-bg slide-parent">
            <td class="text-center">
              <span class="category-tag"><?php echo $arr_cate_notice[$cate_notice_id]; ?></span>
            </td>
            <td class="text-left">
              <div ><span class="onetable-date"><? echo $view_date;?></span></div>
              <? echo $notice_title;?>
            </td>
          </tr>
          <tr class="bdr-none-td slide-child">
            <td class="text-left" colspan="2"><? echo htmlspecialchars_decode($notice_comment);?></td>
          </tr>
          <?php
            }
          }
          ?>
        </tbody>
      </table>
    </div>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

  <!-- paging -->
  <?php $common_connect -> Fn_paging_10_user($view_count, $all_count); ?>
  <!-- END paging -->


</div>
<!-- /.contents -->

<script>
$(function(){
  $('.table-notice tr.slide-parent').click(function(){
    $(this).toggleClass('table-notice-bg');
    $(this).next().fadeToggle();
  });
})
</script>

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>