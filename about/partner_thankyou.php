<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連
  
  foreach($_GET as $key => $value)
  { 
    $$key = $common_connect->h($value);
  }
?>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?
  $meta_title = "제휴문의완료";
  $meta_description = "";
  require_once $_SERVER['DOCUMENT_ROOT']."/include/meta.php";
?>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/about/css/partner_thankyou.css" rel="stylesheet">

</head>
<body>
<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

  <div class="l-container">
    <div class="pankuzu-wapper">
      <ul class="pankuzu">
        <li><a href="/">오키나와오박사 홈</a></li>
        <li>제휴문의완료</li>
      </ul>
    </div>
    <!-- /.pankuzu-wapper -->
  </div>
  <!-- /.l-container -->


  <div class="l-container clearfix">
    <h1 class="headline-glay fsize-lg left-bdr mb-30"><span class="color-blue">제휴문의완료</span></h1>
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <div class="table-responsive">
        <table class="table table-bordered">
          <tbody>
            <tr>
              <td class="main-board">
                <h1 class="headline-glay fsize-lg mb-20">제휴메일이 <span class="color-lightblue"> 성공적으로 전송 되었습니다.</span></h1>
                <p>등록된 이메일로 <span class="color-blue">영업시간 2-3일내에 답장이 없으면</span>다시한번 전송해 주세요.</p>

                <div style="margin-top:21px;padding:50px 0;text-align:center;font-size:19px;letter-spacing:-0.1em;font-weight:600">

                  <a class="btn btn-primary btn-link" href="/member/">오키나와 오박사 로그인하기</a>
                  <a href="/" class="a_top">오키나와 오박사 메인페이지</a>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
    </div>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

</div>
<!-- /.contents -->

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>