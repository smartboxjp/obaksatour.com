
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Language" content="ja" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>와루미 대교 - 새들의 눈으로 본 오키나와</title>
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="와루미대교, 와루미다리, 드론촬영, 오키나와" />
<meta name="description" content="오키나와 오박사투어에서 촬영한 와루미 다리는 오키나와 북부에 위치하여 있고 코리지마의 절경을 감상 할 수 있다." />
<meta property="og:title" content="와루미 대교 - 새들의 눈으로 본 오키나와">
<meta property="og:description" content="오키나와 오박사투어에서 촬영한 와루미 다리는 오키나와 북부에 위치하여 있고 코리지마의 절경을 감상 할 수 있다.">
<!--<meta property="og:image" content="http://www.obaksatour.com/assets/img/common/bev_ogp.png">-->
<meta property="og:url" content="http://www.obaksatour.com/drone/">
<meta property="og:type" content="website">
<meta property="og:locale" content="ko_KO">
<meta property="og:site_name" content="오키나와 오박사투어">
<meta property="fb:app_id" content="1593201150943254" />
<!--[If lte IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[If lte IE 9]><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
<link rel="shortcut icon" href="//www.obaksatour.com/assets/img/bev/favicon.ico" />
<link href='//fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>

<link rel='canonical' href='http://www.obaksatour.com/drone/' />


    <!-- Stylesheets -->
    <!-- Bootstrap is included in its original form, unaltered -->
    <link rel="stylesheet" href="/app_master/css/bootstrap.min.css">

    <!-- Related styles of various icon packs and plugins -->
    <link rel="stylesheet" href="/app_master/css/plugins.css">

    <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
    <link rel="stylesheet" href="/app_master/css/main.css">

    <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->
    
    <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
    <link rel="stylesheet" href="/app_master/css/themes.css">
    <!-- END Stylesheets -->

    <!-- Modernizr (browser feature detection library) -->
    <script src="js/vendor/modernizr.min.js"></script>
</head>

<body id="index" class="bevok">

    <div id="loader"><i class="fa fa-asterisk fa-spin fa-4x text-danger"></i></div>


    <header>
        <div id="header" class="clearfix">
            <div id="home"><a href="/drone/">HOME</a></div>
            <nav id="sns">
                <ul class="clearfix">
                    
                    <li id="sns-fb">
                        <div id="socialarea_facebook2" class="snsCn"><a href="http://www.facebook.com/share.php?u=http://www.kagoshima-trip.jp/drone/" onclick="window.open(this.href, 'FBwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;" class="fbBtn f_right"><span class="count"></span></a></div>
                    </li>
                    <li id="sns-tw">
                        <div id="socialarea_twitter2" class="snsCn"><a href="http://twitter.com/share?count=horizontal&original_referer=http://www.kagoshima-trip.jp/drone/&text=BIRD%27S%20EYE%20VIEW%20OF%20KAGOSHIMA&url=http://www.kagoshima-trip.jp/drone/" onclick="window.open(this.href, 'tweetwindow', 'width=550, height=450,personalbar=0,toolbar=0,scrollbars=1,resizable=1'); return false;" class="twBtn f_right"><span class="count"></span></a></div>
                    </li>
                </ul>
            </nav>
            <nav id="gnavi">
                <ul id="fade-in" class="clearfix dropmenu">
                    <li class="nav"><a href="/drone/ja-bev/">MOVIE LIST / 動画一覧</a></li>             
                    <li class="nav"><a href="/drone/sp-bev/">SPECIAL MOVIE / スペシャルムービー</a></li>
                </ul>
            </nav>
        </div>
    </header>
    <div id="white-map"></div>
    
    <div class="buttons"><a id="show-bev"><img src="http://kagoshima-trip.jp/assets/img/common/btn_navi.png"></a></div>
    
    <section id="content">

        <article id="intro">
            <h1><img src="http://kagoshima-trip.jp/assets/img/index/logo-birds.png" alt="BIRD'S EYE VIEW OF KAGOSHIMA"></h1>
            <div id="down2"><a href="#"><Img src="http://kagoshima-trip.jp/assets/img/index/down.png"></a></div>
        </article>
        <!-- intro -->
        
        <article id="reccomend">
        
            <div id="about-txt">
                <div id="about-ja">
                    <p>南北およそ600kmのエリアに26もの有人離島が点在する鹿児島県。それぞれの島には、驚くような、神秘的で表情ゆたかな自然が残っています。</p>
                </div>
                <div id="about-en">
                    <p>With prefectural boundaries stretching 600 km from north to south, Kagoshima is home to 28 beautiful inhabited islands. Each of these exotic islands has its own unique characteristics and extraordinary pristine nature.</p>
                </div>
            </div>
        
            <ul class="clearfix" id="otherislands">
                <li class="r-thumb-box1">
                    <div class="r-list">
                        <a href="/drone/ja-koshiki/">
                        <div class="r-thumb" id="thumb1"><img src="http://kagoshima-trip.jp/assets/img/bev/thumb_1.jpg"></div>
                        <h3 class="rc-ja-h3">
                            <span class="thumb-title-ja">甑島</span>
                            <span class="thumb-title-en">KOSHIKISHIMA</span>
                            
                        </h3>
                        </a>
                    </div>
                </li>
                <li class="r-thumb-box1">
                    <div class="r-list">
                        <a href="/drone/ja-yakushima/">
                        <div class="r-thumb" id="thumb2"><img src="http://kagoshima-trip.jp/assets/img/bev/thumb_2.jpg"></div>
                        <h3 class="rc-ja-h3">
                            <span class="thumb-title-ja">屋久島</span>
                            <span class="thumb-title-en">YAKUSHIMA</span>
                            
                        </h3>
                        </a>
                    </div>
                </li>
                <li class="r-thumb-box2">
                    <div class="r-list">
                        <a href="/drone/ja-tanegashima/">
                        <div class="r-thumb" id="thumb3"><img src="http://kagoshima-trip.jp/assets/img/bev/thumb_3.jpg"></div>
                        <h3 class="rc-ja-h3">
                            <span class="thumb-title-ja">種子島</span>
                            <span class="thumb-title-en">TANEGASHIMA</span>
                        </h3>
                        </a>
                    </div>
                </li>
                <li class="r-thumb-box1">
                    <div class="r-list">
                        <a href="/drone/ja-amami/">
                        <div class="r-thumb" id="thumb4"><img src="http://kagoshima-trip.jp/assets/img/bev/thumb_4.jpg"></div>
                        <h3 class="rc-ja-h3">
                            <span class="thumb-title-ja">奄美大島</span>
                            <span class="thumb-title-en">AMAMI OSHIMA</span>
                            
                        </h3>
                        </a>
                    </div>
                </li>
                <li class="r-thumb-box1">
                    <div class="r-list">
                        <a href="/drone/ja-kakeroma/">
                        <div class="r-thumb" id="thumb5"><img src="http://kagoshima-trip.jp/assets/img/bev/thumb_5.jpg"></div>
                        <h3 class="rc-ja-h3">
                            <span class="thumb-title-ja">加計呂麻島</span>
                            <span class="thumb-title-en">KAKEROMAJIMA</span>
                            
                        </h3>
                        </a>
                    </div>
                </li>
                <li class="r-thumb-box2">
                    <div class="r-list">
                        <a href="/drone/ja-yoron/">
                        <div class="r-thumb" id="thumb6"><img src="http://kagoshima-trip.jp/assets/img/bev/thumb_6.jpg"></div>
                        <h3 class="rc-ja-h3">
                            <span class="thumb-title-ja">与論島</span>
                            <span class="thumb-title-en">YORON ISLAND</span>
                        </h3>
                        </a>
                    </div>
                </li>
            </ul>
        </article>
        <!-- reccomend -->
        
        <article id="dmap">
            <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> 
            <script src="http://kagoshima-trip.jp/assets/js/gmap.js"></script>
            <div id="map_canvas"></div>
            <h2><img src="http://kagoshima-trip.jp/assets/img/index/map-title.png" alt="MAP OF ISOLATED ISLAND"></h2>
        </article>
        <!--/dmap-->
        
        
    </section>
    <!-- content -->
    
    <footer>
    <ul class="clearfix">
        <li><a href="/" target="_blank"><img src="http://kagoshima-trip.jp/assets/img/common/bn_ejk.jpg" alt="KAGOSHIMA ENERGETIC JAPAN"></a></li>
        <li><a href="https://www.facebook.com/pages/Kagoshima-Energetic-Japan/709046385843095" target="_blank"><img src="http://kagoshima-trip.jp/assets/img/common/bn_facebook.jpg" alt="facebook"></a></li>
    </ul>
    <p id="copy">&copy; 2016 KAGOSHIMA PREFECTURE ALL RIGHTS RESERVED.</p>
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script src="http://kagoshima-trip.jp/assets/js/jquery.easing.min.js"></script>
<script src="http://kagoshima-trip.jp/assets/js/jquery.transit.min.js"></script>
<script src="http://kagoshima-trip.jp/assets/js/avgrund.js"></script> 
<script src="http://kagoshima-trip.jp/assets/js/config.js"></script>


    <script src="./js/jquery.tubular.1.0.js"></script>
    <script>
    if(!navigator.userAgent.match(/(iPhone|iPad|Android)/)){
        $(function(){
            $('document').ready(function() {
                var options = { videoId: '850QkzT4j-E', mute: false};
                $('body.chrome, body.ie8, body.ie9, body.ie10, body.ie11, body.firefox, body.safari, body.opera').tubular(options);
            });
        });
    }
    </script>
    
</body>
</html>