<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title>main</title>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/css/main.css" rel="stylesheet">

<!-- swiper -->
<link href="/common/css/swiper.min.css" rel="stylesheet">
<script src="/common/js/swiper.min.js"></script>

</head>
<body>

<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

<div class="main-vis-wrap">
  <div class="l-container">
    <div class="main-vis">
      <ul class="visual-nav">
        <li><a href ="#"><span class="visual-nav-one">렌터카z</span></a></li>
        <li><a href="#"><span>원데이<br>반나절<br>투어</span></a></li>
        <li><a href="#"><span class="visual-nav-two">패키지<br>투어</span></a></li>
        <li><a href="#"><span class="visual-nav-two">오키나와<br>가이드</span></a></li>
        <li><a href="#"><span class="visual-nav-two">해양<br>스포츠</span></a></li>
      </ul>
      <img src="/images/main-vis.jpg" alt="">
    </div>
    <!-- /.main-vis -->
  </div>
  <!-- /.l-container -->
</div>
<!-- /.main-vis-wrap -->


<div class="l-container">
  <div class="sitenav-wrapper">
    <div class="swiper-container sitenav-container">
      <div class="swiper-wrapper">
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-1.png" alt="오박사투어는 ?" class=" sitenav-img">
            <span class="sitenav-text">오박사투어는 ?</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-2.png" alt="오박사투어 렌터카" class=" sitenav-img">
            <span class="sitenav-text">오박사투어 렌터카</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-2.png" alt="원데이 / 반날절 투어" class=" sitenav-img">
            <span class="sitenav-text">원데이 / 반날절 투어</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-4.png" alt="" class=" sitenav-img">
            <span class="sitenav-text">추천 해양 스포츠</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-5.png" alt="오키나와 가이드" class=" sitenav-img">
            <span class="sitenav-text">오키나와 가이드</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-6.png" alt="패키지 투어" class=" sitenav-img">
            <span class="sitenav-text">오키나와</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-7.png" alt="오박사투어는 ?" class=" sitenav-img">
            <span class="sitenav-text">오키나와</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-7.png" alt="" class=" sitenav-img">
            <span class="sitenav-text">오키나와</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-7.png" alt="" class=" sitenav-img">
            <span class="sitenav-text">오키나와</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
      </div>
      <!-- /.swiper-wrapper -->
    </div>
    <div class="button-prev swiper-button-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
    <div class="button-next swiper-button-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
    <!-- /.swiper-container swiper-a -->
  </div>
  <!-- /.sitenav-wrapper -->
</div>
<!-- /.container -->

  <div class="l-container">
    <div class="pankuzu-wapper">
      <ul class="pankuzu">
        <li><a href="/">오키나와 오박사 홈</a></li>
        <li><a href="#">마이페이지</a></li>
        <li>나의 구매 목록</li>
      </ul>
    </div>
    <!-- /.pankuzu-wapper -->
  </div>
  <!-- /.l-container -->

  <div class="l-container mb-50">
    <h2 class="headline-glay fsize-lg left-bdr mb-30"><span class="color-lightblue">관광버스로 갈 수 있는</span> 오키나와 주요 관광지</h2>
    <ul class="local-nav">
      <li class="local-nav-current"><a href="#">전체 관광지 보기</a></li>
      <li><a href="#">원데이 / 반나절 투어</a></li>
      <li><a href="#">코우리섬 대교</a></li>
      <li><a href="#">오카시코텐</a></li>
      <li><a href="#">만좌모</a></li>
      <li><a href="#">치넨미사키</a></li>
      <li><a href="#">세화우타키</a></li>
      <li><a href="#">미이바루비치</a></li>
      <li><a href="#">나라이카나이 다리</a></li>
    </ul>
    <div class="row l-row">
      <div class="col-sm-6 col-pad">
        <a href="#" class="cardcol" data-mh="match-height">
          <img src="https://api.fnkr.net/testimg/210x210/00CED1/FFF/?text=img+placeholderp" alt="" class="cardcol-img">
          <dl class="cardcol-box">
            <dt class="cardcol-box-cate">원데이 / 반나절 투어</dt>
            <dd class="cardcol-box-title">세계 최대급의 해양 수족관 추라우미</dd>
            <dd  class="cardcol-box-text">모토부 반도의 북쪽 바다에 위치한 코우리 대교로 연결되는 코우리섬. 아름다운 해변이  많은 이 섬에는 오키나와의 아담과 이브 전설이 전해져와 “사랑</dd>
            <dd class="cardcol-box-more"><span class="btn-gy btn-sm btn">내용 더보기</span></dd>
          </dl>
        </a>
        <!-- /.cardcol -->
      </div>
      <!-- /.col-sm-6 -->
      <div class="col-sm-6 col-pad">
        <a href="#" class="cardcol" data-mh="match-height">
          <img src="https://api.fnkr.net/testimg/210x210/00CED1/FFF/?text=img+placeholderp" alt="" class="cardcol-img">
          <dl class="cardcol-box">
            <dt class="cardcol-box-cate">원데이 / 반나절 투어</dt>
            <dd class="cardcol-box-title">세계 최대급의 해양 수족관 추라우미</dd>
            <dd  class="cardcol-box-text">모토부 반도의 북쪽 바다에 위치한 코우리 대교로 연결되는 코우리섬. 아름다운 해변이  많은 이 섬에는 오키나와의 아담과 이브 전설이 전해져와 “사랑</dd>
            <dd class="cardcol-box-more"><span class="btn-gy btn-sm btn">내용 더보기</span></dd>
          </dl>
        </a>
        <!-- /.cardcol -->
      </div>
      <!-- /.col-sm-6 -->
      <div class="col-sm-6 col-pad">
        <a href="#" class="cardcol" data-mh="match-height">
          <img src="https://api.fnkr.net/testimg/210x210/00CED1/FFF/?text=img+placeholderp" alt="" class="cardcol-img">
          <dl class="cardcol-box">
            <dt class="cardcol-box-cate">원데이 / 반나절 투어</dt>
            <dd class="cardcol-box-title">세계 최대급의 해양 수족관 추라우미</dd>
            <dd  class="cardcol-box-text">모토부 반도의 북쪽 바다에 위치한 코우리 대교로 연결되는 코우리섬. 아름다운 해변이  많은 이 섬에는 오키나와의 아담과 이브 전설이 전해져와 “사랑</dd>
            <dd class="cardcol-box-more"><span class="btn-gy btn-sm btn">내용 더보기</span></dd>
          </dl>
        </a>
        <!-- /.cardcol -->
      </div>
      <!-- /.col-sm-6 -->
      <div class="col-sm-6 col-pad">
        <a href="#" class="cardcol" data-mh="match-height">
          <img src="https://api.fnkr.net/testimg/210x210/00CED1/FFF/?text=img+placeholderp" alt="" class="cardcol-img">
          <dl class="cardcol-box">
            <dt class="cardcol-box-cate">원데이 / 반나절 투어</dt>
            <dd class="cardcol-box-title">세계 최대급의 해양 수족관 추라우미</dd>
            <dd  class="cardcol-box-text">모토부 반도의 북쪽 바다에 위치한 코우리 대교로 연결되는 코우리섬. 아름다운 해변이  많은 이 섬에는 오키나와의 아담과 이브 전설이 전해져와 “사랑</dd>
            <dd class="cardcol-box-more"><span class="btn-gy btn-sm btn">내용 더보기</span></dd>
          </dl>
        </a>
        <!-- /.cardcol -->
      </div>
      <!-- /.col-sm-6 -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->

  <div class="l-container mb-50">
    <h2 class="headline-glay fsize-lg left-bdr mb-30"><span class="color-lightblue">관광버스로 갈 수 있는</span> 오키나와 주요 관광지</h2>
    <div class="row l-row">
      <div class="col-sm-6 ballbox col-pad"  data-mh="group1">
        <span class="ball ballbox-left">두번째</span>
        <dl class="ballbox-text">
          <dt>인기 관광 명소를 한 곳에</dt>
          <dd>오키나와 북부의 추라우미수족관, <span class="color-lightblue">코우리섬 대교, 중부의 </span>만좌모, 남부의 치넨미사키, 세화우타키 미이바루비치, 니라이카나이 다리 의 인기관광코스를 저렴하고 알차게 여행하 실 수 있습니다.</dd>
        </dl>
      </div>
      <!-- /.col-sm-6 -->
      <div class="col-sm-6 ballbox col-pad"  data-mh="group1">
        <span class="ball ballbox-left">두번째</span>
        <dl class="ballbox-text">
          <dt>인기 관광 명소를 한 곳에</dt>
          <dd>오키나와 북부의 추라우미수족관, <span class="color-lightblue">코우리섬 대교, 중부의 </span>만좌모, 남부의 치넨미사키, 세화우타키 미이바루비치, 니라이카나이 다리 의 인기관광코스를 저렴하고 알차게 여행하 실 수 있습니다.</dd>
        </dl>
      </div>
      <!-- /.col-sm-6 -->
      <div class="col-sm-6 ballbox col-pad"  data-mh="group1">
        <span class="ball ballbox-left">두번째</span>
        <dl class="ballbox-text">
          <dt>인기 관광 명소를 한 곳에</dt>
          <dd>오키나와 북부의 추라우미수족관, <span class="color-lightblue">코우리섬 대교, 중부의 </span>만좌모, 남부의 치넨미사키, 세화우타키 미이바루비치, 니라이카나이 다리 의 인기관광코스를 저렴하고 알차게 여행하 실 수 있습니다.</dd>
        </dl>
      </div>
      <!-- /.col-sm-6 -->
      <div class="col-sm-6 ballbox col-pad"  data-mh="group1">
        <span class="ball ballbox-left">두번째</span>
        <dl class="ballbox-text">
          <dt>인기 관광 명소를 한 곳에</dt>
          <dd>오키나와 북부의 추라우미수족관, <span class="color-lightblue">코우리섬 대교, 중부의 </span>만좌모, 남부의 치넨미사키, 세화우타키 미이바루비치, 니라이카나이 다리 의 인기관광코스를 저렴하고 알차게 여행하 실 수 있습니다.</dd>
        </dl>
      </div>
      <!-- /.col-sm-6 -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.l-container -->

  <div class="l-container mb-50">
    <div class="title-wrap clearfix">
      <h2 class="headline-glay fsize-lg left-bdr ttl"><span class="color-lightblue">버스투어 </span> 상품 보기</h2>
      <ul class="funcui">
        <li><span><i class="material-icons">&#xE5CB;</i></span></li>
        <li><span><i class="material-icons">&#xE5CC;</i></span></li>
        <li><span><i class="material-icons">&#xE145;</i></span></li>
      </ul>
    </div>
    <!-- /.title-wrap -->
    <div class="row l-row">
      <div class="col-xs-6 col-sm-3 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <span class="ball card-imagearea-ball ball-sm">두번째</span>
            <img src="https://api.fnkr.net/testimg/234x190/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="card-text-sub">코우리지마 대료 / 추라우미 수족관 / 만좌모 아메리칸 빌리지</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-msg"><span>1인</span>기준</div>
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
      <div class="col-xs-6 col-sm-3 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <span class="ball card-imagearea-ball ball-sm">두번째</span>
            <img src="https://api.fnkr.net/testimg/234x190/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="card-text-sub">코우리지마 대료 / 추라우미 수족관 / 만좌모 아메리칸 빌리지</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-msg"><span>1인</span>기준</div>
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
      <div class="col-xs-6 col-sm-3 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <span class="ball ball-gy card-imagearea-ball ball-sm">두번째</span>
            <img src="https://api.fnkr.net/testimg/234x190/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="card-text-sub">코우리지마 대료 / 추라우미 수족관 / 만좌모 아메리칸 빌리지</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-msg"><span>1인</span>기준</div>
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
      <div class="col-xs-6 col-sm-3 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <span class="ball card-imagearea-ball ball-sm">두번째</span>
            <img src="https://api.fnkr.net/testimg/234x190/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="card-text-sub">코우리지마 대료 / 추라우미 수족관 / 만좌모 아메리칸 빌리지</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-msg"><span>1인</span>기준</div>
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.l-container -->

</div>
<!-- /.contents -->

<script>
var swiperA = new Swiper('.sitenav-container', {
  paginationClickable: true,
  slidesPerView: 7,
  speed: 600,
  nextButton: '.button-next',
  prevButton: '.button-prev',
  freeMode: 'auto',
  spaceBetween: 0,
  breakpoints: {
    980: {
      slidesPerView: 5,
      spaceBetween: 10
    },
    780: {
      slidesPerView: 4,
      spaceBetween: 10
    },
    680: {
      slidesPerView: 3,
      spaceBetween: 10
    },
    580: {
      slidesPerView: 2,
      spaceBetween: 10
    }
  }
});
</script>

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>