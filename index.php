<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title>main-b</title>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/css/main.css" rel="stylesheet">

<!-- swiper -->
<link href="/common/css/swiper.min.css" rel="stylesheet">
<script src="/common/js/swiper.min.js"></script>
<script src="//cdn.rawgit.com/tonystar/bootstrap-hover-tabs/master/bootstrap-hover-tabs.js"></script>

</head>
<body>

<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

<div class="main-vis-wrap">
  <div class="l-container">
    <div class="main-vis">
      <ul class="visual-nav">
        <li><a href ="#"><span class="visual-nav-one">렌터카z</span></a></li>
        <li><a href="#"><span>원데이<br>반나절<br>투어</span></a></li>
        <li><a href="#"><span class="visual-nav-two">패키지<br>투어</span></a></li>
        <li><a href="#"><span class="visual-nav-two">오키나와<br>가이드</span></a></li>
        <li><a href="#"><span class="visual-nav-two">해양<br>스포츠</span></a></li>
      </ul>
      <img src="/images/main-vis.jpg" alt="">
    </div>
    <!-- /.main-vis -->
  </div>
  <!-- /.l-container -->
</div>
<!-- /.main-vis-wrap -->


<div class="l-container">
  <div class="sitenav-wrapper">
    <div class="swiper-container sitenav-container">
      <div class="swiper-wrapper">
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-1.png" alt="오박사투어는 ?" class=" sitenav-img">
            <span class="sitenav-text">오박사투어는 ?</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-2.png" alt="오박사투어 렌터카" class=" sitenav-img">
            <span class="sitenav-text">오박사투어 렌터카</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-2.png" alt="원데이 / 반날절 투어" class=" sitenav-img">
            <span class="sitenav-text">원데이 / 반날절 투어</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-4.png" alt="" class=" sitenav-img">
            <span class="sitenav-text">추천 해양 스포츠</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-5.png" alt="오키나와 가이드" class=" sitenav-img">
            <span class="sitenav-text">오키나와 가이드</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-6.png" alt="패키지 투어" class=" sitenav-img">
            <span class="sitenav-text">오키나와</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-7.png" alt="오박사투어는 ?" class=" sitenav-img">
            <span class="sitenav-text">오키나와</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-7.png" alt="" class=" sitenav-img">
            <span class="sitenav-text">오키나와</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-7.png" alt="" class=" sitenav-img">
            <span class="sitenav-text">오키나와</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
      </div>
      <!-- /.swiper-wrapper -->
    </div>
    <div class="button-prev swiper-button-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
    <div class="button-next swiper-button-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
    <!-- /.swiper-container swiper-a -->
  </div>
  <!-- /.sitenav-wrapper -->
</div>
<!-- /.container -->

  <div class="l-container mb-50 mt-50">
    <div class="title-wrap clearfix">
      <h2 class="headline-glay fsize-lg left-bdr ttl"><span class="color-lightblue">원데이 / 반나절 </span>투어</h2>
      <ul class="funcui">
        <li><span><i class="material-icons">&#xE5CB;</i></span></li>
        <li><span><i class="material-icons">&#xE5CC;</i></span></li>
        <li><span><i class="material-icons">&#xE145;</i></span></li>
      </ul>
    </div>
    <!-- /.title-wrap -->
    <div class="row l-row">
      <div class="col-xs-6 col-md-3 col-sm-4 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <span class="ball card-imagearea-ball ball-sm">한국어<br>가이드</span>
            <img src="https://api.fnkr.net/testimg/238x238/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
      <div class="col-xs-6 col-md-3 col-sm-4 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <span class="ball card-imagearea-ball ball-sm">한국어<br>가이드</span>
            <img src="https://api.fnkr.net/testimg/238x238/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
      <div class="col-xs-6 col-md-3 col-sm-4 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <img src="https://api.fnkr.net/testimg/238x238/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
      <div class="col-xs-6 col-md-3 col-sm-4 hidden-sm col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <span class="ball card-imagearea-ball ball-sm">한국어<br>가이드</span>
            <img src="https://api.fnkr.net/testimg/238x238/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.l-container -->

  <div class="l-container mb-70">
    <div class="title-wrap clearfix">
      <h2 class="headline-glay fsize-lg left-bdr ttl"><span class="color-lightblue">추천 해양 </span>스포츠</h2>
      <ul class="funcui">
        <li><span><i class="material-icons">&#xE5CB;</i></span></li>
        <li><span><i class="material-icons">&#xE5CC;</i></span></li>
        <li><span><i class="material-icons">&#xE145;</i></span></li>
      </ul>
    </div>
    <div class="row l-row">
      <div class="col-md-6 col-pad">
        <a href="#" class="card card-flo">
          <div class="card-imagearea">
            <img src="https://api.fnkr.net/testimg/210x210/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="card-text-sub">다이빙 초보자도 쉽게 체험할 수 있는 프로그램들로 구성되어 있습니다 다이빙 초보자도 쉽게 체험할 수 있는 프로그램들로 구성되어 있습니다.</dd>
            <dd class="cardcol-box-more"><span class="btn-gy btn-sm btn">내용 더보기</span></dd>
          </dl>
        </a>
      </div>
      <!-- /.col-md-3 -->
      <div class="col-md-3 col-xs-6 col-pad">
        <a href="#" class="card">
          <div class="card-imagearea">
            <img src="https://api.fnkr.net/testimg/190x122/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text" date-="gp">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
          </dl>
        </a>
      </div>
      <!-- /.col-md-3 -->
      <div class="col-md-3 col-xs-6 col-pad">
        <a href="#" class="card">
          <div class="card-imagearea">
            <img src="https://api.fnkr.net/testimg/190x122/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text" date-="gp">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
          </dl>
        </a>
      </div>
      <!-- /.col-md-3 -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.l-container -->

  <div class="l-container mb-70">
    <h2 class="headline-glay fsize-lg left-bdr mb-30"><span class="color-lightblue">오키나와</span>가이드</h2>
    <div class="row l-row">
      <div class="col-sm-3 col-xs-6 col-pad">
        <a href="#" class="facebox">
          <img src="https://api.fnkr.net/testimg/100x100/00CED1/FFF/?text=img+placeholder" class="img-circle">
          <dl>
            <dt>크리스틴 오키나와</dt>
            <dd>음악과 디자인을좋아하는 행복한 가이드랍니다. </dd>
          </dl>
        </a>
      </div>
      <!-- /.col-sm-3 col-xs-6 -->
      <div class="col-sm-3 col-xs-6 col-pad">
        <a href="#" class="facebox">
          <img src="https://api.fnkr.net/testimg/100x100/00CED1/FFF/?text=img+placeholder" class="img-circle">
          <dl>
            <dt>크리스틴 오키나와</dt>
            <dd>음악과 디자인을좋아하는 행복한 가이드랍니다. </dd>
          </dl>
        </a>
      </div>
      <!-- /.col-sm-3 col-xs-6 -->
      <div class="col-sm-3 col-xs-6 col-pad">
        <a href="#" class="facebox">
          <img src="https://api.fnkr.net/testimg/100x100/00CED1/FFF/?text=img+placeholder" class="img-circle">
          <dl>
            <dt>크리스틴 오키나와</dt>
            <dd>음악과 디자인을좋아하는 행복한 가이드랍니다. </dd>
          </dl>
        </a>
      </div>
      <!-- /.col-sm-3 col-xs-6 -->
      <div class="col-sm-3 col-xs-6 col-pad">
        <a href="#" class="facebox">
          <img src="https://api.fnkr.net/testimg/100x100/00CED1/FFF/?text=img+placeholder" class="img-circle">
          <dl>
            <dt>크리스틴 오키나와</dt>
            <dd>음악과 디자인을좋아하는 행복한 가이드랍니다. </dd>
          </dl>
        </a>
      </div>
      <!-- /.col-sm-3 col-xs-6 -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.l-container -->

  <div class="l-container mb-50">
    <div class="title-wrap clearfix">
      <h2 class="headline-glay fsize-lg left-bdr ttl"><span class="color-lightblue">원데이 / 반나절 </span>투어</h2>
      <ul class="funcui">
        <li><span><i class="material-icons">&#xE5CB;</i></span></li>
        <li><span><i class="material-icons">&#xE5CC;</i></span></li>
        <li><span><i class="material-icons">&#xE145;</i></span></li>
      </ul>
    </div>
    <!-- /.title-wrap -->
    <div class="row l-row">
      <div class="col-xs-6  col-md-3 col-sm-4 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <span class="ball card-imagearea-ball ball-sm">한국어<br>가이드</span>
            <img src="https://api.fnkr.net/testimg/238x238/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
      <div class="col-xs-6 col-md-3 col-sm-4 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <span class="ball card-imagearea-ball ball-sm">한국어<br>가이드</span>
            <img src="https://api.fnkr.net/testimg/238x238/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
      <div class="col-xs-6 col-md-3 col-sm-4 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <span class="ball card-imagearea-ball ball-sm">한국어<br>가이드</span>
            <img src="https://api.fnkr.net/testimg/238x238/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
      <div class="col-xs-6 col-md-3 col-sm-4 hidden-sm col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <img src="https://api.fnkr.net/testimg/238x238/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.l-container -->

</div>
<!-- /.contents -->

<script>
var swiperA = new Swiper('.sitenav-container', {
  paginationClickable: true,
  slidesPerView: 7,
  speed: 600,
  nextButton: '.button-next',
  prevButton: '.button-prev',
  freeMode: 'auto',
  spaceBetween: 0,
  breakpoints: {
    980: {
      slidesPerView: 5,
      spaceBetween: 10
    },
    780: {
      slidesPerView: 4,
      spaceBetween: 10
    },
    680: {
      slidesPerView: 3,
      spaceBetween: 10
    },
    580: {
      slidesPerView: 2,
      spaceBetween: 10
    }
  }
});
</script>

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>