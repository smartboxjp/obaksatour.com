<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_admin_check();
    
    $arr_member_flag_open = array();
    $arr_member_flag_open[1] = "공개";
    $arr_member_flag_open[99] = "비공개";

    $arr_member_flag_open_color = array();
    $arr_member_flag_open_color[1] = "label-success";
    $arr_member_flag_open_color[99] = "label-danger";

    $arr_flag_email = array();
    $arr_flag_email[1] = "사용";
    $arr_flag_email[99] = "비공개";

    $arr_flag_email_color = array();
    $arr_flag_email_color[1] = "label-success";
    $arr_flag_email_color[99] = "label-danger";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/config.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_start.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/page_head.php'; ?>

<script language="javascript"> 
    function fnChangeSel(i) { 
        var result = confirm('削除しますか？'); 
        if(result){ 
            document.location.href = './member_del.php?member_id='+i;
        } 
    }
</script>

<!-- Page content -->
<div id="page-content">
    <!-- 회원리스트 Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-table"></i>회원리스트<br><small></small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/app_member/dashboard/">Dashboard</a></li>
        <li>회원리스트</li>
    </ul>
    <!-- END 회원리스트 Header -->

    <!-- 회원리스트 Content -->
    <div class="block full">
        <div class="block-title">
            <h2><strong>회원리스트</strong></h2>
        </div>
        <p>등록된 회원의 편집 삭제가 가능합니다.</p>
<?php
    
    $view_count=20;   // List count
    $offset=0;

    if(!$page)
    {
        $page=1;
    }
    Else
    {
        $offset=$view_count*($page-1);
    }
    
    $where = "";
    if($s_keyword!="")
    {
        $where .= " and (member_name collate utf8_unicode_ci like '%".$s_keyword."%' or login_email collate utf8_unicode_ci like '%".$s_keyword."%' or member_id = '".$s_keyword."' ) ";
    }

    if($s_flag_open!="")
    {
        $where .= " and flag_open='".$s_flag_open."'";
    }

    //合計
    $all_count = 0;
    $sql = "SELECT count(member_id) as all_count FROM member where 1 ".$where ;
    $arr_member_all = $common_dao->db_query_bind($sql);
    $all_count = $arr_member_all[0]["all_count"];


    $arr_db_field = array("member_id", "login_email", "login_pw", "member_login_cookie", "member_name");
    $arr_db_field = array_merge($arr_db_field, array("facebook_id", "naver_id", "member_img", "flag_email", "flag_open"));
    $arr_db_field = array_merge($arr_db_field, array("lastlogin_date", "regi_date", "up_date"));

    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM member where 1 ".$where;
    if($order_name != "")
    {
        $sql .= " order by ".$order_name." ".$order;
    }
    else
    {
        $sql .= " order by regi_date desc";
    }
    $sql .= " limit $offset,$view_count";
?>
        <!-- search -->
        <div class="table-responsive" id="search_list">
            <div class="dataTables_wrapper">
                <div class="row">
                    <div class="col-sm-3 col-xs-5">
                        <label>전체：<? echo $all_count;?>건</label>
                    </div>
                    <div class="col-sm-3">
                    </div>

                    <form action="<?php echo $_SERVER['PHP_SELF']?>#search_list" method="get" name="form_shop_search">
                    <div class="col-sm-3">
                        <? $var = "s_member_flag_open";?>
                        <select name="<?php echo $var;?>" id="<?php echo $var;?>" class="form-control m-b" >
                        <option value="">사용가능유무</option>
                        <?
                        foreach($arr_member_flag_open as $key=>$value)
                        {
                        ?>
                        <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
                        <?
                        }
                        ?>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">

                            <? $var = "s_keyword";?>
                            <input name="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="회원명,메일,회원ID" class="m-b form-control"> 
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-md btn-primary"> Go!</button> 
                            </span>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END search -->
        
        <div class="table-responsive">
            <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">이름</th>
                        <th>이메일</th>
                        <th>SNS등록</th>
                        <th>메일링</th>
                        <th>사용유무</th>
                        <th>최종로그인</th>
                        <th class="text-center">등록일</th>
                        <th class="text-center">편집</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 

                    $db_result = $common_dao->db_query_bind($sql);
                    if($db_result)
                    {
                        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
                        {
                            foreach($arr_db_field as $val)
                            {
                                $$val = $db_result[$db_loop][$val];
                            }
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $member_id; ?></td>
                        <td class="text-center"><?php echo $member_name; ?></td>
                        <td class="text-center"><?php echo $login_email; ?></td>
                        <td class="text-center">
                        <?php if($naver_id!="") { echo "naver"; } else if($facebook_id!="") { echo "facebook"; }?>
                        </td>
                        <td class="text-center"><span class="label <?php echo $arr_flag_email_color[$flag_email] ?>"><?php echo $arr_flag_email[$flag_email]; ?></span></td>
                        <td class="text-center"><span class="label <?php echo $arr_member_flag_open_color[$flag_open] ?>"><?php echo $arr_member_flag_open[$flag_open]; ?></span></td>
                        <td class="text-center"><?php echo $lastlogin_date; ?></td>
                        <td class="text-center"><?php echo $regi_date; ?></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <? /*<a href="./member_regist.php?member_id=<? echo $member_id;?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>*/ ?>
                                <a href="#" onClick='fnChangeSel("<?php echo $member_id;?>");' data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                            </div>
                        </td>
                    </tr>
                    <?php

                        }
                    }
                     ?>
                </tbody>
            </table>
        </div>

        <!-- paging -->
        <?php $common_connect -> Fn_paging_10_list($view_count, $all_count); ?>
        <!-- END paging -->
    </div>
    <!-- END 商品리스트 Content -->
</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/page_footer.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_scripts.php'; ?>


<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_end.php'; ?>