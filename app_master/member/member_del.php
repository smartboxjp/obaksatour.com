<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー削除</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php   
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
    //管理者チェック
    $common_connect -> Fn_admin_check();
    
    if($member_id=="")
    {
        $common_connect -> Fn_javascript_back("正しく選択してください。");
    }
    
    //削除処理
    $dbup = "delete from member where member_id='".$member_id."' ";
    $common_dao->db_update($dbup);

    
    $common_connect-> Fn_javascript_move("ユーザーを削除しました", "member_list.php");
?>
</body>
</html>