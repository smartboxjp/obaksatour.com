<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_admin_check();

    $page_pre_title = "등록";
    if($notice_id!="")
    {
        $arr_db_field = array("notice_id", "cate_notice_id", "view_date", "notice_title", "notice_comment");
        $arr_db_field = array_merge($arr_db_field, array("flag_top", "regi_date", "up_date"));

        $sql = "SELECT ";
        foreach($arr_db_field as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " 1 FROM notice where notice_id='".$notice_id."' ";

        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
            {
                foreach($arr_db_field as $val)
                {
                    $$val = $db_result[$db_loop][$val];
                }
            }
        }

        $page_pre_title = "수정";
    }

    $arr_cate_notice = array();
    $sql = " SELECT cate_notice_id, cate_notice_title FROM cate_notice order by view_level" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_notice[$db_result[$db_loop]["cate_notice_id"]] = $db_result[$db_loop]["cate_notice_title"];
        }
    }
?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/config.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_start.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/page_head.php'; ?>

<link href="/app_master/css/plugins/summernote/summernote.min.css" rel="stylesheet">
<script src="/app_master/js/vendor/jquery.min.js"></script>
<script type="text/javascript">
    $( function () {

        $( '#form_confirm' ).click( function () {
            $( "#notice_comment" ).val( $( ".note-editable" ).html() );
            err_default = "";
            err_check_count = 0;
            bgcolor_default = "#FFFFFF";
            bgcolor_err = "#FFCCCC";
            background = "background-color";

            err_check_count += check_input( "notice_title" );

            if ( err_check_count != 0 ) {
                alert( "入力に不備があります" );
                return false;
            } else {
                $( '#form_confirm' ).submit();
                //$( '#form_confirm', "body" ).submit();
                return true;
            }
        } );

        function check_input( $str ) {
            $( "#err_" + $str ).html( err_default );
            $( "#" + $str ).css( background, bgcolor_default );

            if ( $( '#' + $str ).val() == "" ) {
                err = "<span class='errorForm'>正しく入力してください。</span>";
                $( "#err_" + $str ).html( err );
                $( "#" + $str ).css( background, bgcolor_err );

                return 1;
            }
            return 0;
        }


    } );

    function fnChangeSel(j) { 
        var result = confirm('削除しますか？'); 
        if(result){ 
            document.location.href = './notice_del.php?notice_id='+j;
        } 
    }
    //-->
</script>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-brush"></i>공지사항관리</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/app_master/dashboard/">Dashboard</a></li>
        <li>공지사항</li>
    </ul>
    <!-- END Blank Header -->

    <!-- Example Block -->
    <div class="block">
        <!-- Example Title -->
        <div class="block-title">
            <h2>공지사항</h2>
        </div>
        <!-- END Example Title -->


<!-- //_/_/_/_/_/_/_ START _/_/_/_/_/_/_// -->
        <form action="./notice_save.php" method="POST" name="form_write" id="form_regist" class="form-horizontal form-bordered">
        <div class="ibox-content">

            <div class="form-group">
                <label class="col-sm-2 control-label">관리ID</label>
                <div class="col-sm-6">
                    <p class="form-control-static">
                        <? $var = "notice_id";?>
                        <?php
                        if($$var=="")
                        {
                        echo "自動生成";
                        }
                        ?>
                        <input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
                        <label id="err_<?php echo $var;?>"><? if($$var!=""){ echo $$var ."(변경안됨)";} ?></label>
                    </p>
                </div>
            </div>

            <div class="form-group">
                <? $var = "cate_notice_id";?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">공지사항분류</label>
                <div class="col-sm-6">
                    <select name="<?php echo $var;?>" id="<?php echo $var;?>" class="form-control" >
                    <option value="">------</option>
                    <?
                    foreach($arr_cate_notice as $key=>$value)
                    {
                    ?>
                    <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
                    <?
                    }
                    ?>
                    </select>
                    <span id="err_<?php echo $var;?>"></span>
                    <a href="/app_master/cate_notice/cate_notice_list.php">해당분류가 없는경우 클릭해서 추가 후 작성해주세요</a>
                </div>
            </div>
            
            <div class="form-group">
                <? $var = "notice_title";?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">타이틀</label>
                <div class="col-sm-6">
                    <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" type="text" class="form-control count-control" maxlength="100" placeholder="タイトルを最大100文字以内で入力して下さい。"> 
                    <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>

            <div class="form-group">
                <? $var = "view_date";?>
                <? if($$var=="") { $$var = date("Y-m-d");}?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">표시일</label>
                <div class="col-sm-6 input-group input-daterange" data-date-format="yyyy/mm/dd">
                    <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" type="text" class="form-control"> 
                    <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>

            <div class="form-group">
                <? $var = "flag_top";?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">선두표시</label>
                <div class="col-sm-6">
                    <label>
                    <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="1" type="checkbox" class="form-control count-control" <?php if($$var=="1") { echo "checked";}?>> 선두표시할 경우체크
                    </label>
                    <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>

            <style type="text/css">
                .note-editable.panel-body > p{
                    margin-bottom: 0px;
                }
            </style>

            <div class="form-group">
                <? $var = "notice_comment";?>
                <label class="col-sm-1 control-label" for="<?php echo $var;?>"></label>
                <div class="col-sm-10">
                    <textarea name="<? echo $var;?>" id="<? echo $var;?>" class="summernote"><?php echo htmlspecialchars_decode($$var);?></textarea>
                </div>
            </div>


<script type="text/javascript">
  $( function () {
    $( '.summernote' ).summernote( {
      height: 500,
      disableDragAndDrop: true,
      toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video', 'hr']],
            ['view', ['fullscreen', 'codeview']],
            ['insert', ['template']],
            ['help', ['help']]
            ],
      defaultFontName: 'Meiryo',
      fontNames: ["YuGothic","Yu Gothic","Hiragino Kaku Gothic Pro","sans-serif", "Arial","Arial Black","Comic Sans MS","Courier New","Helvetica Neue","Helvetica","Impact","Lucida Grande","Tahoma","Times New Roman","Verdana"],
      fontSizes: ['11', '12', '13', '16', '18', '20', '24', '28'],
      template: {
          path: './template', // path to your template folder
          list: [ // list of your template (without the .html extension)
              'template1',
              'template2'
          ]
      }
    } );

    /*
$('form').on('submit', function (e) {
e.preventDefault();
//     alert($('.summernote').summernote('code'));
//     alert($('.summernote').val());
});
*/
  } );

</script>

            <!--ボタン-->
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                <? $var = "form_confirm";?>
                    <button class="btn btn-danger" type="submit" id="<? echo $var;?>" style="width: 100%; height: 50px; font-size: 200%;"><? echo $page_pre_title;?></button>
                </div>
            </div>
            <!--ボタン-->


            
        </div>
        </form>


    </div>
    <!-- END Example Block -->



    <!-- 商品リスト Content -->
    <div class="block full" id="search_list">
        <div class="block-title">
            <h2><strong>공지사항 리스트</strong></h2>
        </div>
        <p>등록된 내용은 수정 삭제가 가능합니다.<br />정렬은 선두표시체크, 표시일 내림차순  </p>
<?php
    
    $view_count=50;   // List count
    $offset=0;

    if(!$page)
    {
        $page=1;
    }
    Else
    {
        $offset=$view_count*($page-1);
    }

    $where = "";
    if($s_keyword!="")
    {
        $where .= " and (notice_title collate utf8_unicode_ci like '%".$s_keyword."%' or notice_comment collate utf8_unicode_ci like '%".$s_keyword."%' or notice_id = '".$s_keyword."' ) ";
    }

    //合計
    $all_count = 0;
    $sql = "SELECT count(notice_id) as all_count FROM notice where 1 ".$where ;
    $arr_goods_all = $common_dao->db_query_bind($sql);
    $all_count = $arr_goods_all[0]["all_count"];


    $arr_db_field = array("notice_id", "cate_notice_id", "view_date", "notice_title", "notice_comment");
    $arr_db_field = array_merge($arr_db_field, array("flag_top", "regi_date", "up_date"));

    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM notice where 1 ".$where;
    if($order_name != "")
    {
        $sql .= " order by ".$order_name." ".$order;
    }
    else
    {
        $sql .= " order by flag_top, view_date desc";
    }
    $sql .= " limit $offset,$view_count";
?>
        <!-- search -->
        <div class="table-responsive">
            <div class="dataTables_wrapper">
                <div class="row">
                    <div class="col-sm-3 col-xs-5">
                        <label>전체：<? echo $all_count;?>건</label>
                    </div>
                    <div class="col-sm-3">
                    </div>

                    <form action="<?php echo $_SERVER['PHP_SELF']?>#search_list" method="get" name="form_shop_search">
                    <div class="col-sm-3">
                        <? $var = "s_cate_notice_id";?>
                        <select name="<?php echo $var;?>" id="<?php echo $var;?>" class="form-control" >
                        <option value="">분류<option>
                        <?
                        foreach($arr_cate_notice as $key=>$value)
                        {
                        ?>
                        <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
                        <?
                        }
                        ?>
                        </select>
                    </div>

                    <div class="col-sm-3">
                        <div class="input-group">

                            <? $var = "s_keyword";?>
                            <input name="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="타이틀,타이틀ID" class="m-b form-control"> 
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-md btn-primary"> Go!</button> 
                            </span>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END search -->

        <div class="table-responsive">
            <table class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th>분류</th>
                        <th>타이틀</th>
                        <th class="text-center">선두표시유무</th>
                        <th class="text-center">표시일</th>
                        <th class="text-center">수정일</th>
                        <th class="text-center">편집</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 

                    $db_result = $common_dao->db_query_bind($sql);
                    if($db_result)
                    {
                        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
                        {
                            foreach($arr_db_field as $val)
                            {
                                $$val = $db_result[$db_loop][$val];
                            }
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $notice_id; ?></td>
                        <td><?php echo $arr_cate_notice[$cate_notice_id]; ?></td>
                        <td><?php echo $notice_title; ?></td>
                        <td class="text-center"><?php if($flag_top==1){ echo "선두에 표시함";} ?></td>
                        <td class="text-center"><?php echo $view_date; ?></td>
                        <td class="text-center"><?php echo $up_date; ?></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="./notice_list.php?notice_id=<? echo $notice_id;?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                <a href="#" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger" onClick='fnChangeSel("<?php echo $notice_id;?>");'><i class="fa fa-times"></i></a>

                            </div>
                        </td>
                    </tr>
                    <?php

                        }
                    }
                     ?>
                </tbody>
            </table>
        </div>

        <!-- paging -->
        <?php $common_connect -> Fn_paging_10_list($view_count, $all_count); ?>
        <!-- END paging -->
    </div>
    <!-- END 商品リスト Content -->

<!-- //_/_/_/_/_/_/_ END _/_/_/_/_/_/_// -->

</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/page_footer.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_scripts.php'; ?>


<!-- SUMMERNOTE -->
<script src="/app_master/js/plugins/summernote/summernote.js"></script>
<script src="/app_master/js/plugins/summernote/summernote-ext-template.js"></script>

<script src="/app_master/js/plugins/count/bootstrap-maxlength.js"></script>


<script>
$(document).ready(function () {
  $('.count-control').maxlength({
    alwaysShow: true,
    warningClass: "label label-success",
    limitReachedClass: "label label-danger"
  });
});
</script>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_end.php'; ?>