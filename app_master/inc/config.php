<?php
/**
 * config.php
 *
 * Author: 合同加盟店smartbox
 *
 * Configuration file. It contains variables used in the template as well as the primary navigation array from which the navigation is created
 *
 */

/* Template variables */
$template = array(
    'name'              => 'OBAKSA TOUR',
    'version'           => '1',
    'author'            => '合同加盟店smartbox',
    'robots'            => 'noindex, nofollow',
    'title'             => '沖縄旅行プラットフォーム OKINAWA OBAKSA',
    'description'       => 'OBAKSA TOURは合同加盟店smartboxが提供する沖縄の旅行プラットフォームです。',
    // true                     enable page preloader
    // false                    disable page preloader
    'page_preloader'    => false,
    // true                     enable main menu auto scrolling when opening a submenu
    // false                    disable main menu auto scrolling when opening a submenu
    'menu_scroll'       => true,
    // 'navbar-default'         for a light header
    // 'navbar-inverse'         for a dark header
    'header_navbar'     => 'navbar-default',
    // ''                       empty for a static layout
    // 'navbar-fixed-top'       for a top fixed header / fixed sidebars
    // 'navbar-fixed-bottom'    for a bottom fixed header / fixed sidebars
    'header'            => '',
    // ''                                               for a full main and alternative sidebar hidden by default (> 991px)
    // 'sidebar-visible-lg'                             for a full main sidebar visible by default (> 991px)
    // 'sidebar-partial'                                for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
    // 'sidebar-partial sidebar-visible-lg'             for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
    // 'sidebar-mini sidebar-visible-lg-mini'           for a mini main sidebar with a flyout menu, enabled by default (> 991px + Best with static layout)
    // 'sidebar-mini sidebar-visible-lg'                for a mini main sidebar with a flyout menu, disabled by default (> 991px + Best with static layout)
    // 'sidebar-alt-visible-lg'                         for a full alternative sidebar visible by default (> 991px)
    // 'sidebar-alt-partial'                            for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
    // 'sidebar-alt-partial sidebar-alt-visible-lg'     for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)
    // 'sidebar-partial sidebar-alt-partial'            for both sidebars partial which open on mouse hover, hidden by default (> 991px)
    // 'sidebar-no-animations'                          add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!
    'sidebar'           => 'sidebar-partial sidebar-visible-lg sidebar-no-animations',
    // ''                       empty for a static footer
    // 'footer-fixed'           for a fixed footer
    'footer'            => '',
    // ''                       empty for default style
    // 'style-alt'              for an alternative main style (affects main page background as well as blocks style)
    'main_style'        => '',
    // ''                           Disable cookies (best for setting an active color theme from the next variable)
    // 'enable-cookies'             Enables cookies for remembering active color theme when changed from the sidebar links (the next color theme variable will be ignored)
    'cookies'           => '',
    // 'night', 'amethyst', 'modern', 'autumn', 'flatie', 'spring', 'fancy', 'fire', 'coral', 'lake',
    // 'forest', 'waterlily', 'emerald', 'blackberry' or '' leave empty for the Default Blue theme
    'theme'             => 'lake',
    // ''                       for default content in header
    // 'horizontal-menu'        for a horizontal menu in header
    // This option is just used for feature demostration and you can remove it if you like. You can keep or alter header's content in page_head.php
    'header_content'    => '',
    'active_page'       => basename($_SERVER['PHP_SELF'])
);

/* Primary navigation array (the primary navigation will be created automatically based on this array, up to 3 levels deep) */
$primary_nav = array(
    array(
        'name'  => 'Dashboard',
        'url'   => '/app_master/dashboard/',
        'icon'  => 'gi gi-stopwatch'
    ),
    array(
        'name'  => '注文情報',
        'icon'  => 'gi gi-shopping_cart',
        'sub'   => array(
            array(
                'name'  => '注文リスト',
                'url'   => '/app_master/goods/goods_list.php'
            ),
            array(
                'name'  => 'Q＆A',
                'url'   => 'page_ecom_orders.php'
            ),
            array(
                'name'  => 'Review',
                'url'   => 'page_ecom_order_view.php'
            )
        )
    ),
    array(
        'name'  => '加盟店管理',
        'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>' .
                   '<a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a>',
        'url'   => 'header',
    ),
    array(
        'name'  => '加盟店登録',
        'url'   => '/app_master/company/company_regist.php',
        'icon'  => 'gi gi-plus'
    ),
    array(
        'name'  => '加盟店リスト',
        'url'   => '/app_master/company/company_list.php',
        'icon'  => 'fa fa-leaf'
    ),
    array(
        'name'  => '商品管理',
        'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>' .
                   '<a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a>',
        'url'   => 'header',
    ),
    array(
        'name'  => '商品登録',
        'url'   => '/app_master/goods/goods_regist.php',
        'icon'  => 'gi gi-plus'
    ),
    array(
        'name'  => '商品リスト',
        'url'   => '/app_master/goods/goods_list.php',
        'icon'  => 'fa fa-bars'
    ),
    array(
        'name'  => '회원관리',
        'url'   => '/app_master/member/member_list.php',
        'icon'  => 'fa fa-user'
    ),
    array(
        'name'  => '공지사항',
        'url'   => '/app_master/notice/notice_list.php',
        'icon'  => 'fa fa-bell'
    ),
    array(
        'name'  => '자주하는 질문',
        'url'   => '/app_master/faq/faq_list.php',
        'icon'  => 'fa fa-tasks'
    ),
    array(
        'name'  => 'テンプレート',
        'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>',
        'url'   => 'header'
    ),
    array(
        'name'  => '商品詳細',
        'icon'  => 'gi gi-brush',
        'url'   => '/app_master/temp_goods_detail/temp_goods_detail_list.php'
    ),
    array(
        'name'  => '商品写真',
        'icon'  => 'fa fa-file-image-o',
        'url'   => '/app_master/temp_goods_detail/goods_detail.php'
    ),
    array(
        'name'  => '在庫',
        'icon'  => 'fa fa-wrench',
        'url'   => '/app_master/temp_goods_detail/goods_detail.php'
    ),
    array(
        'name'  => 'Widget Kit',
        'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>' .
                   '<a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a>',
        'url'   => 'header',
    ),
    array(
        'name'  => 'Statistics',
        'url'   => 'page_widgets_stats.php',
        'icon'  => 'gi gi-charts'
    ),
    array(
        'name'  => 'Social',
        'url'   => 'page_widgets_social.php',
        'icon'  => 'gi gi-share_alt'
    ),
    array(
        'name'  => 'Media',
        'url'   => 'page_widgets_media.php',
        'icon'  => 'gi gi-film'
    ),
    array(
        'name'  => 'Links',
        'url'   => 'page_widgets_links.php',
        'icon'  => 'gi gi-link'
    ),
    array(
        'name'  => 'コンテンツ管理',
        'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>',
        'url'   => 'header'
    ),
    array(
        'name'  => 'カテゴリ',
        'url'   => '/app_master/cate_contents/cate_contents_list.php',
        'icon'  => 'gi gi-link'
    ),
    array(
        'name'  => 'コンテンツ',
        'url'   => '/app_master/contents/contents_list.php',
        'icon'  => 'fa fa-bars'
    ),
    array(
        'name'  => 'Design Kit',
        'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>',
        'url'   => 'header'
    ),
    array(
        'name'  => 'User Interface',
        'icon'  => 'gi gi-certificate',
        'sub'   => array(
            array(
                'name'  => 'Grid &amp; Blocks',
                'url'   => 'page_ui_grid_blocks.php'
            ),
            array(
                'name'  => 'Draggable Blocks',
                'url'   => 'page_ui_draggable_blocks.php'
            ),
            array(
                'name'  => 'Typography',
                'url'   => 'page_ui_typography.php'
            ),
            array(
                'name'  => 'Buttons &amp; Dropdowns',
                'url'   => 'page_ui_buttons_dropdowns.php'
            ),
            array(
                'name'  => 'Navigation &amp; More',
                'url'   => 'page_ui_navigation_more.php'
            ),
            array(
                'name'  => 'Horizontal Menu',
                'url'   => 'page_ui_horizontal_menu.php'
            ),
            array(
                'name'  => 'Progress &amp; Loading',
                'url'   => 'page_ui_progress_loading.php'
            ),
            array(
                'name'  => 'Page Preloader',
                'url'   => 'page_ui_preloader.php'
            ),
            array(
                'name'  => 'Color Themes',
                'url'   => 'page_ui_color_themes.php'
            )
        )
    ),
    array(
        'name'  => 'Forms',
        'icon'  => 'gi gi-notes_2',
        'sub'   => array(
            array(
                'name'  => 'General',
                'url'   => 'page_forms_general.php'
            ),
            array(
                'name'  => 'Components',
                'url'   => 'page_forms_components.php'
            ),
            array(
                'name'  => 'Validation',
                'url'   => 'page_forms_validation.php'
            ),
            array(
                'name'  => 'Wizard',
                'url'   => 'page_forms_wizard.php'
            )
        )
    ),
    array(
        'name'  => 'Tables',
        'icon'  => 'gi gi-table',
        'sub'   => array(
            array(
                'name'  => 'General',
                'url'   => 'page_tables_general.php'
            ),
            array(
                'name'  => 'Responsive',
                'url'   => 'page_tables_responsive.php'
            ),
            array(
                'name'  => 'Datatables',
                'url'   => 'page_tables_datatables.php'
            )
        )
    ),
    array(
        'name'  => 'Icon Sets',
        'icon'  => 'gi gi-cup',
        'sub'   => array(
            array(
                'name'  => 'Font Awesome',
                'url'   => 'page_icons_fontawesome.php'
            ),
            array(
                'name'  => 'Glyphicons Pro',
                'url'   => 'page_icons_glyphicons_pro.php'
            )
        )
    ),
    array(
        'name'  => 'Page Layouts',
        'icon'  => 'gi gi-show_big_thumbnails',
        'sub'   => array(
            array(
                'name'  => 'Static',
                'url'   => 'page_layout_static.php'
            ),
            array(
                'name'  => 'Static + Fixed Footer',
                'url'   => 'page_layout_static_fixed_footer.php'
            ),
            array(
                'name'  => 'Fixed Top Header',
                'url'   => 'page_layout_fixed_top.php'
            ),
            array(
                'name'  => 'Fixed Top Header + Footer',
                'url'   => 'page_layout_fixed_top_footer.php'
            ),
            array(
                'name'  => 'Fixed Bottom Header',
                'url'   => 'page_layout_fixed_bottom.php'
            ),
            array(
                'name'  => 'Fixed Bottom Header + Footer',
                'url'   => 'page_layout_fixed_bottom_footer.php'
            ),
            array(
                'name'  => 'Mini Main Sidebar',
                'url'   => 'page_layout_static_main_sidebar_mini.php'
            ),
            array(
                'name'  => 'Partial Main Sidebar',
                'url'   => 'page_layout_static_main_sidebar_partial.php'
            ),
            array(
                'name'  => 'Visible Main Sidebar',
                'url'   => 'page_layout_static_main_sidebar_visible.php'
            ),
            array(
                'name'  => 'Partial Alternative Sidebar',
                'url'   => 'page_layout_static_alternative_sidebar_partial.php'
            ),
            array(
                'name'  => 'Visible Alternative Sidebar',
                'url'   => 'page_layout_static_alternative_sidebar_visible.php'
            ),
            array(
                'name'  => 'No Sidebars',
                'url'   => 'page_layout_static_no_sidebars.php'
            ),
            array(
                'name'  => 'Both Sidebars Partial',
                'url'   => 'page_layout_static_both_partial.php'
            ),
            array(
                'name'  => 'Animated Sidebar Transitions',
                'url'   => 'page_layout_static_animated.php'
            )
        )
    )
);