<?
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";

    //管理者チェック
    if ($_SESSION['admin_id']=="" && $_SESSION['shop_id']=="")
    {
        Fn_javascript_move("管理者専用ページです", "/management/");
    }
    
    $googlemap_1 = $_GET["googlemap_1"];
    $googlemap_2 = $_GET["googlemap_2"];
    $kubun = $_GET["kubun"];
    $address_txt = $_GET["address_txt"];
    
    if ($googlemap_1=="") { $googlemap_1 = "26.212353";}
    if ($googlemap_2=="") { $googlemap_2 = "127.680777";}
    
    if($address_txt!="")
    {
        $arr_data = get_gps_from_address($address_txt);
        $googlemap_1 = $arr_data["lat"];
        $googlemap_2 = $arr_data["lng"];
    }
    
?>
<!DOCTYPE html>

<html lang="ja-JP">
<head>
<meta charset="UTF-8">
<title>Google Map</title>
    <link rel="stylesheet" type="text/css" href="/_css/base.css">
    <style type="text/css"><!--
        .tbl    { background-color:#cccccc; }
        .tbl_t  { background-color:#7FA47F; color:#ffffff; }
        .td_t   { background-color:#dbe8db; vertical-align:middle; font-weight:bold;}
        .td_t2  { background-color:#dbe8db; vertical-align:middle; }
        .td_d   { background-color:#ffffff; }
    --></style>

    <script src="//maps.google.com/maps/api/js?key=<? echo googlemap_Key;?>&v=3&sensor=false" type="text/javascript" charset="UTF-8"></script>

    <script type="text/javascript">
    //<![CDATA[

    
        var map;
        var googlemap_1    = <?=$googlemap_1;?>;
        var googlemap_2    = <?=$googlemap_2;?>;
        
        // 初期化。bodyのonloadでinit()を指定することで呼び出してます
        function init() {
            // Google Mapで利用する初期設定用の変数
            var latlng = new google.maps.LatLng(googlemap_1, googlemap_2);
            var opts = {
                zoom: 17,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: latlng
            };
            
            // getElementById("map")の"map"は、body内の<div id="map">より
            map = new google.maps.Map(document.getElementById("map"), opts);
    
            var markerOpts = {
            position: new google.maps.LatLng(googlemap_1, googlemap_2),
            map: map
            //,title: infoHtml
            };
            
            var marker = new google.maps.Marker(markerOpts);
            google.maps.event.addListener(map, 'click', mylistener);
            
        }

    
        function mylistener(event) {
            document.save.googlemap_1.value = jfncRoundDecimal(event.latLng.lat(), 6);
            document.save.googlemap_2.value = jfncRoundDecimal(event.latLng.lng(), 6);
            
        }

        /*** 座標を親画面にセット ***/
        function jfncSetXY() {
            if ( window.opener != window.self ) {
                window.opener.document.form_write.googlemap_1.value = document.save.googlemap_1.value;
                window.opener.document.form_write.googlemap_2.value = document.save.googlemap_2.value;
                window.close();
            }
        }

        /*** 少数点指定桁以下四捨五入 ***/
        function jfncRoundDecimal( val, decimal ) {
            var n = 1;
            for ( var i=0; i<decimal; i++ ){
                n *= 10;
            }

            val *= n;
            val =  Math.round( val );
            val /= n;

            return val;
        }
    //]]>
    </script>
</head>
<body onLoad="init();" onContextMenu="return false;" topmargin="0" leftmargin="0">

    <center>
        <div id="map" style="width:580px; height:450px; cursor:pointer;">
            <table id=tblloading height="100%" align=center>
                <tr>
                    <td id="loadingMsg">Now loading... </td>
                </tr>
            </table>
        </div>


    <form name="google_address" style="margin:0px;">
        <table style="width:300px;">
            <tr>
            <td>
          <input type="text" name="address_txt" id="address_txt" value="<? echo $address_txt;?>" style="width:200px;" class="text06">
          <button name="address_btn" id="address_btn">住所から検索</button>
          </td>
        </tr>
    </table>
    </form>
    
    <form name="save" style="margin:0px;">
        <table class="tbl" border="0" cellspacing="1" cellpadding="4">
            <colgroup>
                <col width="70">
                <col width="80">
                <col width="130">
            </colgroup>
            <tr>
                <th class="td_t text06" colspan="3" align="center" height="25" style="color:blue;">
                    <b>※地図上をクリックすると座標が取得できます</b>
                </th>
            </tr>
            <tr>
                <th class="td_t text06" nowrap>緯度（y）</th>
                <td class="td_d text06" nowrap><input type="text" name="googlemap_1" id="googlemap_1" value="<?=$googlemap_1;?>" style="text-align:right; width:100%;" class="text06"></td>
                <td class="td_d text06" nowrap><span id="latFull"></span></td>
            </tr>
            <tr>
                <th class="td_t text06" nowrap>経度（x）</th>
                <td class="td_d text06" nowrap><input type="text" name="googlemap_2" id="googlemap_2" value="<?=$googlemap_2;?>" style="text-align:right; width:100%;" class="text06">
                </td>
                <td class="td_d text06" nowrap><span id="lonFull"></span></td>
            </tr>
            <tr>
                <td class="td_t text06" colspan="3" align="center">
                    <input type="button" name="btnUpdate" value="確定" onClick="jfncSetXY();" style="width:70px; letter-spacing:1em;">　
                    <input type="button" name="btnUpdate" value="キャンセル" onClick="window.close();" style="width:70px">
                </td>
            </tr>
        </table>
    </form>

    </center>

<?
    function get_gps_from_address( $address='' ){
        $res = array();
        $req = 'http://maps.google.com/maps/api/geocode/xml';
        $req .= '?address='.urlencode($address);
        $req .= '&sensor=false';
    //$xml = curl_get_contents($req) or die('XML parsing error');
    
        $result = curl_get_contents($req , 120);
        $xml = simplexml_load_string($result);
        if ($xml->status == 'OK') {
            $location = $xml->result->geometry->location;
            $res['lat'] = (string)$location->lat[0];
            $res['lng'] = (string)$location->lng[0];
        }
        return $res;
    }

    function curl_get_contents( $url, $timeout = 60 ){
            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_HEADER, false );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_TIMEOUT, $timeout );
            $result = curl_exec( $ch );
            curl_close( $ch );
            return $result;
    }
?>
</body>
</html>