<?php
/**
 * template_scripts.php
 *
 * Author: 合同会社smartbox
 *
 * All vital JS scripts are included here
 *
 */
?>

<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
<script src="/app_master/js/vendor/jquery.min.js"></script>
<script src="/app_master/js/vendor/bootstrap.min.js"></script>
<script src="/app_master/js/plugins.js"></script>
<script src="/app_master/js/app.js"></script>
