<?php
	//error_reporting(0);
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	

	foreach($_POST as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}

    $common_connect -> Fn_admin_check();
	

	if($cate_contents_title == "")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
    
    $datetime = date("Y/m/d H:i:s");
    
    //array
    $arr_db_field = array("cate_contents_title", "cate_contents_comment", "cate_contents_folder");
    $arr_db_field = array_merge($arr_db_field, array("view_level"));
    

    if($cate_contents_id=="")
    {
        $db_insert = "insert into cate_contents ( ";
        $db_insert .= " cate_contents_id, ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val.", ";
        }
        $db_insert .= " up_date, regi_date ";
        $db_insert .= " ) values ( ";
        $db_insert .= " '', ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= " '".$$val."', ";
        }
        $db_insert .= " '$datetime', '$datetime')";
    }
    else
    {
        $db_insert = "update cate_contents set ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val."='".$$val."', ";
        }
        $db_insert .= " up_date='".$datetime."' ";
        $db_insert .= " where cate_contents_id='".$cate_contents_id."' ";
    }
    $common_dao -> db_update($db_insert);

    if ($cate_contents_id == "")
    {
        //自動生成されてるID出力
        $sql = "select last_insert_id() as last_id";  
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            $cate_contents_id = $common_connect -> $db_result[0]["last_id"];
        }
    }


	/* 画像処理 start */
	$save_dir_content = $global_path.global_cate_contents_dir.$cate_contents_id."/";
	$common_image -> create_folder ($save_dir_content);

	$dom = new domDocument;
	libxml_use_internal_errors(true);
	$dom->loadHTML(html_entity_decode($cate_contents_comment));
	$dom->preserveWhiteSpace = false;
	$imgs  = $dom->getElementsByTagName("img");
	$links = array();
	$files = glob($path . "*.*");
	for($i = 0; $i < $imgs->length; $i++) {
		 $links[] = $imgs->item($i)->getAttribute("src");
	}

    $arr_basename = array();
    foreach ($links as $key => $value) {
        $pathData = pathinfo($global_path.$value);
        $arr_basename[] = $pathData["basename"];

        if(file_exists($global_path.global_cate_contents_dir.$pathData["basename"])) {
            rename($global_path.global_cate_contents_dir.$pathData["basename"], $save_dir_content.$pathData["basename"]);
            $cate_contents_comment = str_replace(global_cate_contents_dir.$pathData["basename"], global_cate_contents_dir.$cate_contents_id."/".$pathData["basename"], $cate_contents_comment);
        }
    }

	$db_up = "update cate_contents set ";
    $db_up .= " cate_contents_comment='".$cate_contents_comment."', ";
    $db_up .= " up_date='".$datetime."' ";
    $db_up .= " where cate_contents_id='".$cate_contents_id."' ";
    $common_dao -> db_update($db_up);
	/* 画像処理 end */


	/* 登録されているファイル以外削除 start */
	$dir = opendir($global_path.global_cate_contents_dir.$cate_contents_id);
	while (false !== ($file = readdir($dir))){
		if($file[0] != "."){

			if(!in_array($file, $arr_basename))
			{
				unlink ($global_path.global_cate_contents_dir.$cate_contents_id."/".$file);
			}
		}
	}
	closedir($dir);
	/* 登録されているファイル以外削除 end */

	/* 古いファイル削除 start */
	$delete_day = strtotime("48 hours ago");

	$dir = opendir($global_path.global_cate_contents_dir.$cate_contents_id);
	while (false !== ($file = readdir($dir))){
		if($file[0] != "."){
			if ($delete_day > filemtime($global_path.global_cate_contents_dir.$cate_contents_id."/".$file)) {
				unlink($global_path.global_cate_contents_dir.$cate_contents_id."/".$file);
			}
		}
	}
	closedir($dir);
	/* 古いファイル削除 end */

	
	$common_connect-> Fn_javascript_move("登録・修正しました", "cate_contents_list.php?cate_contents_id=".$cate_contents_id);
?>
</body>
</html>