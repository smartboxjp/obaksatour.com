<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_admin_check();

    $page_pre_title = "登録";
    if($cate_contents_id!="")
    {
        $arr_db_field = array("cate_contents_title", "cate_contents_comment", "cate_contents_folder");
        $arr_db_field = array_merge($arr_db_field, array("view_level", "regi_date", "up_date"));

        $sql = "SELECT ";
        foreach($arr_db_field as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " 1 FROM cate_contents where cate_contents_id='".$cate_contents_id."' ";

        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
            {
                foreach($arr_db_field as $val)
                {
                    $$val = $db_result[$db_loop][$val];
                }
            }
        }

        $page_pre_title = "수정";
    }
?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/config.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_start.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/page_head.php'; ?>

<link href="/app_master/css/plugins/summernote/summernote.min.css" rel="stylesheet">
<script src="/app_master/js/vendor/jquery.min.js"></script>
<script type="text/javascript">
    $( function () {

        $( '#form_confirm' ).click( function () {
            $( "#cate_contents_comment" ).val( $( ".note-editable" ).html() );
            err_default = "";
            err_check_count = 0;
            bgcolor_default = "#FFFFFF";
            bgcolor_err = "#FFCCCC";
            background = "background-color";

            err_check_count += check_input( "cate_contents_title" );

            if ( err_check_count != 0 ) {
                alert( "入力に不備があります" );
                return false;
            } else {
                $( '#form_confirm' ).submit();
                //$( '#form_confirm', "body" ).submit();
                return true;
            }
        } );

        function check_input( $str ) {
            $( "#err_" + $str ).html( err_default );
            $( "#" + $str ).css( background, bgcolor_default );

            if ( $( '#' + $str ).val() == "" ) {
                err = "<span class='errorForm'>正しく入力してください。</span>";
                $( "#err_" + $str ).html( err );
                $( "#" + $str ).css( background, bgcolor_err );

                return 1;
            }
            return 0;
        }


    } );

    function fnChangeSel(j) { 
        var result = confirm('削除しますか？'); 
        if(result){ 
            document.location.href = './cate_contents_del.php?cate_contents_id='+j;
        } 
    }
    //-->
</script>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-brush"></i>テンプレート - 詳細情報<br><small>商品の詳細情報をテンプレートとして管理が出来ます。</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/app_master/dashboard/">Dashboard</a></li>
        <li>詳細情報のテンプレート</li>
    </ul>
    <!-- END Blank Header -->

    <!-- Example Block -->
    <div class="block">
        <!-- Example Title -->
        <div class="block-title">
            <h2>詳細情報のテンプレート</h2>
        </div>
        <!-- END Example Title -->


<!-- //_/_/_/_/_/_/_ START _/_/_/_/_/_/_// -->
        <form action="./cate_contents_save.php" method="POST" name="form_write" id="form_regist" class="form-horizontal form-bordered">
        <div class="ibox-content">

            <div class="form-group">
                <label class="col-sm-2 control-label">管理ID</label>
                <div class="col-sm-6">
                    <p class="form-control-static">
                        <? $var = "cate_contents_id";?>
                        <?php
                        if($$var=="")
                        {
                        echo "自動生成";
                        }
                        ?>
                        <input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
                        <label id="err_<?php echo $var;?>"><? if($$var!=""){ echo $$var ."（変更出来ません）";} ?></label>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <? $var = "cate_contents_title";?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">タイトル</label>
                <div class="col-sm-6">
                    <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control count-control" maxlength="50" placeholder="タイトルを最大50文字以内で入力して下さい。"> 
                    <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>

            <div class="form-group">
                <? $var = "cate_contents_folder";?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">フォルダー名</label>
                <div class="col-sm-6">
                    <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control count-control" maxlength="50" placeholder="タイトルを最大50文字以内で入力して下さい。"> 
                    <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>
            

            <div class="form-group">
                <? $var = "view_level";?>
                <?
                if($view_level=="")
                {
                    $sql = "select max(cate_contents_id) as view_level from cate_contents ";
                    $db_result = $common_dao->db_query_bind($sql);
                    $view_level = $db_result[0]["view_level"]+1;
                }
                ?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">表示順位</label>
                <div class="col-sm-6">
                    <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="number" class="form-control count-control" maxlength="5" placeholder="数字のみ入力してください。"> 
                    <span id="err_<?php echo $var;?>"></span>
                    <span class="help-block m-b-none">低い順番から表示されます。</span>
                </div>
            </div>

            <div class="form-group">
                <? $var = "cate_contents_comment";?>
                <label class="col-sm-1 control-label" for="<?php echo $var;?>"></label>
                <div class="col-sm-10">
                	<textarea name="<? echo $var;?>" id="<? echo $var;?>" class="summernote"><?php echo htmlspecialchars_decode($$var);?></textarea>
                </div>
            </div>


<script type="text/javascript">
  $( function () {
    $( '.summernote' ).summernote( {
      height: 500,
      disableDragAndDrop: true,
      toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video', 'hr']],
            ['view', ['fullscreen', 'codeview']],
            ['insert', ['template']],
            ['help', ['help']]
            ],
      defaultFontName: 'Meiryo',
      fontNames: ["YuGothic","Yu Gothic","Hiragino Kaku Gothic Pro","sans-serif", "Arial","Arial Black","Comic Sans MS","Courier New","Helvetica Neue","Helvetica","Impact","Lucida Grande","Tahoma","Times New Roman","Verdana"],
      fontSizes: ['11', '12', '13', '16', '18', '20', '24', '28'],
      template: {
          path: './template', // path to your template folder
          list: [ // list of your template (without the .html extension)
              'template1',
              'template2'
          ]
      },
      callbacks: {
        onImageUpload: function ( files, editor, welEditable ) {
          console.log( 'image upload:', files );
          $.each( files, function ( i, val ) {
            sendFile( files[ i ], editor, welEditable );
          } );
        },
      }
    } );

    function sendFile( file, editor, welEditable ) {
      data = new FormData();
      data.append( "file", file );
      $.ajax( {
        url: "saveimage.php", // image 저장 소스
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function ( data ) {
          //alert(data);
          var image = $( '<img>' ).attr( 'src', '' + data ); // Editorにtagとして保存
          $( '.summernote' ).summernote( "insertNode", image[ 0 ] ); // summernote 에디터에 img 태그를 보여줌
                 editor.insertImage(welEditable, data);
        },
        error: function ( jqXHR, textStatus, errorThrown ) {
          console.log( textStatus + " " + errorThrown );
        }
      } );
    }
    /*
$('form').on('submit', function (e) {
e.preventDefault();
//     alert($('.summernote').summernote('code'));
//     alert($('.summernote').val());
});
*/
  } );

</script>

            <!--ボタン-->
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                <? $var = "form_confirm";?>
                    <button class="btn btn-danger" type="submit" id="<? echo $var;?>" style="width: 100%; height: 50px; font-size: 200%;"><? echo $page_pre_title;?></button>
                </div>
            </div>
            <!--ボタン-->

            
        </div>
        </form>


    </div>
    <!-- END Example Block -->



    <!-- 商品リスト Content -->
    <div class="block full" id="search_list">
        <div class="block-title">
            <h2><strong>컨텐츠 카테고리</strong></h2>
        </div>
        <p>등록된 내용은 수정 삭제가 가능합니다.<br />정렬은 표시순서 오름차순, 등록일내리차순  </p>
<?php
    
    $view_count=20;   // List count
    $offset=0;

    if(!$page)
    {
        $page=1;
    }
    Else
    {
        $offset=$view_count*($page-1);
    }

    $where = "";
    if($s_keyword!="")
    {
        $where .= " and (cate_contents_title collate utf8_unicode_ci like '%".$s_keyword."%' or cate_contents_comment collate utf8_unicode_ci like '%".$s_keyword."%' or cate_contents_id = '".$s_keyword."' ) ";
    }

    //合計
    $all_count = 0;
    $sql = "SELECT count(cate_contents_id) as all_count FROM cate_contents where 1 ".$where ;
    $arr_goods_all = $common_dao->db_query_bind($sql);
    $all_count = $arr_goods_all[0]["all_count"];

    $arr_db_field = array("cate_contents_id", "cate_contents_title", "cate_contents_comment", "cate_contents_folder");
    $arr_db_field = array_merge($arr_db_field, array("view_level", "regi_date", "up_date"));

    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM cate_contents where 1 ".$where;
    if($order_name != "")
    {
        $sql .= " order by ".$order_name." ".$order;
    }
    else
    {
        $sql .= " order by view_level, regi_date desc";
    }
    $sql .= " limit $offset,$view_count";
?>
        <!-- search -->
        <div class="table-responsive">
            <div class="dataTables_wrapper">
                <div class="row">
                    <div class="col-sm-3 col-xs-5">
                        <label>전체：<? echo $all_count;?>건</label>
                    </div>
                    <div class="col-sm-3">
                    </div>

                    <form action="<?php echo $_SERVER['PHP_SELF']?>#search_list" method="get" name="form_shop_search">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">

                            <? $var = "s_keyword";?>
                            <input name="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="카테고리명,카테고리ID" class="m-b form-control"> 
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-md btn-primary"> Go!</button> 
                            </span>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END search -->

        <div class="table-responsive">
            <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th>タイトル</th>
                        <th class="text-center">表示順位</th>
                        <th class="text-center">修正日</th>
                        <th class="text-center">編集</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 

                    $db_result = $common_dao->db_query_bind($sql);
                    if($db_result)
                    {
                        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
                        {
                            foreach($arr_db_field as $val)
                            {
                                $$val = $db_result[$db_loop][$val];
                            }
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $cate_contents_id; ?></td>
                        <td><?php echo $cate_contents_title; ?></td>
                        <td class="text-center"><?php echo $view_level; ?></td>
                        <td class="text-center"><?php echo $up_date; ?></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="./cate_contents_list.php?cate_contents_id=<? echo $cate_contents_id;?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                <a href="#" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger" onClick='fnChangeSel("<?php echo $cate_contents_id;?>");'><i class="fa fa-times"></i></a>

                            </div>
                        </td>
                    </tr>
                    <?php

                        }
                    }
                     ?>
                </tbody>
            </table>
        </div>
        <!-- paging -->
        <?php $common_connect -> Fn_paging_10_list($view_count, $all_count); ?>
        <!-- END paging -->
    </div>
    <!-- END 商品リスト Content -->

<!-- //_/_/_/_/_/_/_ END _/_/_/_/_/_/_// -->

</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/page_footer.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_scripts.php'; ?>


<!-- SUMMERNOTE -->
<script src="/app_master/js/plugins/summernote/summernote.js"></script>
<script src="/app_master/js/plugins/summernote/summernote-ext-template.js"></script>

<script src="/app_master/js/plugins/count/bootstrap-maxlength.js"></script>


<script>
$(document).ready(function () {
  $('.count-control').maxlength({
    alwaysShow: true,
    warningClass: "label label-success",
    limitReachedClass: "label label-danger"
  });
});
</script>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_end.php'; ?>