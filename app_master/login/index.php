<?
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonAdmin.php";
    $common_admin = new CommonAdmin();
    
    //ログイン
    if($_COOKIE['account'] != '')
    {
        $arr_data = array();
        $arr_data[] = "admin_name";
        $arr_data[] = "admin_id";
        $arr_data[] = "flag_open";
        
        $arr_where = array();
        $arr_where["flag_open"] = "all";
        $login_cookie = $_COOKIE['account'];
        $db_result = $common_admin->Fn_db_admin_login_cookie ($login_cookie, $arr_data, $arr_where=null);
        
        if($db_result)
        {
            $db_admin_id = $db_result[0]["admin_id"];
            $db_admin_name = $db_result[0]["admin_name"];
            $db_flag_open = $db_result[0]["flag_open"];
        }
        
        if($db_admin_id!="" && $db_flag_open=="1")
        {
            $_SESSION['admin_id']=$db_admin_id;
            $_SESSION['admin_name']=$db_admin_name;
            $_SESSION['admin_level']=$db_dmin_level;
            
            $common_connect->Fn_redirect(global_ssl."/app_master/dashboard/");
        }
    }
    
    if($_COOKIE['account'] != '')
    {
        $arr_admin = $common_admin->Fn_login_cookie($_COOKIE['account']);
        
        if(!is_null($arr_admin))
        {
            foreach($arr_admin[0] as $key=>$value)
            {
                $$key=$value;
            }
        }
        
        if($admin_id!="" && $flag_open=="1")
        {
            $_SESSION['admin_id']=$admin_id;
            $_SESSION['admin_name']=$admin_name;
            $_SESSION['admin_level']=$dadmin_level;
            
            $common_connect->Fn_redirect(global_ssl."/app_master/dashboard/");
        }
    }
?>

<?php include $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/config.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_start.php'; ?>
<script src="/app_master/js/jquery-2.1.1.js"></script>

<!-- Login Background -->
<div id="login-background">
    <!-- For best results use an image with a resolution of 2560x400 pixels (prefer a blurred image for smaller file size) -->
    <img src="../img/placeholders/headers/login_header.jpg" alt="Login Background" class="animation-pulseSlow">
</div>
<!-- END Login Background -->

<!-- Login Container -->
<div id="login-container" class="animation-fadeIn">
    <!-- Login Title -->
    <div class="login-title text-center">
        <h1><i class="gi gi-flash"></i> <strong><?php echo $template['name']; ?></strong><br><small> <strong>ログイン</strong>IDとパスワードを入力してください。 </small></h1>
    </div>
    <!-- END Login Title -->

    <!-- Login Block -->
    <div class="block push-bit">
        <!-- Login Form -->
        <? $var = "form_regist"; ?>
        <form action="/app_master/login/login_check.php" name="<? echo $var;?>" id="<? echo $var;?>" method="post" class="form-horizontal form-bordered form-control-borderless">
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                        <? $var = "admin_login";?>
                        <input type="text" name="<?=$var;?>" id="<?=$var;?>" class="form-control input-lg" placeholder="ID" required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                        <? $var = "admin_pw";?>
                        <input type="password" name="<?=$var;?>" id="<?=$var;?>" class="form-control input-lg" placeholder="Password" required>
                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-xs-4">
                    <label class="switch switch-primary" data-toggle="tooltip" title="Remember Me?">
                        <input type="checkbox" id="login-remember-me" name="login-remember-me" checked>
                        <span></span>
                    </label>
                </div>
                <div class="col-xs-8 text-right">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Login to Dashboard</button>
                </div>
            </div>
        </form>
        <!-- END Login Form -->

    </div>
    <!-- END Login Block -->

    <!-- Footer -->
    <footer class="text-muted text-center">
        <small><span id="year-copy"></span> &copy; <a href="http://goo.gl/TDOSuC" target="_blank"><?php echo $template['name'] . ' ' . $template['version']; ?></a></small>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Login Container -->


<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/login.js"></script>
<script>$(function(){ Login.init(); });</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_end.php'; ?>