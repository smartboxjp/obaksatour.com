<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonAdmin.php";
	$common_admin = new CommonAdmin();
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ログインチェック</title>
</head>

<body>

<?
	$datetime = date("Y-m-d H:i:s");
	
	if ($admin_login == "" or $admin_pw == "") 
	{
	    $common_connect->Fn_javascript_back("必須項目を正しく入力してください。");
	}
	else
	{
		//ログインチェック
		$arr_where = array();
		$var = "admin_login";
		$arr_where[$var] = $$var;
		$var = "admin_pw";
		$arr_where[$var] = $$var;
		$result_login = $common_admin->Fn_admin_login ($arr_where);
		
		if($result_login)
		{
			$db_admin_id = $result_login[0]["admin_id"];
			$db_admin_login = $result_login[0]["admin_login"];
			$db_admin_name = $result_login[0]["admin_name"];
			$db_flag_open = $result_login[0]["flag_open"];
			$db_admin_level = $result_login[0]["admin_level"];
		}
		else
		{
			$common_connect->Fn_javascript_back("IDとパスワードを確認してください。");
		}
		
		
		if($db_flag_open!="1")
		{
			$common_connect->Fn_javascript_back("ログイン権限がありません。");
		}
		
		if ($db_admin_login == $admin_login)
		{
			//管理者の場合
			session_start();
			$_SESSION['admin_id']=$db_admin_id;
			$_SESSION['admin_name']=$db_admin_name;
			$_SESSION['admin_level']=$db_admin_level;
			
			$arr_where = array();
			$arr_where["admin_id"] = $db_admin_id;
			
			//最終ログイン
			$arr_data = array();
			$arr_data["lastlogin_date"] = $datetime;
			$common_admin->Fn_admin_update ($arr_data, $arr_where);
			
			if($db_admin_id!="")
			{
				$arr_where = array();
				$arr_where["admin_id"] = $db_admin_id;
				
				$login_cookie = sha1($db_admin_id."_".$db_login_email."_".$login_pw);
				$arr_data = array();
				$arr_data["login_cookie"] = $login_cookie;
				$common_admin->Fn_admin_update ($arr_data, $arr_where);
		
				setcookie("account", $login_cookie, time()+3600*24*31);//暗号化してクッキーに保存, 31日間
			}
			
			$common_connect->Fn_redirect(global_ssl."/app_master/dashboard/");

		}
	}
?>
</body>
</html>