<?php
    //error_reporting(0);
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>자주하는 질문분류</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php   

    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_admin_check();
    

    if($cate_faq_title == "")
    {
        $common_connect -> Fn_javascript_back("정확히 입력해주세요.");
    }
    
    $datetime = date("Y/m/d H:i:s");
    
    //array
    $arr_db_field = array("cate_faq_title", "view_level");
    $arr_db_field = array_merge($arr_db_field, array("flag_open"));
    

    if($cate_faq_id=="")
    {
        $db_insert = "insert into cate_faq ( ";
        $db_insert .= " cate_faq_id, ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val.", ";
        }
        $db_insert .= " up_date, regi_date ";
        $db_insert .= " ) values ( ";
        $db_insert .= " '', ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= " '".$$val."', ";
        }
        $db_insert .= " '$datetime', '$datetime')";
    }
    else
    {
        $db_insert = "update cate_faq set ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val."='".$$val."', ";
        }
        $db_insert .= " up_date='".$datetime."' ";
        $db_insert .= " where cate_faq_id='".$cate_faq_id."' ";
    }
    $common_dao -> db_update($db_insert);

    if ($cate_faq_id == "")
    {
        //自動生成されてるID出力
        $sql = "select last_insert_id() as last_id";  
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            $cate_faq_id = $common_connect -> $db_result[0]["last_id"];
        }
    }

    
    $common_connect-> Fn_javascript_move("등록/수정하였습니다.", "cate_faq_list.php?cate_faq_id=".$cate_faq_id);
?>
</body>
</html>