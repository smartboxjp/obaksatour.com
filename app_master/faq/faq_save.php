<?php
    //error_reporting(0);
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    $common_image = new CommonImage(); //画像
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php   

    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_admin_check();
    

    if($faq_title == "")
    {
        $common_connect -> Fn_javascript_back("正しく入力してください。");
    }
    
    $datetime = date("Y/m/d H:i:s");
    
    //array
    $arr_db_field = array("cate_faq_id", "faq_title", "view_date", "faq_comment");
    $arr_db_field = array_merge($arr_db_field, array("flag_top"));
    

    if($faq_id=="")
    {
        $db_insert = "insert into faq ( ";
        $db_insert .= " faq_id, ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val.", ";
        }
        $db_insert .= " up_date, regi_date ";
        $db_insert .= " ) values ( ";
        $db_insert .= " '', ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= " '".$$val."', ";
        }
        $db_insert .= " '$datetime', '$datetime')";
    }
    else
    {
        $db_insert = "update faq set ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val."='".$$val."', ";
        }
        $db_insert .= " up_date='".$datetime."' ";
        $db_insert .= " where faq_id='".$faq_id."' ";
    }
    $common_dao -> db_update($db_insert);

    if ($faq_id == "")
    {
        //自動生成されてるID出力
        $sql = "select last_insert_id() as last_id";  
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            $faq_id = $common_connect -> $db_result[0]["last_id"];
        }
    }

    
    $common_connect-> Fn_javascript_move("登録・修正しました", "faq_list.php?faq_id=".$faq_id);
?>
</body>
</html>