<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_admin_check();

    $page_pre_title = "등록";
    if($cate_notice_id!="")
    {
        $arr_db_field = array("cate_notice_title", "view_level");
        $arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));

        $sql = "SELECT ";
        foreach($arr_db_field as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " 1 FROM cate_notice where cate_notice_id='".$cate_notice_id."' ";

        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
            {
                foreach($arr_db_field as $val)
                {
                    $$val = $db_result[$db_loop][$val];
                }
            }
        }

        $page_pre_title = "수정";
    }
?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/config.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_start.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/page_head.php'; ?>

<script src="/app_master/js/vendor/jquery.min.js"></script>
<script type="text/javascript">
    $( function () {

        $( '#form_confirm' ).click( function () {
            $( "#cate_notice_comment" ).val( $( ".note-editable" ).html() );
            err_default = "";
            err_check_count = 0;
            bgcolor_default = "#FFFFFF";
            bgcolor_err = "#FFCCCC";
            background = "background-color";

            err_check_count += check_input( "cate_notice_title" );

            if ( err_check_count != 0 ) {
                alert( "入力に不備があります" );
                return false;
            } else {
                $( '#form_confirm' ).submit();
                //$( '#form_confirm', "body" ).submit();
                return true;
            }
        } );

        function check_input( $str ) {
            $( "#err_" + $str ).html( err_default );
            $( "#" + $str ).css( background, bgcolor_default );

            if ( $( '#' + $str ).val() == "" ) {
                err = "<span class='errorForm'>正しく入力してください。</span>";
                $( "#err_" + $str ).html( err );
                $( "#" + $str ).css( background, bgcolor_err );

                return 1;
            }
            return 0;
        }


    } );

    function fnChangeSel(j) { 
        var result = confirm('削除しますか？'); 
        if(result){ 
            document.location.href = './cate_notice_del.php?cate_notice_id='+j;
        } 
    }
    //-->
</script>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-brush"></i>공지사항분류관리</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/app_master/dashboard/">Dashboard</a></li>
        <li>공지사항분류</li>
    </ul>
    <!-- END Blank Header -->

    <!-- Example Block -->
    <div class="block">
        <!-- Example Title -->
        <div class="block-title">
            <h2>공지사항분류</h2>
        </div>
        <!-- END Example Title -->


<!-- //_/_/_/_/_/_/_ START _/_/_/_/_/_/_// -->
        <form action="./cate_notice_save.php" method="POST" name="form_write" id="form_regist" class="form-horizontal form-bordered">
        <div class="ibox-content">

            <div class="form-group">
                <label class="col-sm-2 control-label">관리ID</label>
                <div class="col-sm-6">
                    <p class="form-control-static">
                        <? $var = "cate_notice_id";?>
                        <?php
                        if($$var=="")
                        {
                        echo "自動生成";
                        }
                        ?>
                        <input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
                        <label id="err_<?php echo $var;?>"><? if($$var!=""){ echo $$var ."(변경안됨)";} ?></label>
                    </p>
                </div>
            </div>

            <div class="form-group">
                <? $var = "cate_notice_title";?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">분류명</label>
                <div class="col-sm-6">
                    <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" type="text" class="form-control count-control" maxlength="100" placeholder="タイトルを最大100文字以内で入力して下さい。"> 
                    <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>


            <div class="form-group">
                <? $var = "view_level";?>
                <?
                if($view_level=="")
                {
                    $sql = "select max(cate_notice_id) as view_level from cate_notice ";
                    $db_result = $common_dao->db_query_bind($sql);
                    $view_level = $db_result[0]["view_level"]+1;
                }
                ?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">表示順位</label>
                <div class="col-sm-6">
                    <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="number" class="form-control count-control" maxlength="5" placeholder="数字のみ入力してください。"> 
                    <span id="err_<?php echo $var;?>"></span>
                    <span class="help-block m-b-none">低い順番から表示されます。</span>
                </div>
            </div>

            <!--ボタン-->
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                <? $var = "form_confirm";?>
                    <button class="btn btn-danger" type="submit" id="<? echo $var;?>" style="width: 100%; height: 50px; font-size: 200%;"><? echo $page_pre_title;?></button>
                </div>
            </div>
            <!--ボタン-->


            
        </div>
        </form>


    </div>
    <!-- END Example Block -->



    <!-- 商品リスト Content -->
    <div class="block full" id="search_list">
        <div class="block-title">
            <h2><strong>공지사항분류 리스트</strong></h2>
        </div>
        <p>등록된 내용은 수정 삭제가 가능합니다.<br />정렬은 표시순서오름차순  </p>
<?php
    
    $view_count=50;   // List count
    $offset=0;

    if(!$page)
    {
        $page=1;
    }
    Else
    {
        $offset=$view_count*($page-1);
    }

    $where = "";
    if($s_keyword!="")
    {
        $where .= " and (cate_notice_title collate utf8_unicode_ci like '%".$s_keyword."%' or cate_notice_id = '".$s_keyword."' ) ";
    }

    //合計
    $all_count = 0;
    $sql = "SELECT count(cate_notice_id) as all_count FROM cate_notice where 1 ".$where ;
    $arr_goods_all = $common_dao->db_query_bind($sql);
    $all_count = $arr_goods_all[0]["all_count"];


    $arr_db_field = array("cate_notice_id", "cate_notice_title", "view_level");
    $arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));

    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM cate_notice where 1 ".$where;
    if($order_name != "")
    {
        $sql .= " order by ".$order_name." ".$order;
    }
    else
    {
        $sql .= " order by regi_date desc";
    }
    $sql .= " limit $offset,$view_count";
?>
        <!-- search -->
        <div class="table-responsive">
            <div class="dataTables_wrapper">
                <div class="row">
                    <div class="col-sm-3 col-xs-5">
                        <label>전체：<? echo $all_count;?>건</label>
                    </div>
                    <div class="col-sm-3">
                    </div>

                    <form action="<?php echo $_SERVER['PHP_SELF']?>#search_list" method="get" name="form_shop_search">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">

                            <? $var = "s_keyword";?>
                            <input name="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="타이틀,타이틀ID" class="m-b form-control"> 
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-md btn-primary"> Go!</button> 
                            </span>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END search -->

        <div class="table-responsive">
            <table class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th>타이틀</th>
                        <th class="text-center">표시순서</th>
                        <th class="text-center">수정일</th>
                        <th class="text-center">편집</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 

                    $db_result = $common_dao->db_query_bind($sql);
                    if($db_result)
                    {
                        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
                        {
                            foreach($arr_db_field as $val)
                            {
                                $$val = $db_result[$db_loop][$val];
                            }
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $cate_notice_id; ?></td>
                        <td><?php echo $cate_notice_title; ?></td>
                        <td class="text-center"><?php echo $view_level; ?></td>
                        <td class="text-center"><?php echo $up_date; ?></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="./cate_notice_list.php?cate_notice_id=<? echo $cate_notice_id;?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                <a href="#" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger" onClick='fnChangeSel("<?php echo $cate_notice_id;?>");'><i class="fa fa-times"></i></a>

                            </div>
                        </td>
                    </tr>
                    <?php

                        }
                    }
                     ?>
                </tbody>
            </table>
        </div>

        <!-- paging -->
        <?php $common_connect -> Fn_paging_10_list($view_count, $all_count); ?>
        <!-- END paging -->
    </div>
    <!-- END 商品リスト Content -->

<!-- //_/_/_/_/_/_/_ END _/_/_/_/_/_/_// -->

</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/page_footer.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_scripts.php'; ?>



<script src="/app_master/js/plugins/count/bootstrap-maxlength.js"></script>


<script>
$(document).ready(function () {
  $('.count-control').maxlength({
    alwaysShow: true,
    warningClass: "label label-success",
    limitReachedClass: "label label-danger"
  });
});
</script>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_end.php'; ?>