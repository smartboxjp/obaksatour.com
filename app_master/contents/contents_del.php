<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>削除</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php
    $common_connect -> Fn_admin_check();
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    if($contents_id=="")
    {
        $common_connect -> Fn_javascript_back("正しく選択してください。");
    }

    
    //削除処理
    if($contents_id!="")
    {
        $db_del = "Delete from contents where contents_id='".$contents_id."' ";
        $common_dao->db_update($db_del);
    }
    
    $common_connect-> Fn_javascript_move("削除しました", "contents_list.php#search_list");
?>
</body>
</html>