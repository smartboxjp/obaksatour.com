<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonArray.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_admin_check();
?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/config.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_start.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- 加盟店リスト Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-table"></i>加盟店リスト<br><small></small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/app_company/dashboard/">Dashboard</a></li>
        <li>加盟店リスト</li>
    </ul>
    <!-- END 加盟店リスト Header -->

    <!-- 加盟店リスト Content -->
    <div class="block full">
        <div class="block-title">
            <h2><strong>加盟店リスト</strong></h2>
        </div>
        <p>登録した加盟店のリストから編集、削除が可能です。</p>
<?php
    
    $view_count=20;   // List count
    $offset=0;

    if(!$page)
    {
        $page=1;
    }
    Else
    {
        $offset=$view_count*($page-1);
    }
    
    $where = " ";


    if($s_company_id!="")
    {
        $where .= " and company_id='".$s_company_id."'";
    }

    $s_keyword = "";
    if($s_keyword!="")
    {
        $where .= " and (company_name collate utf8_unicode_ci like '%".$s_keyword."%' or company_name_jp collate utf8_unicode_ci like '%".$s_keyword."%' or company_id = '%".$s_keyword."%' ) ";
    }

    if($s_flag_open!="")
    {
        $where .= " and flag_open='".$s_flag_open."'";
    }

    //合計
    $all_count = 0;
    $sql = "SELECT count(company_id) as all_count FROM company where 1 ".$where ;
    $arr_company_all = $common_dao->db_query_bind($sql);
    $all_count = $arr_company_all[0]["all_count"];


    $arr_db_field = array("company_id", "company_pw", "commission", "company_name", "company_name_jp", "company_kana", "company_img");
    $arr_db_field = array_merge($arr_db_field, array("company_email", "company_tel", "company_comment", "post_num", "address", "googlemap_1", "googlemap_2"));
    $arr_db_field = array_merge($arr_db_field, array("last_login", "regi_date", "up_date", "user_agent", "login_cookie", "flag_open"));

    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM company where 1 ".$where;
?>
        <div class="table-responsive">
            <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">加盟店名</th>
                        <th class="text-center">写真</th>
                        <th>電話番号</th>
                        <th>公開有無</th>
                        <th class="text-center">編集</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 

                    $db_result = $common_dao->db_query_bind($sql);
                    if($db_result)
                    {
                        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
                        {
                            foreach($arr_db_field as $val)
                            {
                                $$val = $db_result[$db_loop][$val];
                            }
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $company_id; ?></td>
                        <td class="text-center"><?php echo $company_name."<br >".$company_name_jp; ?></td>
                        <td class="text-center">
                            <div>
                            <?php
                            if($company_img!="")
                            {
                                echo "<a href='/".global_company_dir.$company_id."/".$company_img."?".date("his")."' data-toggle='lightbox-image'><img src='/".global_company_dir.$company_id."/".$company_img."?".date("his")."' style='max-height:100px; '></a>";
                            }
                            ?>
                            </div>
                        </td>
                        <td class="text-center"><?php echo $company_tel; ?></td>
                        <td><span class="label <?php echo $arr_company_flag_open_color[$flag_open] ?>"><?php echo $arr_company_flag_open[$flag_open]; ?></span></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="./company_regist.php?company_id=<? echo $company_id;?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                <a href="javascript:void(0)" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                            </div>
                        </td>
                    </tr>
                    <?php

                        }
                    }
                     ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- END 加盟店リスト Content -->
</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/page_footer.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script>
$(function(){ TablesDatatables.init(); });

var TablesDatatables = function() {
    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#example-datatable').dataTable({
                columnDefs: [ { orderable: false, targets: [ 2, 8 ] } ],
                pageLength: <? echo $view_count;?>,
                lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'All']]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();
</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_end.php'; ?>