<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonArray.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_admin_check();
    
    if($company_id!="")
    {
        $arr_data = array("company_id", "company_pw", "company_name", "company_name_jp", "company_kana", "company_tel", "company_img");
        $arr_data = array_merge($arr_data, array("company_email", "company_comment", "last_login", "user_agent", "login_cookie", "flag_open", "googlemap_1", "googlemap_2"));
        $arr_data = array_merge($arr_data, array("post_num", "address", "commission", "regi_date", "up_date"));

        $sql = "SELECT ";
        foreach($arr_data as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " 1 FROM company where company_id='".$company_id."' ";

        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
            {
                foreach($arr_data as $val)
                {
                    $$val = $db_result[$db_loop][$val];
                }
            }
        }
    }
    
?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/config.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_start.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/page_head.php'; ?>


<link href="/app_master/css/plugins/cropper/cropper.min.css" rel="stylesheet">
<style type="text/css">
.img-container, .img-preview {
    overflow: hidden;
    text-align: center;
    width: 100%;
}
.img-preview-sm {
    height: 150px;
    width: 150px;
}
</style>

<script src="/app_master/js/jquery-2.1.1.js"></script>
<script type="text/javascript">
    $(function() {
            
        $('#form_confirm').click(function() {
            err_default = "";
            err_check_count = 0;
            bgcolor_default = "#FFFFFF";
            bgcolor_err = "#FFCCCC";
            background = "background-color";
            
            err_check_count += check_input("company_name");
            err_check_count += check_input_pw("company_pw");
            
            if(err_check_count!=0)
            {
                alert("入力に不備があります");
                return false;
            }
            else
            {

                var $image = $(".image-crop > img")
                var img = $image.cropper("getDataURL");
                $('.cropped').val(img);

                //$('#form_confirm').submit();
                $('#form_confirm', "body").submit();
                return true;
            }
            
            
        });
                
        function check_input($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);

            if($('#'+$str).val()=="")
            {
                err ="<span class='errorForm'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }

        function check_input_id($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);
            
            if($('#'+$str).val()=="")
            {
                err ="<span class='errorForm'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            else if(checkID($('#'+$str).val())==false)
            {
                err ="<span class='errorForm'>英数半角で入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            else if($('#'+$str).val().length<3)
            {
                err ="<span class='errorForm'>3文字以上で入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }
        
        //英数半角
        function checkID(value){
            if(value.match(/[^0-9a-z]+/) == null){
                return true;
            }else{
                return false;
            }
        } 

        //メールチェック
        function check_input_email($str_1) 
        {
            $("#err_"+$str_1).html(err_default);
            $("#"+$str_1).css(background,bgcolor_default);
            if($('#'+$str_1).val()=="")
            {
                err ="<span class='errorForm'>正しく入力してください。</span>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                
                return 1;
            }
            else if(checkIsEmail($('#'+$str_1).val()) == false)
            {
                err ="<span class='errorForm'>メールアドレスは半角英数字でご入力ください。</span>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                
                return 1;
            }
            
            return 0;
        }

        //メールチェック
        
        function checkIsEmail(value) {
            if (value.match(/.+@.+\..+/) == null) {
                return false;
            }
            return true;
        }
        
        function check_input_pw($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);
            
            if($('#'+$str).val()=="")
            {
                err ="<span class='errorForm'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            else if($('#'+$str).val().length<6)
            {
                err ="<span class='errorForm'>６文字以上入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }
                        

    });
    
//-->
</script>


<script src="//maps.google.com/maps/api/js?v=3&sensor=false" type="text/javascript" charset="UTF-8"></script>

<script language="JavaScript" type="text/JavaScript">
<!--
    var gWinGMap;
    function OpenWinGoogleMap(aVal) {
        var nLat    = $("#googlemap_1").val();
        var nLon    = $("#googlemap_2").val();

        var nWidth;
        var nHeight;
        
        nWidth  = 580;
        nHeight = 700;
    
        var nTop    = ( window.screen.height / 2 ) - ( nHeight / 2 );
        var nLeft   = ( window.screen.width  / 2 ) - ( nWidth  / 2 );
    
        var sUrl = "";
        sUrl    = "/app_master/inc/googlemap.php?googlemap_1="+nLat+"&googlemap_2="+nLon+"&kubun="+aVal;
    
        var sOption = "top=" + nTop + ",left=" + nLeft + ",width=" + nWidth + ",height=" + nHeight + ",resizable=no";
        
        gWinGMap = window.open( sUrl, "gmap", sOption );
        gWinGMap.focus();
    
        return false;
    }
    
    function fnImgDel(shop_id, i, j, k) { 
        var result = confirm('削除しますか？'); 
        if(result){ 
            document.location.href = './img_one_del.php?shop_id='+shop_id+'&shop_id='+i+'&img='+j+'&img_name='+k;
        } 
    }
        
    function fnrotate(shop_id, field, i) { 
        var result = confirm(i+'度回転しますか？'+'\r\n縦横の比率によって回転できない場合もあります。'); 
        if(result){ 
            document.location.href = './shop_rotate.php?shop_id='+shop_id+'&field='+field+'&rotate='+i;
        } 
    }
//-->
</script>



<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-brush"></i>会社情報変更<br><small>会社の基本情報を変更することができます。</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/app_master/dashboard/">Dashboard</a></li>
        <li>会社情報変更</li>
    </ul>
    <!-- END Blank Header -->

    <!-- Example Block -->
    <div class="block">
        <!-- Example Title -->
        <div class="block-title">
            <h2>会社情報変更</h2>
        </div>
        <!-- END Example Title -->

<!-- //_/_/_/_/_/_/_ START _/_/_/_/_/_/_// -->
        <form action="./company_save.php" method="POST" name="form_write" id="form_regist" enctype="multipart/form-data" class="form-horizontal form-bordered">
        <div class="ibox-content">

            <div class="form-group">
                <label class="col-sm-2 control-label">加盟店のID</label>
                <div class="col-sm-6">
                    <p class="form-control-static">
                        <? $var = "company_id";?>
                        <?php
                        if($$var!="")
                        {
                        echo $$var;
                        }
                        ?>
                        <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" type="<? if($$var!=""){ echo "hidden";} else { echo " text ";}?>" class="form-control maxcount" placeholder="4文字以上の英語小文字、数字のみ入力可能" maxlength="50"> 
                        <input type="hidden" name="check_up" value="<? echo $company_id;?>">
                        <label id="err_<?php echo $var;?>"><? if($$var!=""){ echo "（変更出来ません）";} ?></label>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <? $var = "company_name";?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">加盟店の韓国語</label>
                <div class="col-sm-6">
                    <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control" maxlength="50" placeholder="加盟店の名前を最大50文字以内で入力して下さい。"> 
                    <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">加盟店の日本語</label>
                <div class="col-sm-6">
                  <? $var = "company_name_jp";?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control maxcount" maxlength="50" placeholder="加盟店の日本語名を50文字以内で入力して下さい。"> 
                  <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">加盟店のカナ名</label>
                <div class="col-sm-6">
                  <? $var = "company_kana";?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control maxcount" maxlength="50" placeholder="最大50文字以内で入力して下さい。"> 
                  <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">郵便番号</label>
                <div class="col-sm-6">
                  <? $var = "post_num";?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control maxcount" maxlength="8" placeholder="例）900-0000"> 
                  <span id="err_<?php echo $var;?>"></span>
                  <span class="help-block m-b-none">請求書発行の時に使用されます。</span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">住所</label>
                <div class="col-sm-6">
                  <? $var = "address";?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control maxcount" maxlength="50" placeholder="例）沖縄県那覇市○○○"> 
                  <span id="err_<?php echo $var;?>"></span>
                  <span class="help-block m-b-none">請求書発行の時に使用されます。</span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">TEL</label>
                <div class="col-sm-6">
                  <? $var = "company_tel";?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control maxcount" maxlength="20" placeholder="例）090-0000-0000"> 
                  <span id="err_<?php echo $var;?>"></span>
                  <span class="help-block m-b-none">請求書発行の時に使用されます。</span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">加盟店のパスワード</label>
                <div class="col-sm-6">
                    <? $var = "company_pw";?>
                  <? if($$var=="") { $$var=date("Ymd").$common_connect->Fn_random_password(10);}?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" type="text" class="form-control maxcount" maxlength="20" placeholder="加盟店のパスワードを決めて入力して下さい。"> 
                  <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">加盟店のメールアドレス</label>
                <div class="col-sm-6">
                  <? $var = "company_email";?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" type="text" class="form-control maxcount" maxlength="50"> 
                  <span id="err_<?php echo $var;?>"></span>
                  <span class="help-block m-b-none">メールアドレスを間違えると加盟店にユーザーからの問い合わせが届かなくなりますので注意してください。
                </span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">コメント</label>
                <div class="col-sm-6">
                  <? $var = "company_comment";?>
                  <textarea name="<?php echo $var;?>" id="<?php echo $var;?>" class="form-control maxcount" maxlength="50"><?php echo $$var;?></textarea>
                  <span id="err_<?php echo $var;?>"></span>
                  <span class="help-block m-b-none">
                </span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">GOOGLE MAP</label>
                <div class="col-sm-6">
                  <? $var = "googlemap_1";?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" type="text" class="form-control maxcount" maxlength="20"> 

                  <? $var = "googlemap_2";?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" type="text" class="form-control maxcount" maxlength="20"> 
                  
                  <input type="button" value="住所から緯度経度を入力する" id="attrLatLng">
                <input type="button" value="お店の座標を取得" onclick="OpenWinGoogleMap();">
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">公開・非公開</label>
              <div class="col-sm-6">
                <? $var = "flag_open";?>
                <select name="<? echo $var;?>" id="<? echo $var;?>">
                <?
                foreach($arr_company_flag_open as $key=>$value)
                {
                ?>
                  <option value="<? echo $key;?>" <? if($key == $$var) { echo " selected ";}?>><? echo $value;?></option>
                <?
                }
                ?>
                </select>
                <span id="err_<?php echo $var;?>"></span>
              </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">TEL</label>
                <div class="col-sm-6">
                  <? $var = "company_tel";?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control maxcount" maxlength="20" placeholder="例）090-0000-0000"> 
                  <span id="err_<?php echo $var;?>"></span>
                  <span class="help-block m-b-none">請求書発行の時に使用されます。</span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">加盟店のサムネイル画像</label>

                <div class="col-sm-6">
                    <p>
                        ※無登録でも可能。また店舗管理で登録や編集も出来ます。<br />
                        加盟店のサムネイル画像をトリミングで編集登録が出来ます。120px×120pxで表示されます<br />
                        画像の好きな位置に枠をあわせて決定を押してください。
                    </p>

                    <div class="row">
                        <div class="col-md-7">
                            <div class="image-crop">
                                <img src="/app_master/img/company_sample.jpg">
                            </div>
                        </div>
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-4">
                            <h4>Preview image</h4>
                            <div class="img-preview img-preview-sm"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-group">
                                <button class="btn btn-sm btn-info" id="zoomIn" type="button">Zoom In</button>
                                <button class="btn btn-sm btn-info" id="zoomOut" type="button">Zoom Out</button>
                                <button class="btn btn-sm btn-info" id="rotateLeft" type="button">Rotate Left</button>
                                <button class="btn btn-sm btn-info" id="rotateRight" type="button">Rotate Right</button>
                            </div>
                            <br>
                            <?
                            if($company_img!="")
                            {
                                echo "<img src='/".global_company_dir.$company_id."/".$company_img."?".date("his")."'>";
                            }
                            ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-group">
                                <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                    <input type="file" accept="image/*" name="company_img" id="inputImage" class="hide">
                                    カットしたい画像をアップ
                                </label>
                            </div>

                            <div class="btn-group">
                                
                              <div class="onoffswitch">
                              <? $var = "check_img";?>
                              <input type="checkbox" name="<? echo $var;?>" value="1" class="onoffswitch-checkbox" id="checkbox_<? echo $company_id;?>">
                              <label class="onoffswitch-label" for="checkbox_<? echo $company_id;?>">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                              </label>
                              サムネイルを反映する
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        <!--イメージクリッパー-->

<input type="hidden" name="cropped" class="cropped">

<!--イメージクリッパーサムネイル画像表示-->

<script>
    $(document).ready(function(){
        var $image = $(".image-crop > img")
        $($image).cropper({
                aspectRatio: 1,//トリミング比率サイズ
                preview: ".img-preview"
        });

        var $inputImage = $("#inputImage");
        if (window.FileReader) {
                $inputImage.change(function() {
                    var fileReader = new FileReader(),
                        files = this.files,
                        file;

                    if (!files.length) {
                        return;
                    }

                    file = files[0];

                    if (/^image\/\w+$/.test(file.type)) {
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function () {
                            $('#checkbox_<? echo $company_id;?>').prop("checked",true); 
                                $inputImage.val("");
                                $image.cropper("reset", true).cropper("replace", this.result);
                        };
                    } else {
                        showMessage("Please choose an image file.");
                    }
                });
        } else {
                $inputImage.addClass("hide");
        }

        $("#zoomIn").click(function() {
                $image.cropper("zoom", 0.1);
        });

        $("#zoomOut").click(function() {
                $image.cropper("zoom", -0.1);
        });

        $("#rotateLeft").click(function() {
                $image.cropper("rotate", 45);
        });

        $("#rotateRight").click(function() {
                $image.cropper("rotate", -45);
        });

        $("#setDrag").click(function() {
            // crop のデータを取得
             /* start */
             var data = $('#img').cropper('getData');
        
             // 切り抜きした画像のデータ
             // このデータを元にして画像の切り抜きが行われます
             var image = {
                 width: Math.round(data.width),
                 height: Math.round(data.height),
                 x: Math.round(data.x),
                 y: Math.round(data.y),
                 _token: 'jf89ajtr234534829057835wjLA-SF_d8Z' // csrf用
                };
                
                // post 処理
             $.post('/cropper', image, function(res){
                 // 成功すれば trueと表示されます
                 console.log(res);
             });
             /* end */

                $image.cropper("setDragMode", "crop");
        });



    });

</script>


<!--イメージクリッパーサムネイル画像表示-->

<!--登録ボタン-->
            <div class="form-group">
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-2">
                <? $var = "form_confirm";?>
                    <button class="btn btn-danger" type="submit" id="<? echo $var;?>">登録</button>
                  <button class="btn btn-default" type="submit">キャンセル</button>
                </div>
            </div>
<!--登録ボタン-->

            
        </div>
        </form>
<!-- //_/_/_/_/_/_/_ END _/_/_/_/_/_/_// -->
    </div>
    <!-- END Example Block -->
</div>
<!-- END Page Content -->



<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/page_footer.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_scripts.php'; ?>

<!-- Image cropper -->
<script src="/app_master/js/plugins/cropper/cropper.min.js"></script>

<script src="/app_master/js/plugins/count/bootstrap-maxlength.js"></script>

<script>
$(document).ready(function () {
  $('.form-control').maxlength({
    alwaysShow: true,
    warningClass: "label label-success",
    limitReachedClass: "label label-danger"
  });
});
</script>

<!-- googemap start -->
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCvZ7IH5KNrL8a0qY-ez-bLibffBFasGHo "></script>
<script type="text/javascript">
/* ========================================================================
   Attribute Latitude And Longitude From Address With Google Maps API
 ========================================================================== */
$(function(){
    function attrLatLngFromAddress(address){
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': address}, function(results, status){
            if(status == google.maps.GeocoderStatus.OK) {
                var lat = results[0].geometry.location.lat();
                var lng = results[0].geometry.location.lng();
                // 小数点第六位以下を四捨五入した値を緯度経度にセット、小数点以下の値が第六位に満たない場合は0埋め
                document.getElementById("googlemap_1").value = (Math.round(lat * 1000000) / 1000000).toFixed(6);
                document.getElementById("googlemap_2").value = (Math.round(lng * 1000000) / 1000000).toFixed(6);
            }
        });
    }

    $('#attrLatLng').click(function(){
        var address = document.getElementById("address").value;
        attrLatLngFromAddress(address);
    });
});
</script>
<!-- googemap end -->
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_master/inc/template_end.php'; ?>