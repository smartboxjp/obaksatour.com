<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連

  foreach($_POST as $key => $value)
  {
    $$key = $common_connect->h($value);
  }
?>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?
  $meta_title = "회원가입";
  $meta_description = "";
  require_once $_SERVER['DOCUMENT_ROOT']."/include/meta.php";
?>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/common/css/form.css" rel="stylesheet">

  <script type="text/javascript">

  $(function() {
    $('#auth_email').click(function() {
      err_default = "";
      err_check_count = 0;
      bgcolor_default = "#FFFFFF";
      bgcolor_err = "#FFCCCC";
      background = "background-color";


      err_check_count += check_input_email("login_email");

      if(err_check_count!=0)
      {
        alert("인증번호 발송을 위한 이메일을 정확히 입력해주세요.");
      }
      else
      {
        $.ajax({
          type: "POST",
          url: "auth_mail.php",
          data: "login_email="+$('#login_email').val(),
          success: function(data) {

            if(data.trim()!="")
            {
              alert(data);
            }
            else
            {
              alert($('#login_email').val()+" 이메일을 확인해주세요.\n\r없는경우는 스팸함을 확인해주세요.");
            }

          },
          error: function(msg) {
            alert('인증번호 전송에 실패하였습니다. Error: '+msg);
          }
        });
      }

    });

    flag_auth_confirm =false;
    $('#auth_confirm').click(function() {
      err_default = "";
      err_check_count = 0;
      bgcolor_default = "#FFFFFF";
      bgcolor_err = "#FFCCCC";
      background = "background-color";

      $.ajax({
        type: "POST",
        url: "auth_mail_confirm.php",
        data: "login_email="+$('#login_email').val()+"&temp_key="+$('#temp_key').val(),
        success: function(data) {

          err_check_count += check_input_email("login_email");
          err_check_count += check_input("temp_key");

          if(err_check_count!=0)
          {
            alert("이메일로 발송된 인증키를 입력하세요.");
          }
          else
          {
            if(data.trim()=="OK")
            {
              alert("인증되었습니다.");
              flag_auth_confirm =true;
            }
            else
            {
              $("#temp_key").css(background,bgcolor_err);
              err = "인증에 실패하였습니다.";
              alert(err);
            }
          }

        },
        error: function(msg) {
          alert('인증에 실패하였습니다. Error: '+msg);
        }
      });


    });



    $('#form_confirm').click(function() {
      err_default = "";
      err_check_count = 0;
      bgcolor_default = "#FFFFFF";
      bgcolor_err = "#FFCCCC";
      background = "background-color";

      err_check_count += check_input("member_name");
      err_check_count += check_input_email("login_email");
      err_check_count += check_input("temp_key");
      err_check_count += check_input_pw_confirm("login_pw", "login_pw_confirm");

      err_check_count += check_flag("temp_key", flag_auth_confirm);




      if(err_check_count!=0)
      {
        alert("필수 항목을 정확히 입력해주세요.");
        return false;
      }
      else
      {
        //$('#form_confirm').submit();
        $('#form_confirm', "body").submit();
        return true;
      }


    });

    function check_flag($str, flag_auth_confirm)
    {
      $("#err_"+$str).html(err_default);
      $("#"+$str).css(background,bgcolor_default);

      if($('#'+$str).val()=="" || flag_auth_confirm==false)
      {
        err ="인증이 필요합니다.";
        $("#err_"+$str).html(err);
        $("#"+$str).css(background,bgcolor_err);

        return 1;
      }
      return 0;
    }

    function check_input($str)
    {
      $("#err_"+$str).html(err_default);
      $("#"+$str).css(background,bgcolor_default);

      if($('#'+$str).val()=="")
      {
        err ="정확히 입력해주세요.";
        $("#err_"+$str).html(err);
        $("#"+$str).css(background,bgcolor_err);

        return 1;
      }
      return 0;
    }

    function check_input_email($str)
    {
      $("#err_"+$str).html(err_default);
      $("#"+$str).css(background,bgcolor_default);

      if($('#'+$str).val()=="")
      {
        err ="정확히 입력해주세요.";
        $("#err_"+$str).html(err);
        $("#"+$str).css(background,bgcolor_err);

        return 1;
      }
      else if(checkIsEmail($('#'+$str).val()) == false)
      {
        err ="정확한 이메일을 입력해주세요.";
        $("#err_"+$str).html(err);
        $("#"+$str).css(background,bgcolor_err);

        return 1;
      }

      return 0;
    }


    //メールチェック
    function checkIsEmail(value) {
      if (value.match(/.+@.+\..+/) == null) {
        return false;
      }
      return true;
    }

    function check_input_pw_confirm($str_1, $str_2)
    {
      $("#err_"+$str_1).html(err_default);
      $("#"+$str_1).css(background,bgcolor_default);
      $("#err_"+$str_2).html(err_default);
      $("#"+$str_2).css(background,bgcolor_default);

      if($('#'+$str_1).val().length<6)
      {
        err ="패스워드를 6~20자로 정확히 입력해주세요";
        $("#err_"+$str_1).html(err);
        $("#"+$str_1).css(background,bgcolor_err);

        return 1;
      }
      else if($('#'+$str_1).val()!=$('#'+$str_2).val())
      {
        err ="패스워드를 정확히 입력해주세요";
        $("#err_"+$str_2).html(err);
        $("#"+$str_2).css(background,bgcolor_err);

        return 1;
      }

      return 0;
    }

  });

//-->
</script>
</head>
<body>
<?
  if($flag_member!="1" || $flag_privacy!="1")
  {
    $common_connect-> Fn_javascript_move("약관에 동의가 필요합니다.", "/member_regist/");
  }
?>
<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

  <div class="l-container">
    <div class="pankuzu-wapper">
      <ul class="pankuzu">
        <li><a href="/">오키나와오박사 홈</a></li>
        <li>회원가입</li>
      </ul>
    </div>
    <!-- /.pankuzu-wapper -->
  </div>
  <!-- /.l-container -->


  <div class="l-container clearfix">
    <h1 class="headline-glay fsize-lg left-bdr mb-30"><span class="color-blue">회원가입</span></h1>
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <form class="signup-main" action="regist_save.php" name="form_regist" id="form_regist" method="post">
      <table class="table table-bordered mb-0 main-board-wrap">
        <tbody>
          <tr>
            <td colspan="2" class="main-board">

              <ul class="stepnav stepnav-now2">
                <li class="stepnav-after"><span>이용약관 동의</span></li>
                <li class="stepnav-current"><span>회원정보 입력</span></li>
                <li><span>회원가입 완료</span></li>
              </ul>
              <h1 class="headline-glay fsize-lg mb-20">간단한 절차로 빠르게 쉽게 <span class="color-lightblue">회원가입</span>이 가능합니다.</h1>
              <p class="mb-10">이메일을 통하여<span class="color-blue">인증</span>한 후 진행 해 주시기 바람니다.<br />
              메일이 수분이내에 도착되지 않았을 경우는 <span class="color-blue">스팸메일을 확인</span>해 주시기 바랍니다.<br />
              모든 항목은 필수 입력입니다.
              </p>
              <ul class="btn-list">
                <li>

                <?php
                  // 네이버 로그인 접근토큰 요청
                  $client_id = NAVER_Client_ID;
                  $redirectURI = urlencode(global_ssl."/member_regist/naver/callback.php");
                  $state = $common_connect->generate();
                  $apiURL = "https://nid.naver.com/oauth2.0/authorize?response_type=code&client_id=".$client_id."&redirect_uri=".$redirectURI."&state=".$state;
                ?>
                <a href="<?php echo $apiURL ?>" class="btn-naver wid-lg"><img src="/common/images/naver-n.svg" alt="NAVER"><span>naver아이디로 회원가입</span></a>

                </li>
                <li><a href="./facebook/facebook_regist.php" class="btn-facebook wid-lg"><i class="fa fa-facebook" aria-hidden="true"></i><span>facebook 아이디로 회원가입</span></a></li>
              </ul>
            </td>
          </tr>
        </tbody>
      </table>

      <table class="table table-bordered mobiletable-row">
        <tbody>
          <tr>
            <th class="ta-left wid-a">한글이름</th>
            <td class="ta-left b">
              <? $var = "member_name";?>
              <input type="text" class="form-control input-textbox" id="<? echo $var;?>" name="<? echo $var;?>" placeholder="한글실명을 입력하세요" maxlength="30";>
              <span id="err_<?=$var;?>" class="alert-red"></span>
            </td>
          </tr>
          <tr>
            <th class="ta-left wid-a">등록한 이메일</th>
            <td class="ta-left b">
              <? $var = "login_email";?>
              <span class="input-double"><input type="text" class="form-control " id="<? echo $var;?>" name="<? echo $var;?>" placeholder="아이디로 사용할 이메일을 입력하세요" maxlength="100";></span>
              <button id="auth_email" type="button" class="btn btn-nomal sign-up-button">인증번호발송</button>
              <span id="err_<?=$var;?>" class="alert-red"></span>
            </td>
          </tr>
          <tr>
            <th class="ta-left wid-a">인증번호</th>
            <td class="ta-left b">
              <? $var = "temp_key";?>
              <span class="input-double"><input type="text" class="form-control " id="<? echo $var;?>" name="<? echo $var;?>" placeholder="인증번호를 입력하세요" maxlength="10";></span>
              <button id="auth_confirm" type="button" class="btn btn-nomal sign-up-button">인증하기</button>
              <span id="err_<?=$var;?>" class="alert-red"></span>
            </td>
          </tr>
          <tr>
            <th class="ta-left wid-a">패스워드</th>
            <td class="ta-left b">
              <? $var = "login_pw";?>
              <input type="password" class="form-control input-textbox" id="<? echo $var;?>" name="<? echo $var;?>" placeholder="패스워드를 입력하세요" maxlength="20">
              <span id="err_<?=$var;?>" class="alert-red">6자이상 20자이내로 특수키포함</span>
            </td>
          </tr>
          <tr>
            <th class="ta-left wid-a">패스워드 확인</th>
            <td class="ta-left b mb-0">
              <? $var = "login_pw_confirm";?>
              <input type="password" class="form-control input-textbox" id="<? echo $var;?>" name="<? echo $var;?>" placeholder="패스워드를 다시한번 입력하세요" maxlength="20">
              <span id="err_<?=$var;?>" class="alert-red"></span>
            </td>
          </tr>
        </tbody>
      </table>
      <div class="text-center mb-50">
                <? $var = "form_confirm";?>
                <button type="submit" class="btn btn-primary btn-lg2" id="<? echo $var;?>" name="<? echo $var;?>">회원가입하기</button>
              </div>
      <? $var = "flag_email";?>
      <input type="hidden" id="<? echo $var;?>" name="<? echo $var;?>" value="<? echo $$var;?>">
    </form>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

</div>
<!-- /.contents -->

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>