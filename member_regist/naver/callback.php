<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連
  
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/mailer/PHPMailerAutoload.php";
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMailer.php";
  $common_mailer = new CommonMailer(); //メール関連

  // 네이버 로그인 콜백 예제
  $client_id = NAVER_Client_ID;
  $client_secret = NAVER_Client_Secret;
  foreach($_GET as $key => $value)
  { 
      $$key = $common_connect->h($value);
  }
  $redirectURI = urlencode(global_ssl."/");
  $url = "https://nid.naver.com/oauth2.0/token?grant_type=authorization_code&client_id=".$client_id."&client_secret=".$client_secret."&redirect_uri=".$redirectURI."&code=".$code."&state=".$state;

  if(!$common_connect->validate($state, $throw = false))
  {
    echo "RESPONSE_UNAUTHORIZED";
    exit;
  }

  $is_post = false;
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, $is_post);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $headers = array();
  $response = curl_exec ($ch);
  $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  //echo "status_code:".$status_code."<br>";
  curl_close ($ch);
  if($status_code == 200) {
    //echo $response;
  } else {
    echo "Error 내용:".$response;
    exit;
  }

  $json = json_decode($response);

  $options = array('http' => array('header'  => "Authorization: Bearer ".$json->access_token));
  $xml = simplexml_load_string(file_get_contents("https://apis.naver.com/nidlogin/nid/getUserProfile.xml", false, stream_context_create($options)));
  if(!($xml->result->resultcode=="00"&&$xml->result->message=="success")) {
    echo "인증에 실패했습니다.";
    exit;
  } else {
    $login_email = $xml->response->email;
    $naver_id = $xml->response->id;
    $member_name = $xml->response->nickname;
    /*
    echo $xml->response->enc_id."<hr />";
    echo $xml->response->gender."<hr />";
    */
  }
?>


<!DOCTYPE html>
<html>
<head>
    <title>Facebook 회원가입</title>
</head>
<body>
<?
  $datetime = date("Y-m-d H:i:s");
  
  if($login_email == "" || $naver_id == "")
  {
    $common_connect -> Fn_javascript_back("정확히 입력해 주세요");
  }
  
  $sql = "SELECT login_email FROM member where login_email = '".$login_email."' ";
  $db_result = $common_dao->db_query_bind($sql);
  if($db_result)
  {
      $common_connect-> Fn_javascript_move("이미 등록된 메일 입니다.\\r\\n로그인 해주세요.", "/member/");
  }

  
  //すでに登録されていれば、ログイン成功
  $where = " and naver_id='".$naver_id."' ";
  $sql = "SELECT login_email FROM member where 1 ".$where ;
  $arr_member = $common_dao->db_query_bind($sql);


  //facebook_idで一致してるかをチェック
  if($db_result)
  {
    if($login_email == $arr_member[0]["login_email"])
    {
      $common_connect -> Fn_javascript_move("이미 등록된 메일입니다.\\r\\n로그인 해주세요.", "/member/");
    }
    else
    {
      $common_connect -> Fn_javascript_move("메일이 정보가 일치하지 않습니다.\\r\\n naver 정보가 변경되었다면 로그인 후 메일을 수정해주세요.", "/member/");
    }
  }
  //データがなければ会員登録をさせる
  else
  {
    
    $login_pw = $common_connect-> Fn_random_password(8);
    $login_pw = $common_connect-> Fn_generate_pw($login_pw);
    
    //仮登録を行う
    $flag_open=1;
    $arr_db_field = array("login_email", "login_pw", "naver_id", "member_name", "flag_open");
    
    $db_insert = "insert into member ( ";
    $db_insert .= " member_id, ";
    foreach($arr_db_field as $val)
    {
        $db_insert .= $val.", ";
    }
    $db_insert .= " regi_date, up_date ";
    $db_insert .= " ) values ( ";
    $db_insert .= " '', ";
    foreach($arr_db_field as $val)
    {
        $db_insert .= " '".$$val."', ";
    }
    $db_insert .= " '$datetime', '$datetime')";
    $db_result = $common_dao->db_update($db_insert);
    
    //로그인
    $sql = "SELECT member_id, member_name FROM member where login_email = '".$login_email."' ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $member_id = $db_result[0]["member_id"];
        $member_name = $db_result[0]["member_name"];
        session_start();
        $_SESSION['member_id']=$member_id;
        $_SESSION['member_name']=$member_name;
    }

    //Thank youメール
    if ($login_email != "")
    {
        $subject = " [오키나와 오박사]회원 가입을 축하 드립니다. ";


        $body = "";
        $body = file_get_contents($_SERVER['DOCUMENT_ROOT']."/member_regist/mail/regist_save.user.php");
        $body = str_replace("[login_email]", $login_email, $body);
        $body = str_replace("[member_name]", $member_name, $body);
        $body = str_replace("[datetime]", $datetime, $body);

    }
    $common_mailer->Fn_send_utf ($login_email,global_test_mail.$subject,$body,$global_mail_from,$global_send_mail);

    $common_connect -> Fn_email_log($login_email, global_test_mail.$common_connect->h($subject), $common_connect->h($body)); //メールログ

    $common_mailer->Fn_send_utf ($global_send_mail,global_test_mail.$subject,$body,$global_mail_from,$global_send_mail);

    $common_connect-> Fn_redirect(global_no_ssl."/member_regist/regist_thankyou.php?login_email=".$login_email);
  }
  
?>

</body>

</html>