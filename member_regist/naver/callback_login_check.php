<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連

  // 네이버 로그인 콜백 예제
  $client_id = NAVER_Client_ID;
  $client_secret = NAVER_Client_Secret;
  foreach($_GET as $key => $value)
  { 
      $$key = $common_connect->h($value);
  }
  $redirectURI = urlencode(global_ssl."/");
  $url = "https://nid.naver.com/oauth2.0/token?grant_type=authorization_code&client_id=".$client_id."&client_secret=".$client_secret."&redirect_uri=".$redirectURI."&code=".$code."&state=".$state;

  if(!$common_connect->validate($state, $throw = false))
  {
    echo "RESPONSE_UNAUTHORIZED";
    exit;
  }

  $is_post = false;
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, $is_post);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $headers = array();
  $response = curl_exec ($ch);
  $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  //echo "status_code:".$status_code."<br>";
  curl_close ($ch);
  if($status_code == 200) {
    //echo $response;
  } else {
    echo "Error 내용:".$response;
    exit;
  }

  $json = json_decode($response);

  $options = array('http' => array('header'  => "Authorization: Bearer ".$json->access_token));
  $xml = simplexml_load_string(file_get_contents("https://apis.naver.com/nidlogin/nid/getUserProfile.xml", false, stream_context_create($options)));
  if(!($xml->result->resultcode=="00"&&$xml->result->message=="success")) {
    echo "인증에 실패했습니다.";
    exit;
  } else {
    $login_email = $xml->response->email;
    $naver_id = $xml->response->id;
    $member_name = $xml->response->nickname;
    /*
    echo $xml->response->enc_id."<hr />";
    echo $xml->response->gender."<hr />";
    */
  }
?>


<!DOCTYPE html>
<html>
<head>
    <title>Facebook 회원가입</title>
</head>
<body>
<?
  
  $datetime = date("Y-m-d H:i:s");
  
  if($login_email == "" || $naver_id == "")
  {
      $common_connect -> Fn_javascript_back("正しく入力して下さい。");
  }
  
  //すでに登録されていれば、ログイン成功
  $sql = "SELECT member_id, member_name, flag_open FROM member where login_email='".$login_email."' and naver_id='".$naver_id."' " ;
  $arr_member = $common_dao->db_query_bind($sql, $arr_bind);

  if(!is_null($arr_member[0]))
  {
    if($arr_member[0]["flag_open"]=="1")
    {
      //ログイン日
      $db_insert = "update member set lastlogin_date='".$datetime."' ";
      $db_insert .= " where login_email='".$login_email."' ";
      $db_result = $common_dao->db_update($db_insert);
      
      //ログイン
      session_start();
      $_SESSION['member_id']=$arr_member[0]["member_id"];
      $_SESSION['member_name']=$arr_member[0]["member_name"];

      $common_connect-> Fn_javascript_move("로그인 되었습니다.", "/");
    }
    else
    {
      $common_connect -> Fn_javascript_back("사용금지된 아이디 입니다.");
    }
      
  }
  else
  {
    $common_connect -> Fn_javascript_move("일치하는 정보가 없습니다.\\r\\n회원가입을 해주세요", "/member/");
  }
  
?>

</body>

</html>