<?php   
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連

    date_default_timezone_set('Asia/Seoul');
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/mailer/PHPMailerAutoload.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMailer.php";
    $common_mailer = new CommonMailer(); //メール関連

    $datetime = date("Y-m-d H:i:s");
    
    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    if($login_email=="")
    {
        echo "정확한 이메일을 입력해주세요.";
        exit;
    }
    
    $sql = "SELECT login_email FROM member where login_email = '".$login_email."' ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        echo "이미 등록된 메일 입니다.";
        exit;
    }
    
    //1分以内同じメールで更新情報があるかをチェック（連続メール送信防止）
    $check_datetime = date("Y-m-d H:i:s" , strtotime("-1 minute"));
    $sql = "SELECT login_email FROM member_auth where login_email = '".$login_email."' and regi_date>='".$check_datetime."' ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        echo "인증번호를 이미 발송하였습니다.\r\n".$login_email." 을 확인하여 주십시요.";
        exit;
    }
    
    
    //既存会員データがあれば削除
    $db_insert = "Delete from member_auth ";
    $db_insert .= " where login_email = '".$login_email."' ";
    $db_result = $common_dao->db_update($db_insert);

    $temp_key = $common_connect-> Fn_random_num(5);

    $arr_db_field = array("temp_key");
    $db_insert = "insert into member_auth ( ";
    $db_insert .= " login_email, ";
    foreach($arr_db_field as $val)
    {
        $db_insert .= $val.", ";
    }
    $db_insert .= " regi_date ";
    $db_insert .= " ) values ( ";
    $db_insert .= " '".$login_email."', ";
    foreach($arr_db_field as $val)
    {
        $db_insert .= " '".$$val."', ";
    }
    $db_insert .= " '$datetime')";
    $db_result = $common_dao->db_update($db_insert);

    $limit_time = date("Y-m-d H:i:s" , strtotime("24 hour"));

    //Thank youメール
    if ($login_email != "")
    {
        $subject = " [오키나와 오박사]회원 가입을 위한 인증번호 발송입니다. ";
        

        $body = "";
        $body = file_get_contents("./mail/auth_mail.user.php");
        $body = str_replace("[temp_key]", $temp_key, $body);
        $body = str_replace("[limit_time]", $limit_time, $body);

    }

    $common_mailer->Fn_send_utf ($login_email,global_test_mail.$subject,$body,$global_mail_from,$global_send_mail);

    $common_connect -> Fn_email_log($login_email, global_test_mail.$common_connect->h($subject), $common_connect->h($body)); //メールログ

    //$common_mailer->Fn_send_utf ($global_send_mail,global_test_mail.$subject,$body,$global_mail_from,$global_send_mail);

?>