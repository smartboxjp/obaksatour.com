<?php   
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連

    date_default_timezone_set('Asia/Seoul');
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/mailer/PHPMailerAutoload.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMailer.php";
    $common_mailer = new CommonMailer(); //メール関連
?>
<!doctype html>
<html prefix="og: http://ogp.me/ns#">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">

    <title>회원가입</title>
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->

    <!--[if lt IE 8]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="../scripts/html5shiv.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>
<?
    $datetime = date("Y-m-d H:i:s");
    
    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
    
    if($member_name=="" || $login_email=="" || $temp_key=="" || $login_pw=="")
    {
        $common_connect-> Fn_javascript_back("필수항목을 입력해주세요.");
    }


    
    $sql = "SELECT login_email FROM member where login_email = '".$login_email."' ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $common_connect-> Fn_javascript_back("이미 등록된 메일 입니다.");
    }

    //24時間以内認証必要
    $check_datetime = date("Y-m-d H:i:s" , strtotime("-24 hours"));
    $sql = "SELECT login_email FROM member_auth where login_email = '".$login_email."' and temp_key = '".$temp_key."' and regi_date>='".$check_datetime."' ";
    $db_result = $common_dao->db_query_bind($sql);
    if(!$db_result)
    {
        $common_connect-> Fn_javascript_back("정확한 인증정보를 입력하세요.");
    }
    
    $login_pw = $common_connect-> Fn_generate_pw($login_pw);
    
    $flag_open=1;
    $arr_db_field = array("login_email", "login_pw", "member_login_cookie", "member_name", "flag_email", "flag_open");
    
    $db_insert = "insert into member ( ";
    $db_insert .= " member_id, ";
    foreach($arr_db_field as $val)
    {
        $db_insert .= $val.", ";
    }
    $db_insert .= " regi_date, up_date ";
    $db_insert .= " ) values ( ";
    $db_insert .= " '', ";
    foreach($arr_db_field as $val)
    {
        $db_insert .= " '".$$val."', ";
    }
    $db_insert .= " '$datetime', '$datetime')";
    $db_result = $common_dao->db_update($db_insert);
    

    //인증정보 삭제
    $db_insert = "Delete from member_auth ";
    $db_insert .= " where login_email = '".$login_email."' ";
    $db_result = $common_dao->db_update($db_insert);

    //로그인
    $sql = "SELECT member_id, member_name FROM member where login_email = '".$login_email."' ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $member_id = $db_result[0]["member_id"];
        $member_name = $db_result[0]["member_name"];
        session_start();
        $_SESSION['member_id']=$member_id;
        $_SESSION['member_name']=$member_name;
    }

    //Thank youメール
    if ($login_email != "")
    {
        $subject = " [오키나와 오박사]회원 가입을 축하 드립니다. ";


        $body = "";
        $body = file_get_contents($_SERVER['DOCUMENT_ROOT']."/member_regist/mail/regist_save.user.php");
        $body = str_replace("[login_email]", $login_email, $body);
        $body = str_replace("[member_name]", $member_name, $body);
        $body = str_replace("[datetime]", $datetime, $body);

    }
    $common_mailer->Fn_send_utf ($login_email,global_test_mail.$subject,$body,$global_mail_from,$global_send_mail);

    $common_connect -> Fn_email_log($login_email, global_test_mail.$common_connect->h($subject), $common_connect->h($body)); //メールログ

    $common_mailer->Fn_send_utf ($global_send_mail,global_test_mail.$subject,$body,$global_mail_from,$global_send_mail);

    $common_connect-> Fn_redirect(global_no_ssl."/member_regist/regist_thankyou.php");
?>

</body>

</html>
