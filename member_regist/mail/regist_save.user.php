<div id="wrapper" style="margin:30px auto 0;width:800px;font-family:'나눔고딕',NanumGothic;font-size:100%;color:#929292;">
    <div class="header" style="overflow:hidden;width:100%">
        <h1 style="float:left;font-size:0;background:url('http://clinkamp.com/images/h1_logo_obaksa.png') no-repeat left top;width:219px;height:82px"><span style="position:absolute;z-index:-1;visibility:hidden">오키나와 오박사</span></h1>
    </div>
    <!-- Content Container Start -->
    <div class="container" style="margin-top:25px">
        <!-- Header Notice Box -->
        <div style="padding:10px;background:#0272ba;letter-spacing:-0.1em;font-weight:600">
           <p style="font-size:25px;margin:0 8px;padding:10px;border:0px solid #d7d7d7;color:#ffffff;text-decoration:none; letter-spacing: 0.01em;">오키나와 오박사 <em style="color:#fece17;font-weight:bold;font-style:normal">회원가입 완료확인</em> 메일입니다.  </p>
           <p style="font-size:15px;margin:0 8px;padding:10px;border:0px solid #d7d7d7;color:#ffffff;text-decoration:none; letter-spacing: 0.01em;">안녕하세요 오키나와 오박사회원으로 가입해 주셔서 감사합니다. </p>
        </div>

        <!-- Content Start -->
        <div class="content">

            <!-- 이메일 컨텐츠  -->
            <div style="padding: 30px 0px 30px 35px; color:#929292;">안녕하세요, [member_name] 회원님 </div>

            <h2 style="margin:10px 0 10px;font-size:15px;font-weight:600;color:#646464;line-height:1">회원가입 완료 확인 </h2>
            <table cellpadding="0" width="100%" style="border-collapse:collapse;font-family:dotum;color:#929292;line-height:1;background:#fff">
                <tr>
                    <td width='100%' align='left' valign='top'>
                        <table width='100%' cellpadding='0' cellspacing='0' style='border-top: 2px solid #1752b0;font-family: dotum, 돋움, 돋움체, 맑은고딕, sans-serif; font-size: 15px; color: #333333; line-height: 1.4; margin: 0; padding: 0;'>
                            <tr>
                                <td height='15'></td>
                            </tr>
                            <tr>
                                <td align='left' valign='middle' width='100%' height='0'><font style='padding:0 0 0 10px; font-weight: bold; color: #555555;'>회원가입이 완료 되었습니다.</font></td>
                                </td>
                            </tr>
                            <tr>
                                <td height='15'></td>
                            </tr>
                            <tr>
                                <td width='100%' style='border-bottom: 1px solid #ccc;'></td>
                            </tr>
                            <tr>
                                <td height='100'><font style='padding: 15px 0px 15px 25px; font-size:15px; color: #929292;'>등록하신 정보는 로그인 하신 후 <em style="color:#0272ba;font-weight:bold;font-style:normal">마이페이지 > <a href="http://www.okinawaobaksa.com/member/info.php" target="_blank">회원정보 수정</a></em> 메뉴에서 변경하실 수 있습니다.</font> </td>
                            </tr>
                            <tr>
                                <td style='border-bottom: 1px solid #ccc;'></td>
                            </tr>
                            <tr>
                                <td height='15'></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
           <!--// 이메일 컨텐츠  -->
           <!--오키나와 오박사 소개문구  -->
            <div class="address" style="margin-top:10px; margin-bottom:50px; padding:0px 30px; font-size:12px; color:#929292;">
                <li style="line-height:2.0">오키나와 오박사는 <em style="color:#0272ba;font-weight:bold;font-style:normal">오키나와 자유여행</em>을 계획하시는 분들을 위한 서비스입니다.</li>
                <li style="line-height:2.0"><em style="color:#0272ba;font-weight:bold;font-style:normal">렌트카 및 숙박</em>뿐만 아니라 <em style="color:#0272ba;font-weight:bold;font-style:normal">가이드 투어, 버스 투어, 패키지 투어, 맞춤투어, 골프투어, 스노쿨링, 낚시, 다이빙, 맛집 등</em></li>
                <li style="line-height:2.0">오키나와 여행객이 현지 업체와 직거래로 연결을 하여 원하는 서비스를 직접 고를 수 있게 구성하였습니다.</li>
                <div style="line-height:2.0;padding: 15px 0px 0px 15px; font-size: 15px;">가성비를 찾으시는 합리적인 자유여행 문화!!  오키나와 오박사에서 찾으세요!!</divli>
                <div style="line-height:2.0">오키나와 오박사를 이용해 주셔셔 감사합니다.</div>
                <div style="line-height:2.0">더욱 편리한 서비스를 제공하기 위해 항상 최선을 다하겠습니다.</div>
            </div>
            <!--//오키나와 오박사 소개문구  -->

            <!-- Button -->
            <div style="margin-top:21px;padding:50px 0;text-align:center;font-size:19px;letter-spacing:-0.1em;font-weight:600">
                <a href="http://www.okinawaobaksa.com" target="_blank" style="margin:0 8px;padding:20px;border:1px solid #0272ba;background:#0272ba;color:#fff;text-decoration:none; letter-spacing: 0.1em;">오키나와 오박사 바로가기</a>
            </div>
        </div>
        <!--// End Content -->
    </div>
    <!--// End Content Container -->
    <!-- Footer Start -->
    <div class="footer" style="margin-top:30px">
        <!-- Notice -->
        <p style="margin:0;padding:9px;background:#f6f6f6;font-family:dotum;font-size:11px;color:#929292;border:1px solid #d7d7d7;letter-spacing:-0.06em"><em style="color:#0272ba;font-weight:bold;font-style:normal">!</em> 본 메일은 <em style="color:#0272ba;font-weight:bold;font-style:normal">발신전용</em> 메일입니다. 궁금하신 점이나 관련 문의는 <em style="color:#0272ba;font-weight:bold;font-style:normal">고객센터 / 1:1문의</em>를 이용하시기 바랍니다.</p>
        <!-- Address / Copyright -->
        <div class="address" style="margin-top:-1px;margin-bottom:50px;padding:0 30px;border:1px solid #d7d7d7;font-size:13px;">
                <div style="line-height:2.0">주식회사 스마트복스 &nbsp;ㅣ&nbsp;  이용문의 &#58; 평일 09 &#58; 00 &#126; 18 &#58; 00&#44;  토일요일 및 공휴일 휴무</div>
                <div style="line-height:2.0">Copyright &#9374; <a href="http://www.okinawaobaksa.com" target="_blank">www.okinawaobaksa.com</a> all right reserved&#46;</div>
            </ul>
        </div>
    </div>
    <!--// End Footer -->
</div>
