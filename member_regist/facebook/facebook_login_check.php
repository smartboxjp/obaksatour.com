<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/facebook_src/autoload.php";
    
   
    session_start();
    $fb = new Facebook\Facebook([
        'app_id' => MY_APP_ID, // Replace {app-id} with your app id
        'app_secret' => MY_APP_SECRET,
        'default_graph_version' => 'v2.2',
        'persistent_data_handler' => 'session',
    ]);

    $helper = $fb->getRedirectLoginHelper();

    try {
      $accessToken = $helper->getAccessToken();
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    if (! isset($accessToken)) {
      if ($helper->getError()) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Error: " . $helper->getError() . "\n";
        echo "Error Code: " . $helper->getErrorCode() . "\n";
        echo "Error Reason: " . $helper->getErrorReason() . "\n";
        echo "Error Description: " . $helper->getErrorDescription() . "\n";
      } else {
        header('HTTP/1.0 400 Bad Request');
        echo 'Bad request';
      }
      exit;
    }

    try {
      // Returns a `Facebook\FacebookResponse` object
      $response = $fb->get('/me?fields=id,name,email', $accessToken);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $user = $response->getGraphUser();
    
    $meta_title = "Facebookのログイン";
?>


<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?
  $meta_title = "로그인";
  $meta_description = "";
  require_once $_SERVER['DOCUMENT_ROOT']."/include/meta.php";
?>
</head>

<body>
<?

    try {
      // Returns a `Facebook\FacebookResponse` object
      $response = $fb->get('/me?fields=id,name,email', $accessToken);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $user = $response->getGraphUser();

    /*
    echo '<hr />id: ' . $user['id'];
    echo '<hr />Name: ' . $user['name'];
    echo '<hr />Email: ' . $user['email'];
    // OR
    // echo 'Name: ' . $user->getName();
    */
    $login_email = $user['email'];
    $facebook_id = $user["id"];

    
    $datetime = date("Y-m-d H:i:s");
    
    if($login_email == "" || $facebook_id == "")
    {
        $common_connect -> Fn_javascript_back("正しく入力して下さい。");
    }
    
    //すでに登録されていれば、ログイン成功
    $sql = "SELECT member_id, member_name, flag_open FROM member where login_email='".$login_email."' and facebook_id='".$facebook_id."' " ;
    $arr_member = $common_dao->db_query_bind($sql, $arr_bind);

    if(!is_null($arr_member[0]))
    {
      if($arr_member[0]["flag_open"]=="1")
      {
        //ログイン日
        $db_insert = "update member set lastlogin_date='".$datetime."' ";
        $db_insert .= " where login_email='".$login_email."' ";
        $db_result = $common_dao->db_update($db_insert);
        
        //ログイン
        session_start();
        $_SESSION['member_id']=$arr_member[0]["member_id"];
        $_SESSION['member_name']=$arr_member[0]["member_name"];

        $common_connect-> Fn_javascript_move("로그인 되었습니다.", "/");
      }
      else
      {
          $common_connect -> Fn_javascript_back("사용금지된 아이디 입니다.");
      }
        
    }
    else
    {
        $common_connect -> Fn_javascript_move("일치하는 정보가 없습니다.\\r\\n회원가입을 해주세요", "/member/");
    }
    
?>

</body>

</html>