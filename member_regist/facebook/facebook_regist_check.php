<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/mailer/PHPMailerAutoload.php";
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMailer.php";
  $common_mailer = new CommonMailer(); //メール関連
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/facebook_src/autoload.php";
	

  session_start();
  $fb = new Facebook\Facebook([
      'app_id' => MY_APP_ID, // Replace {app-id} with your app id
      'app_secret' => MY_APP_SECRET,
      'default_graph_version' => 'v2.9',
      'persistent_data_handler' => 'session',
  ]);

  $helper = $fb->getRedirectLoginHelper();

  try {
    $accessToken = $helper->getAccessToken();
  } catch(Facebook\Exceptions\FacebookResponseException $e) {
    // When Graph returns an error
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
  } catch(Facebook\Exceptions\FacebookSDKException $e) {
    // When validation fails or other local issues
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
  }

  if (! isset($accessToken)) {
    if ($helper->getError()) {
      header('HTTP/1.0 401 Unauthorized');
      echo "Error: " . $helper->getError() . "\n";
      echo "Error Code: " . $helper->getErrorCode() . "\n";
      echo "Error Reason: " . $helper->getErrorReason() . "\n";
      echo "Error Description: " . $helper->getErrorDescription() . "\n";
    } else {
      header('HTTP/1.0 400 Bad Request');
      echo 'Bad request';
    }
    exit;
  }

/*
// Logged in
echo '<h3>Access Token</h3>';
var_dump($accessToken->getValue());

// The OAuth 2.0 client handler helps us manage access tokens
$oAuth2Client = $fb->getOAuth2Client();

// Get the access token metadata from /debug_token
$tokenMetadata = $oAuth2Client->debugToken($accessToken);
echo '<h3>Metadata</h3>';
var_dump($tokenMetadata);

// Validation (these will throw FacebookSDKException's when they fail)
$tokenMetadata->validateAppId(MY_APP_ID); // Replace {app-id} with your app id
// If you know the user ID this access token belongs to, you can validate it here
//$tokenMetadata->validateUserId('123');
$tokenMetadata->validateExpiration();

if (! $accessToken->isLongLived()) {
  // Exchanges a short-lived access token for a long-lived one
  try {
    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
  } catch (Facebook\Exceptions\FacebookSDKException $e) {
    echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
    exit;
  }

  echo '<h3>Long-lived</h3>';
  print_r($accessToken->getValue());
}

$_SESSION['fb_access_token'] = (string) $accessToken;

// User is logged in with a long-lived access token.
// You can redirect them to a members-only page.
//header('Location: https://example.com/members.php');

*/
    try {
      // Returns a `Facebook\FacebookResponse` object
      $response = $fb->get('/me?fields=id,name,email', $accessToken);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $user = $response->getGraphUser();

    /*
    echo '<hr />id: ' . $user['id'];
    echo '<hr />Name: ' . $user['name'];
    echo '<hr />Email: ' . $user['email'];
    // OR
    // echo 'Name: ' . $user->getName();
    */
	$login_email = $user['email'];
  $facebook_id = $user["id"];
  $member_name = $user["name"];

?>

<!DOCTYPE html>
<html>
<head>
    <title>Facebook 회원가입</title>
</head>
<body>
<?
	$datetime = date("Y-m-d H:i:s");
	
	if($login_email == "" || $facebook_id == "")
	{
		$common_connect -> Fn_javascript_back("정확히 입력해 주세요");
	}
	
  $sql = "SELECT login_email FROM member where login_email = '".$login_email."' ";
  $db_result = $common_dao->db_query_bind($sql);
  if($db_result)
  {
      $common_connect-> Fn_javascript_move("이미 등록된 메일 입니다.\\r\\n로그인 해주세요.", "/member/");
  }

	
	//すでに登録されていれば、ログイン成功
  $where = " and facebook_id='".$facebook_id."' ";
  $sql = "SELECT login_email FROM member where 1 ".$where ;
  $arr_member = $common_dao->db_query_bind($sql);


	//facebook_idで一致してるかをチェック
	if($db_result)
	{
		if($login_email == $arr_member[0]["login_email"])
		{
			$common_connect -> Fn_javascript_move("이미 등록된 메일입니다.\\r\\n로그인 해주세요.", "/member/");
		}
		else
		{
			$common_connect -> Fn_javascript_move("메일이 정보가 일치하지 않습니다.\\r\\nfacebook 정보가 변경되었다면 로그인 후 메일을 수정해주세요.", "/member/");
		}
	}
	//データがなければ会員登録をさせる
	else
	{
		
		$login_pw = $common_connect-> Fn_random_password(8);
    $login_pw = $common_connect-> Fn_generate_pw($login_pw);
		
		//仮登録を行う
    $flag_open=1;
    $arr_db_field = array("login_email", "login_pw", "facebook_id", "member_name", "flag_open");
    
    $db_insert = "insert into member ( ";
    $db_insert .= " member_id, ";
    foreach($arr_db_field as $val)
    {
        $db_insert .= $val.", ";
    }
    $db_insert .= " regi_date, up_date ";
    $db_insert .= " ) values ( ";
    $db_insert .= " '', ";
    foreach($arr_db_field as $val)
    {
        $db_insert .= " '".$$val."', ";
    }
    $db_insert .= " '$datetime', '$datetime')";
    $db_result = $common_dao->db_update($db_insert);
		
    //로그인
    $sql = "SELECT member_id, member_name FROM member where login_email = '".$login_email."' ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $member_id = $db_result[0]["member_id"];
        $member_name = $db_result[0]["member_name"];
        session_start();
        $_SESSION['member_id']=$member_id;
        $_SESSION['member_name']=$member_name;
    }

    //Thank youメール
    if ($login_email != "")
    {
        $subject = " [오키나와 오박사]회원 가입을 축하 드립니다. ";


        $body = "";
        $body = file_get_contents($_SERVER['DOCUMENT_ROOT']."/member_regist/mail/regist_save.user.php");
        $body = str_replace("[login_email]", $login_email, $body);
        $body = str_replace("[member_name]", $member_name, $body);
        $body = str_replace("[datetime]", $datetime, $body);

    }
    $common_mailer->Fn_send_utf ($login_email,global_test_mail.$subject,$body,$global_mail_from,$global_send_mail);

    $common_connect -> Fn_email_log($login_email, global_test_mail.$common_connect->h($subject), $common_connect->h($body)); //メールログ

    $common_mailer->Fn_send_utf ($global_send_mail,global_test_mail.$subject,$body,$global_mail_from,$global_send_mail);

    $common_connect-> Fn_redirect(global_no_ssl."/member_regist/regist_thankyou.php?login_email=".$login_email);
	}
	
?>

</body>

</html>