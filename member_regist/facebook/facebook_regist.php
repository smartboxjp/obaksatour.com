<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/facebook_src/autoload.php";

    $redirect_uri = "http://".$_SERVER['SERVER_NAME']."/member_regist/facebook/facebook_regist_check.php";


    session_start();
    $_SESSION["facebook"] = "facebook";
    $fb = new Facebook\Facebook([
      'app_id' => MY_APP_ID, // Replace {app-id} with your app id
      'app_secret' => MY_APP_SECRET,
      'default_graph_version' => 'v2.2',
      'persistent_data_handler' => 'session',
      ]);

    $helper = $fb->getRedirectLoginHelper();

    $permissions = ['email']; // Optional permissions
    $loginUrl = $helper->getLoginUrl($redirect_uri, $permissions);

    //echo '<a href="' . htmlspecialchars($loginUrl) . '">ログイン</a>';

    header('location: '. $loginUrl);
?>