<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連
  
  foreach($_GET as $key => $value)
  { 
    $$key = $common_connect->h($value);
  }
?>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?
  $meta_title = "회원가입완료";
  $meta_description = "";
  require_once $_SERVER['DOCUMENT_ROOT']."/include/meta.php";
?>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/member_regist/css/regist_thankyou.css" rel="stylesheet">

</head>
<body>
<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

  <div class="l-container">
    <div class="pankuzu-wapper">
      <ul class="pankuzu">
        <li><a href="/">오키나와오박사 홈</a></li>
        <li>회원가입완료</li>
      </ul>
    </div>
    <!-- /.pankuzu-wapper -->
  </div>
  <!-- /.l-container -->


  <div class="l-container clearfix">
    <h1 class="headline-glay fsize-lg left-bdr mb-30"><span class="color-blue">회원가입완료</span></h1>
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <div class="table-responsive">
      <form action="#">
        <table class="table table-bordered">
          <tbody>
            <tr>
              <td class="main-board">

                <ul class="stepnav stepnav-now3">
                  <li class="stepnav-after"><span>이용약관 동의</span></li>
                  <li class="stepnav-after"><span>회원정보 입력</span></li>
                  <li class="stepnav-current"><span>회원가입 완료</span></li>
                </ul>
                <h1 class="headline-glay fsize-lg mb-20">회원가입이 <span class="color-lightblue"> 성공적으로 완료 되었습니다.</span></h1>
                <p>회원가입하신 분들에게 <span class="color-blue">특별한 상품을 소개드립니다.</span><br></p>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

</div>
<!-- /.contents -->

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>