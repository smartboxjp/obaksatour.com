<?php

class CommonAdmin extends CommonDao{
	
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
	
	
	//ログイン
	public function Fn_db_admin_login_cookie ($login_cookie, $arr_data, $arr_where=null) 
	{ 
		$arr_bind = array();
		
		$where = " and login_cookie=:login_cookie ";
		$arr_bind["login_cookie"] = $login_cookie;
		
		$var = "flag_open";
		if($arr_where[$var]!="all")
		{
			$arr_bind[$var] = 1;
			$where .= " and flag_open=:".$var;
		}
		if(!is_null($arr_where)) { 
			foreach($arr_where as $value) {
				if($value!="")
				{
					$arr_bind[$var] = $arr_where[$var];
					$where .= " and ".$var."=:".$var;
				}
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM admin where 1 ".$where ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//ログイン
	public function Fn_admin_login ($arr_where) 
	{ 
		$arr_bind = array();
		
		$var = "admin_login";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$where .= " and admin_login = :".$var." ";
		}
		
		$var = "admin_pw";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$where .= " and admin_pw=:".$var;
		}
		$arr_db_field = array("admin_name", "admin_id", "admin_login", "flag_open", "admin_level");
		
		
		$sql = "SELECT ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM admin where 1 ".$where ;
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//本登録
	//auto login
	public function Fn_admin_update ($arr_data, $arr_where) 
	{ 
		$arr_bind = array();
		$where = "";
		$datetime = date("Y-m-d H:i:s");
		
		if(!is_null($arr_data))
		{
			$db_up = "update admin set ";
			foreach($arr_data as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$db_up .= $key."= :".$key.", ";
			}
			$db_up .= " up_date= '".$datetime."' ";
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
			$db_up .= " where 1  ".$where;

			$db_result = $this->db_update($db_up, $arr_bind);

			return true;
		}
		
		return false;
	} 
}


?>
