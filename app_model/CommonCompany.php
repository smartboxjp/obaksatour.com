<?php

class CommonCompany extends CommonDao{
	
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
	
	///kanto/search/search_result.php　のみ使用
	public function Fn_company_list_index($arr_data, $arr_where=null, $arr_where_not=null, $arr_etc=null, $arr_where_area_s_id=null, $arr_where_job_id=null) 
	{ 
		$arr_bind = array();
		$where = "";
		
		//エリア
		$where_area = "";
		if(!is_null($arr_where_area_s_id)) { 
			foreach($arr_where_area_s_id as $key=>$value) {
				$where_area .= ",".$value;
			}
		}
		if($where_area != "") {
			$where .= " and s.cate_area_s_id in (".substr($where_area, 1).")";
		}
		
		//職種
		$where_job = "";
		if(!is_null($arr_where_job_id)) { 
			foreach($arr_where_job_id as $key=>$value) {
				$where_job .= ",".$value;
			}
		}
		if($where_job != "") {
			$where .= " and s.cate_job_id in (".substr($where_job, 1).")";
		}
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value) {
				if($key=="s_keyword")
				{
					$var_word_1 = "s_word_1";
					$arr_bind[$var_word_1] = '%'.$arr_where["s_keyword"].'%';
					$where .= " and s.company_name collate utf8_unicode_ci like :".$var_word_1." ";
				}
				elseif($key=="voice_count")
				{
					if($arr_where[$key]>0) { 
						$arr_bind[$key] = $arr_where[$key];
						$where .= " and (select count(voice_id) from voice where voice.flag_open=1 and voice.company_id=s.company_id) >=:".$key;
					}
				}
				elseif($key=="ep")
				{
					if($arr_where[$key]>0) { 
						$arr_bind[$key] = $arr_where[$key];
						$where .= " and evaluation >=:".$key;
					}
				}
				elseif($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."=:".$key;
				}
			}
		}
		
		if(!is_null($arr_where_not)) { 
			foreach($arr_where_not as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."!=:".$key;
				}
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " (select count(voice_id) from voice where voice.flag_open=1 and voice.company_id=s.company_id) as voice_count, ";
		
		if(!is_null($arr_etc["order_name"])) {
			if($arr_etc["order_name"]=="voice_regi_date") {
				$sql .= " (select regi_date from voice where voice.flag_open=1 and voice.company_id=s.company_id order by regi_date desc limit 0, 1) as voice_regi_date, ";
			}
		}
		
		$sql .= " 1 FROM company s where 1 ".$where ;
		if(!is_null($arr_etc["order_name"]))
		{
			$sql .= " order by ".$arr_etc["order_name"]." ".$arr_etc["order"];
		}
		else
		{
			$sql .= " order by s.company_view_level, s.regi_date desc";
		}
		if(!is_null($arr_etc["offset"]) && !is_null($arr_etc["view_count"]))
		{
			$sql .= " limit ".$arr_etc["offset"].", ".$arr_etc["view_count"];	
		}
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	
	///kanto/search/search_result.php　のみ使用
	public function Fn_company_count_index($arr_where=null, $arr_where_not=null, $arr_where_area_s_id=null, $arr_where_job_id=null) 
	{
		$arr_bind = array();
		$where = "";
		
		//エリア
		$where_area = "";
		if(!is_null($arr_where_area_s_id)) { 
			foreach($arr_where_area_s_id as $key=>$value) {
				$where_area .= ",".$value;
			}
		}
		if($where_area != "") {
			$where .= " and s.cate_area_s_id in (".substr($where_area, 1).")";
		}
		
		//職種
		$where_job = "";
		if(!is_null($arr_where_job_id)) { 
			foreach($arr_where_job_id as $key=>$value) {
				$where_job .= ",".$value;
			}
		}
		if($where_job != "") {
			$where .= " and s.cate_job_id in (".substr($where_job, 1).")";
		}
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value) {
				if($key=="s_keyword")
				{
					$var_word_1 = "s_word_1";
					$arr_bind[$var_word_1] = '%'.$arr_where["s_keyword"].'%';
					$where .= " and s.company_name collate utf8_unicode_ci like :".$var_word_1." ";
				}
				elseif($key=="voice_count")
				{
					if($arr_where[$key]>0) { 
						$arr_bind[$key] = $arr_where[$key];
						$where .= " and (select count(voice_id) from voice where voice.flag_open=1 and voice.company_id=s.company_id) >=:".$key;
					}
				}
				elseif($key=="ep")
				{
					if($arr_where[$key]>0) { 
						$arr_bind[$key] = $arr_where[$key];
						$where .= " and evaluation >=:".$key;
					}
				}
				elseif($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."=:".$key;
				}
			}
		}
		
		if(!is_null($arr_where_not)) { 
			foreach($arr_where_not as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."!=:".$key;
				}
			}
		}
		
		
		$sql = " SELECT count(company_id) as all_count FROM company s where 1 ".$where ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	
	
	//like検索
	public function Fn_company_all_count ($arr_where) 
	{ 
		$arr_bind = array();
		
		$var = "s_keyword";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$var_word_1 = "s_word_1";
			$arr_bind[$var_word_1] = '%'.$arr_where[$var].'%';
			$var_word_2 = "s_word_2";
			$arr_bind[$var_word_2] = '%'.$arr_where[$var].'%';
			$var_word_3 = "s_word_3";
			$arr_bind[$var_word_3] = '%'.$arr_where[$var].'%';
			$where .= " and (company_name collate utf8_unicode_ci like :".$var_word_1." or company_kana collate utf8_unicode_ci like :".$var_word_2." or company_title collate utf8_unicode_ci like :".$var_word_3." or company_id = :".$var." ) ";
		}
		
		$var = "s_flag_open";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$where .= " and flag_open=:".$var;
		}
		$sql = " SELECT count(company_id) as all_count FROM company where 1 ".$where ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	public function Fn_company_all_list ($arr_data, $arr_where, $arr_etc) 
	{ 
		$arr_bind = array();
		
		$var = "s_keyword";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$var_word_1 = "s_word_1";
			$arr_bind[$var_word_1] = '%'.$arr_where[$var].'%';
			$var_word_2 = "s_word_2";
			$arr_bind[$var_word_2] = '%'.$arr_where[$var].'%';
			$var_word_3 = "s_word_3";
			$arr_bind[$var_word_3] = '%'.$arr_where[$var].'%';
			$where .= " and (company_name collate utf8_unicode_ci like :".$var_word_1." or company_kana collate utf8_unicode_ci like :".$var_word_2." or company_title collate utf8_unicode_ci like :".$var_word_3." or company_id = :".$var." ) ";
		}
		
		$var = "s_flag_open";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$where .= " and flag_open=:".$var;
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM company where 1 ".$where ;
		if(!is_null($arr_etc["order_name"]))
		{
			$sql .= " order by ".$arr_etc["order_name"]." ".$arr_etc["order"];
		}
		else
		{
			$sql .= " order by regi_date desc";
		}
		if(!is_null($arr_etc["offset"]) && !is_null($arr_etc["view_count"]))
		{
			$sql .= " limit ".$arr_etc["offset"].", ".$arr_etc["view_count"];	
		}
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	
	public function Fn_db_company ($company_id, $arr_where=null) 
	{ 
	/*
		$arr_flag_open[1] = "公開";
		$arr_flag_open[99] = "非公開";
	*/
	
		$arr_bind = array();
		
		$where = " and company_id=:company_id ";
		$arr_bind["company_id"] = $company_id;
		
		//リスト表示
		$arr_db_field = array("company_id", "company_name", "company_kana", "company_pw", "cate_area_s_id", "area_detail", "cate_job_id");
		$arr_db_field = array_merge($arr_db_field, array("catchcopy", "company_title", "company_address", "company_tel", "job_comment", "job_salary_pre"));
		$arr_db_field = array_merge($arr_db_field, array("job_price", "job_salary", "job_style", "job_years_from", "job_years_to"));
		$arr_db_field = array_merge($arr_db_field, array("job_time_from", "job_time_to", "job_special", "hp_job", "hp_offical", "penalty", "company_pr", "job_url"));
		$arr_db_field = array_merge($arr_db_field, array("email_kanri", "email_user", "open_time_from", "open_time_to", "company_thumbnail", "pr_comment"));
		$arr_db_field = array_merge($arr_db_field, array("line_id", "img_1", "regi_date", "up_date", "user_agent", "login_cookie", "flag_open"));

		$sql = "SELECT ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM company where 1 ".$where ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	public function Fn_db_company_data ($arr_data, $arr_where=null, $arr_where_not=null) 
	{ 
		$arr_bind = array();
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."=:".$key;
				}
			}
		}
		
		if(!is_null($arr_where_not)) { 
			foreach($arr_where_not as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."!=:".$key;
				}
			}
		}
		
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM company where 1 ".$where ;


		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//ログイン
	public function Fn_db_company_login_cookie ($login_cookie, $arr_data, $arr_where=null) 
	{ 
		$arr_bind = array();
		
		$where = " and login_cookie=:login_cookie ";
		$arr_bind["login_cookie"] = $login_cookie;
		
		$var = "flag_open";
		if($arr_where[$var]!="all")
		{
			$arr_bind[$var] = 1;
			$where .= " and flag_open=:".$var;
		}
		if(!is_null($arr_where)) { 
			foreach($arr_where as $value) {
				if($value!="")
				{
					$arr_bind[$var] = $arr_where[$var];
					$where .= " and ".$var."=:".$var;
				}
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM company where 1 ".$where ;


		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	
	//サブイメージ
	public function Fn_db_company_img($company_id, $arr_where=null)  
	{ 
		$arr_bind = array();
		
		$var = "s_flag_open";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$where .= " and flag_open=:".$var;
		}
		
		$where = " and company_id=:company_id ";
		
		//リスト表示
		$arr_db_field = array("company_img_id", "company_id", "img", "comment");

		$sql = "SELECT ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM company_img where 1 ".$where ;
		$sql .= " order by view_level, up_date desc ";

		$arr_bind = array();
		$arr_bind["company_id"] = $company_id;
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//自動番号
	/*
	public function Fn_db_company_auto()  
	{ 
		$sql = "select last_insert_id() as last_id " ;
		$sql .= " order by view_level, up_date desc ";

		$db_result = $this->db_query_bind($sql, null);
		return $db_result;
	} 
	*/
	
	//auto login
	public function Fn_company_update ($arr_data, $arr_where) 
	{ 
		$arr_bind = array();
		$where = "";
		$datetime = date("Y-m-d H:i:s");
		
		if(!is_null($arr_data))
		{
			$db_up = "update company set ";
			foreach($arr_data as $key=>$value)
			{
				$arr_bind[$key] = $value;
				$db_up .= $key."= :".$key.", ";
			}
			$db_up .= " up_date= '".$datetime."' ";
			foreach($arr_where as $key=>$value)
			{
				$arr_bind[$key] = $value;
				$where .= "and ".$key."= :".$key." ";
			}
			$db_up .= " where 1  ".$where;

			$db_result = $this->db_update($db_up, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	//登録
	//auto login
	public function Fn_company_insert ($arr_data) 
	{ 
		$arr_bind = array();
		$datetime = date("Y-m-d H:i:s");
		
		if(!is_null($arr_data))
		{
			$db_insert = "insert into company ( ";
			foreach($arr_data as $key=>$value)
			{
				$db_insert .= $key.", ";
			}
			$db_insert .= " regi_date, up_date ) values ( ";
			foreach($arr_data as $key=>$value)
			{
				$arr_bind[$key] = $value;
				$db_insert .= ":".$key.", ";
			}
			$db_insert .= " '".$datetime."', '".$datetime."')  ";
			

			$db_result = $this->db_update($db_insert, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	public function Fn_company_delete ($company_id) 
	{ 
		if($company_id!="")
		{
			$db_del = "Delete from company where ";
			$db_del .= " company_id= :company_id ";
			$arr_bind["company_id"] = $company_id;

			$db_result = $this->db_update($db_del, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	public function Fn_flag_open() {
		$arr_return["1"] = "公開";
		$arr_return["99"] = "非公開";
		
		return $arr_return;
	}
	
}


?>
