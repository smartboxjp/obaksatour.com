<?php

class CommonVoice extends CommonDao{
	
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
	
	///kanto/voice/index.php　のみ使用
	public function Fn_voice_list_index($arr_data, $arr_where=null, $arr_where_not=null, $arr_etc=null, $arr_where_area_s_id=null, $arr_where_job_id=null) 
	{ 
		$arr_bind = array();
		$where = "";
		
		//エリア
		$where_area = "";
		if(!is_null($arr_where_area_s_id)) { 
			foreach($arr_where_area_s_id as $key=>$value) {
				$where_area .= ",".$value;
			}
		}
		if($where_area != "") {
			$where .= " and v.cate_area_s_id in (".substr($where_area, 1).")";
		}
		
		//職種
		$where_job = "";
		if(!is_null($arr_where_job_id)) { 
			foreach($arr_where_job_id as $key=>$value) {
				$where_job .= ",".$value;
			}
		}
		if($where_job != "") {
			$where .= " and v.cate_job_id in (".substr($where_job, 1).")";
		}
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value) {
				if($key=="s_keyword")
				{
					$var_word_1 = "s_word_1";
					$arr_bind[$var_word_1] = '%'.$arr_where["s_keyword"].'%';
					$where .= " and s.company_name collate utf8_unicode_ci like :".$var_word_1." ";
				}
				if($key=="voice_star")
				{
					$arr_bind[$key] = $value;
					$where .= " and voice_star>=:".$key." ";
				}
				elseif($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."=:".$key;
				}
			}
		}
		
		if(!is_null($arr_where_not)) { 
			foreach($arr_where_not as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."!=:".$key;
				}
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM company_voice v inner join company s on s.company_id=v.company_id where 1 ".$where ;
		if(!is_null($arr_etc["order_name"]))
		{
			$sql .= " order by ".$arr_etc["order_name"]." ".$arr_etc["order"];
		}
		else
		{
			$sql .= " order by v.regi_date desc";
		}
		if(!is_null($arr_etc["offset"]) && !is_null($arr_etc["view_count"]))
		{
			$sql .= " limit ".$arr_etc["offset"].", ".$arr_etc["view_count"];	
		}
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	
	
	
	
	//user page
	public function Fn_voice_user_count ($arr_where=null, $arr_where_not=null) 
	{ 
		$arr_bind = array();
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."=:".$key;
				}
			}
		}
		
		if(!is_null($arr_where_not)) { 
			foreach($arr_where_not as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."!=:".$key;
				}
			}
		}
		
		$sql = " SELECT count(voice_id) as all_count FROM company_voice where 1 ".$where ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	public function Fn_voice_user_list ($arr_data, $arr_where=null, $arr_where_not=null, $arr_etc=null) 
	{ 
		$arr_bind = array();
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value) {
				$arr_bind[$key] = $value;
				$where .= " and ".$key."=:".$key;
			}
		}
		
		if(!is_null($arr_where_not)) { 
			foreach($arr_where_not as $key=>$value) {
				$arr_bind[$key] = $value;
				$where .= " and ".$key."!=:".$key;
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM company_voice where 1 ".$where ;
		if(!is_null($arr_etc["order_name"]))
		{
			$sql .= " order by ".$arr_etc["order_name"]." ".$arr_etc["order"];
		}
		else
		{
			$sql .= " order by regi_date desc";
		}
		if(!is_null($arr_etc["offset"]) && !is_null($arr_etc["view_count"]))
		{
			$sql .= " limit ".$arr_etc["offset"].", ".$arr_etc["view_count"];	
		}
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	
	
	public function Fn_voice_list ($arr_data, $arr_where=null, $arr_where_not=null, $arr_etc=null) 
	{ 
		$arr_bind = array();
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value) {
				if($key=="s_keyword")
				{
					$var_word_1 = "s_word_1";
					$arr_bind[$var_word_1] = '%'.$arr_where["s_keyword"].'%';
					$where .= " and s.company_name collate utf8_unicode_ci like :".$var_word_1." ";
				}
				elseif($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."=:".$key;
				}
			}
		}
		
		if(!is_null($arr_where_not)) { 
			foreach($arr_where_not as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."!=:".$key;
				}
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM company_voice v inner join company s on s.company_id=v.company_id where s.flag_open=1 and v.flag_open=1  ".$where ;
		if(!is_null($arr_etc["order_name"]))
		{
			$sql .= " order by ".$arr_etc["order_name"]." ".$arr_etc["order"];
		}
		else
		{
			$sql .= " order by v.regi_date desc";
		}
		if(!is_null($arr_etc["offset"]) && !is_null($arr_etc["view_count"]))
		{
			$sql .= " limit ".$arr_etc["offset"].", ".$arr_etc["view_count"];	
		}
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	
	
	//like検索
	public function Fn_voice_all_count ($arr_where) 
	{ 
		$arr_bind = array();
		
		$var = "s_keyword";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$var_word_1 = "s_word_1";
			$arr_bind[$var_word_1] = '%'.$arr_where[$var].'%';
			$var_word_2 = "s_word_2";
			$arr_bind[$var_word_2] = '%'.$arr_where[$var].'%';
			$var_word_3 = "s_word_3";
			$arr_bind[$var_word_3] = '%'.$arr_where[$var].'%';
			$var_word_4 = "s_word_4";
			$arr_bind[$var_word_4] = '%'.$arr_where[$var].'%';
			$where .= " and (voice_title collate utf8_unicode_ci like :".$var_word_1." or voice_comment_1 collate utf8_unicode_ci like :".$var_word_2." or voice_comment_2 collate utf8_unicode_ci like :".$var_word_3." or voice_comment_3 collate utf8_unicode_ci like :".$var_word_4." or voice_id = :".$var." ) ";
		}
		
		$var = "s_flag_open";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$where .= " and flag_open=:".$var;
		}
		$sql = " SELECT count(voice_id) as all_count FROM company_voice where 1 ".$where ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	public function Fn_voice_all_list ($arr_data, $arr_where, $arr_etc) 
	{ 
		$arr_bind = array();
		
		$var = "s_keyword";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$var_word_1 = "s_word_1";
			$arr_bind[$var_word_1] = '%'.$arr_where[$var].'%';
			$var_word_2 = "s_word_2";
			$arr_bind[$var_word_2] = '%'.$arr_where[$var].'%';
			$var_word_3 = "s_word_3";
			$arr_bind[$var_word_3] = '%'.$arr_where[$var].'%';
			$var_word_4 = "s_word_4";
			$arr_bind[$var_word_4] = '%'.$arr_where[$var].'%';
			$where .= " and (voice_title collate utf8_unicode_ci like :".$var_word_1." or voice_comment_1 collate utf8_unicode_ci like :".$var_word_2." or voice_comment_2 collate utf8_unicode_ci like :".$var_word_3." or voice_comment_3 collate utf8_unicode_ci like :".$var_word_4." or voice_id = :".$var." ) ";
		}
		
		$var = "s_flag_open";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$where .= " and flag_open=:".$var;
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM company_voice where 1 ".$where ;
		if(!is_null($arr_etc["order_name"]))
		{
			$sql .= " order by ".$arr_etc["order_name"]." ".$arr_etc["order"];
		}
		else
		{
			$sql .= " order by regi_date desc";
		}
		if(!is_null($arr_etc["offset"]) && !is_null($arr_etc["view_count"]))
		{
			$sql .= " limit ".$arr_etc["offset"].", ".$arr_etc["view_count"];	
		}
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//登録
	public function Fn_voice_insert ($arr_data) 
	{ 
		$arr_bind = array();
		$datetime = date("Y-m-d H:i:s");
		
		if(!is_null($arr_data))
		{
			$db_insert = "insert into company_voice ( voice_id, ";
			foreach($arr_data as $key=>$value)
			{
				$db_insert .= $key.", ";
			}
			$db_insert .= " regi_date, up_date ) values ( '', ";
			foreach($arr_data as $key=>$value)
			{
				$arr_bind[$key] = $value;
				$db_insert .= ":".$key.", ";
			}
			$db_insert .= " '".$datetime."', '".$datetime."')  ";
			
			$db_result = $this->db_update($db_insert, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	//信憑性更新
	public function Fn_voice_ranking_up ($company_id) 
	{
		//既存の平均点
		$sql = "SELECT evaluation FROM company WHERE company_id='".$company_id."' ";
		$result_evaluation = $this->db_query_bind($sql);
		$before_evaluation = $result_evaluation[0]["evaluation"];

		$arr_ranking_count = array();
		$all_account = 0;
		$sql = "SELECT voice_id, ranking, voice_star FROM company_voice WHERE company_id=:company_id and flag_open=1 order by regi_date ";
		$arr_bind = array();
		$arr_bind["company_id"] = $company_id;
		$result_ranking = $this->db_query_bind($sql, $arr_bind);
		
		foreach($result_ranking as $arr_key=>$arr_value)
		{
			foreach($arr_value as $key=>$value)
			{
				$$key=$value;
			}
			$arr_ranking[$ranking] += $voice_star;
			$arr_ranking_count[$ranking] += 1;
		}
		
		//総カウントが５人の計算に無いと点数は動かない
		//信憑性大
		$all_account += $arr_ranking_count[50];
		
		//信憑性小
		//2件以上のみ計算スタート
		if($arr_ranking_count[40]>=2)
		{
			//2の倍数のみ計算
			if(($arr_ranking_count[40]%2)==0)
			{
				$all_account += $arr_ranking_count[40]/2;
			}
			else
			{
				$all_account += ($arr_ranking_count[40]-($arr_ranking_count[40]%2))/2;
			}
		}
		//イマイチ
		//3件以上のみ計算スタート
		if($arr_ranking_count[30]>=3)
		{
			if(($arr_ranking_count[30]%3)==0)
			{
				$all_account += $arr_ranking_count[30]/3;
			}
			else
			{
				$all_account += ($arr_ranking_count[30]-(($arr_ranking_count[30]%3)))/3;
			}
		}
		
		//口コミの件数が５件以上かをチェック
		if($all_account>=5)
		{
			//信憑性大
			$ranking_5 = ($arr_ranking[50]/2)/$all_account;
			
			//信憑性小
			//2件以上のみ計算スタート
			if($arr_ranking_count[40]>=2)
			{
				//2の倍数のみ計算
				if(($arr_ranking_count[40]%2)==0)
				{
					$ranking_4 = (($arr_ranking[40]/2)/2)/$all_account;
				}
				else
				{
					$arr_ranking_40 = array();
					$arr_ranking_count_40 = array();
					$sql = "SELECT voice_id, ranking, voice_star FROM company_voice WHERE company_id=:company_id and ranking=:ranking and flag_open=1 order by regi_date limit 0, :view";
					$arr_bind = array();
					$arr_bind["company_id"] = $company_id;
					$arr_bind["ranking"] = 40;
					$arr_bind["view"] = $arr_ranking_count[40]-1;
					$result_ranking_40 = $this->db_query_bind($sql, $arr_bind);
					
					foreach($result_ranking_40 as $arr_key=>$arr_value)
					{
						foreach($arr_value as $key=>$value)
						{
							$$key=$value;
						}
						$arr_ranking_40[$ranking] += $voice_star;
						$arr_ranking_count_40[$ranking] += 1;
					}
					$ranking_4 = (($arr_ranking[40]/2)/2)/$all_account;
				}
			}
			
			//イマイチ
			//3件以上のみ計算スタート
			if($arr_ranking_count[30]>=3)
			{
				//3の倍数のみ計算
				if(($arr_ranking_count[30]%3)==0)
				{
					$ranking_3 = (($arr_ranking[30]/2)/3)/$all_account;
				}
				else
				{
					$arr_ranking_30 = array();
					$arr_ranking_count_30 = array();
					$sql = "SELECT voice_id, ranking, voice_star FROM company_voice WHERE company_id=:company_id and ranking=:ranking and flag_open=1 order by regi_date limit 0, :view";
					$arr_bind = array();
					$arr_bind["company_id"] = $company_id;
					$arr_bind["ranking"] = 30;
					$arr_bind["view"] = $arr_ranking_count[30]-($arr_ranking_count[30]%3);
					$result_ranking_30 = $this->db_query_bind($sql, $arr_bind);
					
					foreach($result_ranking_30 as $arr_key=>$arr_value)
					{
						foreach($arr_value as $key=>$value)
						{
							$$key=$value;
						}
						$arr_ranking_30[$ranking] += $voice_star;
						$arr_ranking_count_30[$ranking] += 1;
					}
					$ranking_3 = (($arr_ranking[30]/2)/3)/$all_account;
				}
			}
			
			//ショップの点数更新
			$arr_bind = array();
			$arr_bind["company_id"] = $company_id;
			$arr_bind["evaluation"] = $ranking_5+$ranking_4+$ranking_3;
			$after_evaluation = $arr_bind["evaluation"];
			
			$db_up = "update company set evaluation=:evaluation where company_id=:company_id ";
			$this->db_update($db_up, $arr_bind);
		} //if($all_account>=5)
		//点数にならない場合
		else
		{
			//ショップの点数更新
			$arr_bind = array();
			$arr_bind["company_id"] = $company_id;
			$arr_bind["evaluation"] = 0;
			$after_evaluation = $arr_bind["evaluation"];
			$db_up = "update company set evaluation=:evaluation where company_id=:company_id ";
			$this->db_update($db_up, $arr_bind);
		}
		
		if(trim($before_evaluation)!=trim($after_evaluation))
		{
			$datetime = date("Y-m-d H:i:s");
			$db_insert = "insert into history_evaluation ( history_evaluation_id, ";
			$db_insert .= " company_id, before_evaluation, after_evaluation, ";
			$db_insert .= " regi_date ) values ( '', ";
			$db_insert .= "'".$company_id."', '".$before_evaluation."', '".$after_evaluation."', ";
			$db_insert .= " '".$datetime."')  ";
			
			$this->db_update($db_insert);
		}
	} 
	
	public function Fn_voice_update ($arr_data, $arr_where) 
	{ 
		$arr_bind = array();
		$where = "";
		$datetime = date("Y-m-d H:i:s");
		
		if(!is_null($arr_data))
		{
			$db_up = "update company_voice set ";
			foreach($arr_data as $key=>$value)
			{
				$arr_bind[$key] = $value;
				$db_up .= $key."= :".$key.", ";
			}
			$db_up .= " up_date= '".$datetime."' ";
			foreach($arr_where as $key=>$value)
			{
				$arr_bind[$key] = $value;
				$where .= "and ".$key."= :".$key." ";
			}
			$db_up .= " where 1  ".$where;

			$db_result = $this->db_update($db_up, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	public function Fn_voice_delete ($arr_where) 
	{ 
		if(!is_null($arr_where))
		{
			$where = "";
			
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
			
			$db_del = "Delete from company_voice where 1  ".$where;

			$db_result = $this->db_update($db_del, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	//自動番号
	public function Fn_db_voice_auto()  
	{ 
		$sql = "select last_insert_id() as last_id " ;

		$db_result = $this->db_query_bind($sql, null);
		return $db_result;
	} 
	
	
	//前の口コミ ///kanto/voice/detail.php
	public function Fn_db_voice_before($voice_id)  
	{ 
		$arr_bind["voice_id"] = $voice_id;
		$sql = "SELECT voice_id, 1 FROM company_voice v inner join company s on s.company_id=v.company_id where s.flag_open=1 and v.flag_open=1 and voice_id<:voice_id order by voice_id desc limit 0, 1 " ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//次の口コミ ///kanto/voice/detail.php
	public function Fn_db_voice_next($voice_id)  
	{ 
		$arr_bind["voice_id"] = $voice_id;
		$sql = "SELECT voice_id, 1 FROM company_voice v inner join company s on s.company_id=v.company_id where s.flag_open=1 and v.flag_open=1 and voice_id>:voice_id order by voice_id desc limit 0, 1 " ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//件数表示 ///kanto/user/common/adminleft.php
	public function Fn_ranking_count($member_id)  
	{ 
		$arr_bind["member_id"] = $member_id;
		$sql = "SELECT count(voice_id) as voice_count, ranking FROM company_voice v inner join company s on s.company_id=v.company_id ";
		$sql .= " where s.flag_open=1 and v.flag_open=1 ";
		$sql .= " and member_id=:member_id ";
		$sql .= " group by ranking " ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	public function Fn_voice_ranking() {
		$arr_return["50"] = "信憑性あり大";
		$arr_return["40"] = "信憑性あり小";
		$arr_return["30"] = "信憑性イマイチ";
		$arr_return["20"] = "信憑性なし";
		$arr_return["10"] = "判定不可";
		
		return $arr_return;
	}
}


?>
