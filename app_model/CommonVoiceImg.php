<?php

class CommonVoiceImg extends CommonDao{
	
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
	
	///kanto/voice_img/index.php　のみ使用
	public function Fn_voice_img_list_index($arr_data, $arr_where=null, $arr_where_not=null, $arr_etc=null) 
	{ 
		$arr_bind = array();
		$where = "";
		
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value) {
				if($key=="s_keyword")
				{
					$var_word_1 = "s_word_1";
					$arr_bind[$var_word_1] = '%'.$arr_where["s_keyword"].'%';
					$where .= " and s.shop_name collate utf8_unicode_ci like :".$var_word_1." ";
				}
				elseif($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."=:".$key;
				}
			}
		}
		
		if(!is_null($arr_where_not)) { 
			foreach($arr_where_not as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."!=:".$key;
				}
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM voice_img v inner join shop s on s.shop_id=v.shop_id where 1 ".$where ;
		if(!is_null($arr_etc["order_name"]))
		{
			$sql .= " order by ".$arr_etc["order_name"]." ".$arr_etc["order"];
		}
		else
		{
			$sql .= " order by v.regi_date desc";
		}
		if(!is_null($arr_etc["offset"]) && !is_null($arr_etc["view_count"]))
		{
			$sql .= " limit ".$arr_etc["offset"].", ".$arr_etc["view_count"];	
		}
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	
	///kanto/voice_img/index.php　のみ使用
	public function Fn_voice_img_count_index($arr_where=null, $arr_where_not=null) 
	{
		$arr_bind = array();
		$where = "";
		
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value) {
				if($key=="s_keyword")
				{
					$var_word_1 = "s_word_1";
					$arr_bind[$var_word_1] = '%'.$arr_where["s_keyword"].'%';
					$where .= " and s.shop_name collate utf8_unicode_ci like :".$var_word_1." ";
				}
				elseif($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."=:".$key;
				}
			}
		}
		
		if(!is_null($arr_where_not)) { 
			foreach($arr_where_not as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."!=:".$key;
				}
			}
		}
		
		$sql = " SELECT count(v.voice_img_id) as all_count FROM voice_img v inner join shop s on s.shop_id=v.shop_id where 1 ".$where ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	
	
	
	//user page
	public function Fn_voice_img_user_count ($arr_where=null, $arr_where_not=null) 
	{ 
		$arr_bind = array();
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."=:".$key;
				}
			}
		}
		
		if(!is_null($arr_where_not)) { 
			foreach($arr_where_not as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."!=:".$key;
				}
			}
		}
		
		$sql = " SELECT count(voice_img_id) as all_count FROM voice_img where 1 ".$where ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	public function Fn_voice_img_user_list ($arr_data, $arr_where=null, $arr_where_not=null, $arr_etc=null) 
	{ 
		$arr_bind = array();
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."=:".$key;
				}
			}
		}
		
		if(!is_null($arr_where_not)) { 
			foreach($arr_where_not as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."!=:".$key;
				}
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM voice_img where 1 ".$where ;
		if(!is_null($arr_etc["order_name"]))
		{
			$sql .= " order by ".$arr_etc["order_name"]." ".$arr_etc["order"];
		}
		else
		{
			$sql .= " order by regi_date desc";
		}
		if(!is_null($arr_etc["offset"]) && !is_null($arr_etc["view_count"]))
		{
			$sql .= " limit ".$arr_etc["offset"].", ".$arr_etc["view_count"];	
		}
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	
	
	public function Fn_voice_img_list ($arr_data, $arr_where=null, $arr_where_not=null, $arr_etc=null) 
	{ 
		$arr_bind = array();
		
		if(!is_null($arr_where)) { 
			foreach($arr_where as $key=>$value) {
				if($key=="s_keyword")
				{
					$var_word_1 = "s_word_1";
					$arr_bind[$var_word_1] = '%'.$arr_where["s_keyword"].'%';
					$where .= " and s.shop_name collate utf8_unicode_ci like :".$var_word_1." ";
				}
				elseif($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."=:".$key;
				}
			}
		}
		
		if(!is_null($arr_where_not)) { 
			foreach($arr_where_not as $key=>$value) {
				if($value!="")
				{
					$arr_bind[$key] = $value;
					$where .= " and ".$key."!=:".$key;
				}
			}
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM voice_img v inner join shop s on s.shop_id=v.shop_id where s.flag_open=1 and v.flag_open=1  ".$where ;
		if(!is_null($arr_etc["order_name"]))
		{
			$sql .= " order by ".$arr_etc["order_name"]." ".$arr_etc["order"];
		}
		else
		{
			$sql .= " order by v.regi_date desc";
		}
		if(!is_null($arr_etc["offset"]) && !is_null($arr_etc["view_count"]))
		{
			$sql .= " limit ".$arr_etc["offset"].", ".$arr_etc["view_count"];	
		}
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	
	
	//like検索
	public function Fn_voice_img_all_count ($arr_where) 
	{ 
		$arr_bind = array();
		
		$var = "s_keyword";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$var_word_1 = "s_word_1";
			$arr_bind[$var_word_1] = '%'.$arr_where[$var].'%';
			$where .= " and (voice_img_title collate utf8_unicode_ci like :".$var_word_1." or voice_img_id = :".$var." ) ";
		}
		
		$var = "s_flag_open";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$where .= " and flag_open=:".$var;
		}
		$sql = " SELECT count(voice_img_id) as all_count FROM voice_img where 1 ".$where ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	public function Fn_voice_img_all_list ($arr_data, $arr_where, $arr_etc) 
	{ 
		$arr_bind = array();
		
		$var = "s_keyword";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$var_word_1 = "s_word_1";
			$arr_bind[$var_word_1] = '%'.$arr_where[$var].'%';
			$where .= " and (voice_img_title collate utf8_unicode_ci like :".$var_word_1." or voice_img_id = :".$var." ) ";
		}
		
		$var = "s_flag_open";
		if(!is_null($arr_where[$var]))
		{
			$arr_bind[$var] = $arr_where[$var];
			$where .= " and flag_open=:".$var;
		}
		
		$sql = "SELECT ";
		foreach($arr_data as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM voice_img where 1 ".$where ;
		if(!is_null($arr_etc["order_name"]))
		{
			$sql .= " order by ".$arr_etc["order_name"]." ".$arr_etc["order"];
		}
		else
		{
			$sql .= " order by regi_date desc";
		}
		if(!is_null($arr_etc["offset"]) && !is_null($arr_etc["view_count"]))
		{
			$sql .= " limit ".$arr_etc["offset"].", ".$arr_etc["view_count"];	
		}
		
		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//登録
	public function Fn_voice_img_insert ($arr_data) 
	{ 
		$arr_bind = array();
		$datetime = date("Y-m-d H:i:s");
		
		if(!is_null($arr_data))
		{
			$db_insert = "insert into voice_img ( voice_img_id, ";
			foreach($arr_data as $key=>$value)
			{
				$db_insert .= $key.", ";
			}
			$db_insert .= " regi_date, up_date ) values ( '', ";
			foreach($arr_data as $key=>$value)
			{
				$arr_bind[$key] = $value;
				$db_insert .= ":".$key.", ";
			}
			$db_insert .= " '".$datetime."', '".$datetime."')  ";
			
			$db_result = $this->db_update($db_insert, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	public function Fn_voice_img_update ($arr_data, $arr_where) 
	{ 
		$arr_bind = array();
		$where = "";
		$datetime = date("Y-m-d H:i:s");
		
		if(!is_null($arr_data))
		{
			$db_up = "update voice_img set ";
			foreach($arr_data as $key=>$value)
			{
				$arr_bind[$key] = $value;
				$db_up .= $key."= :".$key.", ";
			}
			$db_up .= " up_date= '".$datetime."' ";
			foreach($arr_where as $key=>$value)
			{
				$arr_bind[$key] = $value;
				$where .= "and ".$key."= :".$key." ";
			}
			$db_up .= " where 1  ".$where;

			$db_result = $this->db_update($db_up, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	public function Fn_voice_img_delete ($arr_where) 
	{ 
		if(!is_null($arr_where))
		{
			$where = "";
			
			foreach($arr_where as $key=>$value)
			{
					$arr_bind[$key] = $value;
					$where .= "and ".$key."= :".$key." ";
			}
			
			$db_del = "Delete from voice_img where 1  ".$where;

			$db_result = $this->db_update($db_del, $arr_bind);

			return true;
		}
		
		return false;
	} 
	
	//自動番号
	public function Fn_db_voice_img_auto()  
	{ 
		$sql = "select last_insert_id() as last_id " ;

		$db_result = $this->db_query_bind($sql, null);
		return $db_result;
	} 
	
	
	//前の口コミ ///kanto/voice_img/detail.php
	public function Fn_db_voice_img_before($voice_img_id)  
	{ 
		$arr_bind["voice_img_id"] = $voice_img_id;
		$sql = "SELECT voice_img_id, 1 FROM voice_img v inner join shop s on s.shop_id=v.shop_id where s.flag_open=1 and v.flag_open=1 and voice_img_id<:voice_img_id order by voice_img_id desc limit 0, 1 " ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
	
	//次の口コミ ///kanto/voice_img/detail.php
	public function Fn_db_voice_img_next($voice_img_id)  
	{ 
		$arr_bind["voice_img_id"] = $voice_img_id;
		$sql = "SELECT voice_img_id, 1 FROM voice_img v inner join shop s on s.shop_id=v.shop_id where s.flag_open=1 and v.flag_open=1 and voice_img_id>:voice_img_id order by voice_img_id desc limit 0, 1 " ;

		$db_result = $this->db_query_bind($sql, $arr_bind);
		return $db_result;
	} 
}


?>
