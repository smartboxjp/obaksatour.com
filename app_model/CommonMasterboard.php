<?php

class CommonMasterboard extends CommonDao{
    
    //コンストラクタ
    function __construct(){
        parent::__construct();
    }

    //デストラクタ
    function __destruct(){
        parent::__destruct();
    }
    
    /*
    /app_master/dashboard/index.php
    */
    public function Fn_top_user_count()
    { 
        $all_count = 0;
        $day_differ = "";
        //公開されている会員の登録数
        $sql_count = "SELECT count(member_id) as all_count FROM member where flag_open=1 ";

        $db_result_count = $this->db_query_bind($sql_count);
        if($db_result_count)
        {
            $all_count = $db_result_count[0]["all_count"];
        }
        //最後に登録された日
        $sql = "SELECT regi_date FROM member where flag_open=1 order by regi_date desc limit 0, 1";
        $db_result = $this->db_query_bind($sql);
        if($db_result)
        {
            $datetime1 = date("Y-m-d");
            $datetime2 = substr($db_result[0]["regi_date"], 0, 10);
            $day_differ =  (strtotime($datetime1)-strtotime($datetime2))/(3600*24);
        }
        $color_label = $this->Fn_top_color($day_differ);
        ?>
            <div class="ibox-title">
                <span class="label <? echo $color_label;?> pull-right"><i class="fa fa-clock-o"></i><? if($day_differ==0) { echo "本日";} else {echo $day_differ."時間前";};?></span>
                <h5>User</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins"><? echo number_format($all_count);?>人</h1>
                <small>会員登録数</small>
            </div>
        <?
    } 
    
    /*
    /app_master/dashboard/index.php
    */
    public function Fn_top_voice_count()
    { 
        $all_count = 0;
        $day_differ = "";
        //公開されているblogの登録数
        $sql_count = "SELECT count(voice_id) as all_count FROM company_voice v inner join company s on v.company_id=s.company_id where v.flag_open=1 and s.flag_open=1";

        $db_result_count = $this->db_query_bind($sql_count);
        if($db_result_count)
        {
            $all_count = $db_result_count[0]["all_count"];
        }
        //最後に登録された日
        $sql = "SELECT v.regi_date FROM company_voice v inner join company s on v.company_id=s.company_id where v.flag_open=1 and s.flag_open=1 order by v.regi_date desc limit 0, 1";
        $db_result = $this->db_query_bind($sql);
        if($db_result)
        {
            $datetime1 = date("Y-m-d");
            $datetime2 = substr($db_result[0]["regi_date"], 0, 10);
            $day_differ =  (strtotime($datetime1)-strtotime($datetime2))/(3600*24);
        }
        $color_label=$this->Fn_top_color($day_differ);
        ?>
            <div class="ibox-title">
                <span class="label label-success pull-right"><i class="fa fa-clock-o"></i><? if($day_differ==0) { echo "本日";} else {echo $day_differ."時間前";};?></span>
                <h5>Voice</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins"><? echo number_format($all_count);?>件</h1>
                <small>最新口コミ</small>
            </div>
        <?
    } 

    /*
    /app_master/dashboard/index.php
    */
    public function Fn_top_blog_count()
    { 
        $all_count = 0;
        $day_differ = "";
        //公開されているblogの登録数
        $sql_count = "SELECT count(company_blog_id) as all_count FROM company_blog b inner join company s on b.company_id=s.company_id where b.flag_open=1 and s.flag_open=1";

        $db_result_count = $this->db_query_bind($sql_count);
        if($db_result_count)
        {
            $all_count = $db_result_count[0]["all_count"];
        }
        //最後に登録された日
        $sql = "SELECT b.regi_date FROM company_blog b inner join company s on b.company_id=s.company_id where b.flag_open=1 and s.flag_open=1 order by b.regi_date desc limit 0, 1";
        $db_result = $this->db_query_bind($sql);
        if($db_result)
        {
            $datetime1 = date("Y-m-d");
            $datetime2 = substr($db_result[0]["regi_date"], 0, 10);
            $day_differ =  (strtotime($datetime1)-strtotime($datetime2))/(3600*24);
        }
        $color_label=$this->Fn_top_color($day_differ);
        ?>
            <div class="ibox-title">
                <span class="label <? echo $color_label;?> pull-right"><i class="fa fa-clock-o"></i><? if($day_differ==0) { echo "本日";} else {echo $day_differ."時間前";}?></span>
                <h5>Blog</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins"><? echo number_format($all_count);?>件</h1>
                <small>お店のブログ</small>
            </div>
        <?
    } 
    
    
    /*
    /app_master/dashboard/index.php
    */
    public function Fn_top_company_count()
    { 
        $all_count = 0;
        $day_differ = "";
        //公開されているblogの登録数
        $sql_count = "SELECT count(company_id) as all_count FROM company where flag_open=1 ";

        $db_result_count = $this->db_query_bind($sql_count);
        if($db_result_count)
        {
            $all_count = $db_result_count[0]["all_count"];
        }
        //最後に更新された日
        $sql = "SELECT up_date FROM company where flag_open=1 order by up_date desc limit 0, 1";
        $db_result = $this->db_query_bind($sql);
        if($db_result)
        {
            $datetime1 = date("Y-m-d");
            $datetime2 = substr($db_result[0]["up_date"], 0, 10);
            $day_differ =  (strtotime($datetime1)-strtotime($datetime2))/(3600*24);
        }
        $color_label = $this->Fn_top_color($day_differ);
        ?>
            <div class="ibox-title">
                <span class="label <? echo $color_label;?> pull-right"><i class="fa fa-clock-o"></i><? if($day_differ==0) { echo "本日";} else {echo $day_differ."時間前";}?></span>
                <h5>company A</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins"><? echo number_format($all_count);?>店</h1>
                
                <small>有料掲載店</small>
            </div>
        <?
    } 


    public function Fn_top_color($day_differ)
    { 
        $color_label = "label-danger";
        if($day_differ==0) { $color_label = "label-success";}
        elseif($day_differ<3) { $color_label = "label-info";}
        return $color_label;
    } 
}


?>
