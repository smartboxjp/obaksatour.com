
<?
    $Us = $_SERVER["PHP_SELF"];
    $Ds = (strpos($Us,'?') !== false) ? '&' : '?';
    $Qs = ($_SERVER['QUERY_STRING'] ? $Ds.$_SERVER['QUERY_STRING']:'');

    if($meta_title == "") {$meta_title = "오키나와 오박사";}
    if($meta_description == "") {$meta_description = "오키나와 자유여행 정보와 예약은 오키나와 오박사에서 가능합니다.";}
?>
<title><? echo $meta_title;?></title>
<meta property="og:title" content="<? echo $meta_title;?>">
<meta itemprop="og:image" content="/images/common/logo.png">
<meta property="og:url" content="<? echo global_ssl.$Us.$Qs;?>">
<meta property="og:locale" content="ko_KR">
<meta property="og:site_name" content="오키나와 오박사">
<meta property="og:description" content="<? echo $meta_description;?>">
<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="<? echo $meta_title;?>">
<meta name="twitter:description" content="<? echo $meta_description;?>">
<meta name="twitter:image" content="<? echo global_ssl;?>/images/common/logo.png">
