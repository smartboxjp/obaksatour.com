<!-- icon -->
<link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script src="//use.fontawesome.com/c2dd02cc04.js"></script>

<!-- 共通css -->
<link href="/common/fonts/nanumbarungothic/nanumbarungothic.css" rel="stylesheet">
<link href="/common/css/bootstrap.min.css" rel="stylesheet">
<link href="/common/css/common.css" rel="stylesheet">
<link href="/common/css/module.css" rel="stylesheet">

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/common/js/jquery.min.js"></script>
<script src="/common/js/bootstrap.min.js"></script>

<!-- drawer -->
<link href="/common/css/drawer.css" rel="stylesheet">
<script src="/common/js/drawer.js"></script>

<!-- matchHeight -->
<script src="/common/js/jquery.matchHeight.js"></script>

<!-- common -->
<script src="/common/js/common.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->