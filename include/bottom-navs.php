</div>
<!-- /.contents -->

<div class="bottom-navs-wrapper">
  <div class="bottom-navs l-container">
    <ul class="bottom-navs-lt">
      <li><a href="#">회사소개</a></li>
      <li class="bottom-navs-current"><a href="#">개인정보보호정책</a></li>
      <li><a href="#">이용약관</a></li>
      <li><a href="#">고객센터</a></li>
      <li><a href="#">제휴안내</a></li>
    </ul>
    <ul class="bottom-navs-rt">
      <li><a href="#">네이버블로그</a></li>
      <li><a href="#">페이스북</a></li>
      <li><a href="#">인스타그램</a></li>
    </ul>
    <!-- /.l-container -->
  </div>
  <!-- /.bottom-navs -->
</div>
<!-- /.bottom-navs -->

<div class="footer-wrapper">
  <footer class="footer">
    <img src="/common/images/footer-logo.png" alt="오키나와 오박사" class="footer-log">
    <div class="footer-text">
      <p>
        (일본법인회사) 스마트박스<span>|</span>대표 : 이주영<span>|</span>개인정보보호책임자 : 김승현<br>
        대표전화 : +000 - 000 - 0000<span>|</span>주소 : 일본 오키나와현 1234 스마트박스 빌딩 123호<br>
        이용문의 : 평일 09 : 00 ~ 22 : 00,  토요일 10 : 00 ~ 17 : 00,  일요일 및 공휴일 휴무 이메일 : obaksatour@obaksatour.com<br>
      </p>
      <p class="copyright">Copyright  (c) Obaksatour all rights reserved.</p>
    </div>
    <!-- /.footer-text -->
  </footer>
</div>
<!-- /.footer-wrapper -->

</div>
<!-- /.site-container -->

<div class="totop">
  <a href="#totop"><i class="material-icons">&#xE5CE;</i></a>
</div>
<!-- /.totop -->

<div id="drawernav" class="mobile-nav-wrapper">
  <ul class="mobile-nav">
    <li><a href="/oneday_main.html">원데이 / 반나절 투어</a></li>
    <li><a href="#">패키지 투어</a></li>
    <li><a href="#">오키나와 가이드</a></li>
    <li><a href="#">옵션 투어</a></li>
  </ul>
  <ul class="mobile-nav mobile-nav-btm">
    <li><a href="/rentcar_main.html">렌트카</a></li>
    <li class="last"><a href="#">호텔</a></li>
  </ul>
  <!-- Start Gnb Menu For Mobile -->
  <p class="mobile-nav-title"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;&nbsp;마이페이지</p>
  <ul class="mobile-nav mobile-nav-flo">
    <li><a href="#">·&nbsp;&nbsp;나의 구매목록</a></li>
    <li><a href="#">·&nbsp;&nbsp;취소 및 변경 현황</a></li>
    <li><a href="#">·&nbsp;&nbsp;상품 Q &amp; A</a></li>
    <li><a href="#">·&nbsp;&nbsp;구매 후기</a></li>
  </ul>
  <p class="mobile-nav-title"><span class="glyphicon glyphicon-earphone"></span>&nbsp;&nbsp;고객센터</p>
  <ul class="mobile-nav mobile-nav-flo">
    <li><a href="#">·&nbsp;&nbsp;공지사항</a></li>
    <li><a href="#">·&nbsp;&nbsp;자주하는 질문</a></li>
    <li><a href="#">·&nbsp;&nbsp;1:1 문의</a></li>
    <li><a href="#">·&nbsp;&nbsp;제휴문의</a></li>
  </ul>
  <ul class="mobile-nav mobile-nav-btm mobile-nav-btm-last">
    <!-- After Login -->
    <li><a href="#"><span class="glyphicon glyphicon-off"></span>&nbsp;&nbsp;로그아웃</a></li>
    <li><a href="#"><span class="glyphicon glyphicon-lock"></span>&nbsp;&nbsp;회원정보수정</a></li>
    <!-- Before Login
    <li><a href="#"><span class="glyphicon glyphicon-off"></span>&nbsp;&nbsp;로그인</a></li>
    <li><a href="#"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;회원가입</a></li>
    -->
  </ul>
  <!--// End Gnb Menu For Mobile -->
</div>
<!-- /#drawernav -->

<div class="drawer-overlay"></div>

</body>
</html>