<div id="drawernav" class="mobile-nav-wrapper">
  <div class="drawer-close"><i class="material-icons">&#xE5CD;</i></div>
  <ul class="mobile-nav mobile-nav-btm mobile-nav-btm-last">
    <? if($_SESSION['member_id']=="") { ?>
    <li><a href="/member/"><span class="glyphicon glyphicon-off"></span>&nbsp;&nbsp;로그인</a></li>
    <li><a href="/member_regist/"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;회원가입</a></li>
    <? } else { ?>
    <li><a href="/member/logout.php"><span class="glyphicon glyphicon-off"></span>&nbsp;&nbsp;로그아웃</a></li>
    <li><a href="/member/info.php"><span class="glyphicon glyphicon-lock"></span>&nbsp;&nbsp;회원정보수정</a></li>
    <? } ?>
  </ul>
  <ul class="mobile-nav">
    <li><a href="/oneday/">원데이 / 반나절 투어</a></li>
    <li><a href="#">패키지 투어</a></li>
    <li><a href="#">오키나와 가이드</a></li>
    <li><a href="#">옵션 투어</a></li>
  </ul>
  <ul class="mobile-nav mobile-nav-btm">
    <li><a href="/rentacar/">렌트카</a></li>
    <li class="last"><a href="#">호텔</a></li>
  </ul>
  <!-- Start Gnb Menu For Mobile -->
  <p class="mobile-nav-title"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;&nbsp;마이페이지</p>
  <ul class="mobile-nav mobile-nav-flo">
    <li><a href="/mypage/myinfo.php">·&nbsp;&nbsp;나의 구매목록</a></li>
    <li><a href="#">·&nbsp;&nbsp;취소 및 변경 현황</a></li>
    <li><a href="#">·&nbsp;&nbsp;상품 Q &amp; A</a></li>
    <li><a href="#">·&nbsp;&nbsp;구매 후기</a></li>
  </ul>
  <p class="mobile-nav-title"><span class="glyphicon glyphicon-earphone"></span>&nbsp;&nbsp;고객센터</p>
  <ul class="mobile-nav mobile-nav-flo">
    <li><a href="/about/notice.php">·&nbsp;&nbsp;공지사항</a></li>
    <li><a href="/about/faq.php">·&nbsp;&nbsp;자주하는 질문</a></li>
    <li><a href="/about/onebyone.php">·&nbsp;&nbsp;1:1 문의</a></li>
    <li><a href="/about/partner.php">·&nbsp;&nbsp;제휴문의</a></li>
  </ul>
</div>
<!-- /#drawernav -->

<div class="drawer-overlay"></div>