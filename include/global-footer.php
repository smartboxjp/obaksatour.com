<div class="bottom-navs-wrapper">
  <div class="bottom-navs l-container">
    <ul class="bottom-navs-lt">
      <li><a href="#">회사소개</a></li>
      <li class="bottom-navs-current"><a href="#">개인정보보호정책</a></li>
      <li><a href="#">이용약관</a></li>
      <li><a href="#">고객센터</a></li>
      <li><a href="/about/partner.php">제휴안내</a></li>
    </ul>
    <ul class="bottom-navs-rt">
      <li><a href="#">네이버블로그</a></li>
      <li><a href="#">페이스북</a></li>
      <li><a href="#">인스타그램</a></li>
    </ul>
    <!-- /.l-container -->
  </div>
  <!-- /.bottom-navs -->
</div>
<!-- /.bottom-navs -->

<div class="footer-wrapper">
  <footer class="footer">
    <img src="/common/images/footer-logo.png" alt="오키나와 오박사" class="footer-log">
    <div class="footer-text">
      <p>
        (주) 스마트복스<span>|</span>대표 : 오주영<span>|</span>개인정보보호책임자 : 김승현<br>
        대표전화 : 070-4007-3433 <span>|</span>주소 : 서울시 서초구 바외뫼로7길 8, 306(우면동, 서초케이허브)<br>
        이용문의 : 평일 09:00 ~ 18:00,  토, 일요일 및 공휴일 휴무<br>
      </p>
      <p class="copyright">Copyright  (c) Okinawaobaksa all rights reserved.</p>
    </div>
    <!-- /.footer-text -->
  </footer>
</div>
<!-- /.footer-wrapper -->

</div>
<!-- /.site-container -->

<div class="totop">
  <a href="#totop"><i class="material-icons">&#xE5CE;</i></a>
</div>
<!-- /.totop -->

</body>
</html>
