
<div class="site-container" id="totop">

<div class="header-wrapper">
  <header class="header clearfix">
    <div class="header-log">
      <a href="/">
        <img src="/common/images/logo.png" alt="오키나와 오박사">
      </a>
    </div>
    <!-- /.header-log -->
    <ul class="hnav">
      <? if($_SESSION['member_id']=="") { ?>
      <li><a href="/member/">로그인</a></li>
      <li><a href="/member_regist/">회원가입</a></li>
      <? } else { ?>
      <li><a href="/member/logout.php">로그아웃</a></li>
      <li><a href="/member/info.php">&middot; 회원정보수정</a></li>
      <? } ?>
      <li class="hnav-login-user">
        <div class="dropdown">
          <a data-target="#" href="#" class="" data-toggle="dropdown" role="button" aria-expanded="false">
            <img src="/common/images/user.png" alt="">
            <? echo $_SESSION['member_name'];?>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li role="presentation"><a href="/mypage/myinfo.php">&middot; 나의구매목록</a></li>
          </ul>
        </div>
      </li>
      <li>
        <div class="dropdown">
          <a data-target="#" href="/member_regist/" class="" data-toggle="dropdown" role="button" aria-expanded="false">
            Q&nbsp;A
          </a>
          <ul class="dropdown-menu" role="menu">
            <li role="presentation"><a href="#">&middot; 텍스트</a></li>
            <li role="presentation"><a href="#">&middot; 텍스트</a></li>
            <li role="presentation"><a href="#">&middot; 텍스트</a></li>
          </ul>
        </div>
      </li>
      <li>
        <div class="dropdown">
          <a data-target="#" href="/mypage/myinfo.php" class="" data-toggle="dropdown" role="button" aria-expanded="false">
            마이페이지
          </a>
          <ul class="dropdown-menu dropdown-menu-right" role="menu">
            <li role="presentation"><a href="#">&middot; 텍스트</a></li>
            <li role="presentation"><a href="#">&middot; 텍스트</a></li>
            <li role="presentation"><a href="#">&middot; 텍스트</a></li>
          </ul>
        </div>
      </li>
    </ul>
    <nav class="gnav-wrapper">
      <ul class="gnav">
        <li><a href="/oneday/">원데이 / 반나절 투어</a></li>
        <li class="gnav-current"><a href="#">패키지 투어</a></li>
        <li><a href="#">오키나와 가이드</a></li>
        <li><a href="#">옵션투어</a></li>
      </ul>
      <ul class="snav">
        <li><a href="/rentacar/">렌트카</a></li>
        <li class="snav-current"><a href="#">호텔 / 팬션</a></li>
      </ul>
    </nav>
    <!-- /#drawernav -->
    <div class="drawer-btn">
      <div id="humberger">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar icon-bar-last"></span>
      </div>
      <!-- /drawer-btn -->
    </div>
    <!-- /.drawer-btn -->
  </header>
</div>
<!-- /.header-wrapper -->

