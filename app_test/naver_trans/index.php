
<?php
// 네이버 기계번역 API 예제
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連

  // 네이버 로그인 콜백 예제
  $client_id = NAVER_Client_ID;
  $client_secret = NAVER_Client_Secret;

  $txt = $_POST["txt"];

  if($txt == "")
  {
    $txt = "글을 입력해 주세요";
  }
  $encText = urlencode($txt);
  $postvars = "source=ko&target=ja&text=".$encText;
  $url = "https://openapi.naver.com/v1/language/translate";
  $is_post = true;
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, $is_post);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch,CURLOPT_POSTFIELDS, $postvars);
  $headers = array();
  $headers[] = "X-Naver-Client-Id: ".$client_id;
  $headers[] = "X-Naver-Client-Secret: ".$client_secret;
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  $response = curl_exec ($ch);
  $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  //echo "status_code:".$status_code."<br>";
  echo "원문:".$txt."<hr />";
  curl_close ($ch);
  if($status_code == 200) {
    echo $response;
  } else {
    echo "Error 내용:".$response;
  }
?>
<hr />
<form action="" method="post">

<textarea name="txt" style="height: 300px; width: 300px;">
  날짜를 변경하고 싶은데 괜찮나요? 언제로 변경할까요? 내일로 하고 싶은데.. 그럼 변경해 주세요
</textarea>
<br />
<input type="submit">
</form>