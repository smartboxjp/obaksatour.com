<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title>rentacar</title>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/css/main.css" rel="stylesheet">

<!-- swiper -->
<link href="/common/css/swiper.min.css" rel="stylesheet">
<script src="/common/js/swiper.min.js"></script>
<script src="//cdn.rawgit.com/tonystar/bootstrap-hover-tabs/master/bootstrap-hover-tabs.js"></script>

</head>
<body>

<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

<div class="main-vis-wrap">
  <div class="l-container">
    <div class="main-vis">
      <ul class="visual-nav">
        <li><a href ="#"><span class="visual-nav-one">렌터카z</span></a></li>
        <li><a href="#"><span>원데이<br>반나절<br>투어</span></a></li>
        <li><a href="#"><span class="visual-nav-two">패키지<br>투어</span></a></li>
        <li><a href="#"><span class="visual-nav-two">오키나와<br>가이드</span></a></li>
        <li><a href="#"><span class="visual-nav-two">해양<br>스포츠</span></a></li>
      </ul>
      <img src="/images/main-vis.jpg" alt="">
    </div>
    <!-- /.main-vis -->
  </div>
  <!-- /.l-container -->
</div>
<!-- /.main-vis-wrap -->


<div class="l-container">
  <div class="sitenav-wrapper">
    <div class="swiper-container sitenav-container">
      <div class="swiper-wrapper">
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-1.png" alt="오박사투어는 ?" class=" sitenav-img">
            <span class="sitenav-text">오박사투어는 ?</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-2.png" alt="오박사투어 렌터카" class=" sitenav-img">
            <span class="sitenav-text">오박사투어 렌터카</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-2.png" alt="원데이 / 반날절 투어" class=" sitenav-img">
            <span class="sitenav-text">원데이 / 반날절 투어</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-4.png" alt="" class=" sitenav-img">
            <span class="sitenav-text">추천 해양 스포츠</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-5.png" alt="오키나와 가이드" class=" sitenav-img">
            <span class="sitenav-text">오키나와 가이드</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-6.png" alt="패키지 투어" class=" sitenav-img">
            <span class="sitenav-text">오키나와</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-7.png" alt="오박사투어는 ?" class=" sitenav-img">
            <span class="sitenav-text">오키나와</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-7.png" alt="" class=" sitenav-img">
            <span class="sitenav-text">오키나와</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
        <div class="swiper-slide">
          <a href="#" class="sitenav">
            <img src="/images/sitenav-7.png" alt="" class=" sitenav-img">
            <span class="sitenav-text">오키나와</span>
          </a>
          <!-- /.swiper-slide -->
        </div>
        <!-- /.swiper-slide -->
      </div>
      <!-- /.swiper-wrapper -->
    </div>
    <div class="button-prev swiper-button-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
    <div class="button-next swiper-button-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
    <!-- /.swiper-container swiper-a -->
  </div>
  <!-- /.sitenav-wrapper -->
</div>
<!-- /.container -->


  <div class="l-container">
    <div class="pankuzu-wapper">
      <ul class="pankuzu mt-0">
        <li><a href="/">오박사 홈</a></li>
        <li><a href="#">원데이 / 반날절</a></li>
        <li>오키나와 주요 관광지</li>
      </ul>
    </div>
    <!-- /.pankuzu-wapper -->
  </div>
  <!-- /.l-container -->

  <div class="l-container cartype-nav-wrap">
    <ul  class="nav cartype-nav nav-pills">
      <li class="active"><a href="#1a" data-toggle="tab"><span class="cartype-nav-1"></span>전체</a></li>
      <li><a href="#2a" data-toggle="tab"><span class="cartype-nav-2"></span>경형</a></li>
      <li><a href="#3a" data-toggle="tab"><span class="cartype-nav-3"></span>소형</a></li>
      <li><a href="#4a" data-toggle="tab"><span class="cartype-nav-4"></span>준중형</a></li>
      <li><a href="#5a" data-toggle="tab"><span class="cartype-nav-5"></span>중형</a></li>
      <li><a href="#6a" data-toggle="tab"><span class="cartype-nav-6"></span>대형</a></li>
      <li><a href="#7a" data-toggle="tab"><span class="cartype-nav-7"></span>스포츠카</a></li>
    </ul>

    <div class="tab-content clearfix cartype-more">
      <div class="tab-pane active" id="1a">
        <p>あああ</p>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="2a">
        <p>いいい</p>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="3a">
        <p>ううう</p>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="4a">
        <p>AAAA</p>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="5a">
        <p>BBB</p>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="6a">
        <p>CCC</p>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="7a">
        <p>DDD</p>
      </div>
      <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
  </div>
  <!-- /.container -->


  <div class="l-container mb-50">
    <div class="title-wrap clearfix">
      <h2 class="headline-glay fsize-lg left-bdr ttl"><span class="color-lightblue">오박사투어  </span>렌터카 상품 목록</h2>
      <ul class="navlist">
        <li><a href="#">최신순</a></li>
        <li><a href="#">추천순</a></li>
        <li><a href="#">낮은 가격순</a></li>
        <li><a href="#">높은 가격순</a></li>
      </ul>
    </div>
    <!-- /.title-wrap -->
    <div class="row l-row">
      <div class="col-xs-6 col-sm-3 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <div class="ball card-imagearea-ball ball-sale"><span>20%</span></div>
            <img src="https://api.fnkr.net/testimg/234x190/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="card-text-sub">코우리지마 대료 / 추라우미 수족관 / 만좌모 아메리칸 빌리지</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-msg"><span>1인</span>기준</div>
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
      <div class="col-xs-6 col-sm-3 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <img src="https://api.fnkr.net/testimg/234x190/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="card-text-sub">코우리지마 대료 / 추라우미 수족관 / 만좌모 아메리칸 빌리지</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-msg"><span>1인</span>기준</div>
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
      <div class="col-xs-6 col-sm-3 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <img src="https://api.fnkr.net/testimg/234x190/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate color-glay">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="card-text-sub">코우리지마 대료 / 추라우미 수족관 / 만좌모 아메리칸 빌리지</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-msg"><span>1인</span>기준</div>
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
      <div class="col-xs-6 col-sm-3 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <img src="https://api.fnkr.net/testimg/234x190/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="card-text-sub">코우리지마 대료 / 추라우미 수족관 / 만좌모 아메리칸 빌리지</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-msg"><span>1인</span>기준</div>
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
      <div class="col-xs-6 col-sm-3 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <div class="ball card-imagearea-ball ball-sale"><span>20%</span></div>
            <img src="https://api.fnkr.net/testimg/234x190/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="card-text-sub">코우리지마 대료 / 추라우미 수족관 / 만좌모 아메리칸 빌리지</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-msg"><span>1인</span>기준</div>
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
      <div class="col-xs-6 col-sm-3 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <img src="https://api.fnkr.net/testimg/234x190/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="card-text-sub">코우리지마 대료 / 추라우미 수족관 / 만좌모 아메리칸 빌리지</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-msg"><span>1인</span>기준</div>
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
      <div class="col-xs-6 col-sm-3 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <img src="https://api.fnkr.net/testimg/234x190/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate color-glay">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="card-text-sub">코우리지마 대료 / 추라우미 수족관 / 만좌모 아메리칸 빌리지</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-msg"><span>1인</span>기준</div>
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
      <div class="col-xs-6 col-sm-3 col-pad" data-mh="group2">
        <a href="#" class="card">
          <div class="card-imagearea">
            <img src="https://api.fnkr.net/testimg/234x190/00CED1/FFF/?text=img+placeholder" alt="" class="card-imagearea-img">
          </div>
          <!-- /.card-img -->
          <dl class="card-text">
            <dt class="card-text-cate">반나절 북부 / 오전 출발</dt>
            <dd class="card-text-title">오키나와 북부 원데이 투어</dd>
            <dd class="card-text-sub">코우리지마 대료 / 추라우미 수족관 / 만좌모 아메리칸 빌리지</dd>
            <dd class="priceblock text-right">
              <div class="priceblock-msg"><span>1인</span>기준</div>
              <div class="priceblock-prc"><span>50,000</span> 원</div>
            </dd>
          </dl>
        </a>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.l-container -->

</div>
<!-- /.contents -->

<script>
var swiperA = new Swiper('.sitenav-container', {
  paginationClickable: true,
  slidesPerView: 7,
  speed: 600,
  nextButton: '.button-next',
  prevButton: '.button-prev',
  freeMode: 'auto',
  spaceBetween: 0,
  breakpoints: {
    980: {
      slidesPerView: 5
    },
    780: {
      slidesPerView: 5
    },
    580: {
      slidesPerView: 3
    }
  }
});
</script>

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>