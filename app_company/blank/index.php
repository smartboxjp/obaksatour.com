<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_company_check();
    $company_id = $_SESSION["company_id"];
?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/config.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_start.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-brush"></i>Blank<br><small>A clean page to help you start!</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/app_company/dashboard/">Dashboard</a></li>
        <li>Get Started</li>
        <li><a href="">Blank</a></li>
    </ul>
    <!-- END Blank Header -->

    <!-- Example Block -->
    <div class="block">
        <!-- Example Title -->
        <div class="block-title">
            <h2>Block Title</h2>
        </div>
        <!-- END Example Title -->

        <!-- //_/_/_/_/_/_/_ START _/_/_/_/_/_/_// -->
        <p>Your content..</p>
        <!-- //_/_/_/_/_/_/_ END _/_/_/_/_/_/_// -->
    </div>
    <!-- END Example Block -->
</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/page_footer.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_scripts.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_end.php'; ?>