
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="description" content="BootStrap MaxLength のデモでーす。">
<title>BootStrap MaxLength - jQueryプラグイン</title>
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
<div class="container">
  <h1>BootStrap MaxLength のデモでーす。</h1>
  <input type="text" class="form-control" maxlength="10" name="moreoptions" id="demo" />
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="/app_company/js/plugins/count/bootstrap-maxlength.js"></script>
<script>
$(document).ready(function () {
  $('#demo').maxlength({
    alwaysShow: true,
    warningClass: "label label-success",
    limitReachedClass: "label label-danger"
  });
});
</script>
</body>
</html>
