<?php
	//error_reporting(0);
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	

	foreach($_POST as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}

    $common_connect -> Fn_company_check();
    $company_id = $_SESSION["company_id"];
	

	if($temp_goods_detail_title == "")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
    
    $datetime = date("Y/m/d H:i:s");
    if($view_level!=9) { $view_level=10; }
    
    //array
    $arr_db_field = array("temp_goods_detail_title", "temp_goods_detail_comment");
    $arr_db_field = array_merge($arr_db_field, array("view_level"));
    

    if($temp_goods_detail_id=="")
    {
        $db_insert = "insert into temp_goods_detail ( ";
        $db_insert .= " temp_goods_detail_id, company_id, ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val.", ";
        }
        $db_insert .= " up_date, regi_date ";
        $db_insert .= " ) values ( ";
        $db_insert .= " '', '".$company_id."', ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= " '".$$val."', ";
        }
        $db_insert .= " '$datetime', '$datetime')";
    }
    else
    {
        $db_insert = "update temp_goods_detail set ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val."='".$$val."', ";
        }
        $db_insert .= " up_date='".$datetime."' ";
        $db_insert .= " where company_id='".$company_id."' and temp_goods_detail_id='".$temp_goods_detail_id."' ";
    }
    $common_dao -> db_update($db_insert);

    if ($temp_goods_detail_id == "")
    {
        //自動生成されてるID出力
        $sql = "select last_insert_id() as last_id";  
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            $temp_goods_detail_id = $common_connect -> $db_result[0]["last_id"];
        }
    }


	/* 画像処理 start */
	$save_dir_content = $global_path.global_company_dir.$company_id."/goods_detail/";
	$common_image -> create_folder ($save_dir_content);

	$dom = new domDocument;
	libxml_use_internal_errors(true);
	$dom->loadHTML(html_entity_decode($temp_goods_detail_comment));
	$dom->preserveWhiteSpace = false;
	$imgs  = $dom->getElementsByTagName("img");
	$links = array();
	$files = glob($path . "*.*");
	for($i = 0; $i < $imgs->length; $i++) {
		 $links[] = $imgs->item($i)->getAttribute("src");
	}

	$arr_basename = array();
	foreach ($links as $key => $value) {
		$pathData = pathinfo($global_path.$value);
		$arr_basename[] = $pathData["basename"];

		if(file_exists(trim($global_path.global_shop_temp_dir.$company_id."/".$pathData["basename"]))) {
			rename(trim($global_path.global_shop_temp_dir.$company_id."/".$pathData["basename"]), $save_dir_content.$pathData["basename"]);
		}
	}

	$temp_goods_detail_comment = str_replace(global_shop_temp_dir.$company_id."/", global_company_dir.$company_id."/goods_detail/", $temp_goods_detail_comment);

	$db_up = "update temp_goods_detail set ";
    $db_up .= " temp_goods_detail_comment='".$temp_goods_detail_comment."', ";
    $db_up .= " up_date='".$datetime."' ";
    $db_up .= " where company_id='".$company_id."' and temp_goods_detail_id='".$temp_goods_detail_id."' ";
    $common_dao -> db_update($db_up);
	/* 画像処理 end */


	/* 登録されているファイル以外削除 start */
	$dir = opendir($global_path.global_shop_temp_dir.$company_id);
	while (false !== ($file = readdir($dir))){
		if($file[0] != "."){

			if(!in_array($file, $arr_basename))
			{
				unlink ($global_path.global_shop_temp_dir.$company_id."/".$file);
			}
		}
	}
	closedir($dir);
	/* 登録されているファイル以外削除 end */

	/* 古いファイル削除 start */
	$delete_day = strtotime("48 hours ago");

	$dir = opendir($global_path.global_shop_temp_dir.$company_id);
	while (false !== ($file = readdir($dir))){
		if($file[0] != "."){
			if ($delete_day > filemtime($global_path.global_shop_temp_dir.$company_id."/".$file)) {
				unlink($global_path.global_shop_temp_dir.$company_id."/".$file);
			}
		}
	}
	closedir($dir);
	/* 古いファイル削除 end */

	
	$common_connect-> Fn_javascript_move("登録・修正しました", "temp_goods_detail_list.php?temp_goods_detail_id=".$temp_goods_detail_id);
?>
</body>
</html>