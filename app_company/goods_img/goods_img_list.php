<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_company_check();
    $company_id = $_SESSION["company_id"];

    if($goods_id=="")
    {
        $common_connect -> Fn_javascript_back("正しく入力してください。");
    }

    $page_pre_title = "登録";
    if($goods_img_id!="")
    {
        $arr_db_field = array("goods_img_id", "goods_img_title", "img_1");

        $sql = "SELECT ";
        foreach($arr_db_field as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " g.goods_id FROM goods_img i inner join goods g on i.goods_id=g.goods_id where g.company_id='".$company_id."' and g.goods_id='".$goods_id."' and goods_img_id='".$goods_img_id."' ";

        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
            {
                foreach($arr_db_field as $val)
                {
                    $$val = $db_result[$db_loop][$val];
                }
            }
        }

        $page_pre_title = "修正";
    }
?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/config.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_start.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/page_head.php'; ?>

<link href="/app_master/css/plugins/cropper/cropper.min.css" rel="stylesheet">
<link href="/app_company/css/company.css" rel="stylesheet">

<script src="/app_company/js/vendor/jquery.min.js"></script>
<script type="text/javascript">
    $( function () {

        $( '#form_confirm' ).click( function () {
            $( "#goods_img_comment" ).val( $( ".note-editable" ).html() );
            err_default = "";
            err_check_count = 0;
            bgcolor_default = "#FFFFFF";
            bgcolor_err = "#FFCCCC";
            background = "background-color";

            err_check_count += check_input( "goods_img_title" );

            if ( err_check_count != 0 ) {
                alert( "入力に不備があります" );
                return false;
            } else {
              $('.cropped').val($image.cropper("getCroppedCanvas").toDataURL('image/png'));
              $image.cropper("setDragMode", "crop");

                $( '#form_confirm' ).submit();
                //$( '#form_confirm', "body" ).submit();
                return true;
            }
        } );

        function check_input( $str ) {
            $( "#err_" + $str ).html( err_default );
            $( "#" + $str ).css( background, bgcolor_default );

            if ( $( '#' + $str ).val() == "" ) {
                err = "<span class='errorForm'>正しく入力してください。</span>";
                $( "#err_" + $str ).html( err );
                $( "#" + $str ).css( background, bgcolor_err );

                return 1;
            }
            return 0;
        }


    } );

    function fnChangeSel(i, j) { 
        var result = confirm('削除しますか？'); 
        if(result){ 
            document.location.href = './goods_img_del.php?goods_img_id='+j+'&goods_id='+i;
        } 
    }
    //-->
</script>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-brush"></i>テンプレート - 詳細情報<br><small>商品の詳細情報をテンプレートとして管理が出来ます。</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/app_company/dashboard/">Dashboard</a></li>
        <li>詳細情報のテンプレート</li>
    </ul>
    <!-- END Blank Header -->

    <div>
    <a href="./goods_img_list.php?goods_id=<? echo $goods_id;?>" class="text-button">新規登録</a>
    </div>

    <!-- Example Block -->
    <div class="block">
        <!-- Example Title -->
        <div class="block-title">
            <h2>詳細情報のテンプレート</h2>
        </div>
        <!-- END Example Title -->


<!-- //_/_/_/_/_/_/_ START _/_/_/_/_/_/_// -->
        <form action="./goods_img_save.php" method="POST" name="form_write" id="form_regist" class="form-horizontal form-bordered">
        <div class="ibox-content">


            <div class="form-group">
                <label class="col-sm-2 control-label">管理ID</label>
                <div class="col-sm-6">
                    <p class="form-control-static">
                        <? $var = "goods_img_id";?>
                        <?php
                        if($$var=="")
                        {
                            echo "自動生成";
                        }
                        else
                        {
                            echo $$var;
                        }
                        ?>
                        <input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
                        <label id="err_<?php echo $var;?>"><? if($$var!=""){ echo "（変更出来ません）";} ?></label>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <? $var = "goods_img_title";?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">タイトル</label>
                <div class="col-sm-6">
                    <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control count-control" maxlength="50" placeholder="タイトルを最大50文字以内で入力して下さい。"> 
                    <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>
            <div class="form-group">
                <? $var = "goods_img_title";?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">表示順位</label>
                <div class="col-sm-6">
                    <? $var = "view_level";?>
                    <?
                    if($view_level=="")
                    {
                        $sql = "select max(goods_img_id) as view_level from goods_img ";
                        $db_result = $common_dao->db_query_bind($sql);
                        $view_level = $db_result[0]["view_level"]+1;
                    }
                    ?>
                                <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="number" class="form-control count-control" maxlength="5" placeholder="数字のみ入力してください。" style="width: 100px;">
                                <span id="err_<?php echo $var;?>"></span>
                                <span class="help-block m-b-none">低い順番から表示されます。</span>
                </div>
            </div>
            <?
            if($img_1!="")
            {
            ?>
            <div class="form-group">
                <? $var = "goods_img_title";?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">登録されている画像</label>
                <div class="col-sm-6">
                <?
                    echo "<img src='/".global_company_dir.$company_id."/goods_img/".$img_1."?".date("his")."' width='150'>";
                ?>
                </div>
            </div>
            <?
            }
            ?>

            


            <div class="row">
              <div class="col-md-8">
                <div class="wrap-crop">
                    <div class="image-crop">
                      <img src="/app_company/img/goods_sample.jpg">
                    </div>
                </div>
              </div>

              <div class="col-md-4">
                <h4>Preview image</h4>
                <div class="img-preview img-preview-main"></div>
                <p>
                  画像の好きな位置に枠を合わせて下さい。<br>
                  枠は好きな大きさに変えられます。
                </p>
              
                <div class="btn-group">
                  <button class="btn btn-primary" id="zoomIn" type="button"><i class="fa fa-search-plus"></i></button>
                  <button class="btn btn-primary" id="zoomOut" type="button"><i class="fa fa-search-minus"></i></button>
                  <button class="btn btn-primary" id="rotateRight" type="button"><i class="fa fa-rotate-left"></i></button>
                  <button class="btn btn-primary" id="rotateLeft" type="button"><i class="fa fa-rotate-right"></i></button>
                  <label title="画像をアップロードする" for="inputImage" class="btn btn-primary">
                    <input type="file" accept="image/*" name="file" id="inputImage" class="hide">
                    <i class="fa fa-upload"></i>
                  </label>
                </div>
                <br>
                <br>
                <div class="onoffswitch">
                    <? $var = "check_img";?>
                <label class="switch switch-primary">
                    <input type="checkbox" name="<? echo $var;?>" value="1" class="onoffswitch-checkbox" id="<? echo $var;?>">
                    <span class="onoffswitch-inner"></span>
                </label>
            サムネイルを反映する
                </div>
                <div class="form-group">
                    <? $var = "form_confirm";?>
                    <button class="btn btn-danger" type="submit" id="<? echo $var;?>"><? echo $page_pre_title;?></button>
                </div>

            </div>
          </div>
  




        <!--イメージクリッパー-->

<input type="hidden" name="cropped" class="cropped" >

<!--イメージクリッパーサムネイル画像表示-->





            
        </div>
        <input type="hidden" name="company_id" value="<? echo $_SESSION["company_id"]?>">
        <input type="hidden" name="goods_id" value="<? echo $goods_id;?>">
        </form>


    </div>
    <!-- END Example Block -->



    <!-- 商品リスト Content -->
    <div class="block full" id="search_list">
        <div class="block-title">
            <h2><strong>テンプレートリスト</strong></h2>
        </div>
        <p>登録したテンプレートのリストから編集、削除が可能です。<br />表示順位昇順、ID降順で表示されます。</p>
<?php
    
    $view_count=20;   // List count
    $offset=0;

    if(!$page)
    {
        $page=1;
    }
    Else
    {
        $offset=$view_count*($page-1);
    }
    
    $where = " and goods_id='".$goods_id."' ";

    if($s_keyword!="")
    {
        $where .= " and (goods_img_title collate utf8_unicode_ci like '%".$s_keyword."%') ";
    }

    //合計
    $all_count = 0;
    $sql = "SELECT count(goods_img_id) as all_count FROM goods_img where 1 ".$where ;
    $arr_goods_all = $common_dao->db_query_bind($sql);
    $all_count = $arr_goods_all[0]["all_count"];


    $arr_db_field = array("goods_img_id", "goods_id", "goods_img_title", "img_1");
    $arr_db_field = array_merge($arr_db_field, array("view_level", "regi_date", "up_date"));

    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM goods_img where 1 ".$where;
    if($order_name != "")
    {
        $sql .= " order by ".$order_name." ".$order;
    }
    else
    {
        $sql .= " order by view_level, up_date desc";
    }
    $sql .= " limit $offset,$view_count";
?>
        <!-- search -->
        <div class="table-responsive">
            <div class="dataTables_wrapper">
                <div class="row">
                    <div class="col-sm-3 col-xs-5">
                        <label>전체：<? echo $all_count;?>건</label>
                    </div>
                    <div class="col-sm-3">
                    </div>

                    <form action="<?php echo $_SERVER['PHP_SELF']?>#search_list" method="get" name="form_shop_search">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">

                            <? $var = "s_keyword";?>
                            <input name="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="テンプレート名" class="m-b form-control"> 
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-md btn-primary"> Go!</button> 
                            </span>
                        </div>
                    </div>
                    <? $var = "goods_id";?>
                    <input name="<? echo $var;?>" value="<? echo $$var;?>" type="hidden"> 
                    </form>
                </div>
            </div>
        </div>
        <!-- END search -->

        <div class="table-responsive">
            <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th>写真</th>
                        <th>タイトル</th>
                        <th class="text-center">表示順位</th>
                        <th class="text-center">修正日</th>
                        <th class="text-center">編集</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 

                    $db_result = $common_dao->db_query_bind($sql);
                    if($db_result)
                    {
                        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
                        {
                            foreach($arr_db_field as $val)
                            {
                                $$val = $db_result[$db_loop][$val];
                            }
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $goods_img_id; ?></td>
                        <td class="text-center">
                            <div>
                            <?php
                            if($img_1!="")
                            {
                                echo "<a href='/".global_company_dir.$company_id."/goods_img/".$img_1."?".date("his")."' data-toggle='lightbox-image'><img src='/".global_company_dir.$company_id."/goods_img/".$img_1."?".date("his")."' style='max-height:100px; '></a>";
                            }
                            ?>
                            </div>
                        </td>
                        <td><?php echo $goods_img_title; ?></td>
                        <td class="text-center"><?php echo $view_level; ?></td>
                        <td class="text-center"><?php echo $up_date; ?></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="./goods_img_list.php?goods_img_id=<? echo $goods_img_id;?>&goods_id=<? echo $goods_id;?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                <a href="#" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger" onClick='fnChangeSel("<?php echo $goods_id;?>", "<?php echo $goods_img_id;?>");'><i class="fa fa-times"></i></a>

                            </div>
                        </td>
                    </tr>
                    <?php

                        }
                    }
                     ?>
                </tbody>
            </table>
        </div>
        <!-- paging -->
        <?php $common_connect -> Fn_paging_10_list($view_count, $all_count); ?>
        <!-- END paging -->
    </div>
    <!-- END 商品リスト Content -->

<!-- //_/_/_/_/_/_/_ END _/_/_/_/_/_/_// -->

</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/page_footer.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_scripts.php'; ?>


<!-- Image cropper -->
<script src="/app_company/js/plugins/cropper/cropper.min.js"></script>

<script>//トリミング
    var $image = $(".image-crop > img");
    $(document).ready(function(){
        var URL = window.URL || window.webkitURL;
        var uploadedImageURL;
        var options = {
            aspectRatio:  1,//1.7777777777777777
            preview: '.img-preview'
        };

        $($image).cropper(options);

        var $inputImage = $("#inputImage");
        if (URL) {
            $inputImage.change(function() {
                var files = this.files;
                var file;

                if (!files.length) {
                    return;
                }

                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    if (uploadedImageURL) {
                        URL.revokeObjectURL(uploadedImageURL);
                    }

                    uploadedImageURL = URL.createObjectURL(file);
                        $('#check_img').prop("checked",true); 

                    $image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
                    $inputImage.val('');
                } else {
                    showMessage("Please choose an image file.");
                }
            });
        } else {
            $inputImage.addClass("hide");
        }

        $("#zoomIn").click(function() {
            $image.cropper("zoom", 0.1);
        });

        $("#zoomOut").click(function() {
            $image.cropper("zoom", -0.1);
        });

        $("#rotateLeft").click(function() {
            $image.cropper('clear');
            $image.cropper("rotate", 45);
            $image.cropper('crop');
        });

        $("#rotateRight").click(function() {
            $image.cropper('clear');
            $image.cropper("rotate", -45);
            $image.cropper('crop');
        });

        $("#setDrag").click(function() {
            $('.cropped').val($image.cropper("getCroppedCanvas").toDataURL('image/png'));
            $image.cropper("setDragMode", "crop");
        });


    });

</script>

<script src="/app_company/js/plugins/count/bootstrap-maxlength.js"></script>

<script>
$(document).ready(function () {
  $('.count-control').maxlength({
    alwaysShow: true,
    warningClass: "label label-success",
    limitReachedClass: "label label-danger"
  });
});
</script>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_end.php'; ?>