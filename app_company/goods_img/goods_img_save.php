<?php
	//error_reporting(0);
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	

	foreach($_POST as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}

    $common_connect -> Fn_company_check();
    $company_id = $_SESSION["company_id"];
	

	if($goods_img_title == "" || $company_id=="")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
    
    $datetime = date("Y/m/d H:i:s");
    
    //array
    $arr_db_field = array("goods_img_title");
    $arr_db_field = array_merge($arr_db_field, array("view_level"));
    

    if($goods_img_id=="")
    {
        $db_insert = "insert into goods_img ( ";
        $db_insert .= " goods_img_id, goods_id, ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val.", ";
        }
        $db_insert .= " up_date, regi_date ";
        $db_insert .= " ) values ( ";
        $db_insert .= " '', '".$goods_id."', ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= " '".$$val."', ";
        }
        $db_insert .= " '$datetime', '$datetime')";
    }
    else
    {
        $db_insert = "update goods_img set ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val."='".$$val."', ";
        }
        $db_insert .= " up_date='".$datetime."' ";
        $db_insert .= " where goods_id='".$goods_id."' and goods_img_id='".$goods_img_id."' ";
    }
    $common_dao -> db_update($db_insert);

    if ($goods_img_id == "")
    {
        //自動生成されてるID出力
        $sql = "select last_insert_id() as last_id";
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            $goods_img_id = $db_result[0]["last_id"];
        }
    }


    if($check_img=="1")
    {
        //Folder生成
        $up_file_name = "goods_img_".$goods_img_id.".png";
        $save_dir = $global_path.global_company_dir.$company_id."/goods_img/";
        $fname_new_name[1] = $up_file_name;

        //Folder生成
        $common_image -> create_folder ($save_dir);
        //ヘッダに「data:image/png;base64,」が付いているので、それは外す
        $cropped = preg_replace("/data:[^,]+,/i","",$cropped);
        //残りのデータはbase64エンコードされているので、デコードする
        $cropped = base64_decode($cropped);
        
        //まだ文字列の状態なので、画像リソース化
        $image = imagecreatefromstring($cropped);
     
        imagesavealpha($image, TRUE); // 透明色の有効
        imagepng($image ,$save_dir.$fname_new_name[1]);
        
        $common_image -> create_thumbnail($save_dir.$up_file_name, "", $save_dir, 300);
        /*
        $new_end_name="_1";
        $fname_new_name[1] = $common_image -> img_save("img_1", $common_connect, $save_dir, $new_end_name, $company_id, $text_img_1, "");
        */
        
        $img_1 = $fname_new_name[1];

        //array
        $db_insert = "update goods_img set ";
        $db_insert .= " img_1= '".$img_1."', ";
        $db_insert .= " up_date= '".$datetime."' ";
        $db_insert .= " where goods_id='".$goods_id."' and goods_img_id='".$goods_img_id."' ";
        $common_dao -> db_update($db_insert);

    }


	
	$common_connect-> Fn_javascript_move("登録・修正しました", "goods_img_list.php?goods_id=".$goods_id."&goods_img_id=".$goods_img_id);
?>
</body>
</html>