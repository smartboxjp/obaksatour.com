<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    $common_image = new CommonImage(); //画像
    
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>店舗登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php   
    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_company_check();
    $session_company_id = $_SESSION["company_id"];
    

    
    if($session_company_id!=$company_id)
    {
        $common_connect-> Fn_javascript_back("変更するログイン情報が違います。再度ログインしてください。");
    }

    if($goods_name == "" || $search_price == "")
    {
        $common_connect -> Fn_javascript_back("正しく入力してください。");
    }
    
    $datetime = date("Y/m/d H:i:s");
    $user_agent = substr(getenv("HTTP_USER_AGENT"), 0, 250);
    
    //array
    $arr_db_field = array("cate_goods_s_id", "cate_area_id", "goods_name", "goods_name_jp");
    $arr_db_field = array_merge($arr_db_field, array("goods_summary", "goods_from", "goods_to", "search_price", "search_price_view"));
    $arr_db_field = array_merge($arr_db_field, array("temp_goods_detail_id"));
    $arr_db_field = array_merge($arr_db_field, array("goods_keyword", "googlemap_1", "googlemap_2"));
    $arr_db_field = array_merge($arr_db_field, array("flag_open"));
    

    if($goods_id=="")
    {
        $db_insert = "insert into goods ( ";
        $db_insert .= " goods_id, ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val.", ";
        }
        $db_insert .= " up_date, regi_date ";
        $db_insert .= " ) values ( ";
        $db_insert .= " '', ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= " '".$$val."', ";
        }
        $db_insert .= " '$datetime', '$datetime')";
    }
    else
    {
        $db_insert = "update goods set ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val."='".$$val."', ";
        }
        $db_insert .= " up_date='".$datetime."' ";
        $db_insert .= " where company_id='".$company_id."' and goods_id='".$goods_id."' ";
    }
    $common_dao -> db_update($db_insert);

    if ($goods_id == "")
    {
        //自動生成されてるID出力
        $sql = "select last_insert_id() as last_id";  
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            $goods_id = $common_connect -> $db_result[0]["last_id"];
        }
    }

    if($check_img=="1")
    {
        //Folder生成
        $up_file_name = "goods_img_".$goods_id.".png";
        $save_dir = $global_path.global_company_dir.$company_id."/goods/";
        $fname_new_name[1] = $up_file_name;
        
        //Folder生成
        $common_image -> create_folder ($save_dir);
        //ヘッダに「data:image/png;base64,」が付いているので、それは外す
        $cropped = preg_replace("/data:[^,]+,/i","",$cropped);
        
        //残りのデータはbase64エンコードされているので、デコードする
        $cropped = base64_decode($cropped);
        
        //まだ文字列の状態なので、画像リソース化
        $image = imagecreatefromstring($cropped);
     
        imagesavealpha($image, TRUE); // 透明色の有効
        imagepng($image ,$save_dir.$fname_new_name[1]);
        
        $common_image -> create_thumbnail($save_dir.$up_file_name, "", $save_dir, 300);
        /*
        $new_end_name="_1";
        $fname_new_name[1] = $common_image -> img_save("img_1", $common_connect, $save_dir, $new_end_name, $company_id, $text_img_1, "");
        */
        
        //画像
        $arr_where = array();
        $arr_where["company_id"] = $company_id;
        
        $goods_thumbnail = $fname_new_name[1];
        $up_date = $datetime;
        
        //array
        $arr_db_field = array("goods_thumbnail", "up_date");
        
        $db_insert = "update goods set ";
        foreach($arr_db_field as $key=>$value)
        {
            $db_insert .= $value."= '".$$value."', ";
        }
        $db_insert .= " up_date= '".$datetime."' ";
        $db_insert .= " where company_id='".$company_id."' and goods_id='".$goods_id."' ";
        $common_dao -> db_update($db_insert);

    }
    
    $common_connect-> Fn_javascript_move("登録・修正しました", "./goods_list.php");
?>
</body>
</html>