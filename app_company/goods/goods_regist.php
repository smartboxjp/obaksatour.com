<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_company_check();
    $company_id = $_SESSION["company_id"];

    $page_pre_title = "登録";
    if($goods_id!="")
    {
        $arr_db_field = array("goods_id", "company_id", "cate_goods_s_id", "cate_area_id", "goods_name", "goods_name_jp");
        $arr_db_field = array_merge($arr_db_field, array("goods_summary", "goods_from", "goods_to", "search_price", "search_price_view"));
        $arr_db_field = array_merge($arr_db_field, array("temp_goods_detail_id"));
        $arr_db_field = array_merge($arr_db_field, array("goods_keyword", "googlemap_1", "googlemap_2", "goods_thumbnail", "goods_comment"));
        $arr_db_field = array_merge($arr_db_field, array("regi_date", "up_date", "flag_open"));

        $sql = "SELECT ";
        foreach($arr_db_field as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " 1 FROM goods where company_id='".$company_id."' and goods_id='".$goods_id."' ";

        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
            {
                foreach($arr_db_field as $val)
                {
                    $$val = $db_result[$db_loop][$val];
                }
            }
        }

        $page_pre_title = "修正";
    }

    //商品カテゴリ
    $arr_cate_goods_s = array();
    $sql = " select s.cate_goods_s_id, s.cate_goods_l_id, cate_goods_l_title, cate_goods_s_title FROM cate_goods_l l inner join cate_goods_s s on l.cate_goods_l_id=s.cate_goods_l_id order by l.view_level, s.view_level" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_goods_s[$db_result[$db_loop]["cate_goods_s_id"]] = $db_result[$db_loop]["cate_goods_l_title"]." - ".$db_result[$db_loop]["cate_goods_s_title"];
        }
    }

    //商品エリア
    $arr_cate_area = array();
    $sql = " SELECT cate_area_id, cate_area_title FROM cate_area order by view_level" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_area[$db_result[$db_loop]["cate_area_id"]] = $db_result[$db_loop]["cate_area_title"];
        }
    }

    //商品説明
    $arr_temp_goods_detail = array();
    $sql = " SELECT temp_goods_detail_id, temp_goods_detail_title FROM temp_goods_detail order by view_level" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_temp_goods_detail[$db_result[$db_loop]["temp_goods_detail_id"]] = $db_result[$db_loop]["temp_goods_detail_title"];
        }
    }
    
    $arr_goods_flag_open = array();
    $arr_goods_flag_open[1] = "公開";
    $arr_goods_flag_open[99] = "非公開";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/config.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_start.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/page_head.php'; ?>

<link href="/app_master/css/plugins/cropper/cropper.min.css" rel="stylesheet">
<link href="/app_company/css/company.css" rel="stylesheet">

<script src="/app_company/js/jquery-2.1.1.js"></script>
<script type="text/javascript">
    $(function() {
            
        $('#form_confirm').click(function() {
            err_default = "";
            err_check_count = 0;
            bgcolor_default = "#FFFFFF";
            bgcolor_err = "#FFCCCC";
            background = "background-color";
            
            err_check_count += check_input("goods_name");
            err_check_count += check_input("search_price");
            
            if(err_check_count!=0)
            {
                alert("入力に不備があります");
                return false;
            }
            else
            {
                $('.cropped').val($image.cropper("getCroppedCanvas").toDataURL('image/png'));
                $image.cropper("setDragMode", "crop");

                //$('#form_confirm').submit();
                $('#form_confirm', "body").submit();
                return true;
            }
            
            
        });
                
        function check_input($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);

            if($('#'+$str).val()=="")
            {
                err ="<span class='errorForm'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }

        function check_input_id($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);
            
            if($('#'+$str).val()=="")
            {
                err ="<span class='errorForm'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            else if(checkID($('#'+$str).val())==false)
            {
                err ="<span class='errorForm'>英数半角で入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            else if($('#'+$str).val().length<3)
            {
                err ="<span class='errorForm'>3文字以上で入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }
        
        //英数半角
        function checkID(value){
            if(value.match(/[^0-9a-z]+/) == null){
                return true;
            }else{
                return false;
            }
        } 
             

    });
    
//-->
</script>



<!-- Page content -->
<div id="page-content">
    <!-- 商品基本<? echo $page_pre_title;?> Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-brush"></i>商品基本<? echo $page_pre_title;?><br><small>商品を<? echo $page_pre_title;?>することが出来ます。</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/app_goods/dashboard/">Dashboard</a></li>
        <li><a href="">商品基本<? echo $page_pre_title;?></a></li>
    </ul>
    <!-- END 商品基本<? echo $page_pre_title;?> Header -->


    <!-- Example Block -->
    <div class="block">
        <!-- Example Title -->
        <div class="block-title">
            <h2>商品基本設定</h2>
        </div>
        <!-- END Example Title -->

<!-- //_/_/_/_/_/_/_ START _/_/_/_/_/_/_// -->
        <form action="./goods_save.php" method="POST" name="form_write" id="form_regist" enctype="multipart/form-data" class="form-horizontal form-bordered">
        <div class="ibox-content">

            <div class="form-group">
                <label class="col-sm-2 control-label">商品管理ID</label>
                <div class="col-sm-6">
                    <p class="form-control-static">
                        <? $var = "goods_id";?>
                        <?php
                        if($$var=="")
                        {
                            echo "自動生成";
                        }
                        else
                        {
                            echo $$var;
                        }
                        ?>
                        <input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
                        <label id="err_<?php echo $var;?>"><? if($$var!=""){ echo "（変更出来ません）";} ?></label>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <? $var = "goods_name";?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">商品名</label>
                <div class="col-sm-6">
                    <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control count-control" maxlength="50" placeholder="商品名を最大50文字以内で入力して下さい。"> 
                    <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>

            <div class="form-group">
                <? $var = "goods_name_jp";?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">商品日本語名</label>
                <div class="col-sm-6">
                    <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control count-control" maxlength="50" placeholder="商品名を日本語で入力して下さい。"> 
                    <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>

            <div class="form-group">
                <? $var = "cate_goods_s_id";?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">商品カテゴリ</label>
                <div class="col-sm-6">
                    <select name="<?php echo $var;?>" id="<?php echo $var;?>" class="form-control" >
                    <option value="">商品カテゴリを選択してください</option>
                    <?
                    foreach($arr_cate_goods_s as $key=>$value)
                    {
                    ?>
                    <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
                    <?
                    }
                    ?>
                    </select>
                    <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>
            
            <div class="form-group">
                <? $var = "cate_area_id";?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">商品エリア</label>
                <div class="col-sm-6">
                    <select name="<?php echo $var;?>" id="<?php echo $var;?>" class="form-control" >
                    <option value="">商品エリアを選択してください</option>
                    <?
                    foreach($arr_cate_area as $key=>$value)
                    {
                    ?>
                    <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
                    <?
                    }
                    ?>
                    </select>
                    <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>
            
            <div class="form-group">
                <? $var = "temp_goods_detail_id";?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">詳細情報テンプレート</label>
                <div class="col-sm-6">
                    <select name="<?php echo $var;?>" id="<?php echo $var;?>" class="form-control" >
                    <option value="">詳細情報テンプレートを選択してください</option>
                    <?
                    foreach($arr_temp_goods_detail as $key=>$value)
                    {
                    ?>
                    <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
                    <?
                    }
                    ?>
                    </select>
                    <span id="err_<?php echo $var;?>"></span>
                    <span class="help-block m-b-none">テンプレートから選択してください。<a href="/app_company/temp_goods_detail/temp_goods_detail_list.php">詳細情報テンプレート登録</a></span>
                </div>
            </div>

            <div class="form-group">
                <? $var = "goods_summary";?>
                <label class="col-sm-2 control-label">商品説明</label>
                <div class="col-sm-6">
                    <textarea name="<?php echo $var;?>" id="<?php echo $var;?>" class="form-control count-control" maxlength="50"><?php echo $$var;?></textarea>
                    <span id="err_<?php echo $var;?>"></span>
                    <span class="help-block m-b-none">検索一覧に表示されます</span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">販売金額</label>
                <div class="col-sm-6">
                  <? $var = "search_price";?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" type="number" class="form-control count-control" maxlength="10" placeholder="数字のみ表示">
                  <span id="err_<?php echo $var;?>"></span>
                  <span class="help-block m-b-none">販売している最低の表示金額<br />検索一覧に「 00,000円〜」と表示されます</span>
                </div>
            </div>

            <div class="form-group">
                <? $var = "goods_keyword";?>
                <label class="col-sm-2 control-label">キーワード</label>
                <div class="col-sm-6">
                    <input type="text" id="<?php echo $var;?>" name="<?php echo $var;?>" class="input-tags" value="<?php echo $$var;?>" >
                    <span id="err_<?php echo $var;?>"></span>
                    <span class="help-block m-b-none">キーワードを入力してください。</span>
                </div>
            </div>

            <div class="form-group">
                <? $var = "goods_from";?>
                <? if($$var=="") { $$var = date("Y-m-d");}?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">販売可能期間</label>
                <div class="col-sm-6">
                    <div class="input-group input-daterange" data-date-format="yyyy/mm/dd">
                        <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" class="form-control text-center" placeholder="から">
                        <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>
                        <? $var = "goods_to";?>
                        <? if($$var=="") { $$var = date("9999-12-31");}?>
                        <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>"  class="form-control text-center" placeholder="まで">
                    </div>

                    <span id="err_goods_from_to"></span>
                    <span class="help-block m-b-none">設定は必須です<br />設定期間がない場合は「<php echo date("Y-m-d");>」から「9999-12-31」までに設定してください。</span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">公開・非公開</label>
              <div class="col-sm-6">
                <? $var = "flag_open";?>
                <select name="<? echo $var;?>" id="<? echo $var;?>">
                <?
                foreach($arr_goods_flag_open as $key=>$value)
                {
                ?>
                  <option value="<? echo $key;?>" <? if($key == $$var) { echo " selected ";}?>><? echo $value;?></option>
                <?
                }
                ?>
                </select>
                <span id="err_<?php echo $var;?>"></span>
              </div>
            </div>

            <div class="ibox-title  back-change">
                <h5>お店のサムネイル画像を登録 <small>※無登録でも可能。また店舗管理で登録や編集も出来ます。</small></h5>
            </div>
            <div class="ibox-content">
                <p>
                    お店のサムネイル画像をトリミングで編集登録が出来ます。120px×120pxで表示されます
                </p>
                <div class="row">
                    <div class="col-md-6">
                       <div class="wrap-crop">
                            <div class="image-crop">
                                <img src="/app_company/img/goods_sample.jpg">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h4>Preview image</h4>
                        <div class="img-preview img-preview-sm"></div>
                        <p>
                            画像の好きな位置に枠をあわせて決定を押してください。
                        </p>
                        <div class="btn-group">
                            <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                <input type="file" accept="image/*" name="file" id="inputImage" class="hide">
                                カットしたい画像をアップ
                            </label>
                            
                        </div>
        <br>
                        <div class="btn-group">
                          <button class="btn btn-primary" id="zoomIn" type="button"><i class="fa fa-search-plus"></i></button>
                          <button class="btn btn-primary" id="zoomOut" type="button"><i class="fa fa-search-minus"></i></button>
                          <button class="btn btn-primary" id="rotateRight" type="button"><i class="fa fa-rotate-left"></i></button>
                          <button class="btn btn-primary" id="rotateLeft" type="button"><i class="fa fa-rotate-right"></i></button>
                        </div>
        <br>
                        <div class="btn-group">
                            <div class="onoffswitch">
                                <? $var = "check_img";?>
                            <label class="switch switch-primary">
                                <input type="checkbox" name="<? echo $var;?>" value="1" class="onoffswitch-checkbox" id="<? echo $var;?>">
                                <span class="onoffswitch-inner"></span>
                            </label>
                            サムネイルを反映する
                            </div>
                          <?
                            if($goods_thumbnail!="")
                            {
                                echo "<img src='/".global_company_dir.$company_id."/goods/".$goods_thumbnail."?".date("his")."'>";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>




        <!--イメージクリッパー-->

<input type="hidden" name="cropped" class="cropped">

<!--イメージクリッパーサムネイル画像表示-->



<!--イメージクリッパーサムネイル画像表示-->

<!--ボタン-->
            <div class="form-group">
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-2">
                    <? $var = "form_confirm";?>
                    <button class="btn btn-danger" type="submit" id="<? echo $var;?>" style="width: 100%; height: 50px; font-size: 200%;"><? echo $page_pre_title;?></button>
                </div>
            </div>
<!--ボタン-->


            
        </div>
        <input type="hidden" name="company_id" value="<? echo $_SESSION["company_id"]?>">
        </form>

<!-- //_/_/_/_/_/_/_ END _/_/_/_/_/_/_// -->
    </div>
    <!-- END Example Block -->
</div>
<!-- END Page Content -->


<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/page_footer.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_scripts.php'; ?>









<!-- Image cropper -->
<script src="/app_company/js/plugins/cropper/cropper.min.js"></script>

<script>//トリミング
    var $image = $(".image-crop > img");
    $(document).ready(function(){
        var URL = window.URL || window.webkitURL;
        var uploadedImageURL;
        var options = {
            aspectRatio:  1,//1.7777777777777777
            preview: '.img-preview'
        };

        $($image).cropper(options);

        var $inputImage = $("#inputImage");
        if (URL) {
            $inputImage.change(function() {
                var files = this.files;
                var file;

                if (!files.length) {
                    return;
                }

                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    if (uploadedImageURL) {
                        URL.revokeObjectURL(uploadedImageURL);
                    }

                    uploadedImageURL = URL.createObjectURL(file);
                        $('#check_img').prop("checked",true); 

                    $image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
                    $inputImage.val('');
                } else {
                    showMessage("Please choose an image file.");
                }
            });
        } else {
            $inputImage.addClass("hide");
        }

        $("#zoomIn").click(function() {
            $image.cropper("zoom", 0.1);
        });

        $("#zoomOut").click(function() {
            $image.cropper("zoom", -0.1);
        });

        $("#rotateLeft").click(function() {
            $image.cropper('clear');
            $image.cropper("rotate", 45);
            $image.cropper('crop');
        });

        $("#rotateRight").click(function() {
            $image.cropper('clear');
            $image.cropper("rotate", -45);
            $image.cropper('crop');
        });

        $("#setDrag").click(function() {
            $('.cropped').val($image.cropper("getCroppedCanvas").toDataURL('image/png'));
            $image.cropper("setDragMode", "crop");
        });


    });

</script>

<script src="/app_company/js/plugins/count/bootstrap-maxlength.js"></script>

<script>
$(document).ready(function () {
  $('.count-control').maxlength({
    alwaysShow: true,
    warningClass: "label label-success",
    limitReachedClass: "label label-danger"
  });
});
</script>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_end.php'; ?>