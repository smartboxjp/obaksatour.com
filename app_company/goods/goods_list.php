<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_company_check();
    $company_id = $_SESSION["company_id"];
    

    //商品カテゴリ
    $arr_cate_goods_s = array();
    $sql = " select s.cate_goods_s_id, s.cate_goods_l_id, cate_goods_l_title, cate_goods_s_title FROM cate_goods_l l inner join cate_goods_s s on l.cate_goods_l_id=s.cate_goods_l_id order by l.view_level, s.view_level" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_goods_s[$db_result[$db_loop]["cate_goods_s_id"]] = $db_result[$db_loop]["cate_goods_l_title"]." - ".$db_result[$db_loop]["cate_goods_s_title"];
        }
    }

    //商品エリア
    $arr_cate_area = array();
    $sql = " SELECT cate_area_id, cate_area_title FROM cate_area order by view_level" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_area[$db_result[$db_loop]["cate_area_id"]] = $db_result[$db_loop]["cate_area_title"];
        }
    }
    
    $arr_company_flag_open = array();
    $arr_company_flag_open[1] = "公開";
    $arr_company_flag_open[99] = "非公開";

    $arr_company_flag_open_color = array();
    $arr_company_flag_open_color[1] = "label-success";
    $arr_company_flag_open_color[99] = "label-danger";
?>

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/config.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_start.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- 商品リスト Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-table"></i>商品リスト<br><small></small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/app_goods/dashboard/">Dashboard</a></li>
        <li>商品リスト</li>
    </ul>
    <!-- END 商品リスト Header -->

    <!-- 商品リスト Content -->
    <div class="block full">
        <div class="block-title">
            <h2><strong>商品リスト</strong></h2>
        </div>
        <p>登録した商品のリストから編集、削除が可能です。</p>
<?php
    
    $view_count=20;   // List count
    $offset=0;

    if(!$page)
    {
        $page=1;
    }
    Else
    {
        $offset=$view_count*($page-1);
    }
    
    $where = " and company_id='".$company_id."' ";

    if($s_keyword!="")
    {
        $where .= " and (goods_name collate utf8_unicode_ci like '%".$s_keyword."%' or goods_name_jp collate utf8_unicode_ci like '%".$s_keyword."%' or goods_keyword collate utf8_unicode_ci like '%".$s_keyword."%' or goods_id = '".$s_keyword."' ) ";
    }

    if($s_flag_open!="")
    {
        $where .= " and flag_open='".$s_flag_open."'";
    }

    //合計
    $all_count = 0;
    $sql = "SELECT count(goods_id) as all_count FROM goods where 1 ".$where ;
    $arr_goods_all = $common_dao->db_query_bind($sql);
    $all_count = $arr_goods_all[0]["all_count"];


    $arr_db_field = array("goods_id", "company_id", "cate_goods_s_id", "cate_area_id", "goods_name", "goods_name_jp");
    $arr_db_field = array_merge($arr_db_field, array("goods_summary", "goods_from", "goods_to", "search_price", "search_price_view"));
    $arr_db_field = array_merge($arr_db_field, array("temp_goods_detail_id"));
    $arr_db_field = array_merge($arr_db_field, array("goods_keyword", "googlemap_1", "googlemap_2", "goods_thumbnail", "goods_comment"));
    $arr_db_field = array_merge($arr_db_field, array("regi_date", "up_date", "flag_open"));

    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM goods where 1 ".$where;
    if($order_name != "")
    {
        $sql .= " order by ".$order_name." ".$order;
    }
    else
    {
        $sql .= " order by regi_date desc";
    }
    $sql .= " limit $offset,$view_count";
?>
        <!-- search -->
        <div class="table-responsive">
            <div id="example-datatable_wrapper" class="dataTables_wrapper form-inline no-footer">
                <div class="row">
                    <div class="col-sm-3 col-xs-5">
                        <div class="dataTables_length" id="example-datatable_length">
                            <label>すべて：<? echo $all_count;?>件</label>
                        </div>
                    </div>

                      <form action="<?php echo $_SERVER['PHP_SELF']?>" method="get" name="form_shop_search">
                      <div class="col-sm-3">
                          <? $var = "s_cate_goods_s";?>
                          <select name="<?php echo $var;?>" id="<?php echo $var;?>" class="form-control m-b" >
                          <option value="">検索したいお店のカテゴリ</option>
                          <?
                          foreach($arr_cate_goods_s as $key=>$value)
                          {
                          ?>
                          <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
                          <?
                          }
                          ?>
                          </select>
                      </div>
                      <div class="col-sm-3">
                        <? $var = "s_arr_cate_area";?>
                        <select class="form-control m-b" name="<?php echo $var;?>" id="<?php echo $var;?>">
                            <option value="">検索したいお店のエリア</option>
                            <?
                            foreach($arr_cate_area as $key=>$value)
                            {
                            ?>
                            <option value="<? echo $key;?>" <? if($$var==$key) { echo " selected ";}?>><? echo $value;?></option>
                            <?
                            }
                            ?>
                        </select>
                      </div>
                      <div class="col-sm-3">
                        <div class="input-group">

                          <? $var = "s_keyword";?>
                          <input name="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="商品名又は管理ID" class="m-b form-control"> 
                          <span class="input-group-btn">
                            <button type="submit" class="btn btn-md btn-primary"> Go!</button> 
                          </span>
                        </div>
                      </div>
                      </form>
                </div>
            </div>
        </div>
        <!-- END search -->
        
        <div class="table-responsive">
            <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">商品名</th>
                        <th class="text-center">写真</th>
                        <th>表示金額</th>
                        <th>カテゴリ</th>
                        <th>エリア</th>
                        <th>販売期間</th>
                        <th>公開有無</th>
                        <th class="text-center">写真</th>
                        <th class="text-center">編集</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 

                    $db_result = $common_dao->db_query_bind($sql);
                    if($db_result)
                    {
                        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
                        {
                            foreach($arr_db_field as $val)
                            {
                                $$val = $db_result[$db_loop][$val];
                            }
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $goods_id; ?></td>
                        <td class="text-center"><?php echo $goods_name."<br >".$goods_name_jp; ?></td>
                        <td class="text-center">
                            <div>
                            <?php
                            if($goods_thumbnail!="")
                            {
                                echo "<a href='/".global_company_dir.$company_id."/goods/".$goods_thumbnail."?".date("his")."' data-toggle='lightbox-image'><img src='/".global_company_dir.$company_id."/goods/".$goods_thumbnail."?".date("his")."' style='max-height:100px; '></a>";
                            }
                            ?>
                            </div>
                        </td>
                        <td class="text-center"><?php echo number_format($search_price); ?></td>
                        <td class="text-center"><?php echo $arr_cate_goods_s[$cate_goods_s_id]; ?></td>
                        <td class="text-center"><?php echo $arr_cate_area[$cate_area_id]; ?></td>
                        <td class="text-center"><?php echo $goods_from."〜".$goods_to; ?></td>
                        <?php $rand = rand(0, 3); ?>
                        <td class="text-center"><span class="label <?php echo $arr_company_flag_open_color[$flag_open] ?>"><?php echo $arr_company_flag_open[$flag_open]; ?></span></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="/app_company/goods_img/goods_img_list.php?goods_id=<? echo $goods_id;?>" data-toggle="tooltip" title="写真管理" class="btn btn-xs btn-default"><i class="fa fa-file-photo-o"></i></a>
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="./goods_regist.php?goods_id=<? echo $goods_id;?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                <a href="javascript:void(0)" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                            </div>
                        </td>
                    </tr>
                    <?php

                        }
                    }
                     ?>
                </tbody>
            </table>
        </div>

        <!-- paging -->
        <?php $common_connect -> Fn_paging_10_list($view_count, $all_count); ?>
        <!-- END paging -->
    </div>
    <!-- END 商品リスト Content -->
</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/page_footer.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_scripts.php'; ?>


<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_end.php'; ?>