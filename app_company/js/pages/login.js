
    $(function() {
        $('#form_confirm').click(function() {
            err_default = "";
            err_check_count = 0;
            err_check = false;
            bgcolor_default = "#FFFFFF";
            bgcolor_err = "#FFCCCC";
            background = "background-color";

            err_check_count += check_input("company_id");
            err_check_count += check_input_password("company_pw");
            
            if(err_check_count)
            {
                alert("入力に不備があります");
                return false;
            }
            else
            {
                $('#form_confirm').submit();
                return true;
            }
            
            
        });
        
        function check_input($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);
            
            if($('#'+$str).val().replace(/　/g," ").match(/^\s+$/))
            {
                err ="<span style='color:#F00'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            else if($('#'+$str).val()=="")
            {
                err ="<span style='color:#F00'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }
        
        function check_input_password($str_1) 
        {
            $("#err_"+$str_1).html(err_default);
            $("#"+$str_1).css(background,bgcolor_default);
            if(checkIsPassword($('#'+$str_1).val()) == false)
            {
                err ="<span style='color:#F00'>4文字以上正しく入力してください。</span>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                
                return 1;
            }
            
            return 0;
        }
        
        function checkIsPassword(value) 
        {
            if (value.match(/^[0-9a-zA-Z!#$%&@()*+,./_-]{4,}$/) == null) {
                return false;
            }
            return true;
        }

        
    });