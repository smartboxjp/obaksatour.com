<?php
	//error_reporting(0);
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	

	foreach($_POST as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}

    $common_connect -> Fn_company_check();
    $session_company_id = $_SESSION["company_id"];
    
    if($session_company_id!=$company_id)
    {
        $common_connect-> Fn_javascript_back("変更するログイン情報が違います。再度ログインしてください。");
    }

	if($goods_stock_title == "")
	{
		$common_connect -> Fn_javascript_back("正しく入力してください。");
	}
    
    $datetime = date("Y/m/d H:i:s");
    
    //array
    $arr_db_field = array("goods_stock_title");
    $arr_db_field = array_merge($arr_db_field, array("view_level"));
    

    if($goods_id=="")
    {
        $db_insert = "insert into goods_stock_title ( ";
        $db_insert .= " goods_stock_title_id, ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val.", ";
        }
        $db_insert .= " company_id ";
        $db_insert .= " ) values ( ";
        $db_insert .= " '', ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= " '".$$val."', ";
        }
        $db_insert .= " '".$company_id."')";
    }
    else
    {
        $db_insert = "update goods_stock_title set ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val."='".$$val."', ";
        }
        $db_insert .= " up_date='".$datetime."' ";
        $db_insert .= " where company_id='".$company_id."' and goods_stock_title_id='".$goods_stock_title_id."' ";
    }
    $common_dao -> db_update($db_insert);

    if ($goods_stock_title_id == "")
    {
        //自動生成されてるID出力
        $sql = "select last_insert_id() as last_id";  
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            $goods_stock_title_id = $db_result[0]["last_id"];
        }
    }



	
	$common_connect-> Fn_javascript_move("登録・修正しました", "goods_stock_title_list.php?goods_stock_title_id=".$goods_stock_title_id);
?>
</body>
</html>