<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    $max_month = 6;

    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_company_check();
    $company_id = $_SESSION["company_id"];

    $page_pre_title = "登録";
    if($goods_stock_title_id!="")
    {
        $arr_db_field = array("company_id", "goods_stock_title");
        $arr_db_field = array_merge($arr_db_field, array("view_level"));

        $sql = "SELECT ";
        foreach($arr_db_field as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " 1 FROM goods_stock_title where company_id='".$company_id."' and goods_stock_title_id='".$goods_stock_title_id."' ";

        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
            {
                foreach($arr_db_field as $val)
                {
                    $$val = $db_result[$db_loop][$val];
                }
            }
        }

        $page_pre_title = "修正";
    }
?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/config.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_start.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/page_head.php'; ?>
<script language="javascript"> 
    function fnUp(yyyymm, lastday) {
        var s;
        var d;
        
        for (var q=1;q<=lastday;q++) 
        {
            s = new String(q+100);
            d = s.slice(1);
            document.getElementsByName("s_"+yyyymm+d)[0].value = document.getElementsByName("num_"+yyyymm)[0].value;
        }
    } 
</script>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-brush"></i>テンプレート - 詳細情報<br><small>商品の詳細情報をテンプレートとして管理が出来ます。</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/app_company/dashboard/">Dashboard</a></li>
        <li>詳細情報のテンプレート</li>
    </ul>
    <!-- END Blank Header -->

    <!-- Example Block -->
    <div class="block">
        <!-- Example Title -->
        <div class="block-title">
            <h2>詳細情報のテンプレート</h2>
        </div>
        <!-- END Example Title -->


<!-- //_/_/_/_/_/_/_ START _/_/_/_/_/_/_// -->
        <form action="./goods_stock_save.php" method="POST" name="form_write" id="form_regist" class="form-horizontal form-bordered">
        <div class="ibox-content">

            <div class="form-group">
                <label class="col-sm-2 control-label">管理ID</label>
                <div class="col-sm-6">
                    <p class="form-control-static">
                        <? $var = "goods_stock_id";?>
                        <?php
                        if($$var=="")
                        {
                        echo "自動生成";
                        }
                        ?>
                        <input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
                        <label id="err_<?php echo $var;?>"><? if($$var!=""){ echo $$var ."（変更出来ません）";} ?></label>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">在庫管理タイトル</label>
                <div class="col-sm-6">
                    <p class="form-control-static">
                        <? $var = "goods_stock_title";?>
                        <? echo $$var;?>
                    </p>
                </div>
            </div>

            <div class="form-group">

                <table width="100%" border="0" cellspacing="0" cellpadding="6" align="center" bgcolor="white">
                  <tr valign="top">
                    <td align="center">
                    <?
                    $yyyymmdd = date("Y-m-01");
                    for ($i=0 ; $i < $max_month ; $i++) 
                        {
                            $sql = "SELECT '$yyyymmdd' + INTERVAL $i MONTH as yyyymm";
                            $db_result = $common_dao->db_query_bind($sql); 
                            $yyyy = substr($db_result[0]["yyyymm"], 0, 4);
                            $mm = substr($db_result[0]["yyyymm"], 5, 2);
                            
                            
                            //最初の曜日
                            $firstTime = strtotime($yyyy . "-" . $mm . "-01"); 
                            $firstWeek = date("w", $firstTime); 
                            
                            //最後の日
                            for ($ld = 28; checkdate($mm,$ld,$yyyy); $ld ++); 
                            $lastDay = $ld - 1;
                            
                            //最後の曜日
                            $lastTime = strtotime($yyyy . "-" . $mm . "-" . $lastDay); 
                            $lastWeek = date("w", $lastTime); 
            
                            //DBからarrayへ    
                            $sql = "select * from goods_stock where goods_stock_title_id = '".$goods_stock_title_id."' and yyyymm = '".$yyyy.$mm."'";
                            $db_result = $common_dao->db_query_bind($sql);
                            if($db_result)
                            {
                                for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
                                {
                                    for($schDay = 1; $schDay <= $lastDay; $schDay ++) {
                                        $stock[$yyyy.$mm][substr(($schDay+100),1,2)] = $db_result["s_".$schDay];
                                        $cancel[$yyyy.$mm][substr(($schDay+100),1,2)] = $db_result["c_".$schDay];
                                        $reserve[$yyyy.$mm][substr(($schDay+100),1,2)] = $db_result["r_".$schDay];
                                    }
                                }
                            }
                            else
                            {
                                    //DBをチェックして無ければ生成
                                    $dbup = "insert into goods_stock (goods_stock_title_id, yyyymm) values ('".$goods_stock_title_id."', '".$yyyy.$mm."')";
                                    $common_dao -> db_update($dbup);
                            }
                            
                                    
                            //arrへ保存
                            $arrSCH = array(array(),array(),array(),array(),array(),array(),array()); 
                            
                            //該当する最初の曜日になる前にはブランク
                            for($j = 0; $j < $firstWeek; $j ++) {
                                    $arrSCH[$j][] = ""; 
                            }
                            
                            $maxRow = 0; 
                            for($schDay = 1; $schDay <= $lastDay; $schDay ++) { 
                                    $checkday = mktime(0, 0, 0, $mm, $schDay, $yyyy); 
                                    $week = date("w", $checkday); 
                                    $arrSCH[$week][] = $schDay; 
                            
                                    if(count($arrSCH[$week]) > $maxRow) $maxRow = count($arrSCH[$week]); 
                            }
                            ?>
                            <br />
                            <div style="color:#000419; font-size:40px;" align="center">－　<?=$yyyy."年".$mm."月";?>　－</div>
                            <input type="text" name="num_<?=$yyyy.$mm;?>" value="10" />　
                            <input type="button" onclick="fnUp(<?=$yyyy.$mm;?>, <?=$lastDay;?>)" value="左の数値でリセットする" /><br />
                            <br />
                            
                            <table width="100%" cellpadding="0" cellspacing="1" border="1" align="center">
                            <tr>
                            <td class="text10"><font color="red">日</font></td>
                            <td class="text101">月</td>
                            <td class="text101">火</td>
                            <td class="text101">水</td>
                            <td class="text101">木</td>
                            <td class="text101">金</td>
                            <td class="text102"><font color="#0000FF">土</font></td>
                            </tr>
                            <?
                            for($row = 0; $row < $maxRow; $row ++) { 
                                echo "<tr>"; 
                                for($col = 0; $col < 7; $col ++) { 
                                    $day = $arrSCH[$col][$row]; 
                                    echo "<td>";
                                    if($day)
                                    {
                                        $dd = substr(($day+100),1,2);
                                        echo $day."<br />";
                                        echo "<input type='text' size='10' name='s_$yyyy$mm$dd' value='".$stock[$yyyy.$mm][$dd]."' ";
                                        echo "><br />";
                                        if($reserve[$yyyy.$mm][$dd]>0)
                                        {
                                            echo "<font color=blue><b>予：".$reserve[$yyyy.$mm][$dd]."</b></font><br />";
                                        }
                                        if($cancel[$yyyy.$mm][$dd]>0)
                                        {
                                            echo "キ：".$cancel[$yyyy.$mm][$dd]."<br />";
                                        }
                                        $reserve_count = $stock[$yyyy.$mm][$dd]+$cancel[$yyyy.$mm][$dd]-$reserve[$yyyy.$mm][$dd];
                                        if($reserve_count<3)
                                        {
                                            echo "<font color=red><b>残：".$reserve_count."</b></font>";
                                        }
                                        else
                                        {
                                            echo "残：".$reserve_count;
                                        }
                                    }
                                    echo "</td>"; 
                                } 
                                echo "</tr>"; 
                            } 
                            ?>
                            </table>
                            <BR><input type="submit" value="登録・修正"><BR>
                            <?
                        }
                        ?>
                
                
                      </td>
                  </tr>
                </table>
            </div>

<!--ボタン-->
            <div class="form-group">
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-2">
                <? $var = "form_confirm";?>
                    <button class="btn btn-danger" type="submit" id="<? echo $var;?>" style="width: 100%; height: 50px; font-size: 200%;"><? echo $page_pre_title;?></button>
                </div>
            </div>
<!--ボタン-->


            
        </div>
        <input type="hidden" name="company_id" value="<? echo $_SESSION["company_id"]?>">
        </form>


    </div>
    <!-- END Example Block -->



    <!-- 商品リスト Content -->
    <div class="block full">
        <div class="block-title">
            <h2><strong>テンプレートリスト</strong></h2>
        </div>
        <p>登録したテンプレートのリストから編集、削除が可能です。<br />表示順位昇順、ID降順で表示されます。</p>
<?php
    


    $arr_db_field = array( "goods_stock_id", "goods_stock" );
    $arr_db_field = array_merge( $arr_db_field, array("view_level") );

    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM goods_stock where 1 ".$where;
    $sql .= " order by view_level ";
?>
        <div class="table-responsive">
            <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th>タイトル</th>
                        <th class="text-center">在庫設定</th>
                        <th class="text-center">表示順位</th>
                        <th class="text-center">編集</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 

                    $db_result = $common_dao->db_query_bind($sql);
                    if($db_result)
                    {
                        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
                        {
                            foreach($arr_db_field as $val)
                            {
                                $$val = $db_result[$db_loop][$val];
                            }
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $goods_stock_id; ?></td>
                        <td><?php echo $goods_stock; ?></td>
                        <td class="text-center"><a href="/app_company/goods_stock/goods_stock_list.php?goods_stock_id=<? echo $goods_stock_id;?>">在庫設定</a></td>
                        <td class="text-center"><?php echo $view_level; ?></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="./goods_stock_list.php?goods_stock_id=<? echo $goods_stock_id;?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                <a href="#" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger" onClick='fnChangeSel("<?php echo $goods_stock_id;?>");'><i class="fa fa-times"></i></a>

                            </div>
                        </td>
                    </tr>
                    <?php

                        }
                    }
                     ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- END 商品リスト Content -->

<!-- //_/_/_/_/_/_/_ END _/_/_/_/_/_/_// -->

</div>
<!-- END Page Content -->

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/page_footer.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_scripts.php'; ?>



<script src="/app_company/js/plugins/count/bootstrap-maxlength.js"></script>


<!-- Load and execute javascript code used only in this page -->
<script>
$(function(){ TablesDatatables.init(); });

var TablesDatatables = function() {
    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#example-datatable').dataTable({
            	order: [[2, "asc"], [ 0, "desc"]],
                columnDefs: [ { orderable: false, targets: [ 4 ] } ],
                pageLength: <? echo $view_count;?>,
                lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'All']]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();
</script>

<script>
$(document).ready(function () {
  $('.count-control').maxlength({
    alwaysShow: true,
    warningClass: "label label-success",
    limitReachedClass: "label label-danger"
  });
});
</script>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_end.php'; ?>