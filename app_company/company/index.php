<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_company_check();
    $company_id = $_SESSION["company_id"];
    
    if($company_id!="")
    {
        $arr_data = array("company_id", "company_pw", "company_name", "company_name_jp", "company_kana", "company_tel", "company_img");
        $arr_data = array_merge($arr_data, array("company_email", "company_comment", "last_login", "user_agent", "login_cookie", "flag_open"));
        $arr_data = array_merge($arr_data, array("post_num", "address", "commission", "regi_date", "up_date"));

        $sql = "SELECT ";
        foreach($arr_data as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " 1 FROM company where company_id='".$company_id."' ";

        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
            {
                foreach($arr_data as $val)
                {
                    $$val = $db_result[$db_loop][$val];
                }
            }
        }
    }
    
    $arr_company_flag_open = array();
    $arr_company_flag_open[1] = "公開";
    $arr_company_flag_open[99] = "非公開";
?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/config.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_start.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/page_head.php'; ?>


<link href="/app_master/css/plugins/cropper/cropper.min.css" rel="stylesheet">
<link href="/app_company/css/company.css" rel="stylesheet">

<script src="/app_company/js/jquery-2.1.1.js"></script>
<script type="text/javascript">
    $(function() {
            
        $('#form_confirm').click(function() {
            err_default = "";
            err_check_count = 0;
            bgcolor_default = "#FFFFFF";
            bgcolor_err = "#FFCCCC";
            background = "background-color";
            
            err_check_count += check_input("company_name");
            err_check_count += check_input_pw("company_pw");
            
            if(err_check_count!=0)
            {
                alert("入力に不備があります");
                return false;
            }
            else
            {
                $('.cropped').val($image.cropper("getCroppedCanvas").toDataURL('image/png'));
                $image.cropper("setDragMode", "crop");

                //$('#form_confirm').submit();
                $('#form_confirm', "body").submit();
                return true;
            }
            
            
        });
                
        function check_input($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);

            if($('#'+$str).val()=="")
            {
                err ="<span class='errorForm'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }

        function check_input_id($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);
            
            if($('#'+$str).val()=="")
            {
                err ="<span class='errorForm'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            else if(checkID($('#'+$str).val())==false)
            {
                err ="<span class='errorForm'>英数半角で入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            else if($('#'+$str).val().length<3)
            {
                err ="<span class='errorForm'>3文字以上で入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }
        
        //英数半角
        function checkID(value){
            if(value.match(/[^0-9a-z]+/) == null){
                return true;
            }else{
                return false;
            }
        } 

        //メールチェック
        function check_input_email($str_1) 
        {
            $("#err_"+$str_1).html(err_default);
            $("#"+$str_1).css(background,bgcolor_default);
            if($('#'+$str_1).val()=="")
            {
                err ="<span class='errorForm'>正しく入力してください。</span>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                
                return 1;
            }
            else if(checkIsEmail($('#'+$str_1).val()) == false)
            {
                err ="<span class='errorForm'>メールアドレスは半角英数字でご入力ください。</span>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                
                return 1;
            }
            
            return 0;
        }

        //メールチェック
        
        function checkIsEmail(value) {
            if (value.match(/.+@.+\..+/) == null) {
                return false;
            }
            return true;
        }
        
        function check_input_pw($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);
            
            if($('#'+$str).val()=="")
            {
                err ="<span class='errorForm'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            else if($('#'+$str).val().length<6)
            {
                err ="<span class='errorForm'>６文字以上入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }
                        

    });
    
//-->
</script>





<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-brush"></i>会社情報変更<br><small>会社の基本情報を変更することができます。</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/app_company/dashboard/">Dashboard</a></li>
        <li>会社情報変更</li>
    </ul>
    <!-- END Blank Header -->

    <!-- Example Block -->
    <div class="block">
        <!-- Example Title -->
        <div class="block-title">
            <h2>会社情報変更</h2>
        </div>
        <!-- END Example Title -->

<!-- //_/_/_/_/_/_/_ START _/_/_/_/_/_/_// -->
        <form action="./save.php" method="POST" name="form_write" id="form_regist" enctype="multipart/form-data" class="form-horizontal form-bordered">
        <div class="ibox-content">

            <div class="form-group">
                <label class="col-sm-2 control-label">加盟店のID</label>
                <div class="col-sm-6">
                    <p class="form-control-static">
                        <? $var = "company_id";?>
                        <?php
                        if($$var!="")
                        {
                        echo $$var;
                        }
                        ?>
                        <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" type="<? if($$var!=""){ echo "hidden";} else { echo " text ";}?>" class="form-control maxcount" placeholder="4文字以上の英語小文字、数字のみ入力可能" maxlength="50"> 
                        <input type="hidden" name="check_up" value="<? echo $company_id;?>">
                        <label id="err_<?php echo $var;?>"><? if($$var!=""){ echo "（変更出来ません）";} ?></label>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <? $var = "company_name";?>
                <label class="col-sm-2 control-label" for="<?php echo $var;?>">加盟店の韓国語</label>
                <div class="col-sm-6">
                    <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control" maxlength="50" placeholder="加盟店の名前を最大50文字以内で入力して下さい。"> 
                    <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">加盟店の日本語</label>
                <div class="col-sm-6">
                  <? $var = "company_name_jp";?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control maxcount" maxlength="50" placeholder="加盟店の日本語名を50文字以内で入力して下さい。"> 
                  <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">加盟店のカナ名</label>
                <div class="col-sm-6">
                  <? $var = "company_kana";?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control maxcount" maxlength="50" placeholder="最大50文字以内で入力して下さい。"> 
                  <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">郵便番号</label>
                <div class="col-sm-6">
                  <? $var = "post_num";?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control maxcount" maxlength="8" placeholder="例）900-0000"> 
                  <span id="err_<?php echo $var;?>"></span>
                  <span class="help-block m-b-none">請求書発行の時に使用されます。</span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">住所</label>
                <div class="col-sm-6">
                  <? $var = "address";?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control maxcount" maxlength="50" placeholder="例）沖縄県那覇市○○○"> 
                  <span id="err_<?php echo $var;?>"></span>
                  <span class="help-block m-b-none">請求書発行の時に使用されます。</span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">TEL</label>
                <div class="col-sm-6">
                  <? $var = "company_tel";?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $common_connect->h($$var);?>" type="text" class="form-control maxcount" maxlength="20" placeholder="例）090-0000-0000"> 
                  <span id="err_<?php echo $var;?>"></span>
                  <span class="help-block m-b-none">請求書発行の時に使用されます。</span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">加盟店のパスワード</label>
                <div class="col-sm-6">
                    <? $var = "company_pw";?>
                  <? if($$var=="") { $$var=date("Ymd").$common_connect->Fn_random_password(10);}?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" type="text" class="form-control maxcount" maxlength="20" placeholder="加盟店のパスワードを決めて入力して下さい。"> 
                  <span id="err_<?php echo $var;?>"></span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">加盟店のメールアドレス</label>
                <div class="col-sm-6">
                  <? $var = "company_email";?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" type="text" class="form-control maxcount" maxlength="50"> 
                  <span id="err_<?php echo $var;?>"></span>
                  <span class="help-block m-b-none">メールアドレスを間違えると加盟店にユーザーからの問い合わせが届かなくなりますので注意してください。
                </span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">コメント</label>
                <div class="col-sm-6">
                  <? $var = "company_comment";?>
                  <textarea name="<?php echo $var;?>" id="<?php echo $var;?>" class="form-control maxcount" maxlength="50"><?php echo $$var;?></textarea>
                  <span id="err_<?php echo $var;?>"></span>
                  <span class="help-block m-b-none">
                </span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">手数料</label>
                <div class="col-sm-6">
                  <? $var = "commission";?>
                  <input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>" type="number" class="form-control maxcount" maxlength="2"> 
                  <span id="err_<?php echo $var;?>"></span>
                  <span class="help-block m-b-none">決済料金の基本手数料</span>
                </div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">公開・非公開</label>
              <div class="col-sm-6">
                <? $var = "flag_open";?>
                <select name="<? echo $var;?>" id="<? echo $var;?>">
                <?
                foreach($arr_company_flag_open as $key=>$value)
                {
                ?>
                  <option value="<? echo $key;?>" <? if($key == $$var) { echo " selected ";}?>><? echo $value;?></option>
                <?
                }
                ?>
                </select>
                <span id="err_<?php echo $var;?>"></span>
              </div>
            </div>



            <div class="ibox-title  back-change">
                <h5>お店のサムネイル画像を登録 <small>※無登録でも可能。また店舗管理で登録や編集も出来ます。</small></h5>
            </div>
            <div class="ibox-content">
                <p>
                    お店のサムネイル画像をトリミングで編集登録が出来ます。120px×120pxで表示されます
                </p>
                <div class="row">
                    <div class="col-md-6">
                       <div class="wrap-crop">
                            <div class="image-crop">
                                <img src="/app_company/img/goods_sample.jpg">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h4>Preview image</h4>
                        <div class="img-preview img-preview-sm"></div>
                        <p>
                            画像の好きな位置に枠をあわせて決定を押してください。
                        </p>
                        <div class="btn-group">
                            <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                <input type="file" accept="image/*" name="file" id="inputImage" class="hide">
                                カットしたい画像をアップ
                            </label>
                            
                        </div>
        <br>
                        <div class="btn-group">
                          <button class="btn btn-primary" id="zoomIn" type="button"><i class="fa fa-search-plus"></i></button>
                          <button class="btn btn-primary" id="zoomOut" type="button"><i class="fa fa-search-minus"></i></button>
                          <button class="btn btn-primary" id="rotateRight" type="button"><i class="fa fa-rotate-left"></i></button>
                          <button class="btn btn-primary" id="rotateLeft" type="button"><i class="fa fa-rotate-right"></i></button>
                        </div>
        <br>
                        <div class="btn-group">
                            <div class="onoffswitch">
                                <? $var = "check_img";?>
                            <label class="switch switch-primary">
                                <input type="checkbox" name="<? echo $var;?>" value="1" class="onoffswitch-checkbox" id="<? echo $var;?>">
                                <span class="onoffswitch-inner"></span>
                            </label>
                            サムネイルを反映する
                            </div>
                          <?
                            if($company_img!="")
                            {
                                echo "<img src='/".global_company_dir.$company_id."/".$company_img."?".date("his")."'>";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>


        <!--イメージクリッパー-->

<input type="hidden" name="cropped" class="cropped">

<!--イメージクリッパーサムネイル画像表示-->



<!--イメージクリッパーサムネイル画像表示-->

<!--登録ボタン-->
            <div class="form-group">
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-2">
                <? $var = "form_confirm";?>
                    <button class="btn btn-danger" type="submit" id="<? echo $var;?>" style="width: 100%; height: 50px; font-size: 200%;">登録</button>
                </div>
            </div>
<!--登録ボタン-->

            
        </div>
        <input type="hidden" name="company_id" value="<? echo $_SESSION["company_id"]?>">
        </form>
<!-- //_/_/_/_/_/_/_ END _/_/_/_/_/_/_// -->
    </div>
    <!-- END Example Block -->
</div>
<!-- END Page Content -->



<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/page_footer.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_scripts.php'; ?>

<!-- Image cropper -->
<script src="/app_company/js/plugins/cropper/cropper.min.js"></script>

<script>//トリミング
    var $image = $(".image-crop > img");
    $(document).ready(function(){
        var URL = window.URL || window.webkitURL;
        var uploadedImageURL;
        var options = {
            aspectRatio:  1,//1.7777777777777777
            preview: '.img-preview'
        };

        $($image).cropper(options);

        var $inputImage = $("#inputImage");
        if (URL) {
            $inputImage.change(function() {
                var files = this.files;
                var file;

                if (!files.length) {
                    return;
                }

                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    if (uploadedImageURL) {
                        URL.revokeObjectURL(uploadedImageURL);
                    }

                    uploadedImageURL = URL.createObjectURL(file);
                        $('#check_img').prop("checked",true); 

                    $image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
                    $inputImage.val('');
                } else {
                    showMessage("Please choose an image file.");
                }
            });
        } else {
            $inputImage.addClass("hide");
        }

        $("#zoomIn").click(function() {
            $image.cropper("zoom", 0.1);
        });

        $("#zoomOut").click(function() {
            $image.cropper("zoom", -0.1);
        });

        $("#rotateLeft").click(function() {
            $image.cropper('clear');
            $image.cropper("rotate", 45);
            $image.cropper('crop');
        });

        $("#rotateRight").click(function() {
            $image.cropper('clear');
            $image.cropper("rotate", -45);
            $image.cropper('crop');
        });

        $("#setDrag").click(function() {
            $('.cropped').val($image.cropper("getCroppedCanvas").toDataURL('image/png'));
            $image.cropper("setDragMode", "crop");
        });


    });

</script>

<script src="/app_company/js/plugins/count/bootstrap-maxlength.js"></script>

<script>
$(document).ready(function () {
  $('.form-control').maxlength({
    alwaysShow: true,
    warningClass: "label label-success",
    limitReachedClass: "label label-danger"
  });
});
</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_end.php'; ?>