<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    $common_image = new CommonImage(); //画像
    
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>店舗登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php   
    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    $common_connect -> Fn_company_check();
    $session_company_id = $_SESSION["company_id"];
    

    
    if($session_company_id!=$company_id)
    {
        $common_connect-> Fn_javascript_back("変更するログイン情報が違います。再度ログインしてください。");
    }

    if($company_name == "" || $company_pw == "")
    {
        $common_connect -> Fn_javascript_back("正しく入力してください。");
    }
    
    $datetime = date("Y/m/d H:i:s");
    $user_agent = substr(getenv("HTTP_USER_AGENT"), 0, 250);
    
    //array
    $arr_db_field = array("company_pw", "company_name", "company_name_jp", "company_kana", "company_img", "company_tel");
    $arr_db_field = array_merge($arr_db_field, array("company_email", "company_comment", "user_agent", "flag_open"));
    $arr_db_field = array_merge($arr_db_field, array("post_num", "address", "commission"));
    

    $db_insert = "update company set ";
    foreach($arr_db_field as $key=>$value)
    {
        $db_insert .= $value."= '".$$value."', ";
    }
    $db_insert .= " up_date= '".$datetime."' ";
    $db_insert .= " where company_id='".$company_id."' ";
    $common_dao -> db_update($db_insert);

    if($check_img=="1")
    {
        //Folder生成
        $up_file_name = "company_img.png";
        $save_dir = $global_path.global_company_dir.$company_id."/";
        $fname_new_name[1] = $up_file_name;
        
        //Folder生成
        $common_image -> create_folder ($save_dir);
        //ヘッダに「data:image/png;base64,」が付いているので、それは外す
        $cropped = preg_replace("/data:[^,]+,/i","",$cropped);
        
        //残りのデータはbase64エンコードされているので、デコードする
        $cropped = base64_decode($cropped);
        
        //まだ文字列の状態なので、画像リソース化
        $image = imagecreatefromstring($cropped);
     
        imagesavealpha($image, TRUE); // 透明色の有効
        imagepng($image ,$save_dir.$fname_new_name[1]);
        
        $common_image -> create_thumbnail($save_dir.$up_file_name, "", $save_dir, 300);
        /*
        $new_end_name="_1";
        $fname_new_name[1] = $common_image -> img_save("img_1", $common_connect, $save_dir, $new_end_name, $company_id, $text_img_1, "");
        */
        
        //画像
        $arr_where = array();
        $arr_where["company_id"] = $company_id;
        
        $company_img = $fname_new_name[1];
        $up_date = $datetime;
        
        //array
        $arr_db_field = array("company_img", "up_date");
        
        $db_insert = "update company set ";
        foreach($arr_db_field as $key=>$value)
        {
            $db_insert .= $value."= '".$$value."', ";
        }
        $db_insert .= " up_date= '".$datetime."' ";
        $db_insert .= " where company_id='".$company_id."' ";
        $common_dao -> db_update($db_insert);

    }
    
    $common_connect-> Fn_javascript_move("登録・修正しました", "./");
?>
</body>
</html>