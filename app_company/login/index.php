<?
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCompany.php";
    $common_company = new CommonCompany();
    
    if($_COOKIE['company'] != '')
    {
        $arr_data = array();
        $arr_data[] = "company_name";
        $arr_data[] = "company_id";
        $arr_data[] = "flag_open";
        
        $arr_where = array();
        $arr_where["flag_open"] = "all";
        $login_cookie = $_COOKIE['company'];

        $db_result = $common_company->Fn_db_company_login_cookie ($login_cookie, $arr_data, $arr_where=null);
        
        if($db_result)
        {
            $db_company_id = $db_result[0]["company_id"];
            $db_company_name = $db_result[0]["company_name"];
            $db_flag_open = $db_result[0]["flag_open"];
        }
        
        if($db_company_id!="" && $db_flag_open=="1")
        {
            $_SESSION['company_id']=$db_company_id;
            $_SESSION['company_name']=$db_company_name;

            //last_login
            $datetime = date("Y/m/d H:i:s");
            $db_up = "update company set last_login='".$datetime."' where company_id='".$db_company_id."' ";
            $common_dao->db_update($db_up);

            
            $common_connect->Fn_redirect(global_ssl."/app_company/dashboard/");
        }
    }
?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/config.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_start.php';?>



<!-- Login Alternative Row -->
<div class="container">
    <div class="row">
        <div class="col-md-5 col-md-offset-1">
            <div id="login-alt-container">
                <!-- Title -->
                <h1 class="push-top-bottom">
                    <i class="gi gi-flash"></i> <strong><?php echo $template['name']; ?></strong><br>
                    <small>沖縄の旅行プラットフォーム <?php echo $template['name']; ?> </small>
                </h1>
                <!-- END Title -->

                <!-- Key Features -->
                <ul class="fa-ul text-muted">
                    <li><i class="fa fa-check fa-li text-success"></i> 商品作成に困った時は連絡してください。</li>
                    <li><i class="fa fa-check fa-li text-success"></i> イベント商品募集中</li>
                </ul>
                <!-- END Key Features -->

                <!-- Footer -->
                <footer class="text-muted push-top-bottom">
                    <small><span id="year-copy"></span> &copy; <a href="http://www.smartboxjp.com" target="_blank"><?php echo $template['name']; ?></a></small>
                </footer>
                <!-- END Footer -->
            </div>
        </div>
        <div class="col-md-5">
            <!-- Login Container -->
            <div id="login-container">
                <!-- Login Title -->
                <div class="login-title text-center">
                    <h1><strong>加盟店ログイン</strong></h1>
                </div>
                <!-- END Login Title -->

                <!-- Login Block -->
                <div class="block push-bit">
                    <!-- Login Form -->
                    <? $var = "form-login"; ?>
                    <form action="login_check.php" method="post" name="<? echo $var;?>" id="<? echo $var;?>" class="form-horizontal">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    <? $var = "company_id"; ?>
                                    <input type="text" name="<?=$var;?>" id="<?=$var;?>" class="form-control input-lg" placeholder="IDを入力してください。">
                                </div>
                                <label id="err_<?=$var;?>"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                                    <? $var = "company_pw"; ?>
                                    <input type="password" name="<?=$var;?>" id="<?=$var;?>" class="form-control input-lg" placeholder="パスワードを入力してください。">
                                </div>
                                <label id="err_<?=$var;?>"></label>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-xs-4">
                                <label class="switch switch-primary" data-toggle="tooltip" title="Remember Me?">
                                    <input type="checkbox" id="login-remember-me" name="company_remember" checked>
                                    <span></span>
                                </label>
                            </div>
                            <div class="col-xs-8 text-right">
                                <button type="submit" id="form_confirm" name="form_confirm" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> ログインする</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12 text-center">
                                <a href="javascript:void(0)" id="link-reminder-login"><small>パスワード忘れた場合?</small></a>
                            </div>
                        </div>
                        <input type="hidden" name="token" value="<? echo $common_connect->generate();?>">
                    </form>
                    <!-- END Login Form -->

                </div>
                <!-- END Login Block -->
            </div>
            <!-- END Login Container -->
        </div>
    </div>
</div>
<!-- END Login Alternative Row -->


<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_scripts.php';?>

<!-- Load and execute javascript code used only in this page -->
<script src="/app_company/js/pages/login.js"></script>

<?php require_once $_SERVER['DOCUMENT_ROOT'].'/app_company/inc/template_end.php'; ?>