<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    
    require_once $_SERVER['DOCUMENT_ROOT']."/app_model/CommonCompany.php";
    $common_company = new CommonCompany();
    
    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ログインチェック</title>
</head>

<body>

<?
    if ($company_id == "" or $company_pw == "") 
    {
        $common_connect->Fn_javascript_back("必須項目を正しく入力してください。");
    }
    else
    {
            //referチェック
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
        if (!preg_match("|^https?://[a-zA-Z0-9-]+\.".globla_domain_1."\.".globla_domain_2."|", $referer)) {
            $common_connect->Fn_javascript_back("正しく入力下さい。");
        }

        //tokenチェック
        if (!$common_connect->validate(filter_input(INPUT_POST, 'token'))) {
            $common_connect->Fn_javascript_back("正しく入力下さい。");
        }

        //加盟店の基本情報
        $where .= " and company_id='".$company_id."' ";
        $where .= " and company_pw='".$company_pw."' ";

        //リスト表示
        $sql = "SELECT company_id, company_name, flag_open FROM company where 1 ".$where ;
        $db_result = $common_dao->db_query_bind($sql);

        if($db_result)
        {
            $db_company_id = $db_result["0"]["company_id"];
            $db_company_name = $db_result["0"]["company_name"];
            $db_flag_open = $db_result["0"]["flag_open"];
        }
        else
        {
            $common_connect->Fn_javascript_back("IDとパスワードを確認してください。");
        }

        
        if($db_flag_open!="1")
        {
            $common_connect->Fn_javascript_back("ログイン権限がありません。");
        }
        
        if ($db_company_id == $company_id)
        {
            //管理者の場合
            session_start();
            $_SESSION['company_id']=$db_company_id;
            $_SESSION['company_name']=$db_company_name;

            if($db_company_id!="" and $company_remember=="on")
            {
                $login_cookie = sha1($db_company_id."_".$company_pw);

                $datetime = date("Y/m/d H:i:s");
                $db_up = "update company set login_cookie= '".$login_cookie."', last_login='".$datetime."' ";
                $db_up .= " where company_id ='".$db_company_id."' ";
                $common_dao->db_update($db_up);

        
                setcookie("company", $login_cookie, time()+3600*24*31);//暗号化してクッキーに保存, 31日間
            }
            
            $common_connect->Fn_redirect(global_ssl."/app_company/dashboard/");

        }
    }
?>
</body>
</html>