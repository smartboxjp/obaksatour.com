<?php
/**
 * template_scripts.php
 *
 * Author: 合同会社smartbox
 *
 * All vital JS scripts are included here
 *
 */
?>

<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
<script src="/app_company/js/vendor/jquery.min.js"></script>
<script src="/app_company/js/vendor/bootstrap.min.js"></script>
<script src="/app_company/js/plugins.js"></script>
<script src="/app_company/js/app.js"></script>
