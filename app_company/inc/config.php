<?php
/**
 * config.php
 *
 * Author: 合同会社smartbox
 *
 * Configuration file. It contains variables used in the template as well as the primary navigation array from which the navigation is created
 *
 */

/* Template variables */
$template = array(
    'name'              => 'OBAKSA TOUR',
    'version'           => '1',
    'author'            => '合同会社smartbox',
    'robots'            => 'noindex, nofollow',
    'title'             => '沖縄旅行プラットフォーム OBAKSA TOUR',
    'description'       => 'OBAKSA TOURは合同会社smartboxが提供する沖縄の旅行プラットフォームです。',
    // true                     enable page preloader
    // false                    disable page preloader
    'page_preloader'    => false,
    // true                     enable main menu auto scrolling when opening a submenu
    // false                    disable main menu auto scrolling when opening a submenu
    'menu_scroll'       => true,
    // 'navbar-default'         for a light header
    // 'navbar-inverse'         for a dark header
    'header_navbar'     => 'navbar-default',
    // ''                       empty for a static layout
    // 'navbar-fixed-top'       for a top fixed header / fixed sidebars
    // 'navbar-fixed-bottom'    for a bottom fixed header / fixed sidebars
    'header'            => '',
    // ''                                               for a full main and alternative sidebar hidden by default (> 991px)
    // 'sidebar-visible-lg'                             for a full main sidebar visible by default (> 991px)
    // 'sidebar-partial'                                for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
    // 'sidebar-partial sidebar-visible-lg'             for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
    // 'sidebar-mini sidebar-visible-lg-mini'           for a mini main sidebar with a flyout menu, enabled by default (> 991px + Best with static layout)
    // 'sidebar-mini sidebar-visible-lg'                for a mini main sidebar with a flyout menu, disabled by default (> 991px + Best with static layout)
    // 'sidebar-alt-visible-lg'                         for a full alternative sidebar visible by default (> 991px)
    // 'sidebar-alt-partial'                            for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
    // 'sidebar-alt-partial sidebar-alt-visible-lg'     for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)
    // 'sidebar-partial sidebar-alt-partial'            for both sidebars partial which open on mouse hover, hidden by default (> 991px)
    // 'sidebar-no-animations'                          add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!
    'sidebar'           => 'sidebar-partial sidebar-visible-lg sidebar-no-animations',
    // ''                       empty for a static footer
    // 'footer-fixed'           for a fixed footer
    'footer'            => '',
    // ''                       empty for default style
    // 'style-alt'              for an alternative main style (affects main page background as well as blocks style)
    'main_style'        => '',
    // ''                           Disable cookies (best for setting an active color theme from the next variable)
    // 'enable-cookies'             Enables cookies for remembering active color theme when changed from the sidebar links (the next color theme variable will be ignored)
    'cookies'           => '',
    // 'night', 'amethyst', 'modern', 'autumn', 'flatie', 'spring', 'fancy', 'fire', 'coral', 'lake',
    // 'forest', 'waterlily', 'emerald', 'blackberry' or '' leave empty for the Default Blue theme
    'theme'             => '',
    // ''                       for default content in header
    // 'horizontal-menu'        for a horizontal menu in header
    // This option is just used for feature demostration and you can remove it if you like. You can keep or alter header's content in page_head.php
    'active_page'       => basename($_SERVER['PHP_SELF'])
);

/* Primary navigation array (the primary navigation will be created automatically based on this array, up to 3 levels deep) */
$primary_nav = array(
    array(
        'name'  => 'Dashboard',
        'url'   => '/app_company/dashboard/',
        'icon'  => 'gi gi-stopwatch'
    ),
    array(
        'name'  => '会社情報変更',
        'url'   => '/app_company/company/',
        'icon'  => 'gi gi-leaf'
    ),
    array(
        'name'  => '注文情報',
        'icon'  => 'gi gi-shopping_cart',
        'sub'   => array(
            array(
                'name'  => '注文リスト',
                'url'   => '/app_company/goods/goods_list.php'
            ),
            array(
                'name'  => 'Q＆A',
                'url'   => 'page_ecom_orders.php'
            ),
            array(
                'name'  => 'Review',
                'url'   => 'page_ecom_order_view.php'
            )
        )
    ),
    array(
        'name'  => '商品管理',
        'url'   => 'header',
    ),
    array(
        'name'  => '商品登録',
        'url'   => '/app_company/goods/goods_regist.php',
        'icon'  => 'gi gi-plus'
    ),
    array(
        'name'  => '商品リスト',
        'url'   => '/app_company/goods/goods_list.php',
        'icon'  => 'fa fa-bars'
    ),
    array(
        'name'  => 'テンプレート',
        'url'   => 'header'
    ),
    array(
        'name'  => '商品詳細',
        'icon'  => 'gi gi-brush',
        'url'   => '/app_company/temp_goods_detail/temp_goods_detail_list.php'
    ),
    array(
        'name'  => '商品写真',
        'icon'  => 'fa fa-wrench',
        'url'   => '/app_company/temp_goods_detail/goods_detail.php'
    ),
    array(
        'name'  => '在庫',
        'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="日毎の在庫設定が可能です。"><i class="gi gi-lightbulb"></i></a>',
        'url'   => 'header',
    ),
    array(
        'name'  => '在庫管理',
        'icon'  => 'fa fa-wrench',
        'url'   => '/app_company/goods_stock_title/goods_stock_title_list.php'
    ),
    array(
        'name'  => '料金管理',
        'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="シーズンの料金設定後料金設定を行います。"><i class="gi gi-lightbulb"></i></a>',
        'url'   => 'header',
    ),
    array(
        'name'  => 'シーズン料金設定',
        'url'   => 'page_widgets_links.php',
        'icon'  => 'gi gi-share_alt'
    ),
    array(
        'name'  => '料金設定',
        'url'   => 'page_widgets_links.php',
        'icon'  => 'gi gi-table'
    )
);