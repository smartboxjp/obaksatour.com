$(document).ready(function(){

  var touch = false;
  var current_scrollY;

  $('.drawer-btn').on('click touchstart',function(e){
    switch (e.type) {
      case 'touchstart':
        drawerToggle();
        drawerOpen();
        touch = true;
        return false;
      break;
      case 'click':
        if(!touch)
          drawerToggle();
          drawerOpen();
        return false;
      break;
     }
    function drawerToggle(){
        $('body').toggleClass('drawer-opened');
        touch = false;
    };
  });

  $('.drawer-overlay,.drawer-close').on('click touchstart',function(event){
    $('body').removeClass('drawer-opened');
    event.preventDefault();
  })

  function drawerOpen() {
    if($('body').hasClass('drawer-opened')) {

      current_scrollY =$(window).scrollTop();
      /*スマホスクロールOFF*/



    } else {

      //スクロール活性化
      $('html, body').prop({scrollTop: current_scrollY});
    }
  }

  $('.drawer-overlay').on('click touchstart',function(){
      $('body').removeClass('drawer-opened');
      drawerOpen();
  });

  $('#drawernav a').on('click touchstart',function(event){
      event.stopPropagation();
  });

});

