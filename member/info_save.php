<?php   
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html prefix="og: http://ogp.me/ns#">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">

    <title>회원가입</title>
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->

    <!--[if lt IE 8]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="../scripts/html5shiv.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>
<?
    $common_connect -> Fn_member_check();
    $member_id = $_SESSION['member_id'];

    $sql = "SELECT member_id, ";
    $sql .= " regi_date FROM member where member_id='".$member_id."'";
    $sql .= " and flag_open='1'";

    $db_result = $common_dao->db_query_bind($sql);
    if(!$db_result)
    {
        $common_connect-> Fn_javascript_back("로그인 정보가 일치하지 않습니다.");
    }

    $datetime = date("Y-m-d H:i:s");
    
    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    $db_insert = "update member set member_name='".$member_name."', flag_email='".$flag_email."', ";
    if($login_pw!="")
    {
        $login_pw = $common_connect-> Fn_generate_pw($login_pw);

        $db_insert .= " login_pw='".$login_pw."', ";
    }
    $db_insert .= " up_date='".$datetime."' where login_email='".$login_email."' ";
    $db_result = $common_dao->db_update($db_insert);
    
    $common_connect-> Fn_javascript_back("회원정보가 수정되었습니다.", global_ssl."/member/info.php");
?>

</body>

</html>
