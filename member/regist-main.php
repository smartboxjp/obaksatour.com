<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連
  
  foreach($_GET as $key => $value)
  { 
    $$key = $common_connect->h($value);
  }
?>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?
  $meta_title = "이메일 인증";
  $meta_description = "";
  require_once $_SERVER['DOCUMENT_ROOT']."/include/meta.php";
?>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/mypage/css/mypage.css" rel="stylesheet">
<link href="/mypage/css/myinfo.css" rel="stylesheet">

  <script type="text/javascript">
  $(function() {
    $('#form_confirm').click(function() {
      err_default = "";
      err_check_count = 0;
      bgcolor_default = "#FFFFFF";
      bgcolor_err = "#FFCCCC";
      background = "background-color";

      err_check_count += check_input_email("login_email");

      if(err_check_count!=0)
      {
        alert("이메일을 정확히 입력해주세요.");
        return false;
      }
      else
      {
        $.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } }); 
 
        setTimeout($.unblockUI, 10000); 

      //$('#form_confirm').submit();
      $('#form_confirm', "body").submit();
      return true;
    }
      
      
    });

    function check_input($str) 
    {
      $("#err_"+$str).html(err_default);
      $("#"+$str).css(background,bgcolor_default);

      if($('#'+$str).val()=="")
      {
        err ="<div style='color:#F00'>정확히 입력해주세요.</div>";
        $("#err_"+$str).html(err);
        $("#"+$str).css(background,bgcolor_err);

        return 1;
      }
      return 0;
    }

    function check_input_email($str) 
    {
      $("#err_"+$str).html(err_default);
      $("#"+$str).css(background,bgcolor_default);

      if($('#'+$str).val()=="")
      {
        err ="<div style='color:#F00'>정확히 입력해주세요.</div>";
        $("#err_"+$str).html(err);
        $("#"+$str).css(background,bgcolor_err);

        return 1;
      }
      else if(checkIsEmail($('#'+$str).val()) == false)
      {
        err ="<div style='color:#F00'>정확한 이메일을 입력해주세요.</div>";
        $("#err_"+$str).html(err);
        $("#"+$str).css(background,bgcolor_err);

        return 1;
      }

      return 0;
    }


    //メールチェック
    function checkIsEmail(value) {
      if (value.match(/.+@.+\..+/) == null) {
        return false;
      }
      return true;
    }

  });

//-->
</script>
</head>
<body>

<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

  <div class="l-container">
    <div class="pankuzu-wapper">
      <ul class="pankuzu">
        <li><a href="/">오키나와오박사 홈</a></li>
        <li>회원가입</li>
      </ul>
    </div>
    <!-- /.pankuzu-wapper -->
  </div>
  <!-- /.l-container -->


  <div class="l-container clearfix">
    <h1 class="headline-glay fsize-lg left-bdr mb-30"><span class="color-blue">회원가입</span></h1>
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <div class="table-responsive">
      <form action="#">
        <table class="table table-bordered">
          <tbody>
            <tr>
              <td colspan="2" class="main-board">
                <h1 class="headline-glay fsize-lg mb-20">간단한 절차로 빠르게 쉽게 <span class="color-lightblue">회원가입</span>이 가능합니다.</h1>
                <p>이메일을 통하여<span class="color-blue">인증</span>한 후 진행 해 주시기 바람니다.<br>
                메일이 수분이내에 도착되지 않았을 경우는 <span class="color-blue">스팸메일을 확인</span>해 주시기 바랍니다.</p>
              </td>
            </tr>
            <tr>
              <td class="ta-left wid-a">이름</td>
              <td class="ta-left b">
                <? $var = "login_email";?>
                <input type="text" class="form-control input-sm form-text" id="<? echo $var;?>" name="<? echo $var;?>" placeholder="실명을 입력하세요">
                <label id="err_<?=$var;?>"></label>
              </td>
            </tr>
            <tr>
              <td class="ta-left wid-a">등록한 이메일</td>
              <td class="ta-left b">
                <? $var = "login_email";?>
                <input type="text" class="form-control input-sm form-text" id="<? echo $var;?>" name="<? echo $var;?>" placeholder="이메일을 입력하세요"><button type="button" class="btn btn-nomal sign-up-button">인증번호발송</button>
                <label id="err_<?=$var;?>"></label>
              </td>
            </tr>
            <tr>
              <td class="ta-left wid-a">인증번호</td>
              <td class="ta-left b">
                <? $var = "login_email";?>
                <input type="text" class="form-control input-sm form-text" id="<? echo $var;?>" name="<? echo $var;?>" placeholder="인증번호를 입력하세요"><button type="button" class="btn btn-nomal sign-up-button">인증확인</button>
                <label id="err_<?=$var;?>"></label>
              </td>
            </tr>
            <tr>
              <td class="ta-left wid-a">패스워드</td>
              <td class="ta-left b">
                <? $var = "login_email";?>
                <input type="password" class="form-control input-sm form-text" id="<? echo $var;?>" name="<? echo $var;?>" placeholder="패스워드를 입력하세요"></label>
              </td>
            </tr>
            <tr>
              <td class="ta-left wid-a">패스워드 확인</td>
              <td class="ta-left b">
                <? $var = "login_email";?>
                <input type="password_confirm" class="form-control input-sm form-text" id="<? echo $var;?>" name="<? echo $var;?>" placeholder="패스워드를 다시한번 입력하세요"></label>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

</div>
<!-- /.contents -->

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>