<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連
?>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?
  $meta_title = "로그인";
  $meta_description = "";
  require_once $_SERVER['DOCUMENT_ROOT']."/include/meta.php";
?>
</head>

<body>
<?
    $login_email = $common_connect->h($_POST['login_email']);
    $login_email = mb_strtolower($login_email);
    $login_pw = $common_connect->h($_POST['login_pw']);
    
    if ($login_email == "" or $login_pw == "") 
    {
        $common_connect->Fn_javascript_back("이메일과 패스워드를 정확히 입력해주세요.");
    }
    else
    {
        $db_check_pw = $common_connect-> Fn_generate_pw($login_pw);
        $sql = "select member_id, member_name, login_email from member where login_email ='".$login_email."' and login_pw='".$db_check_pw."' ";

        $db_result = $common_dao->db_query_bind($sql);
        if($db_result){
            $db_member_id = $db_result[0]["member_id"];
            $db_login_email = mb_strtolower($db_result[0]["login_email"]);
            $db_member_name = $db_result[0]["member_name"];
        }
        else
        {
            $common_connect->Fn_javascript_back("이메일과 패스워드를 정확히 입력해주세요.");
        }
        
        if ($db_login_email == $login_email)
        {
            $datetime = date("Y-m-d H:i:s");
            $db_up = "update member set lastlogin_date='".$datetime."' ";
            $db_up .= " where member_id='".$db_member_id."' ";
            $common_dao->db_update($db_up);

            //管理者の場合
            session_start();
            $_SESSION['member_id']=$db_member_id;
            $_SESSION['member_name']=$db_member_name;
            $common_connect->Fn_redirect(global_ssl."/");

        }
    }
?>
</body>
</html>