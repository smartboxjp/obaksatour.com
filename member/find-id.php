<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title>find-id</title>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/member/css/member.css" rel="stylesheet">
<link href="/member/css/find.css" rel="stylesheet">

</head>
<body>

<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

  <div class="signup">
    <form class="signup-main" data-mh="match-height">
      <img src="/common/images/member-logo.png" alt="아직 오키나와 오박사 회원이 아니신가요?" class="signup-main-img">
      <div class="signup-main-body">
        <h1 class="headline-blue signup-main-body-title"><span class="color-blue">오키나와 오박사</span> 아이디 찾기</h1>
        <div class="signup-main-body-input"><input type="text" class="form-control input-sm form-text" id="name" placeholder="이름"></div>
        <div class="signup-main-body-input"><input type="text" class="form-control input-sm form-text" id="name" placeholder="이메일"></div>
        <p class="fsize-sm signup-main-body-cap">이름과 이메일을 입력해 주세요. </p>
      </div>
      <!-- /.signup-main-body -->
      <div class="signup-main-btm">
        <p class="signup-main-btm-text fsize-sm">오키나와 오박사 비밀번호 찾기<span class="line">ㅣ</span>회원가입</p>
        <button type="button" class="btn btn-nomal sign-up-button">아이디 확인</button>
      </div>
      <!-- /.signup-main-btm -->
    </form>
    <!-- /.signup-main -->
    <div class="signup-side" data-mh="match-height">
      <p class="signup-side-link"><a href="#" class="color-green">네이버 계정으로 가입</a></p>
      <p class="signup-side-link"><a href="#" class="color-blue">facebook 계정으로 가입</a></p>
      <img src="images/sign-side-logo.png" alt="오박사와 함께 만드는 오키나와 여행의 모든 것">
      <p class="signup-side-btmtext">오박사와 함께 만드는<br><span>오키나와 여행</span>의 모든 것</p>
    </div>
    <!-- /.signup-side -->
  </div>
  <!-- /.signup -->

</div>
<!-- /.contents -->

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>