<?php   
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連

    date_default_timezone_set('Asia/Seoul');
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/mailer/PHPMailerAutoload.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonMailer.php";
    $common_mailer = new CommonMailer(); //メール関連
?>
<!doctype html>
<html prefix="og: http://ogp.me/ns#">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">

    <title>비밀번호 재발행</title>
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->

    <!--[if lt IE 8]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="../scripts/html5shiv.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>
<?
    $datetime = date("Y-m-d H:i:s");
    
    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
    
    if($login_email=="")
    {
        $common_connect-> Fn_javascript_back("필수항목을 입력해주세요.");
    }
    
    $sql = "SELECT login_email, member_name FROM member where login_email = '".$login_email."' and flag_open=1";
    $db_result = $common_dao->db_query_bind($sql);
    if(!$db_result)
    {
        $common_connect-> Fn_javascript_back("유효한 메일이 아닙니다.");
    }
    else
    {
        $member_name = $db_result[0]["member_name"];
    }


    $login_pw_view = $common_connect-> Fn_random_password(8);
    $login_pw = $common_connect-> Fn_generate_pw($login_pw_view);
    
    $flag_open=1;
    $arr_db_field = array("login_email", "login_pw", "member_login_cookie", "member_name", "flag_email", "flag_open");
    
    $db_insert = "update member set login_pw='".$login_pw."', up_date='".$datetime."' where login_email='".$login_email."' ";
    $db_result = $common_dao->db_update($db_insert);
    
    //인증정보 삭제
    $db_insert = "Delete from member_auth ";
    $db_insert .= " where login_email = '".$login_email."' ";
    $db_result = $common_dao->db_update($db_insert);


    //Thank youメール
    if ($login_email != "")
    {
        $subject = " [오키나와 오박사]패스워드 재발행. ";


        $body = "";
        $body = file_get_contents($_SERVER['DOCUMENT_ROOT']."/member/mail/find-password_save.user.php");
        $body = str_replace("[login_pw]", $login_pw_view, $body);
        $body = str_replace("[member_name]", $member_name, $body);
        $body = str_replace("[datetime]", $datetime, $body);

    }
    $common_mailer->Fn_send_utf ($login_email,global_test_mail.$subject,$body,$global_mail_from,$global_send_mail);

    $common_connect -> Fn_email_log($login_email, global_test_mail.$common_connect->h($subject), $common_connect->h($body)); //メールログ

    //$common_mailer->Fn_send_utf ($global_send_mail,global_test_mail.$subject,$body,$global_mail_from,$global_send_mail);

    $common_connect-> Fn_redirect(global_no_ssl."/member/find-password_thankyou.php");
?>

</body>

</html>
