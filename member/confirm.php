<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title>confirm</title>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/member/css/member.css" rel="stylesheet">
<link href="/member/css/confirm.css" rel="stylesheet">

</head>
<body>

<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

  <div class="signup">
    <form class="signup-main">
      <img src="/common/images/member-logo.png" alt="아직 오키나와 오박사 회원이 아니신가요?" class="signup-main-img">
      <div class="signup-main-body">
        <h1 class="headline-blue signup-main-body-title"><span class="color-blue">오키나와 오박사</span> 비밀번호 찾기</h1>
        <p class="fsize-sm signup-main-body-cap">등록하신 이메일로 임시비밀번호를 발송합니다. </p>
        <div class="signup-main-body-input"><input type="text" class="form-control input-sm form-text" id="name" placeholder="이메일"></div>
      </div>
      <!-- /.signup-main-body -->
      <div class="signup-main-btm">
        <p class="signup-main-btm-text fsize-sm">오키나와 오박사 아이디 찾기<span class="line">ㅣ</span>회원가입</p>
        <button type="button" class="btn btn-nomal sign-up-button">임시 비밀번호 요청</button>
      </div>
      <!-- /.signup-main-btm -->
    </form>
    <!-- /.signup-main -->
  </div>
  <!-- /.signup -->

</div>
<!-- /.contents -->

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>