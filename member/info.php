<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連
  
  foreach($_GET as $key => $value)
  { 
    $$key = $common_connect->h($value);
  }
?>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?
  $meta_title = "회원정보수정";
  $meta_description = "";
  require_once $_SERVER['DOCUMENT_ROOT']."/include/meta.php";
?>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/common/css/form.css" rel="stylesheet">

  <script type="text/javascript">

  $(function() {

    
    $('#form_confirm').click(function() {
      err_default = "";
      err_check_count = 0;
      bgcolor_default = "#FFFFFF";
      bgcolor_err = "#FFCCCC";
      background = "background-color";

      err_check_count += check_input("member_name");
      err_check_count += check_input_pw_confirm("login_pw", "login_pw_confirm");
      
      if(err_check_count!=0)
      {
        alert("필수 항목을 정확히 입력해주세요.");
        return false;
      }
      else
      {
        //$('#form_confirm').submit();
        $('#form_confirm', "body").submit();
        return true;
      }
      
      
    });

    function check_input($str) 
    {
      $("#err_"+$str).html(err_default);
      $("#"+$str).css(background,bgcolor_default);

      if($('#'+$str).val()=="")
      {
        err ="정확히 입력해주세요.";
        $("#err_"+$str).html(err);
        $("#"+$str).css(background,bgcolor_err);

        return 1;
      }
      return 0;
    }


    function check_input_pw_confirm($str_1, $str_2) 
    {
      $("#err_"+$str_1).html(err_default);
      $("#"+$str_1).css(background,bgcolor_default);
      $("#err_"+$str_2).html(err_default);
      $("#"+$str_2).css(background,bgcolor_default);

      if($('#'+$str_1).val()!="")
      {
        if($('#'+$str_1).val().length<6)
        {
          err ="패스워드를 6~20자로 정확히 입력해주세요";
          $("#err_"+$str_1).html(err);
          $("#"+$str_1).css(background,bgcolor_err);

          return 1;
        }
        else if($('#'+$str_1).val()!=$('#'+$str_2).val())
        {
          err ="패스워드를 정확히 입력해주세요";
          $("#err_"+$str_2).html(err);
          $("#"+$str_2).css(background,bgcolor_err);

          return 1;
        }

      }

      return 0;
    }

  });

//-->
</script>
</head>
<body>
<?
  $common_connect -> Fn_member_check();
  $member_id = $_SESSION['member_id'];

  $arr_db_field = array("member_id", "login_email", "login_pw", "member_name", "member_img", "flag_email");

  $sql = "SELECT member_id, ";
  foreach($arr_db_field as $val)
  {
    $sql .= $val.", ";
  }
  $sql .= " regi_date FROM member where member_id='".$member_id."'";
  $sql .= " and flag_open='1'";
  
  $db_result = $common_dao->db_query_bind($sql);
  if($db_result)
  {
    foreach($arr_db_field as $val)
    {
      $$val = $db_result[0][$val];
    }
  }
  else
  {
    $common_connect-> Fn_javascript_back("로그인 정보가 일치하지 않습니다.");
  }
?>

<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

  <div class="l-container">
    <div class="pankuzu-wapper">
      <ul class="pankuzu">
        <li><a href="/">오키나와오박사 홈</a></li>
        <li>회원정보수정</li>
      </ul>
    </div>
    <!-- /.pankuzu-wapper -->
  </div>
  <!-- /.l-container -->


  <div class="l-container clearfix">
    <h1 class="headline-glay fsize-lg left-bdr mb-30"><span class="color-blue">회원정보수정</span></h1>
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <div class="table-responsive">
      <form class="signup-main" action="info_save.php" name="form_regist" id="form_regist" method="post">
        <table class="table table-bordered">
          <tbody>
            <tr>
              <td colspan="2" class="main-board">
                <h1 class="headline-glay fsize-lg mb-20">등록된 정보를 확인해주세요.<span class="color-lightblue">회원정보의 수정이</span> 가능합니다.</h1>
              </td>
            </tr>
            <tr>
              <td class="ta-left wid-a hidden-xs">등록한 이메일</td>
              <td class="ta-left b">
                <? $var = "login_email";?>
                <span class="input-double"><input type="hidden" id="<? echo $var;?>" name="<? echo $var;?>" value="<? echo $$var;?>"><? echo $$var;?></span>
                <span id="err_<?=$var;?>" class="alert-red"></span>
              </td>
            </tr>
            <tr>
              <td class="ta-left wid-a hidden-xs">한글이름</td>
              <td class="ta-left b">
                <? $var = "member_name";?>
                <input type="text" class="form-control input-textbox" id="<? echo $var;?>" name="<? echo $var;?>" placeholder="한글실명을 입력하세요" maxlength="30" value="<? echo $$var;?>">
                <span id="err_<?=$var;?>" class="alert-red"></span>
              </td>
            </tr>
            <tr>
              <td class="ta-left wid-a hidden-xs">패스워드</td>
              <td class="ta-left b">
                <? $var = "login_pw";?>
                <input type="password" class="form-control input-textbox" id="<? echo $var;?>" name="<? echo $var;?>" placeholder="수정할 경우 패스워드를 입력하세요" maxlength="20">
                <span id="err_<?=$var;?>" class="alert-red">수정할 경우만 입력하세요.<br />6자이상 20자이내로 특수키포함</span>
              </td>
            </tr>
            <tr>
              <td class="ta-left wid-a hidden-xs">패스워드 확인</td>
              <td class="ta-left b">
                <? $var = "login_pw_confirm";?>
                <input type="password" class="form-control input-textbox" id="<? echo $var;?>" name="<? echo $var;?>" placeholder="패스워드를 다시한번 입력하세요" maxlength="20">
                <span id="err_<?=$var;?>" class="alert-red"></span>
              </td>
            </tr>
            <tr>
              <td class="ta-left" colspan="2">
                <label class="checkbox-agree">
                  <? $var = "flag_email";?>
                  <input type="checkbox" id="<? echo $var;?>" name="<? echo $var;?>" value="1" <? if($$var==1){ echo " checked ";}?>>
                  정보 메일 수신 동의 (<span>*선택</span>) <a href="/about/email.php" target="_blank" class="bg-primary checkbox-agree-span">내용보기</a>
                </label>

              </td>
            </tr>
            <tr>
              <td class="b text-center" colspan="2">
                <div class="col-xs-12">
                  <? $var = "form_confirm";?>
                  <button type="submit" class="btn btn-primary btn-lg2" id="<? echo $var;?>" name="<? echo $var;?>">회원정보 수정하기</button>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

</div>
<!-- /.contents -->

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>