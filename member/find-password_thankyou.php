<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連
  
  foreach($_GET as $key => $value)
  { 
    $$key = $common_connect->h($value);
  }
?>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?
  $meta_title = "비밀번호 재발행완료";
  $meta_description = "";
  require_once $_SERVER['DOCUMENT_ROOT']."/include/meta.php";
?>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/member/css/find-password_thankyou.css" rel="stylesheet">

</head>
<body>
<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

  <div class="l-container">
    <div class="pankuzu-wapper">
      <ul class="pankuzu">
        <li><a href="/">오키나와오박사 홈</a></li>
        <li>비밀번호 재발행완료</li>
      </ul>
    </div>
    <!-- /.pankuzu-wapper -->
  </div>
  <!-- /.l-container -->


  <div class="l-container clearfix">
    <h1 class="headline-glay fsize-lg left-bdr mb-30"><span class="color-blue">비밀번호 재발행완료</span></h1>
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <div class="table-responsive">
        <table class="table table-bordered">
          <tbody>
            <tr>
              <td class="main-board">
                <h1 class="headline-glay fsize-lg mb-20">비밀번호가 <span class="color-lightblue"> 성공적으로 변경 되었습니다.</span></h1>
                <p>등록된 이메일로 <span class="color-blue">변경된 패스워드가 전송되었으니</span>확인하여 주십시요.</p>

                <div style="margin-top:21px;padding:50px 0;text-align:center;font-size:19px;letter-spacing:-0.1em;font-weight:600">
                    <a href="/member/" class="a_login">오키나와 오박사 로그인하기</a>
                    <a href="/" class="a_top">오키나와 오박사 바로가기</a>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
    </div>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

</div>
<!-- /.contents -->

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>