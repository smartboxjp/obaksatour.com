<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連
?>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?
  $meta_title = "비밀번호 재발행";
  $meta_description = "";
  require_once $_SERVER['DOCUMENT_ROOT']."/include/meta.php";
?>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/member/css/member.css" rel="stylesheet">
<link href="/member/css/index.css" rel="stylesheet">
<link href="/common/css/form.css" rel="stylesheet">

  <script type="text/javascript">
    $(function() {
      $('#form_confirm').click(function() {
        err_default = "";
        err_check_count = 0;
        bgcolor_default = "#FFFFFF";
        bgcolor_err = "#FFCCCC";
        background = "background-color";

        err_check_count += check_input_email("login_email");

        if(err_check_count!=0)
        {
          alert("정확히 입력해주세요");
          return false;
        }
        else
        {
      //$('#form_confirm').submit();
      $('#form_confirm', "body").submit();
      return true;
    }

  });

  function check_input($str) 
  {
    $("#err_"+$str).html(err_default);
    $("#"+$str).css(background,bgcolor_default);

    if($('#'+$str).val()=="")
    {
      err ="정확히 입력해주세요.";
      $("#err_"+$str).html(err);
      $("#"+$str).css(background,bgcolor_err);

      return 1;
    }
    return 0;
  }

  function check_input_email($str) 
  {
    $("#err_"+$str).html(err_default);
    $("#"+$str).css(background,bgcolor_default);

    if($('#'+$str).val()=="")
    {
      err ="정확히 입력해주세요.";
      $("#err_"+$str).html(err);
      $("#"+$str).css(background,bgcolor_err);

      return 1;
    }
    else if(checkIsEmail($('#'+$str).val()) == false)
    {
      err ="메일형식에 맞게 입력해주세요.";
      $("#err_"+$str).html(err);
      $("#"+$str).css(background,bgcolor_err);

      return 1;
    }

    return 0;
  }

  //メールチェック
  function checkIsEmail(value) {
    if (value.match(/.+@.+\..+/) == null) {
      return false;
    }
    return true;
  }
    

  });

//-->
</script>
</head>
<body>

<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

  <div class="l-container">
    <div class="pankuzu-wapper">
      <ul class="pankuzu">
        <li><a href="/">오박사 홈</a></li>
        <li>비밀번호 재발행</li>
      </ul>
    </div>
    <!-- /.pankuzu-wapper -->
  </div>
  <!-- /.l-container -->

  <div class="l-container clearfix">
    <h1 class="headline-glay fsize-lg left-bdr mb-30"><span class="color-blue">비밀번호 재발행</span></h1>
  </div>
  <!-- /.l-container -->

  <div class="signup">
    <? $var = "form_regist";?>
    <form action="find-password_save.php" name="<? echo $var;?>" id="<? echo $var;?>" method="post" class="signup-main" data-mh="match-height">
      <img src="/common/images/member-logo.png" alt="아직 오키나와 오박사 회원이 아니신가요?" class="signup-main-img">
      <div class="signup-main-body">
        <h1 class="headline-blue signup-main-body-title"><span class="color-blue">패스워드 재발행</span></h1>
        <h2 class="headline-glay signup-main-body-subtitle">패스워드는 안전하게 암호화 되어 있어 <span class="color-lightblue">등록된 이메일로 재발행</span> 됩니다.</h2>
        <div class="signup-main-body-input">
          <? $var = "login_email"; ?>
          <input type="text" class="form-control input-sm form-text input-error" name="<? echo $var;?>" id="<? echo $var;?>" placeholder="이메일을 입력해주세요">
          <span id="err_<?=$var;?>" class="alert-red"></span>
        </div>
      </div>
      <!-- /.signup-main-body -->
      <div class="signup-main-btm">
        <p class="signup-main-btm-text fsize-sm"><a href="/member/">로그인</a><span class="line">ㅣ</span><a href="/member_regist/">회원가입</a></p>
        <button type="submit" class="btn btn-nomal sign-up-button" name="form_confirm" id="form_confirm">패스워드 재발행 하기</button>
      </div>
      <!-- /.signup-main-btm -->
    </form>
    <!-- /.signup-main -->
  </div>
  <!-- /.signup -->


</div>
<!-- /.contents -->

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>