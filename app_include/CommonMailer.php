<?php

class CommonMailer extends PHPMailer{ 

/*
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
*/

	//メール送信する
	public function Fn_send_utf ($to, $subject, $body, $fromname = null, $fromaddress) { 

		global $global_smtp_user;
		global $global_smtp_password;
		global $global_send_mail;
		global $global_mail_from;

		$this->IsSMTP();
	    //Enable SMTP debugging
	    // 0 = off (for production use)
	    // 1 = client messages
	    // 2 = client and server messages
	    //$this->SMTPDebug = 3;

	    $this->isSMTP();
	    $this->CharSet = 'UTF-8';
	    $this->Host = "smtp.naver.com";
	    $this->SMTPAuth = true;
		$this->Username = $global_smtp_user;
		$this->Password = $global_smtp_password; 
	    $this->SMTPSecure = "tls";
	    $this->Port = 587;

	    $this->From = $global_send_mail;
	    $this->FromName = $global_mail_from;

	    $this->addAddress($global_send_mail, $global_mail_from);

	//  $this->addAddress('neo201@naver.com', 'User');
	//  $this->addReplyTo('info@example.com', 'Information');
	//  $this->addCC('ohm@raonable.com');
	//  $this->addBCC('bcc@example.com');
	//  $this->addAttachment('/var/tmp/file.tar.gz');
	//  $this->addAttachment('/tmp/image.jpg', 'new.jpg');

	    $this->isHTML(true);
	    $this->Subject = "[오박사] 오박사 회원 가입을 위한 인증번호 발송입니다.";
	    $this->Body = 
	    "<table width='100%' height='100%' cellpadding='0' cellspacing='0' style='background: #e5e8eb;'>
	        <tr>
	            <td colspan='3' height='100'></td>
	        </tr>
	        <tr>
	            <td width='50%'></td>
	            <td width='900' align='left' valign='top'>
	            <table width='100%' cellpadding='0' cellspacing='0'>
	                <tr>
	                    <td width='900' height='150' align='left' valign='top' style='background: #1d4078;'>
	                    <table width='900' cellpadding='0' cellspacing='0'>
	                        <tr>
	                            <td width='40'></td>
	                            <td width='736' align='left' valign='middle'>
	                                <p style='font-family: Arial, Tahoma, Verdana, sans-serif; color: #999999; font-size: 18px; margin: 25px 0 3px; padding: 0; line-height: 1.2;'>AUTHENTICATION NUMBER</p>
	                                <h4 style='font-family: Nanum Gothic, 나눔고딕, 맑은고딕, sans-serif; color: #ffffff; font-size: 25px; font-weight: normal; line-height: 1.2; margin: 0; padding: 0;'>오박사 회원 가입 이메일 인증번호</h4>
	                            </td>
	                            <td align='right'><img src='http://obaksatour.cdn3.cafe24.com/mail-form-top.jpg' alt='오박사 로고'></td>
	                        </tr>
	                    </table>
	                    </td>
	                </tr>
	                <tr>
	                    <td width='900' align='left' valign='top' style='border-left: 1px solid #ccc; border-right: 1px solid #ccc; border-bottom: 1px solid #ccc;padding: 80px 49px 80px; background: #ffffff;'>
	                    <table width='800' cellpadding='0' cellspacing='0'>
	                        <tr>
	                            <td align='center'>
	                                <p style='margin: 0 0 10px; padding: 0;'><img src='http://obaksatour.cdn3.cafe24.com/mail-form-txt08.jpg'></p>
	                                <p style='font-family: 'dotum', 돋움, 돋움체, 맑은고딕, sans-serif; font-size: 12px; color: #333333; line-height: 1.4;'>오박사 회원 가입을 위해 다음의 인증번호를 정확히 입력해주세요.</p>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td height='60'></td>
	                        </tr>
	                        <tr>
	                            <td width='800' align='left' valign='top'>
	                                <table width='800' cellpadding='0' cellspacing='0' style='border-top: 2px solid #1752b0;font-family: dotum, 돋움, 돋움체, 맑은고딕, sans-serif; font-size: 12px; color: #333333; line-height: 1.4; margin: 0; padding: 0;'>
	                                    <tr>
	                                        <td align='left' valign='middle' width='140' height='40' style='border-bottom: 1px solid #ccc;'><font style='padding:0 0 0 20px; font-weight: bold; color: #555555;'>인증번호</font></td>
	                                        <td align='left' valign='middle' width='760' style='border-bottom: 1px solid #ccc;'><font style='font-weight: bold; color: #ff0000;'>".$randomNum."</font></td>
	                                    </tr>
	                                </table>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td height='80'></td>
	                        </tr>
	                        <tr>
	                            <td align='center'><a href='http://obaksatour.com'><img src='http://obaksatour.cdn3.cafe24.com/mail-form-btn01.jpg' alt='오박사'></a></td>
	                        </tr>
	                        <tr>
	                            <td height='110' style='border-bottom: 1px solid #ccc;'></td>
	                        </tr>
	                        <tr>
	                            <td height='15'></td>
	                        </tr>
	                        <tr>
	                            <td align='center' valign='top' style='font-family: dotum, 돋움, 돋움체, 맑은고딕, sans-serif; font-size: 11px; color: #666666; line-height: 1.7;'>
	                                OKIZONE PASS&nbsp;&nbsp;|&nbsp;&nbsp;대표자 : 오주영7&nbsp;&nbsp;|&nbsp;&nbsp;Naha Shopping Center 5F, Nishi 2-4-17, Naha-city Okinawa, Japan<br>Tel. +81 98 869 5233(5244)&nbsp;&nbsp;|&nbsp;&nbsp;Fax. +81 98 869 5230&nbsp;&nbsp;|&nbsp;&nbsp;E-mail. okihans@gmail.com<br>Copyright © OKIZONE PASS 2016 All rights reserved.
	                            </td>
	                        </tr>
	                    </table>
	                    </td>
	                </tr>
	            </table>
	            </td>
	            <td width='50%'></td>
	        </tr>
	        <tr>
	            <td colspan='3' height='200'></td>
	        </tr>
	    </table>";
	//  $this->AltBody = "This is the body in plain text for non-HTML mail clients";


/*
		//$this->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
		$this->SMTPAuth = true;
		$this->CharSet = 'utf-8';
		$this->SMTPSecure = 'tls';
		$this->Host = "smtp.gmail.com";
		$this->Port = 587;
		$this->IsHTML(false);
		$this->Username = $global_smtp_user;
		$this->Password = $global_smtp_password; 
		//$this->SetFrom($fromaddress, $fromname);

		$this->From     = $fromaddress;
		$this->FromName     = $fromname;
		$this->Subject = $subject;
		$this->Body = $body;
		$this->ClearAddresses();
		$this->AddAddress($to);
		
		if( !$this -> Send() ){
		    $message  = "Message was not sent<br/ >";
		    $message .= "Mailer Error: " . $this->ErrorInfo;
		} else {
		    $message  = "Message has been sent";
		}
*/
	} 
}

?>
