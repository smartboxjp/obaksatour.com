<?php

class CommonContents{
	
	
	//良く検索されてる地域リスト
	public function Fn_db_cate_area_list ($common_dao) 
	{ 
		$where = " and flag_search='1' and flag_open='1' ";
		$sql = " select cate_area_s_id, cate_area_l_title, cate_area_s_title ";
		$sql .= " FROM cate_area_s s inner join cate_area_l l on s.cate_area_l_id=l.cate_area_l_id ";
		$sql .= " where 1 ".$where." order by l.view_level, s.view_level ";
		$db_result = $common_dao->db_query_bind($sql);
		
		return $db_result;
	}

	//最近見た求人の履歴
	public function Fn_shop_cookie($shop_id){
		if (isset($_COOKIE['cookie_shop_visited'])) {
			$arr_data = preg_split('/\|\|\|/', $_COOKIE['cookie_shop_visited']);
			$save_cookie = "";
	
			for ($i=0 ; $i<count($arr_data)-1 ; $i++)
			{
				if($shop_id!=$arr_data[$i])
				{
					$save_cookie .= $arr_data[$i]."|||";
				}
			}
			$save_cookie = $shop_id."|||".$save_cookie;
			setcookie("cookie_shop_visited", $save_cookie, time()+3600*30, "/");
	
		} else {
			setcookie("cookie_shop_visited", $shop_id."|||", time()+3600*30, "/");	
		}
	}
}


?>
