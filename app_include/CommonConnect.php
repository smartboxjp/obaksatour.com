<?php

class CommonConnect {

/*
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
*/

	public function Fn_admin_check()
	{
		if(isset($_SESSION['admin_id'])!=true || $_SESSION['admin_id'] == '')
		{
			$this -> Fn_javascript_move("管理者専用ページです", "/app_master/login/");
		}
	}
	
	public function Fn_company_check()
	{
		if(isset($_SESSION['company_id'])!=true || $_SESSION['company_id'] == '')
		{
			$this -> Fn_javascript_move("ログインが必要です。", "/app_company/login/");
		}
	}
	
	public function Fn_member_login_check()
	{
		if(isset($_SESSION['member_id'])!=true || $_SESSION['member_id'] == '')
		{
			return false;
		}
		return true;
	}
	
	public function Fn_member_check()
	{
		if(isset($_SESSION['member_id'])!=true || $_SESSION['member_id'] == '')
		{
			//ログインチェック ///kanto/member/login_check.php
			if(isset($_COOKIE["pck"])!=true || $_COOKIE["pck"] == '') {
				/*
				if($_SERVER['QUERY_STRING']!=""){ $query = "?".$_SERVER['QUERY_STRING'];}
				echo "/app_include/login/cookie_check.php?url=".$_SERVER['PHP_SELF'].$query;
				exit;
				$this -> Fn_redirect("/app_include/login/cookie_check.php?url=".$_SERVER['PHP_SELF'].$query);
				*/
			}
			$this -> Fn_javascript_move("ログインが必要です。", "/member/");
		}
	}
	
	public function Fn_member_check_return($return_url)
	{
		if(isset($_SESSION['member_id'])!=true || $_SESSION['member_id'] == '')
		{
			$this -> Fn_javascript_move("ログインが必要です。", "/member/login/?url=".$return_url);
		}
	}
	
	
	/* すべて半角に変換 */
	public function Fn_shiftjis($str)
	{
		$str= mb_convert_kana($str,"rnask","UTF-8");
	
		return $str;
		/* すべて全角に変換 */
		//$str = mb_convert_kana($str,"RNASKV","EUC-JP");
	}

	//メールチェック
	public function Fn_valid_email($mail_address)
	{
		if (!preg_match('/^[a-zA-Z0-9_\.\-]+?@[A-Za-z0-9_\.\-]+$/',$mail_address))
			return false;
		else
			return true;
	}
	
	//Javascriptのhistoryback
	public function Fn_javascript_back($message)
	{
		echo ("
			<SCRIPT LANGUAGE=JavaScript>
			<!--
			alert('$message');
			history.back();
			//-->
			</SCRIPT>
		");
		exit;
	}
	
	//Javascriptのmove
	public function Fn_javascript_move($message1, $message2)
	{
		echo ("
			<SCRIPT LANGUAGE=JavaScript>
			<!--
			alert('$message1');
			document.location.href = '$message2';
			//-->
			</SCRIPT>
		");
		exit;
	}

	//URL移動
	public function Fn_redirect($url)
	{
		echo ("<meta http-equiv='Refresh' content='0; URL=$url'>");
		exit;
	}
	

	//SQL injection対策
	public function Fn_filter($str) 
	{
		$str=htmlspecialchars($str); //特殊文字を HTML エンティティに変換 例）&→&amp;
		$str=strip_tags($str); //html tag delete
		$str=addslashes($str);
		$str=mysql_real_escape_string($str);//文字列の特殊文字をエスケープ
		return $str;
	}

	//inputにダブルクゥーテーション表示エラー
	//$common_connect->h();
	public function h($str) 
	{
        if(!is_array($str))
        {
			return preg_replace('/&amp;(?=#[\d;])/', '&', htmlspecialchars(trim($str), ENT_QUOTES, 'UTF-8'));
		}
	}

	//$common_connect->hd();
	public function hd($str) 
	{
		return htmlspecialchars_decode($str);
	}

	
	//時間変更
	public function Fn_date($str1)
	{
		return substr($str1,0,4)."年".substr($str1,5,2)."月".substr($str1,8,2)."日(".substr($str1,11,2).":".substr($str1,17,2).")";
	}
	
	//時間変更
	public function Fn_date_day($str1)
	{
		switch(date("w", strtotime($str1))){ 
			case "0": 
			$view_day="日"; 
			break; 
			case "1": 
			$view_day="月"; 
			break; 
			case "2": 
			$view_day="火"; 
			break; 
			case "3": 
			$view_day="水"; 
			break; 
			case "4": 
			$view_day="木"; 
			break; 
			case "5": 
			$view_day="金"; 
			break; 
			case "6": 
			$view_day="土"; 
			break; 
		}
		return $view_day;
	}
	
	
	//paging 一覧
	public function Fn_paging_10_user($view_count, $all_count, $query="")
	{
		foreach($_GET as $key => $value){ 
			if($key!="page" && $key != "submit_button" && $key != "arr_genre" && $key != "arr_area_m")
			{
				$query .= "&".$this->h($key)."=".$this->h($value);
			}
		}
		$page = $this->h($_GET["page"]);
		if($page=="")
		{
			$page=1;
		}
		
		$search_page_count = 10;
		
		echo "<div class=\"l-container\">";
		echo "<ul class=\"paging\">";

		If ($page!="1")
		{
			echo "<li><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".((int)$page-1).$query."#search_list\">←</a></li>";
		}
	
		If ($view_count < $all_count)
		{
	
			//表示したいページ件数より少ない場合
			If(ceil(($all_count/$view_count)+1) < $search_page_count)
			{
				For ($i=1;$i<ceil(($all_count/$view_count)+1);$i++) 
				{
					If ($i == $page)
					{
            			echo "<li class=\"paging-current\"><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".$i.$query."#search_list\">".$i."</a></li>";
					}
					Else
					{
						echo "<li><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".$i.$query."#search_list\">".$i."</a></li>";
					}
				}
			}
			else
			{
				//現在ページが表示したいページより少ない
				if(($page+($search_page_count/2)-1) < $search_page_count)
				{
					For ($i=1;$i<$search_page_count+1;$i++) 
					{
						If ($i == $page)
						{
							echo "<li class=\"paging-current\"><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
				//現在ページが表示したいページより多い
				else if((ceil(($all_count/$view_count)+1) > $search_page_count) && ((ceil($all_count/$view_count)+1)-($search_page_count/2) < $page))
				{
					$start = ($page-$search_page_count+(ceil(($all_count/$view_count)+1)-$page));
					For ($i=$start;$i<ceil(($all_count/$view_count)+1);$i++) 
					{
						If ($i == $page)
						{
							echo "<li class=\"paging-current\"><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
				else
				{
		
					$min_page = ($page-($search_page_count/2));
					if ($min_page<1)
					{
						$min_page = 1;
					}
					$max_page = ceil(($all_count/$view_count)+1);
					
					if ($max_page>($page+($search_page_count/2)-1))
					{
						$max_page = ($page+($search_page_count/2));
					}
					
					For ($i=$min_page;$i<$max_page;$i++) 
					{
						If ($i == $page)
						{
							echo "<li class=\"paging-current\"><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
			}
		}
	
		if (ceil(($all_count/$view_count)+1)!="1" && $page<ceil(($all_count/$view_count)))
		{
			echo "<li><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".((int)$page+1).$query."#search_list\">→</a></li>";
		}
		echo "</ul>";
		echo "</div><!-- /.paging -->";
	}
	
	//paging 一覧
	public function Fn_paging_10_list($view_count, $all_count, $query="")
	{
		foreach($_GET as $key => $value){ 
			if($key!="page" && $key != "submit_button" && $key != "arr_genre" && $key != "arr_area_m")
			{
				$query .= "&".$this->h($key)."=".$this->h($value);
			}
		}
		$page = $this->h($_GET["page"]);
		if($page=="")
		{
			$page=1;
		}
		
		$search_page_count = 10;
		

		if ($view_count < $all_count)
		{
        	echo "<div class=\"table-responsive\">";
            echo "<div class=\"dataTables_wrapper\">";
            echo "<div class=\"row\">";
            echo "<div class=\"dataTables_paginate paging_bootstrap\">";

			echo "<div class=\"paging\">";
			echo "<ul class=\"pagination\">";
		}

		if ($page!="1")
		{
			echo "<li class=\"prev\"><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".((int)$page-1).$query."#search_list\">←前</a></li>";
		}
	
		if ($view_count < $all_count)
		{
	
			//表示したいページ件数より少ない場合
			If(ceil(($all_count/$view_count)+1) < $search_page_count)
			{
				For ($i=1;$i<ceil(($all_count/$view_count)+1);$i++) 
				{
					If ($i == $page)
					{
            			echo "<li class=\"active\"><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".$i.$query."#search_list\">".$i."</a></li>";
					}
					Else
					{
						echo "<li><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".$i.$query."#search_list\">".$i."</a></li>";
					}
				}
			}
			else
			{
				//現在ページが表示したいページより少ない
				if(($page+($search_page_count/2)-1) < $search_page_count)
				{
					For ($i=1;$i<$search_page_count+1;$i++) 
					{
						If ($i == $page)
						{
							echo "<li class=\"active\"><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
				//現在ページが表示したいページより多い
				else if((ceil(($all_count/$view_count)+1) > $search_page_count) && ((ceil($all_count/$view_count)+1)-($search_page_count/2) < $page))
				{
					$start = ($page-$search_page_count+(ceil(($all_count/$view_count)+1)-$page));
					For ($i=$start;$i<ceil(($all_count/$view_count)+1);$i++) 
					{
						If ($i == $page)
						{
							echo "<li class=\"active\"><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".$i.$query."#search_list\" >".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
				else
				{
		
					$min_page = ($page-($search_page_count/2));
					if ($min_page<1)
					{
						$min_page = 1;
					}
					$max_page = ceil(($all_count/$view_count)+1);
					
					if ($max_page>($page+($search_page_count/2)-1))
					{
						$max_page = ($page+($search_page_count/2));
					}
					
					For ($i=$min_page;$i<$max_page;$i++) 
					{
						If ($i == $page)
						{
							echo "<li class=\"active\"><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
			}
		}
	
		if (ceil(($all_count/$view_count)+1)!="1" && $page<ceil(($all_count/$view_count)))
		{
			echo "<li class=\"next\"><a href=\"".$this->h($_SERVER["PHP_SELF"])."?page=".((int)$page+1).$query."#search_list\">次→</a></li>";
		}

		if ($view_count < $all_count)
		{
			echo "</ul>";
			echo "</div><!-- /.paging -->";
			echo "</div>";
			echo "</div>";
			echo "</div>";
			echo "</div>";
		}
	}

	//フォルダ全て削除
	public function Fn_deldir($dir)
	{
		$handle = opendir($dir);
		while (false!==($FolderOrFile = readdir($handle)))
		{
			if($FolderOrFile != "." && $FolderOrFile != "..") 
			{ 
		
				if(is_dir("$dir/$FolderOrFile")) 
				{ $this -> Fn_deldir("$dir/$FolderOrFile"); } // recursive
					else
				{ unlink("$dir/$FolderOrFile"); }
			} 
		}
		closedir($handle);
		if(rmdir($dir))
		{ $success = true; }
		return $success; 
		
	} 
	
	//フォルダ内古いファイル削除
	//$deadline = 24*60*60;  //削除期限（指定秒数以上経過で削除）
	//$save_dir = $global_path.global_temp_img."/";
	public function Fn_old_delete($path, $deadline)
	{
		
		$count = 0;
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				if (is_file($path."/".$file)) {
					if((time() - filemtime($path."/".$file) > $deadline)){
						if(unlink($path."/".$file)){
							//echo("{$file}を削除しました。<br />\n");
							//$count += 1;
						}else{
							//echo("{$file}の削除に失敗しました。<br />\n");
						}
					}else{
						//echo "{$file}は削除しませんでした。<br />\n";
					}
				}else{
					//echo("{$file}はファイルではありません。<br />\n");
				}
			}
			closedir($handle);
		}
		
		//echo("{$count}ファイル削除しました。");
	}
	
	//文字制限
	public function Fn_short_string($str, $len, $last_str="…")
	{
		if(mb_strlen($str, "UTF-8")>=$len)
		{
			$return_str = mb_substr($str, 0, $len, "UTF-8").$last_str;
		}
		else 
		{
			$return_str = $str;
		}
		
		return $return_str;
	}
	
		
	//directory内ファイルコピー
	public function copyDirectory($imageDir, $destDir)
	{
		$handle=opendir($imageDir);   
		while($filename=readdir($handle))
		{       
			if(strcmp($filename,".")!=0	&& strcmp($filename,"..")!=0)
		 {
			 if(is_dir("$imageDir/$filename"))
			 {
				if(!empty($filename) && !file_exists("$destDir/$filename"))
				mkdir("$destDir/$filename");
				copyDirectory("$imageDir/$filename","$destDir/$filename");
			 }
		 else
			 {
				if(file_exists("$destDir/$filename"))
				unlink("$destDir/$filename");
				copy("$imageDir/$filename","$destDir/$filename");
			 }
		 }
		}     
	}
	
	//ランダム生成
	public function Fn_random_password($str_len)
	{
		$chars = "abcdefghijklmnopqrstuvwxyz1234567890";
		for($i = 0; $i < $str_len; $i++){
			$result .= $chars{mt_rand(0, strlen($chars)-1)};
		}
		return $result;
	}
	
	//ランダム
	public function Fn_random_data($str_len)
	{
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		for($i = 0; $i < $str_len; $i++){
			$result .= $chars{mt_rand(0, strlen($chars)-1)};
		}
		return $result;
	}

	const HASH_ALGO = 'sha256';
	
	public function generate()
	{
		return hash(self::HASH_ALGO, session_id());
	}
	
	public function validate($token, $throw = false)
	{
		$success = self::generate() === $token;
		if (!$success && $throw) {
			throw new \RuntimeException('CSRF validation failed.', 400);
		}
		return $success;
	}

	public function Fn_generate_pw($str)
	{
		return hash(self::HASH_ALGO, md5(trim($str)));
	}
	

	public function Fn_email_log($email, $email_title, $email_body)
	{	
		global $common_dao;
		
		$db_insert = "insert into email_log (email_log_id, email, email_title, email_body, regi_date) values ('', '".$email."', '".$email_title."', '".$email_body."', now())";
		$db_result = $common_dao->db_update($db_insert);
	}
}


?>
