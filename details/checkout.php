<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title>checkout</title>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/details/css/details.css" rel="stylesheet">

</head>
<body id="totop">


<div class="header-wrapper">
  <header class="header clearfix">

<div class="details-header">
  <div class="header-log">
    <a href="/">
      <img src="/common/images/logo.png" alt="오키나와 오박사">
    </a>
  </div>
  <div class="step-wrapper">
    <ul class="step">
      <li class="step-current"><span class="step-boll">1</span><div>추가항목 입력<span class="step-text"> / 추가입력 항목을 완성해 주세요.</span></div></li>
      <li><span class="step-boll">2</span><div>주문 및 결제</div></li>
      <li><span class="step-boll">3</span><div>주문 완료</div></li>
    </ul>
  </div>
  <!-- /.step-wrapper -->
</div>
<!-- /.details-header -->

  </header>
</div>
<!-- /.header-wrapper -->

<div class="contents details-contents">
<form action="#" class="form-inline form-horizontal">

  <div class="l-container mb-50">
    <h1 class="color-glay fsize-md mb-20">구매자 정보</h1>
    <table class="table table-bordered onetable">
      <tbody>
        <tr>
          <td class="ta-left">
            <img src="https://api.fnkr.net/testimg/64x64/00CED1/FFF/?text=img+placeholder" alt="" class="onetable-img">
            <dl class="onetable-info">
              <dt class="onetable-info-title">텍스트텍스트텍스트텍스트텍스트텍스트텍스트텍스트 텍스트</dt>
              <dd class="onetable-info-cap">텍스트텍스트텍스트텍스트</dd>
            </dl>
            <p class="onetable-text">오키나와 오박사 렌터카 서비스 너무 편하고 좋네요. 친절하신 직원분의 안내에 만족스러웠구요. 오키나와 여행하면서도 정말 편하게 잘 구경햇네요.</p>
            <div class="onetable-btmwrap clearfix">
              <div class="onetalbe-funcs"><span class="category-tag">선택 상품 수정하기</span></div>
              <div class="onetable-btmbox">
                <ul class="count">
                  <li class="count-plus"><div class="plus icon"></div></li>
                  <li class="count-num">10</li>
                  <li class="count-minus"><div class="minus icon"></div></li>
                </ul>
                <div><span>차량 보험료</span>30,000원</div>
                <div><span>상품 금액</span>150,000원</div>
              </div>
              <!-- /.onetable-btmbox -->
            </div>
            <!-- /.onetable-btmwrap -->
          </td>
        </tr>
      </tbody>
    </table>

<!-- 元レイアウトテーブル   -->
<?/*
    <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th class="col-a">선택 상품 및 옵션 정보</th>
            <th class="col-b">상품 수량</th>
            <th class="col-c">차량 보험료</th>
            <th class="col-d">상품 금액</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="ta-left pos-rel">
              <img src="https://api.fnkr.net/testimg/64x64/00CED1/FFF/?text=img+placeholder" alt="" class="table-img">
              <p class="table-img-p"><span class="bold">텍스트텍스트텍스트텍스트텍스트텍스트텍스트텍스트 텍스트</span class="bold"><br>텍스트텍스트텍스트텍스트</p>
              <div class="pos-rel-note"><span class="category-tag">선택 상품 수정하기</span></div>
            </td>
            <td rowspan="2">
              <ul class="count">
                <li class="count-plus"><div class="plus icon"></div></li>
                <li class="count-num">10</li>
                <li class="count-minus"><div class="minus icon"></div></li>
              </ul>
            </td>
            <td rowspan="2">30,000원</td>
            <td rowspan="2">150,000원</td>
          </tr>
          <tr>
            <td class="ta-left">텍스트텍스트텍스트</td>
          </tr>
        </tbody>
      </table>
    </div>
*/?>
  </div>
  <!-- /.l-container -->

  <div class="l-container mb-50">
    <h2 class="color-glay fsize-md mb-20">구매자 정보</h2>
    <table class="table table-bordered table-2col table-text-left mobiletable-row">
      <tbody>
        <tr>
          <th>구매자 이름</th>
          <td>천명관</td>
        </tr>
        <tr>
          <th>구매자 이메일</th>
          <td>chris9cc@naver.com</td>
        </tr>
        <tr>
          <th>구매자 휴대폰</th>
          <td>
          <div class="form-group-inline">
            <select name="#" class="form-control">
              <option value="#">010</option>
            </select>
            <input type="text" class="form-control wid-80">
            －
            <input type="text" class="form-control wid-80">
          </div>
          <!-- /.form-group -->
          </td>
        </tr>
      </tbody>
    </table>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

  <div class="l-container mb-50">
    <h2 class="color-glay fsize-md mb-20 va-middle">운전자 정보 <span class="category-tag ml-10">구매자 정보와 동일</span><span class="square-box hinto" data-toggle="tooltip" data-placement="right" data-html="true" data-original-title="운전하시는 분의 <span class='color-lightblue'>①유효기간 내의 </span>국제운전면허증 ②한국면허증 ③여권이 필요하며 미소지시 무면허로 이용이 불가능합니다.<br>추가 운전자도 동일한 서류를 준비하여 주시고, 차량 인수시 등록해야만 법적보호를 받을 수 있습니다.">?</span></h2>
    <table class="table table-bordered table-2col table-text-left mobiletable-row">
      <tbody>
        <tr>
          <th>대표 운전자 이름</th>
          <td>
            <div class="form-group-inline">
            <label class="control-label text-center label-name">한글</label>
            <input type="text" name="name" class="form-control wid-90 mr-20">
            <label class="control-label text-center label-name">영문</label>
            <input type="text" name="name" class="form-control wid-90">
            </div>
          </td>
        </tr>
        <tr>
          <th>대표 운전자 생년월일</th>
          <td>
            <div class="form-group-inline">
              <select name="#" class="form-control">
                <option value="#">생년</option>
              </select>
              <select name="#" class="form-control">
                <option value="#">월</option>
              </select>
              <select name="#" class="form-control">
                <option value="#">월</option>
              </select>
            </div>
            <!-- /.form-group-inline -->
          </td>
        </tr>
        <tr>
          <th>대표 운전자 휴대폰</th>
          <td>
            <div class="form-group-inline">
            <select name="#" class="form-control">
              <option value="#">010</option>
            </select>
            <input type="text" class="form-control wid-80">
            －
            <input type="text" class="form-control wid-80">
          </div>
          <!-- /.form-group -->
          </td>
        </tr>
      </tbody>
    </table>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

  <div class="l-container mb-50">
    <h2 class="color-glay fsize-md mb-20">항공편</h2>
    <table class="table table-bordered table-2col table-text-left mobiletable-row">
      <tbody>
        <tr>
          <th>오키나와 도착편</th>
          <td>
            <div class="form-group-inline">
              <select name="#" class="form-control">
                <option value="#">대여 장소 선택</option>
              </select>
              <span class="input-date-wrapper"><input type="text" placeholder="날짜 및 시간 선택" class="form-control input-date"></span>
            </div>
            <!-- /.form-group-inline -->
          </td>
        </tr>
        <tr>
          <th>오키나와 출발편</th>
          <td>
            <div class="form-group-inline">
              <select name="#" class="form-control">
                <option value="#">대여 장소 선택</option>
              </select>
              <span class="input-date-wrapper"><input type="text" placeholder="날짜 및 시간 선택" class="form-control input-date"></span>
            </div>
            <!-- /.form-group-inline -->
          </td>
        </tr>
      </tbody>
    </table>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

  <div class="l-container mb-50">
    <h2 class="color-glay fsize-md mb-20">대여 및 반납 정보</h2>
    <table class="table table-bordered table-2col table-text-left mobiletable-row">
      <tbody>
        <tr>
          <th>대여 일정 및 장소</th>
          <td>
            <div class="form-group-inline">
              <span class="input-date-wrapper"><input type="text" placeholder="날짜 및 시간 선택" class="form-control input-date"></span>
              <select name="#" class="form-control">
                <option value="#">대여 장소 선택</option>
              </select>
            </div>
            <!-- /.form-group-inline -->
          </td>
        </tr>
        <tr>
          <th>반납 일정 및 장소</th>
          <td>
            <div class="form-group-inline">
              <span class="input-date-wrapper"><input type="text" placeholder="날짜 및 시간 선택" class="form-control input-date"></span>
              <select name="#" class="form-control">
                <option value="#">대여 장소 선택</option>
              </select>
            </div>
            <!-- /.form-group-inline -->
          </td>
        </tr>
      </tbody>
    </table>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

  <div class="l-container clearfix mb-20">
    <h2 class="color-glay fsize-md mb-20">대여 및 반납 정보</h2>
    <div class="pricebox pricebox-one clearfix">
      <div class="pricebox-head">
        <b>주문금액</b>
        <div class="pricebox-head-num">180,000 <b>원</b></div>
      </div>
      <!-- /.pricebox-head -->
      <div class="pricebox-more" data-mh="match-height">
        <p><span>상품금액</span><span>150,000 원</span></p>
        <p><span>상품금액</span><span>30,000 원</span></p>
      </div>
      <!-- /.pricebox-more -->
    </div>
    <!-- /.pricebox-one -->
    <div class="pricebox pricebox-two clearfix">
      <div class="pricebox-head">
        <b>주문금액</b>
        <div class="pricebox-head-num">180,000 <b>원</b></div>
      </div>
      <!-- /.pricebox-head -->
      <div class="pricebox-more" data-mh="match-height">
        <p><span>상품금액</span><span>150,000 원</span></p>
        <p><span>상품금액</span><span>30,000 원</span></p>
      </div>
      <!-- /.pricebox-more -->
    </div>
    <!-- /.pricebox-two -->
    <div class="pricebox pricebox-all clearfix">
      <div class="pricebox-head">
        <b>주문금액</b>
        <div class="pricebox-head-num color-lightblue">300,000 <b>원</b></div>
      </div>
      <!-- /.pricebox-head -->
      <div class="pricebox-more" data-mh="match-height">
        <p>렌터카 상품의 경우 <span class="color-lightblue">선결제비용(차량보험료)</span>을 제외한 상품 금액은 현지에서 엔화로 후불결제하시면됩니다. </p>
      </div>
      <!-- /.pricebox-more -->
    </div>
    <!-- /.pricebox-all -->
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <ul class="btn-wrapper">
      <li><button type="submit" class="btn btn-default  btn-lg">정보 수정 취소</button></li>
      <li><button type="submit" class="btn btn-primary btn-lg">입력 정보 확인</button></li>
    </ul>
  </div>
  <!-- /.l-container -->

</form>
</div>
<!-- /.contents -->

<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});
</script>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>