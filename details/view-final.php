<?php
  require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
  $common_connect = new CommonConnect();
  $common_dao = new CommonDao(); //DB関連

  foreach($_GET as $key => $value)
  {
    $$key = $common_connect->h($value);
  }

  if($goods_id!="")
  {
    $arr_db_field = array("goods_id", "company_id", "cate_goods_s_id", "cate_area_id", "goods_name", "goods_name_jp");
    $arr_db_field = array_merge($arr_db_field, array("goods_summary", "goods_from", "goods_to", "search_price", "search_price_view"));
    $arr_db_field = array_merge($arr_db_field, array("temp_goods_detail_id"));
    $arr_db_field = array_merge($arr_db_field, array("goods_keyword", "googlemap_1", "googlemap_2", "goods_thumbnail", "goods_comment"));
    $arr_db_field = array_merge($arr_db_field, array("regi_date", "up_date", "flag_open"));

    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
      $sql .= $val.", ";
    }
    $sql .= " 1 FROM goods where goods_id='".$goods_id."' ";

    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
      for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
      {
        foreach($arr_db_field as $val)
        {
          $$val = $db_result[$db_loop][$val];
        }
      }
    }
  }
?>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?
  $meta_title = $goods_name;
  $meta_description = "";
  require_once $_SERVER['DOCUMENT_ROOT']."/include/meta.php";
?>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>


<!-- 個別ページcss -->
<link href="/details/css/details.css" rel="stylesheet">
<link href="/details/css/view-final.css" rel="stylesheet">

<!-- swiper -->
<link href="/common/css/swiper.min.css" rel="stylesheet">
<script src="/common/js/swiper.min.js"></script>

</head>
<body>
<?
  if($goods_id=="" || $goods_name=="")
  {
    $common_connect -> Fn_javascript_back("정확히 입력해주세요.");
  }
?>

<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents details-contents">
<form action="#" class="form-horizontal">

  <div class="l-container">
    <div class="final-head">
      <div class="final-head-img">

        <?php
        if($goods_thumbnail!="")
        {
          echo "<img src='http://www.obaksatour.com/".global_company_dir.$company_id."/goods/".$goods_thumbnail."?".date("his")."'>";
        }
        ?>
      </div>
      <!-- /.final-head-img -->
      <div class="final-head-info">
        <h1>당일 예약 한정</h1>
        <h2><? echo $goods_name;?></h2>
        <p class="final-head-info-price"><span class="color-lightblue fs-s">1일</span><span class="fs-s">기준</span><strong class="fs-l"><? echo number_format($search_price);?></strong><span class="color-lightblue fs-m">엔</span></p>
        <div class="final-head-info-selects">
          <select name="#" class="form-control mb-10">
            <option value="#">상품 구분</option>
          </select>
          <select name="#" class="form-control mb-10">
            <option value="#">상품 구분</option>
          </select>
          <select name="#" class="form-control mb-10">
            <option value="#">상품 구분</option>
          </select>
        </div>
        <!-- /.final-head-info-selects -->
        <div class="final-head-row">
          <p>01. 오프시즌 2박 3일 48시간 렌터카 / 6월1일</p>
          <ul class="count">
            <li class="count-plus"><div class="plus icon"></div></li>
            <li class="count-num">10</li>
            <li class="count-minus"><div class="minus icon"></div></li>
          </ul>
          <span class="color-lightblue">15,0000원</span><span class="close icon"></span>
        </div>
        <!-- /.final-head-row -->
        <div class="final-head-row">
          <p>01. 오프시즌 2박 3일 48시간 렌터카 / 6월1일</p>
          <ul class="count">
            <li class="count-plus"><div class="plus icon"></div></li>
            <li class="count-num">10</li>
            <li class="count-minus"><div class="minus icon"></div></li>
          </ul>
          <span class="color-lightblue">15,0000원</span><span class="close icon"></span>
        </div>
        <!-- /.final-head-row -->
        <p class="color-lightblue bg-gy mt-20 mb-20">! 오박사투어 렌터카는 해외 여행객의 안전을 위해 차량보험을 의무적으로 가입하도록 하고 있습니다.</p>
        <p class="text-right">차량 보험료 : 30,000원</p>
        <div class="final-head-btm">
          <p>총 상품금액<span class="color-lightblue fsize-lg">330,000</span>원</p>
          <button type="submit" class="btn btn-primary btn-lg">이 조건의 상품 구매하기</button>
        </div>
        <!-- /.final-head-btm -->
      </div>
      <!-- /.final-head-info -->
    </div>
    <!-- /.final-head -->

    <div class="final-rowtab">
      <ul class="tab-row tab-nav clearfix">
        <li class="tab-row-current">오박사투어<br>추천 상품</li>
        <li><span class="color-lightblue">오박사투어</span><br>연관 상품</li>
      </ul>

      <div class="slide-images-wrapper">
        <div class="tab-conts swiper-a">
          <div class="swiper-container">
            <div class="swiper-wrapper">
              <div class="swiper-slide slide-images-box">
                <img src="https://api.fnkr.net/testimg/160x130/00CED1/FFF/?text=img+placeholder" alt="" class=" slide-images-box-img">
                <dl class="slide-images-box-item">
                  <dt><span class="bold">[원데이/반나절]</span> 세계 최대급 수급 수</dt>
                  <dd class="color-lightblue">60,000원</dd>
                </dl>
              </div>
              <!-- /.swiper-slide -->
              <div class="swiper-slide slide-images-box">
                <img src="https://api.fnkr.net/testimg/160x130/00CED1/FFF/?text=img+placeholder" alt="" class=" slide-images-box-img">
                <dl class="slide-images-box-item">
                  <dt><span class="bold">[원데이/반나절]</span> 세계 최대급 수급 수</dt>
                  <dd class="color-lightblue">60,000원</dd>
                </dl>
              </div>
              <!-- /.swiper-slide -->
              <div class="swiper-slide slide-images-box">
                <img src="https://api.fnkr.net/testimg/160x130/00CED1/FFF/?text=img+placeholder" alt="" class=" slide-images-box-img">
                <dl class="slide-images-box-item">
                  <dt><span class="bold">[원데이/반나절]</span> 세계 최대급 수급 수</dt>
                  <dd class="color-lightblue">60,000원</dd>
                </dl>
              </div>
              <!-- /.swiper-slide -->
              <div class="swiper-slide slide-images-box">
                <img src="https://api.fnkr.net/testimg/160x130/00CED1/FFF/?text=img+placeholder" alt="" class=" slide-images-box-img">
                <dl class="slide-images-box-item">
                  <dt><span class="bold">[원데이/반나절]</span> 세계 최대급 수급 수</dt>
                  <dd class="color-lightblue">60,000원</dd>
                </dl>
              </div>
              <!-- /.swiper-slide -->
              <div class="swiper-slide slide-images-box">
                <img src="https://api.fnkr.net/testimg/160x130/00CED1/FFF/?text=img+placeholder" alt="" class=" slide-images-box-img">
                <dl class="slide-images-box-item">
                  <dt><span class="bold">[원데이/반나절]</span> 세계 최대급 수급 수</dt>
                  <dd class="color-lightblue">60,000원</dd>
                </dl>
              </div>
              <!-- /.swiper-slide -->
              <div class="swiper-slide slide-images-box">
                <img src="https://api.fnkr.net/testimg/160x130/00CED1/FFF/?text=img+placeholder" alt="" class=" slide-images-box-img">
                <dl class="slide-images-box-item">
                  <dt><span class="bold">[원데이/반나절]</span> 세계 최대급 수급 수</dt>
                  <dd class="color-lightblue">60,000원</dd>
                </dl>
              </div>
              <!-- /.swiper-slide -->
              <div class="swiper-slide slide-images-box">
                <img src="https://api.fnkr.net/testimg/160x130/00CED1/FFF/?text=img+placeholder" alt="" class=" slide-images-box-img">
                <dl class="slide-images-box-item">
                  <dt><span class="bold">[원데이/반나절]</span> 세계 최대급 수급 수</dt>
                  <dd class="color-lightblue">60,000원</dd>
                </dl>
              </div>
              <!-- /.swiper-slide -->
              <div class="swiper-slide slide-images-box">
                <img src="https://api.fnkr.net/testimg/160x130/00CED1/FFF/?text=img+placeholder" alt="" class=" slide-images-box-img">
                <dl class="slide-images-box-item">
                  <dt><span class="bold">[원데이/반나절]</span> 세계 최대급 수급 수</dt>
                  <dd class="color-lightblue">60,000원</dd>
                </dl>
              </div>
              <!-- /.swiper-slide -->
            </div>
            <!-- /.swiper-wrapper -->
          </div>
          <!-- /.swiper-container swiper-a -->
          <div class="swiper-a swiper-button-prev"></div>
          <div class="swiper-a swiper-button-next"></div>
        </div>
        <!-- /.tab-conts -->

        <div class="tab-conts swiper-b">
          <div class="swiper-container">
            <div class="swiper-wrapper">
              <div class="swiper-slide slide-images-box">
                <img src="https://api.fnkr.net/testimg/160x130/dddddd/FFF/?text=img+placeholder" alt="" class=" slide-images-box-img">
                <dl class="slide-images-box-item">
                  <dt><span class="bold">[원데이/반나절]</span> 세계 최대급 수급 수</dt>
                  <dd class="color-lightblue">60,000원</dd>
                </dl>
              </div>
              <!-- /.swiper-slide -->
              <div class="swiper-slide slide-images-box">
                <img src="https://api.fnkr.net/testimg/160x130/dddddd/FFF/?text=img+placeholder" alt="" class=" slide-images-box-img">
                <dl class="slide-images-box-item">
                  <dt><span class="bold">[원데이/반나절]</span> 세계 최대급 수급 수</dt>
                  <dd class="color-lightblue">60,000원</dd>
                </dl>
              </div>
              <!-- /.swiper-slide -->
              <div class="swiper-slide slide-images-box">
                <img src="https://api.fnkr.net/testimg/160x130/dddddd/FFF/?text=img+placeholder" alt="" class=" slide-images-box-img">
                <dl class="slide-images-box-item">
                  <dt><span class="bold">[원데이/반나절]</span> 세계 최대급 수급 수</dt>
                  <dd class="color-lightblue">60,000원</dd>
                </dl>
              </div>
              <!-- /.swiper-slide -->
              <div class="swiper-slide slide-images-box">
                <img src="https://api.fnkr.net/testimg/160x130/dddddd/FFF/?text=img+placeholder" alt="" class=" slide-images-box-img">
                <dl class="slide-images-box-item">
                  <dt><span class="bold">[원데이/반나절]</span> 세계 최대급 수급 수</dt>
                  <dd class="color-lightblue">60,000원</dd>
                </dl>
              </div>
              <!-- /.swiper-slide -->
              <div class="swiper-slide slide-images-box">
                <img src="https://api.fnkr.net/testimg/160x130/dddddd/FFF/?text=img+placeholder" alt="" class=" slide-images-box-img">
                <dl class="slide-images-box-item">
                  <dt><span class="bold">[원데이/반나절]</span> 세계 최대급 수급 수</dt>
                  <dd class="color-lightblue">60,000원</dd>
                </dl>
              </div>
              <!-- /.swiper-slide -->
              <div class="swiper-slide slide-images-box">
                <img src="https://api.fnkr.net/testimg/160x130/00CED1/FFF/?text=img+placeholder" alt="" class=" slide-images-box-img">
                <dl class="slide-images-box-item">
                  <dt><span class="bold">[원데이/반나절]</span> 세계 최대급 수급 수</dt>
                  <dd class="color-lightblue">60,000원</dd>
                </dl>
              </div>
              <!-- /.swiper-slide -->
              <div class="swiper-slide slide-images-box">
                <img src="https://api.fnkr.net/testimg/160x130/00CED1/FFF/?text=img+placeholder" alt="" class=" slide-images-box-img">
                <dl class="slide-images-box-item">
                  <dt><span class="bold">[원데이/반나절]</span> 세계 최대급 수급 수</dt>
                  <dd class="color-lightblue">60,000원</dd>
                </dl>
              </div>
              <!-- /.swiper-slide -->
              <div class="swiper-slide slide-images-box">
                <img src="https://api.fnkr.net/testimg/160x130/00CED1/FFF/?text=img+placeholder" alt="" class=" slide-images-box-img">
                <dl class="slide-images-box-item">
                  <dt><span class="bold">[원데이/반나절]</span> 세계 최대급 수급 수</dt>
                  <dd class="color-lightblue">60,000원</dd>
                </dl>
              </div>
              <!-- /.swiper-slide -->
            </div>
            <!-- /.swiper-wrapper -->
          </div>
          <!-- /.swiper-container swiper-a -->
          <div class="swiper-b swiper-button-prev"></div>
          <div class="swiper-b swiper-button-next"></div>
        </div>
        <!-- /.tab-conts -->
      </div>
      <!-- /.slide-images-wrapper -->

    </div>
    <!-- final-rowtab -->


    <ul class="nav nav-tabs">
      <li class="active"><a  href="#1" data-toggle="tab">상품정보</a></li>
      <li><a href="#2" data-toggle="tab">구매정보</a></li>
      <li><a href="#3" data-toggle="tab">구매후기<span>(200)</span></a></li>
      <li><a href="#4" data-toggle="tab">상품 Q &amp; A</a></li>
    </ul>

    <div class="tab-content">
      <div class="tab-pane active" id="1">
<?
  if($temp_goods_detail_id!="0")
  {
    $sql = "SELECT temp_goods_detail_comment FROM temp_goods_detail where company_id='".$company_id."' and temp_goods_detail_id='".$temp_goods_detail_id."' ";

    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
      echo htmlspecialchars_decode($db_result[0]["temp_goods_detail_comment"]);
    }
  }
?>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="2">

        <div class="megahon">
          <i class="fa fa-bullhorn" aria-hidden="true"></i>
          <div class="megahon-text">
            <h3>오늘 출발하는 상품입니다.</h3>
            <p>낮 12시까지 주문하시면 오늘 출발하여 빠르게 받아보실 수 있습니다. (주말, 공휴일 제외)</p>
          </div>
          <!-- /.megahon-text -->
        </div>
        <!-- /.megahon -->

        <table class="table table-bordered talbe-half">
          <tr>
            <th class="text-left">상품 구매정보</th>
            <th class="text-left">상품 안내사항</th>
          </tr>
          <tr>
            <td class="text-left">
              <div>상품 출고전 주문 취소와 배송상태 확인은 <span class="color-blue">마이페이지의 나의 구매목록을</span> 이용해주세요.[배송정보]</div>
            </td>
            <td class="text-left"><div><span class="color-blue">[반품기간]</span></div></td>
          </tr>
        </table>

        <h2 class="color-glay fsize-md mb-30">대여 및 반납 정보</h2>
        <div class="table-responsive-wrapper">
          <table class="table table-bordered table-2col table-text-left">
            <tbody>
              <tr>
                <th class="va-top">기준 가격정보</th>
                <td>
                  <div>2017년 06월 26일 오키나와 오박사가<br>기준 가격정보 산정이 어려워 오키나와 오박사가 제시하는 합리적인 가격입니다.</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->

        <h2 class="color-glay fsize-md mb-30">대여 및 반납 정보</h2>
        <div class="table-responsive-wrapper">
          <table class="table table-bordered table-2col table-text-left mb-10">
            <tbody>
              <tr>
                <th class="va-top">재품소재</th>
                <td>
                  상세페이지 표기
                </td>
              </tr>
              <tr>
                <th class="va-top">색상</th>
                <td>
                  상세페이지 표기
                </td>
              </tr>
              <tr>
                <th class="va-top">치수</th>
                <td>
                  상세페이지 표기
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->

      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="3">
      コンテンツ
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="3">
      コンテンツ
      </div>
      <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->

  </div>
  <!-- /.l-container -->

</form>
</div>
<!-- /.contents -->

<script>
var swiperA = new Swiper('.swiper-a .swiper-container', {
  paginationClickable: true,
  slidesPerView: 5,
  speed: 600,
  nextButton: '.swiper-a .swiper-button-next',
  prevButton: '.swiper-a .swiper-button-prev',
  freeMode: 'auto',
  spaceBetween: 20,
  breakpoints: {
    980: {
      slidesPerView: 4,
      spaceBetween: 10
    },
    600: {
      slidesPerView: 3,
      spaceBetween: 10
    },
    400: {
      slidesPerView: 2,
      spaceBetween: 10
    }
  }
});

var swiperB = new Swiper('.swiper-b .swiper-container', {
  paginationClickable: true,
  slidesPerView: 5,
  speed: 600,
  nextButton: '.swiper-b .swiper-button-next',
  prevButton: '.swiper-b .swiper-button-prev',
  freeMode: 'auto',
  spaceBetween: 20,
  breakpoints: {
    980: {
      slidesPerView: 4,
      spaceBetween: 10
    },
    600: {
      slidesPerView: 3,
      spaceBetween: 10
    },
    400: {
      slidesPerView: 2,
      spaceBetween: 10
    }
  }
});

$(function(){
  $('.tab-conts').hide();
  $('.tab-conts').eq(0).show();

    $(".tab-row li").click(function() {
      $(".tab-row li").removeClass("tab-row-current ");
      $(this).addClass("tab-row-current");

      var index = $(".tab-row li").index(this);

      $('.tab-conts').hide();
      $('.tab-conts').eq(index).fadeIn(800);
    });
});
</script>

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>