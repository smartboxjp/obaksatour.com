<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title>payment</title>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/details/css/details.css" rel="stylesheet">

</head>
<body id="totop">


<div class="header-wrapper">
  <header class="header clearfix">

<div class="details-header">
  <div class="header-log">
    <a href="/">
      <img src="/common/images/logo.png" alt="오키나와 오박사">
    </a>
  </div>
  <div class="step-wrapper">
    <ul class="step">
      <li class="step-current-off"><span class="step-boll">1</span><div>추가항목 입력</div></li>
      <li class="step-current"><span class="step-boll">2</span><div>주문 및 결제<span class="step-text"> / 주문 상품을 결제해 주세요.</span></div></li>
      <li><span class="step-boll">3</span><div>주문 완료</div></li>
    </ul>
  </div>
  <!-- /.step-wrapper -->
</div>
<!-- /.details-header -->

  </header>
</div>
<!-- /.header-wrapper -->

<div class="contents details-contents">
<form action="#" class="form-inline form-horizontal">

  <div class="l-container mb-50">
    <h1 class="color-glay fsize-md mb-20">결제하실 상품</h1>
    <table class="table table-bordered onetable">
      <tbody>
        <tr>
          <td class="ta-left">
            <img src="https://api.fnkr.net/testimg/64x64/00CED1/FFF/?text=img+placeholder" alt="" class="onetable-img">
            <dl class="onetable-info">
              <dt class="onetable-info-title">텍스트텍스트텍스트텍스트텍스트텍스트텍스트텍스트 텍스트</dt>
              <dd class="onetable-info-cap">텍스트텍스트텍스트텍스트</dd>
            </dl>
            <p class="onetable-text">오키나와 오박사 렌터카 서비스 너무 편하고 좋네요. 친절하신 직원분의 안내에 만족스러웠구요. 오키나와 여행하면서도 정말 편하게 잘 구경햇네요.</p>
            <div class="onetable-btmwrap clearfix">
              <div class="onetalbe-funcs"><span class="category-tag">선택 상품 수정하기</span></div>
              <div class="onetable-btmbox">
                <div><span>상품 수량</span>1개</div>
                <div><span>차량 보험료</span>30,000원</div>
                <div><span>상품 금액</span>150,000원</div>
              </div>
              <!-- /.onetable-btmbox -->
            </div>
            <!-- /.onetable-btmwrap -->
          </td>
        </tr>
      </tbody>
    </table>

<?/*
    <div class="table-responsive">
      <table class="table table-bordered mb-10">
        <thead>
          <tr>
            <th class="col-a">선택 상품 및 옵션 정보</th>
            <th class="col-b">상품 수량</th>
            <th class="col-c">차량 보험료</th>
            <th class="col-d">상품 금액</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="ta-left pos-rel">
              <img src="https://api.fnkr.net/testimg/64x64/00CED1/FFF/?text=img+placeholder" alt="" class="table-img">
              <p class="table-img-p"><span class="bold">텍스트텍스트텍스트텍스트텍스트텍스트텍스트텍스트 텍스트</span class="bold"><br>텍스트텍스트텍스트텍스트</p>
              <div class="pos-rel-note"><span class="category-tag">선택 상품 수정하기</span></div>
            </td>
            <td rowspan="2">1개</td>
            <td rowspan="2">30,000원</td>
            <td rowspan="2">150,000원</td>
          </tr>
          <tr>
            <td class="ta-left">텍스트텍스트텍스트</td>
          </tr>
        </tbody>
      </table>
    </div>
    <!-- /.table-responsive -->
    <p class="text-right"><span class="tag">주문 상품 수정하기</span></p>
*/?>

  </div>
  <!-- /.l-container -->

  <div class="l-container mb-50">
    <h2 class="color-glay fsize-md mb-20">구매자 정보</h2>
    <table class="table table-bordered table-2col table-text-left mobiletable-row">
      <tbody>
        <tr>
          <th>구매자 이름</th>
          <td>천명관</td>
        </tr>
        <tr>
          <th>구매자 이메일</th>
          <td>chris9cc@naver.com</td>
        </tr>
        <tr>
          <th>구매자 휴대폰</th>
          <td>
            010 - 1234 - 5678
          </td>
        </tr>
      </tbody>
    </table>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

  <div class="l-container mb-50">
    <h2 class="color-glay fsize-md mb-20 va-middle">운전자 정보 <span class="category-tag ml-10">구매자 정보와 동일</span><span class="square-box hinto" data-toggle="tooltip" data-placement="right" data-html="true" data-original-title="운전하시는 분의 <span class='color-lightblue'>①유효기간 내의 </span>국제운전면허증 ②한국면허증 ③여권이 필요하며 미소지시 무면허로 이용이 불가능합니다.<br>추가 운전자도 동일한 서류를 준비하여 주시고, 차량 인수시 등록해야만 법적보호를 받을 수 있습니다.">?</span></h2>
    <table class="table table-bordered table-2col table-text-left mobiletable-row">
      <tbody>
        <tr>
          <th>대표 운전자 이름</th>
          <td>
            천명관 (Chris Cheon)
          </td>
        </tr>
        <tr>
          <th>대표 운전자 생년월일</th>
          <td>
            1976년 10월 14일
          </td>
        </tr>
        <tr>
          <th>대표 운전자 휴대폰</th>
          <td>
            010 - 1234 - 5678
          </td>
        </tr>
      </tbody>
    </table>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

  <div class="l-container mb-50">
    <h2 class="color-glay fsize-md mb-20">항공편</h2>
    <table class="table table-bordered table-2col table-text-left mobiletable-row">
      <tbody>
        <tr>
          <th>오키나와 도착편</th>
          <td>
            아시아나 OCJKR편 / 2017년 5월 25일 오후 3시 00분
          </td>
        </tr>
        <tr>
          <th>오키나와 출발편</th>
          <td>
            아시아나 OCJKR편 /
          </td>
        </tr>
      </tbody>
    </table>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

  <div class="l-container mb-50">
    <h2 class="color-glay fsize-md mb-20">대여 및 반납 정보</h2>
    <table class="table table-bordered table-2col table-text-left mobiletable-row">
      <tbody>
        <tr>
          <th>대여 일정 및 장소</th>
          <td>
            2017년 5월 25일 오후 3시 00분 /  오키나와 공항 지점
          </td>
        </tr>
        <tr>
          <th>반납 일정 및 장소</th>
          <td>
            2017년 5월 25일 오후 3시 00분 /  오키나와 공항 지점
          </td>
        </tr>
      </tbody>
    </table>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

  <div class="l-container clearfix mb-30">
    <h2 class="color-glay fsize-md mb-20">대여 및 반납 정보</h2>
    <div class="pricebox pricebox-one clearfix pricebox-payment mobiletable-row">
      <div class="pricebox-head">
        <b>주문금액</b>
        <div class="pricebox-head-num">180,000 <b>원</b></div>
      </div>
      <!-- /.pricebox-head -->
    </div>
    <!-- /.pricebox-one -->
    <div class="pricebox pricebox-two clearfix pricebox-payment">
      <div class="pricebox-head">
        <b>주문금액</b>
        <div class="pricebox-head-num">180,000 <b>원</b></div>
      </div>
      <!-- /.pricebox-head -->
    </div>
    <!-- /.pricebox-two -->
    <div class="pricebox pricebox-all clearfix pricebox-payment">
      <div class="pricebox-head">
        <b>주문금액</b>
        <div class="pricebox-head-num color-lightblue">300,000 <b>원</b></div>
      </div>
      <!-- /.pricebox-head -->
    </div>
    <!-- /.pricebox-all -->
    <div class="payment-table">
      <table class="table table-bordered table-2col table-text-left">
      <tr class="bdr-none-td">
        <th class="payment-table-rsp-th">결제 방법</th>
        <td>
          <div class="form-group">
            <div class="radio-inline">
              <input type="radio" value="1" name="radio01" id="0101" class="input-radio">
              <label for="0101" class="input-radio-text">신용카드</label>
            </div>
            <div class="radio-inline">
              <input type="radio" value="2" name="radio01" id="0102" class="input-radio">
              <label for="0102" class="input-radio-text">무통장입금 (1시간 내 자동확인)</label>
            </div>
            <div class="radio-inline">
              <input type="radio" value="3" name="radio01" id="0103" class="input-radio">
              <label for="0103" class="input-radio-text">실시간 계좌이체</label>
            </div>
            <div class="radio-inline">
              <input type="radio" value="4" name="radio01" id="0104" class="input-radio">
              <label for="0104" class="input-radio-text">에스크로</label>
            </div>
            <div class="radio-inline">
              <input type="radio" value="5" name="radio01" id="0105" class="input-radio">
              <label for="0105" class="input-radio-text">페이팔</label>
            </div>
          </div>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <div class="payment-table-bclok">
            <div class="payment-table-left">
              <div class="mb-20">
                <b class="mr-10">입금은행</b>
                <select name="#" class="form-control input-inline">
                  <option value="#">대여 장소 선택</option>
                </select>
              </div>
              <p class="bold mb-10">무통장입금 안내</p>
              <ul class="list-style-dot fsize-sm-list">
                <li>입금 시 구매자 이름과 동일한 이름으로 입금해주시기 바랍니다.</li>
                <li>입금 기한 내에 반드시 정확한 결제금액을 입금해 주세요.</li>
                <li>입금 기한 경과 시 주문은 자동으로 취소가 됨을 알려드립니다.</li>
              </ul>
              <!-- /.payment-table-left-box -->
            </div>
            <!-- /.payment-table-left -->

            <div class="payment-table-left">
              <p class="mb-20"><span class="bold mr-20">입금기한</span>2017년 5월 27일 23:59 까지</p>
              <p class="bold mb-10">현금 영수증 발행</p>
              <div class="mb-10">
                <div class="radio-inline">
                  <input type="radio" value="5" name="radio02" id="0105" class="input-radio">
                  <label for="0105" class="input-radio-text">개인 소득공제용</label>
                </div>
                <div class="radio-inline">
                  <input type="radio" value="5" name="radio02" id="0105" class="input-radio">
                  <label for="0105" class="input-radio-text">사업자 증빙용</label>
                </div>
              </div>
              <select name="#" class="form-control input-inline">
                <option value="#">010</option>
              </select>
              <input type="text" class="form-control wid-70 input-inline">
              －
              <input type="text" class="form-control wid-70 input-inline">
            </div>
            <!-- /.payment-table-right -->
          </div>
          <!-- /.payment-table-bclok -->
        </td>
      </tr>
      </table>
    </div>
    <!-- /.payment-table -->
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <ul class="btn-wrapper">
      <li><button type="submit" class="btn btn-primary btn-lg">입력 정보 확인</button></li>
    </ul>
  </div>
  <!-- /.l-container -->

</form>
</div>
<!-- /.contents -->

<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});
</script>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>