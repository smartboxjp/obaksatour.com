<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title>order-detail-view</title>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/mypage/css/mypage.css" rel="stylesheet">
<link href="/mypage/css/order-detail-view.css" rel="stylesheet">

</head>
<body>

<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

  <div class="l-container">
    <div class="pankuzu-wapper">
      <ul class="pankuzu">
        <li><a href="/">오키나와 오박사 홈</a></li>
        <li><a href="#">마이페이지</a></li>
        <li>나의 구매 목록</li>
      </ul>
    </div>
    <!-- /.pankuzu-wapper -->
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <div class="localnavs">
      <p class="localnavs-title">마이페이지</p>
      <ul class="localnavs-list">
        <li class="localnavs-list-current"><a href="#">나의 구매 목록</a></li>
        <li><a href="#">취소 및 변경 현황</a></li>
        <li><a href="#">상품 Q＆A</a></li>
        <li><a href="#">구매 후기</a></li>
        <li><a href="#">회원정보수정</a></li>
      </ul>
      <p class="localnavs-text color-lightblue bg-gy">! 구매하신 상품의 내역 확인 및 진행 상황, 취소, 변경 등의 관리를 하실 수 있습니다.</p>
    </div>
    <!-- /.localnavs -->
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <h1 class="headline-glay fsize-lg left-bdr mb-20"><span class="color-blue">나의 구매</span> 목록</h1>
    <table class="table table-bordered onetable mb-50">
      <tbody>
        <tr>
          <td class="ta-left">
            <img src="https://api.fnkr.net/testimg/64x64/00CED1/FFF/?text=img+placeholder" alt="" class="onetable-img">
            <dl class="onetable-info">
              <dt class="onetable-info-title">텍스트텍스트텍스트텍스트텍스트텍스트텍스트텍스트 텍스트</dt>
              <dd class="onetable-info-cap">텍스트텍스트텍스트텍스트</dd>
            </dl>
            <p class="onetable-text">오키나와 오박사 렌터카 서비스 너무 편하고 좋네요. 친절하신 직원분의 안내에 만족스러웠구요. 오키나와 여행하면서도 정말 편하게 잘 구경햇네요.</p>
            <div class="onetable-btmwrap clearfix">
              <div class="onetalbe-funcs"><span class="category-tag">선택 상품 수정하기</span></div>
              <div class="onetable-btmbox">
                <div><span>상품 수량</span>1개</div>
                <div><span>차량 보험료</span>30,000원</div>
                <div><span>상품 금액</span>150,000원</div>
              </div>
              <!-- /.onetable-btmbox -->
            </div>
            <!-- /.onetable-btmwrap -->
          </td>
        </tr>
      </tbody>
    </table>
<?/*
    <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th class="col-a">선택 상품 및 옵션 정보</th>
            <th class="col-b">상품 수량</th>
            <th class="col-c">차량 보험료</th>
            <th class="col-d">상품 금액</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="ta-left">
              <img src="https://api.fnkr.net/testimg/64x64/00CED1/FFF/?text=img+placeholder" alt="" class="-lt table-img">
              <p class="table-img-p"><span class="bold">텍스트텍스트텍스트텍스트텍스트텍스트텍스트텍스트 텍스트</span class="bold"><br>텍스트텍스트텍스트텍스트</p>
            </td>
            <td rowspan="2">1개</td>
            <td rowspan="2">30,000원</td>
            <td rowspan="2">150,000원</td>
          </tr>
          <tr>
            <td class="ta-left">텍스트텍스트텍스트</td>
          </tr>
        </tbody>
      </table>
    </div>
    <!-- /.table-responsive -->
*/?>
    <h2 class="color-glay fsize-md mb-20">구매자 정보</h2>
    <table class="table table-bordered mb-50 mobiletable-row">
      <tr>
        <th class="cel-a text-left">구매자 이름</th>
        <td class="text-left">천명관</td>
      </tr>
      <tr>
        <th class="text-left">구매자 이메일</th>
        <td class="text-left">chris9cc@naver.com</td>
      </tr>
      <tr>
        <th class="text-left">구매자 휴대폰</th>
        <td class="text-left">010 - 1234 - 5678</td>
      </tr>
    </table>
    <!-- /.table-responsive -->

    <h2 class="color-glay fsize-md mb-20">운전자 정보</h2>
    <table class="table table-bordered mb-50 mobiletable-row">
      <tr>
        <th class="cel-a text-left">대표 운전자 이름</th>
        <td class="text-left">천명관 (Chris Cheon)</td>
      </tr>
      <tr>
        <th class="text-left">대표 운전자 생년월일</th>
        <td class="text-left">1976년 10월 14일</td>
      </tr>
      <tr>
        <th class="text-left">대표 운전자 휴대폰</th>
        <td class="text-left">010 - 1234 - 5678</td>
      </tr>
    </table>
    <!-- /.table-responsive -->

    <h2 class="color-glay fsize-md mb-20">항공편</h2>
    <table class="table table-bordered mb-50 mobiletable-row">
      <tr>
        <th class="cel-a text-left">오키나와 도착편</th>
        <td class="text-left">아시아나 OCJKR편  /  2017년 5월 25일 오후 3시 00분</td>
      </tr>
      <tr>
        <th class="text-left">오키나와 출발편</th>
        <td class="text-left">아시아나 OCJKR편  /  2017년 5월 27일 오전 9시 00분</td>
      </tr>
    </table>
    <!-- /.table-responsive -->

    <h2 class="color-glay fsize-md mb-20">대여 및 반납 정보</h2>
    <table class="table table-bordered mb-50 mobiletable-row">
      <tr>
        <th class="cel-a text-left">대여 일정 및 장소</th>
        <td class="text-left">2017년 5월 25일 오후 3시 00분  /  오키나와 공항 지점</td>
      </tr>
      <tr>
        <th class="text-left">반납 일정 및 장소</th>
        <td class="text-left">2017년 5월 27일 오전 9시 00분  /  나하토마리항 지점</td>
      </tr>
    </table>
    <!-- /.table-responsive -->

    <div class="clearfix mb-30">
      <h2 class="color-glay fsize-md mb-20">대여 및 반납 정보</h2>
      <div class="pricebox pricebox-one">
        <div class="pricebox-head">
          <b>주문금액</b>
          <div class="pricebox-head-num">180,000 <b>원</b></div>
        </div>
        <!-- /.pricebox-head -->
      </div>
      <!-- /.pricebox-one -->
      <div class="pricebox pricebox-two">
        <div class="pricebox-head">
          <b>주문금액</b>
          <div class="pricebox-head-num">180,000 <b>원</b></div>
        </div>
        <!-- /.pricebox-head -->
      </div>
      <!-- /.pricebox-two -->
      <div class="pricebox pricebox-all">
        <div class="pricebox-head">
          <b>주문금액</b>
          <div class="pricebox-head-num color-lightblue">300,000 <b>원</b></div>
        </div>
        <!-- /.pricebox-head -->
      </div>
      <!-- /.pricebox-all -->
      <div class="pricebox pricebox-btm">
        <b>결제 방법</b>
        <p>신용카드신용카드신용카드신용카드신용카드신용카드</p>
      </div>
      <!-- /.pricebox-btm -->
    </div>
    <!-- /.l-container -->

    <ul class="btn-wrapper">
      <li><button type="submit" class="btn btn-primary btn-lg">목록으로 가기</button></li>
    </ul>

  </div>
  <!-- /.l-container -->

</div>
<!-- /.contents -->

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>