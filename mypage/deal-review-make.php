<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title>deal-review</title>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/mypage/css/mypage.css" rel="stylesheet">
<link href="/mypage/css/order-detail-view.css" rel="stylesheet">

</head>
<body>

<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

  <div class="l-container">
    <div class="pankuzu-wapper">
      <ul class="pankuzu">
        <li><a href="/">오키나와 오박사 홈</a></li>
        <li><a href="#">마이페이지</a></li>
        <li>나의 구매 목록</li>
      </ul>
    </div>
    <!-- /.pankuzu-wapper -->
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <div class="localnavs">
      <p class="localnavs-title">마이페이지</p>
      <ul class="localnavs-list">
        <li class="localnavs-list-current"><a href="#">나의 구매 목록</a></li>
        <li><a href="#">취소 및 변경 현황</a></li>
        <li><a href="#">상품 Q＆A</a></li>
        <li><a href="#">구매 후기</a></li>
        <li><a href="#">회원정보수정</a></li>
      </ul>
      <p class="localnavs-text color-lightblue bg-gy">! 구매하신 상품의 내역 확인 및 진행 상황, 취소, 변경 등의 관리를 하실 수 있습니다.</p>
    </div>
    <!-- /.localnavs -->
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <h1 class="headline-glay fsize-lg left-bdr mb-30"><span class="color-blue">나의 구매</span> 목록</h1>
      <form action="#">
        <table class="table table-bordered text-left mobiletable-row">
          <tr>
            <th class="col-a">구매 상품</th>
            <td>
              <select name="#" class="form-control wid-a">
                <option value="#">*******</option>
                <option value="#">*******</option>
                <option value="#">*******</option>
                <option value="#">*******</option>
              </select>
            </td>
          </tr>
          <tr>
            <th>상품 만족도</th>
            <td>
              <select name="#" class="form-control wid-b">
                <option value="#">*******</option>
                <option value="#">*******</option>
                <option value="#">*******</option>
                <option value="#">*******</option>
              </select>
            </td>
          </tr>
          <tr>
            <th class="valign-top">후기 내용</th>
            <td>
              <textarea name="" class="form-control textarea"></textarea>
            </td>
          </tr>
          <tr>
            <th>첨부 파일</th>
            <td>
              <label for="file_photo" class="input-file">파일 첨부<input type="file" id="file_photo"></label>
            <span class="color-lightblue volume">! 2MB 이하의 jpeg, jpg, png 형식의 파일만 첨부 가능합니다.</span>
            </td>
          </tr>
        </table>
        <ul class="btn-wrapper">
          <li><button type="submit" class="btn btn-primary btn-lg">입력 정보 확인</button></li>
          <li><button type="submit" class="btn btn-default  btn-lg">정보 수정 취소</button></li>
        </ul>
      </form>
    <!-- /.table-responsive -->

  </div>
  <!-- /.l-container -->

</div>
<!-- /.contents -->

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>