<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title>myinfo</title>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/mypage/css/mypage.css" rel="stylesheet">
<link href="/mypage/css/myinfo.css" rel="stylesheet">

</head>
<body>

<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

  <div class="l-container">
    <div class="pankuzu-wapper">
      <ul class="pankuzu">
        <li><a href="/">오키나와 오박사 홈</a></li>
        <li><a href="#">마이페이지</a></li>
        <li>나의 구매 목록</li>
      </ul>
    </div>
    <!-- /.pankuzu-wapper -->
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <div class="localnavs">
      <p class="localnavs-title">마이페이지</p>
      <ul class="localnavs-list">
        <li class="localnavs-list-current"><a href="#">나의 구매 목록</a></li>
        <li><a href="#">취소 및 변경 현황</a></li>
        <li><a href="#">상품 Q＆A</a></li>
        <li><a href="#">구매 후기</a></li>
        <li><a href="#">회원정보수정</a></li>
      </ul>
      <p class="localnavs-text color-lightblue bg-gy">! 구매하신 상품의 내역 확인 및 진행 상황, 취소, 변경 등의 관리를 하실 수 있습니다.</p>
    </div>
    <!-- /.localnavs -->
  </div>
  <!-- /.l-container -->

  <div class="l-container clearfix">
    <h1 class="headline-glay fsize-lg left-bdr mb-30"><span class="color-blue">나의 구매</span> 목록</h1>
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <div class="table-responsive">
      <form action="#">
        <table class="table table-bordered">
          <tbody>
            <tr>
              <td colspan="2" class="main-board">
                <h1 class="headline-glay fsize-lg mb-20">회원님의 안전한 정보보호를 위해 <span class="color-lightblue"> 비밀번호를 다시 한 번 확인합니다.</span></h1>
                <p>회원 정보 확인을 위해 입력하시는 <span class="color-blue">비밀번호</span>가 타인에게<br><span class="color-blue">노출되지 않도록 주의</span>해 주시기 바랍니다.</p>
              </td>
            </tr>
            <tr>
              <td class="ta-left wid-a">회원 아이디</td>
              <td class="ta-left">chris9cc@naver.com</td>
            </tr>
            <tr>
              <td class="ta-left">회원 비밀번호</td>
              <td class="ta-left"><input type="password" class="form-control input-pw"></td>
            </tr>
          </tbody>
        </table>
        <ul class="btn-wrapper">
          <li><button type="submit" class="btn btn-primary btn-lg">입력 정보 확인</button></li>
          <li><button type="submit" class="btn btn-default  btn-lg">정보 수정 취소</button></li>
        </ul>
      </form>
    </div>
    <!-- /.table-responsive -->
  </div>
  <!-- /.l-container -->

</div>
<!-- /.contents -->

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>