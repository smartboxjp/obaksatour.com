<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title>deal-review</title>

<?php  //サイト全体で使うCSS・JSなど
  require_once ($_SERVER['DOCUMENT_ROOT'] .'/include/common-header.php');
?>

<!-- 個別ページcss -->
<link href="/mypage/css/mypage.css" rel="stylesheet">
<link href="/mypage/css/deal-review.css" rel="stylesheet">

</head>
<body>

<?php  //グローバルヘッダー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-header.php');
?>

<div class="contents">

  <div class="l-container">
    <div class="pankuzu-wapper">
      <ul class="pankuzu">
        <li><a href="/">오키나와 오박사 홈</a></li>
        <li><a href="#">마이페이지</a></li>
        <li>나의 구매 목록</li>
      </ul>
    </div>
    <!-- /.pankuzu-wapper -->
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <div class="localnavs">
      <p class="localnavs-title">마이페이지</p>
      <ul class="localnavs-list">
        <li class="localnavs-list-current"><a href="#">나의 구매 목록</a></li>
        <li><a href="#">취소 및 변경 현황</a></li>
        <li><a href="#">상품 Q＆A</a></li>
        <li><a href="#">구매 후기</a></li>
        <li><a href="#">회원정보수정</a></li>
      </ul>
      <p class="localnavs-text color-lightblue bg-gy">! 구매하신 상품의 내역 확인 및 진행 상황, 취소, 변경 등의 관리를 하실 수 있습니다.</p>
    </div>
    <!-- /.localnavs -->
  </div>
  <!-- /.l-container -->

  <div class="l-container">
    <h1 class="headline-glay fsize-lg left-bdr mb-20"><span class="color-blue">나의 구매</span> 목록</h1>
    <table class="table table-bordered onetable">
      <tbody>
        <tr>
          <td class="ta-left">
            <span class="onetable-date">2017. 06. 01 14:30:45</span>
            <img src="https://api.fnkr.net/testimg/64x64/00CED1/FFF/?text=img+placeholder" alt="" class="onetable-img">
            <dl class="onetable-info">
              <dt class="onetable-info-title">텍스트텍스트텍스트텍스트텍스트텍스트텍스트텍스트 텍스트<img src="images/star_10.svg" alt="" class="icon-star"></dt>
              <dd class="onetable-info-cap">텍스트텍스트텍스트텍스트</dd>
            </dl>
            <p class="onetable-text">오키나와 오박사 렌터카 서비스 너무 편하고 좋네요. 친절하신 직원분의 안내에 만족스러웠구요. 오키나와 여행하면서도 정말 편하게 잘 구경햇네요.</p>
            <div class="onetalbe-funcs"><span class="table-meta-note">후기 수정</span> <span class="table-meta-note">후기 수정</span></div>
          </td>
        </tr>
        <tr>
          <td class="ta-left">
            <span class="onetable-date">2017. 06. 01 14:30:45</span>
            <img src="https://api.fnkr.net/testimg/64x64/00CED1/FFF/?text=img+placeholder" alt="" class="onetable-img">
            <dl class="onetable-info">
              <dt class="onetable-info-title">텍스트텍스트텍스트텍스트텍스트텍스트텍스트텍스트 텍스트<img src="images/star_10.svg" alt="" class="icon-star"></dt>
              <dd class="onetable-info-cap">텍스트텍스트텍스트텍스트</dd>
              <dd></dd>
            </dl>
            <p class="onetable-text">오키나와 오박사 렌터카 서비스 너무 편하고 좋네요. 친절하신 직원분의 안내에 만족스러웠구요. 오키나와 여행하면서도 정말 편하게 잘 구경햇네요.오키나와 오박사 렌터카 서비스 너무 편하고 좋네요. 친절하신 직원분의 안내에 만족스러웠구요. 오키나와 여행하면서도 정말 편하게 잘 구경햇네요.</p>
            <div class="onetalbe-funcs"><span class="table-meta-note">후기 수정</span> <span class="table-meta-note">후기 수정</span></div>
          </td>
        </tr>
        <tr>
          <td class="ta-left">
            <span class="onetable-date">2017. 06. 01 14:30:45</span>
            <img src="https://api.fnkr.net/testimg/64x64/00CED1/FFF/?text=img+placeholder" alt="" class="onetable-img">
            <dl class="onetable-info">
              <dt class="onetable-info-title">텍스트텍스트텍스트텍스트텍스트텍스트텍스트텍스트 텍스트<img src="images/star_5.svg" alt="" class="icon-star"></dt>
              <dd class="onetable-info-cap">텍스트텍스트텍스트텍스트</dd>
            </dl>
            <div class="onetalbe-funcs"><span class="table-meta-note">후기 수정</span> <span class="table-meta-note">후기 수정</span></div>
          </td>
        </tr>
      </tbody>
    </table>

  <div class="l-container">
    <ul class="paging">
      <li><a href="#"><span></span></a></li>
      <li class="paging-current"><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li><a href="#">6</a></li>
      <li><a href="#">7</a></li>
      <li><a href="#">8</a></li>
      <li><a href="#">9</a></li>
      <li><a href="#">10</a></li>
      <li><a href="#"><span></span></a></li>
    </ul>
  </div>

  </div>
  <!-- /.l-container -->

</div>
<!-- /.contents -->

<?php  //モバイル用サイドバー
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/mobile-sidebar.php');
?>

<?php  //共通フッター コピーライト、トップに戻る含む
  require_once ($_SERVER['DOCUMENT_ROOT'].'/include/global-footer.php');
?>